import { Component, OnInit } from '@angular/core';
import {MatchingStatusModel} from '../models/matching-status.model';
import {Subscription} from 'rxjs';
import {GenomaRestService} from '../services/genoma-rest.service';
import {ConfirmationService} from 'primeng/api';
import {EngineGraphModel} from '../models/graph-model/engine-graph.model';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.less']
})
export class ResultsComponent implements OnInit {
  alignments: MatchingStatusModel[];
  engineName: string;
  display: boolean;
  engineGraph: EngineGraphModel;

  constructor(private genomaRestService: GenomaRestService, private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.getRows();
    this.engineGraph = new EngineGraphModel([], []);
  }

  getRows() {
    const subscr: Subscription = this.genomaRestService.getAlignments().subscribe(
      res => { this.alignments = res; subscr.unsubscribe(); },
      error => { console.log(error); subscr.unsubscribe(); }
    );
  }

  delete(id: string) {
    const subscr: Subscription = this.genomaRestService.deleteAlignment(id).subscribe(
      res => { this.getRows(); subscr.unsubscribe(); },
      error => { console.log(error); subscr.unsubscribe(); }
    );
  }

  getVOWLLink(ontology1: string): string {
    return 'http://visualdataweb.de/webvowl/#iri=' + ontology1;
  }

  getEngineName(engine: string) {
    return engine.substring(engine.lastIndexOf('/') + 1, engine.length );
  }

  getDownloadLink(id: string, type: string): string {
    return this.genomaRestService.getDownloadLink(id, type);
  }

  confirm(alignmentId: string) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      accept: () => {
        this.delete(alignmentId);
      }
    });
  }

  hideEngine() {
    this.display = false;
  }

  showEngineGraph(engineName: string) {
    this.engineName = engineName;
    const subscr: Subscription = this.genomaRestService.getEngineGraph(engineName).subscribe(
      res => {
        this.engineGraph.setLinks(res.links);
        this.engineGraph.setNodes(res.nodes);
        this.engineGraph.updateGraphImages();
        this.display = true;
        subscr.unsubscribe();
      },
      error => { console.log(error); subscr.unsubscribe(); }
    );
  }
}
