import { Component, OnInit } from '@angular/core';
import {GenomaRestService} from '../services/genoma-rest.service';
import {HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-load-configurations',
  templateUrl: './load-configurations.component.html',
  styleUrls: ['./load-configurations.component.less']
})
export class LoadConfigurationsComponent implements OnInit {

  selectedFiles: FileList;
  currentFile: File;
  message: any;
  engineId: string;
  engineDescription: string;
  engineName: string;
  display: boolean;

  constructor(private genomaRestService: GenomaRestService) { }

  ngOnInit() {
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
    this.engineId = this.selectedFiles.item(0) !== undefined ? this.selectedFiles.item(0).name : '';
  }

  upload() {
    this.currentFile = this.selectedFiles.item(0);
    this.genomaRestService.uploadFile(this.currentFile, this.engineId, this.engineName, this.engineDescription).subscribe(response => {
      this.selectedFiles = null;
      if (response instanceof HttpResponse) {
        this.display = true;
        console.log(response.body);
      }
    });
  }

  onUploadClick() {
    this.upload();
  }

  hideDialog() {
    this.display = false;
  }
}
