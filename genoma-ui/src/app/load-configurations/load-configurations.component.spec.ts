import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadConfigurationsComponent } from './load-configurations.component';

describe('LoadConfigurationsComponent', () => {
  let component: LoadConfigurationsComponent;
  let fixture: ComponentFixture<LoadConfigurationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadConfigurationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadConfigurationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
