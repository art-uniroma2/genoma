import {Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs';

export abstract class RemoteCallService {
  constructor(protected router: Router) {}

  protected handleError(error: any) {
    let message: string;
    if (error instanceof HttpErrorResponse) {
      const status: number = error.status;
      const statusText: string = error.statusText;
      if (error.error && error.error.error) {
        message = error.error.error.message || '';
      } else {
        message = JSON.stringify(error.error, null, 4);
      }
      message = `${status} - ${statusText} - ${message}`;
    } else {
      message = error.message ? error.message : error.toString();
    }
    console.error(message);
    return throwError(message);
  }
}
