import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {Router} from '@angular/router';
import {RemoteCallService} from './remote-call.service';
import {MatchingRequestModel} from '../models/matching-request.model';
import {catchError} from 'rxjs/operators';
import {HttpClient, HttpEvent, HttpHeaders, HttpParams, HttpRequest} from '@angular/common/http';
import {EngineModel} from '../models/engine.model';
import {EngineGraphModel} from '../models/graph-model/engine-graph.model';
import {MatcherModel} from '../models/matcher.model';


@Injectable({
  providedIn: 'root'
})
export class GenomaRestService extends RemoteCallService {


  constructor(private http: HttpClient, router: Router) {
    super(router);
    console.log('GenomaRest Service created');
  }

  runMatching(request: MatchingRequestModel): Observable<any> {
    const url = environment.genomaRest + '/runMatching';
    const body = request;

    console.log(url);

    return this.http.post(url, body)
      .pipe(
        catchError(this.handleError)
      ) as Observable<any>;
  }

  deleteAlignment(id: string): Observable<any> {
    const url = environment.genomaRest + '/delete';
    const params = new HttpParams()
      .set('id', id);
    return this.http.delete(url, {params});
  }


  getEngines(): Observable<EngineModel[]> {
    const url = environment.genomaRest + '/getEnginesList';
    return this.http.get<EngineModel[]>(url);
  }

  getAlignments(): Observable<any> {
    const url = environment.genomaRest + '/getMatchingList';
    return this.http.get<MatchingRequestModel[]>(url);
  }

  getMatchers(): Observable<any> {
    const url = environment.genomaRest + '/getMatchersList';
    return this.http.get <MatcherModel[]>(url);
  }

  getEngineGraph(engineId: string): Observable<EngineModel> {
    const url = environment.genomaRest + '/getEngineGraph?id=' + engineId;
    return this.http.get <EngineModel>(url);
  }

  uploadFile(file: File, id: string, name: string, description: string): Observable<HttpEvent<{}>> {
    const formdata: FormData = new FormData();
    const url = environment.genomaRest + '/uploadFile';
    const headers = new HttpHeaders({'Access-Control-Allow-Origin' : '*'});
    formdata.append('file', file);
    formdata.append('id', id);
    formdata.append('name', name);
    formdata.append('description', description);
    const req = new HttpRequest('POST', url, formdata, {
      headers,
      reportProgress: true,
      responseType: 'text'
    });

    return this.http.request(req);
  }

  getDownloadLink(id: any, type: string): string {
    const url = environment.genomaRest + '/downloadFile/' + id + '?fileType=' + type;
    return url;
  }

  saveEngine(engine: EngineModel) {
    const url = environment.genomaRest + '/saveEngine';
    const body = engine;

    console.log(url);

    return this.http.post(url, body)
      .pipe(
        catchError(this.handleError)
      ) as Observable<any>;
  }

}
