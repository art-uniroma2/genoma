import { TestBed } from '@angular/core/testing';

import { GenomaRestService } from './genoma-rest.service';

describe('GenomaRestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GenomaRestService = TestBed.get(GenomaRestService);
    expect(service).toBeTruthy();
  });
});
