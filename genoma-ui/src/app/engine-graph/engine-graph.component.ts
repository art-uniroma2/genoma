import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EngineGraphModel} from '../models/graph-model/engine-graph.model';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-engine-graph',
  templateUrl: './engine-graph.component.html',
  styleUrls: ['./engine-graph.component.less']
})
export class EngineGraphComponent implements OnInit {
  @Input() engineGraph: EngineGraphModel;
  @Input() readonly: boolean;
  @Input() size: number[];
  @Input() name: string;
  @Input() description: string;
  @Output() nodeClick: EventEmitter<any>;
  @Output() linkClick: EventEmitter<any>;
  update$: Subject<any> = new Subject();


  constructor() {
    this.nodeClick = new EventEmitter();
    this.linkClick = new EventEmitter();
  }

  ngOnInit() {

  }

  onNodeClick(event) {
    if (this.readonly === false) {
      this.nodeClick.emit(event.currentTarget.id);
    }
  }

  onLinkClick(event) {
    if (this.readonly === false) {
      this.linkClick.emit(event.currentTarget.id);
    }
  }

  updateChart() {
    this.engineGraph.updateGraphImages();
    this.update$.next(true);
  }
}
