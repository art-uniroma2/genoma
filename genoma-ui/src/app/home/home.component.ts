import {Component, OnInit, ViewChild} from '@angular/core';
import {GenomaRestService} from '../services/genoma-rest.service';
import {Subscription} from 'rxjs';
import {SelectItem} from 'primeng/api';
import {MatchingRequestModel} from '../models/matching-request.model';
import {DatasetModel} from '../models/dataset.model';
import {EngineGraphModel} from '../models/graph-model/engine-graph.model';
import {EngineModel} from '../models/engine.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
  dataset1: DatasetModel;
  dataset2: DatasetModel;
  engine: EngineModel;
  refAlignment: string;
  engines: SelectItem[];
  display: boolean;
  engineGraph: EngineGraphModel;

  constructor(private genomaRestService: GenomaRestService) { }

  ngOnInit() {
    this.engines = [];
    this.engineGraph = new EngineGraphModel([], []);
    this.dataset1 = new DatasetModel(null, null, null, null, 'http://www.w3.org/2000/01/rdf-schema', 'en');
    this.dataset2 = new DatasetModel(null, null, null, null, 'http://www.w3.org/2000/01/rdf-schema', 'en');
    const subscr: Subscription = this.genomaRestService.getEngines().subscribe(
      res => {
        for (const engine of res) {
          this.engines.push({label: engine.name, value: engine});
        }

      },
      error => { console.log(error); subscr.unsubscribe(); }
    );
  }

  onRunMatchingClick() {
    if (this.dataset1.sparqlEndpoint === '') {
      this.dataset1.sparqlEndpoint = null;
    }
    if (this.dataset2.sparqlEndpoint === '') {
      this.dataset2.sparqlEndpoint = null;
    }
    const matchingRequest = new MatchingRequestModel(this.dataset1, this.dataset2, null, this.engine.id, this.refAlignment);
    const subscr: Subscription = this.genomaRestService.runMatching(matchingRequest).subscribe(
      res => {

      },
      error => { console.log(error); subscr.unsubscribe(); }
    );
  }

  showEngineGraph(engine: EngineModel) {
    const subscr: Subscription = this.genomaRestService.getEngineGraph(engine.id).subscribe(
      res => {
        this.engineGraph.setLinks(res.links);
        this.engineGraph.setNodes(res.nodes);
        this.engineGraph.updateGraphImages();
        this.display = true;
        subscr.unsubscribe();
      },
      error => { console.log(error); subscr.unsubscribe(); }
    );
  }

  hideEngine() {
    this.display = false;
  }
}
