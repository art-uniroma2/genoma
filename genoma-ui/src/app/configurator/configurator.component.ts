import {Component, OnInit, ViewChild} from '@angular/core';
import {Subscription, timer} from 'rxjs';
import {GenomaRestService} from '../services/genoma-rest.service';
import {SelectItem} from 'primeng/api';
import {EngineGraphModel} from '../models/graph-model/engine-graph.model';
import {EngineGraphComponent} from '../engine-graph/engine-graph.component';
import {MatcherModel} from '../models/matcher.model';
import {NodeModel} from '../models/graph-model/node.model';
import {EngineModel} from '../models/engine.model';

@Component({
  selector: 'app-configurator',
  templateUrl: './configurator.component.html',
  styleUrls: ['./configurator.component.less']
})
export class ConfiguratorComponent implements OnInit {
  // @ts-ignore
  @ViewChild(EngineGraphComponent)
  engineGraphComponent: EngineGraphComponent;
  matchersOptions: SelectItem[];
  enginesOptions: SelectItem[];
  engineGraph: EngineGraphModel;
  matcher: MatcherModel;
  node: NodeModel;
  engine: string;
  refresh: boolean;
  createLink: boolean;
  selectedNode: string;
  display: boolean;
  saveDialogDisplay: boolean;
  engineId: string;
  engineName: string;
  engineDescription: string;
  errorVisible: boolean;
  errorMessage: string;

  constructor(private genomaRestService: GenomaRestService) {
    this.matchersOptions = [];
    this.enginesOptions = [];
    this.engineGraph = new EngineGraphModel([], []);
    this.refresh = true;
    this.display = false;
  }

  ngOnInit() {
    const subscrMa: Subscription = this.genomaRestService.getMatchers().subscribe(
      res => {
        for (const matcher of res) {
          this.matchersOptions.push({label: matcher.name, value: matcher});
        }
        subscrMa.unsubscribe();
      },
      error => { console.log(error); subscrMa.unsubscribe(); }
    );
    const subscrEn: Subscription = this.genomaRestService.getEngines().subscribe(
      res => {
        for (const engine of res) {
          this.enginesOptions.push({label: engine.name, value: engine.id});
        }
        subscrEn.unsubscribe();
      },
      error => { console.log(error); subscrEn.unsubscribe(); }
    );
  }

  onClickPlus() {
    const params = Object.assign({}, this.matcher.defaultInputParams);
    this.engineGraph.addNode(this.matcher.id, 1.0, params);
    this.refreshGraph();
  }

  onNodeClick(node) {
    if (this.selectedNode !== undefined && this.createLink && this.selectedNode !== node) {
      this.engineGraph.addLink(this.selectedNode, node);
      this.selectedNode = undefined;
      this.display = false;
      this.createLink = false;
      this.refreshGraph();
    } else {
      this.selectedNode = node;
      this.node = this.engineGraph.nodes[node];
      this.display = true;
    }
  }

  refreshGraph() {
   this.engineGraphComponent.updateChart();
  }

  onClickLoadEngine() {
    const subscr: Subscription = this.genomaRestService.getEngineGraph(this.engine).subscribe(
       res => {
        this.engineGraph.setLinks(res.links);
        this.engineGraph.setNodes(res.nodes);
        this.engineId = res.id;
        this.engineName = res.name;
        this.engineDescription = res.description;
        this.refreshGraph();
        if (this.engineGraph.cyclic) {
          this.errorMessage = 'Loaded graph is not valid because contains cycles';
          this.errorVisible = true;
        }
        subscr.unsubscribe();
      },
      error => { console.log(error); subscr.unsubscribe(); }
    );
  }

  onClickReset() {
    this.engineGraph.resetGraph();
    this.engineId = '';
    this.engineName = '';
    this.engineDescription = '';
  }

  onHideClick() {
    this.display = false;
    this.selectedNode = undefined;
  }

  onClickDeleteNode() {
    if (this.selectedNode !== undefined) {
      this.engineGraph.deleteNode(this.selectedNode);
      this.display = false;
      this.refreshGraph();
    }
  }

  onClickSaveEngine() {
    const message = this.isGraphValid();
    if (message === 'Ok') {
        this.saveDialogDisplay = true;
    } else {
        this.errorMessage = message;
        this.errorVisible = true;
    }
  }

  private isGraphValid(): string {
    let noStart = true;
    let noEnd = true;
    let startId = null;
    let endId = null;
    if (!this.engineGraph.isAllConnected()) {
      return 'Disconnected nodes not allowed';
    }
    for (const node of this.engineGraph.nodes) {
      if (node.label === 'AlignmentExtractor') {
        noEnd = false;
        endId = node.id;
      } else if (node.label === 'SimilarityMatrixExtractor') {
        noStart = false;
        startId = node.id;
      }
    }
    if (noStart || noEnd) {
      return 'At least one SimilarityMatrixExtractor and one AlignmentExtractor are required';
    } else {
      return 'Ok';
    }
  }

  onClickDialogSaveEngine() {
    const engineModel: EngineModel = new EngineModel(this.engineId, this.engineName, this.engineDescription,
      this.engineGraph.nodes, this.engineGraph.links);
    const subscr: Subscription = this.genomaRestService.saveEngine(engineModel).subscribe(
      res => {
        this.refreshGraph();
        this.saveDialogDisplay = false;
        subscr.unsubscribe();
      },
      error => { console.log(error); subscr.unsubscribe(); }
    );
  }
}
