import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NodeModel} from '../models/graph-model/node.model';

@Component({
  selector: 'app-matcher-options-sidebar',
  templateUrl: './matcher-options-sidebar.component.html',
  styleUrls: ['./matcher-options-sidebar.component.less']
})
export class MatcherOptionsSidebarComponent implements OnInit {
  @Input() display: boolean;
  @Input() node: NodeModel;
  @Output() hideClick: EventEmitter<void>;

  constructor() {
    this.node = new NodeModel('', '', 0.0, null);
    this.hideClick = new EventEmitter();
  }

  ngOnInit() {
  }

  onHideClick() {
    this.hideClick.emit();
  }

  trackByFn(index: any, item: any) {
    return index;
  }
}
