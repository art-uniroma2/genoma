import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatcherOptionsSidebarComponent } from './matcher-options-sidebar.component';

describe('MatcherOptionsSidebarComponent', () => {
  let component: MatcherOptionsSidebarComponent;
  let fixture: ComponentFixture<MatcherOptionsSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatcherOptionsSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatcherOptionsSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
