import {Component, Input, OnInit} from '@angular/core';
import {SelectItem} from 'primeng/api';
import {DatasetModel} from '../models/dataset.model';

@Component({
  selector: 'app-ontology-settings',
  templateUrl: './ontology-settings.component.html',
  styleUrls: ['./ontology-settings.component.less']
})
export class OntologySettingsComponent implements OnInit {
  @Input() title: any;
  @Input() dataset: DatasetModel;
  lexicalizations: SelectItem[];
  isTripleStoreUsed: boolean;


  constructor() {
   this.lexicalizations = [
      {label: 'RDF', value: 'http://www.w3.org/2000/01/rdf-schema'},
      {label: 'SKOS', value: 'http://www.w3.org/2004/02/skos/core'},
      {label: 'SKOS-XL', value: 'http://www.w3.org/2008/05/skos-xl'},
      {label: 'ONTOLEX', value: 'http://www.w3.org/ns/lemon/ontolex'},
    ];
  }

  ngOnInit() {
  }


}
