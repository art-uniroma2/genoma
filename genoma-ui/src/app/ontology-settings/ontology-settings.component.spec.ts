import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OntologySettingsComponent } from './ontology-settings.component';

describe('OntologySettingsComponent', () => {
  let component: OntologySettingsComponent;
  let fixture: ComponentFixture<OntologySettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OntologySettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OntologySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
