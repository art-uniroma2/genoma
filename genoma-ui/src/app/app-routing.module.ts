import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ResultsComponent} from './results/results.component';
import { ConfiguratorComponent } from './configurator/configurator.component';
import {LoadConfigurationsComponent} from './load-configurations/load-configurations.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'results', component: ResultsComponent },
  { path: 'configurator', component: ConfiguratorComponent },
  { path: 'loadconf', component: LoadConfigurationsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
