import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {
  ButtonModule, ConfirmationService,
  ConfirmDialogModule, DialogModule,
  DropdownModule,
  FieldsetModule,
  InputSwitchModule, SidebarModule,
  TabMenuModule, ToggleButtonModule, ToolbarModule, TooltipModule
} from 'primeng';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import { HomeComponent } from './home/home.component';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import { HeaderComponent } from './header/header.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { OntologySettingsComponent } from './ontology-settings/ontology-settings.component';
import {FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { ResultsComponent } from './results/results.component';
import { ConfiguratorComponent } from './configurator/configurator.component';
import {TableModule} from 'primeng/table';
import { LoadConfigurationsComponent } from './load-configurations/load-configurations.component';
import { EngineGraphComponent } from './engine-graph/engine-graph.component';
import {NgxGraphModule} from '@swimlane/ngx-graph';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import { MatcherOptionsSidebarComponent } from './matcher-options-sidebar/matcher-options-sidebar.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    OntologySettingsComponent,
    ResultsComponent,
    ConfiguratorComponent,
    LoadConfigurationsComponent,
    EngineGraphComponent,
    MatcherOptionsSidebarComponent,
  ],
  imports: [
    RouterModule.forRoot(
      [
        {path: '', component: HomeComponent}
      ]
    ),
    BrowserModule,
    BrowserAnimationsModule,
    TabMenuModule,
    HttpClientModule,
    AngularFontAwesomeModule,
    FieldsetModule,
    FormsModule,
    DropdownModule,
    InputSwitchModule,
    AppRoutingModule,
    TableModule,
    ButtonModule,
    ConfirmDialogModule,
    DialogModule,
    NgxGraphModule,
    NgxChartsModule,
    ToolbarModule,
    ToggleButtonModule,
    SidebarModule,
    TooltipModule
  ],
  providers: [ConfirmationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
