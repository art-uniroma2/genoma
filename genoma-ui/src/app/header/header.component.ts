import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {
  items: MenuItem[];

  constructor() { }

  ngOnInit() {
    this.items = [
      {label: 'Home', icon: 'fa fa-fw fa-home', routerLink: '/'},
      {label: 'Results', icon: 'fa fa-fw fa-database', routerLink: '/results'},
      {label: 'Configurator', icon: 'fa fa-fw fa-gear', routerLink: '/configurator'},
      {label: 'Load Configurations', icon: 'fa fa-fw fa-sliders', routerLink: '/loadconf'}
    ];
  }

}
