import {NodeModel} from './graph-model/node.model';
import {LinkModel} from './graph-model/link-model';

export class EngineModel {
  constructor(
    public id: string,
    public name: string,
    public description: string,
    public nodes?: NodeModel[],
    public links?: LinkModel[]) { }
}
