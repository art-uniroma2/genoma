export class MatchingStatusModel {
  constructor(
    public id: string,
    public ontology1: string,
    public ontology2: string,
    public engine: string,
    public status: string,
    public startTime: string,
    public endTime: string) { }
}
