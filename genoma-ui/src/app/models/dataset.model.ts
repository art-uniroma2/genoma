export class DatasetModel {
  constructor(
    public uri: string,
    public sparqlEndpoint: string,
    public synonymizerUri: string,
    public synonymiserSparqlEndpoint: string,
    public lexicalization: string,
    public language: string) { }
}
