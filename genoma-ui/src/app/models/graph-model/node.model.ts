export class NodeModel {
  public image: string;
  constructor(
    public id: string,
    public label: string,
    public weight: number,
    public params?: any) {
    //this.image = 'assets/matchersicons/' + label + '.gif';
  }
}
