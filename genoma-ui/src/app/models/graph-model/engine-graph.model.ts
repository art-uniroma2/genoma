import {NodeModel} from './node.model';
import {LinkModel} from './link-model';
import {GenericGraphAdapter} from 'incremental-cycle-detect';

export class EngineGraphModel {
  private nodesCounter: number;
  private graph: GenericGraphAdapter;

  start: boolean;
  end: boolean;
  cyclic: boolean;
  startId: string;

  constructor(
    public nodes: NodeModel[],
    public links: LinkModel[]) {
    this.nodesCounter = -1;
    this.graph = GenericGraphAdapter.create();
  }

  public addNode(nodeName: string, weight: number, params: any) {
    if (nodeName === 'SimilarityMatrixExtractor' && this.start ||
      nodeName === 'AlignmentExtractor' && this.end) {
      return;
    }
    this.nodesCounter = this.nodesCounter + 1;
    const node = new NodeModel(this.nodesCounter.toString(), nodeName, weight, params);
    this.nodes.push(node);
    if (nodeName === 'SimilarityMatrixExtractor') {
      this.start = true;
      this.startId = '' + (this.nodes.length - 1);
    } else if (nodeName === 'AlignmentExtractor') {
      this.end = true;
    }
  }

  public addLink(sourceId: string, targetId: string, params?: any, label?: string) {
    const sourceLabel = this.nodes[sourceId].label;
    const targetLabel = this.nodes[targetId].label;
    if (sourceLabel === 'AlignmentExtractor' ||
       targetLabel === 'SimilarityMatrixExtractor') {
      return;
    }
    if (this.graph.addEdge(sourceId, targetId) === false) {
      return;
    }
    const link = new LinkModel('ED' + sourceId + targetId, sourceId, targetId, label, params);
    if (!this.isLinkAlreadyIn(link)) {
      this.links.push(link);
    }
  }

  public resetGraph() {
    this.nodes = [];
    this.links = [];
    this.nodesCounter = -1;
    this.graph = GenericGraphAdapter.create();
  }

  public setNodes(nodes: NodeModel[]) {
    this.nodes = nodes;
    this.nodesCounter = nodes.length - 1;
  }

  public setLinks(links: LinkModel[]) {
    this.graph = GenericGraphAdapter.create();
    this.links = links;
    this.cyclic = false;
    for (const link of this.links) {
      if (this.graph.addEdge(link.source, link.target) === false) {
        this.cyclic = true;
      }
    }
  }

  public updateGraphImages() {
    for (const node of this.nodes) {
      node.image = 'assets/matchersicons/' + node.label + '.gif';
    }
  }

  private isLinkAlreadyIn(newLink: LinkModel) {
    for (const link of this.links) {
      if (link.source === newLink.source && link.target === newLink.target ||
        link.source === newLink.target && link.target === newLink.source) {
        return true;
      }
    }
  }

  deleteNode(selectedNode: string) {
    // tslint:disable-next-line:prefer-for-of variable-name
    for (let _i = 0; _i < this.nodes.length; _i++) {
      this.graph.deleteEdge(selectedNode, this.nodes[_i].id);
      this.graph.deleteEdge(this.nodes[_i].id, selectedNode);
      if (this.nodes[_i].id === selectedNode) {
        this.nodes.splice(_i, 1);
      }
      if (+this.nodes[_i].id > +selectedNode) {
        this.nodes[_i].id = String(_i);
      }
    }
    this.nodesCounter =  this.nodes.length - 1;
    // tslint:disable-next-line:variable-name
    for (let _i = 0; _i < this.links.length;) {
      if (this.links[_i].source === selectedNode || this.links[_i].target === selectedNode) {
        this.links.splice(_i, 1);
      } else {
        _i++;
      }
    }
    // tslint:disable-next-line:variable-name prefer-for-of
    for (let _i = 0; _i < this.links.length; _i++) {
      if (+this.links[_i].source > +selectedNode) {
        this.links[_i].source = String(+this.links[_i].source - 1);
      }
      if (+this.links[_i].target > +selectedNode) {
        this.links[_i].target = String(+this.links[_i].target - 1);
      }
    }
  }

  isAllConnected(): boolean {
    if (this.links.length >= this.nodes.length - 1) {
      return true;
    } else {
      return false;
    }
  }

}
