export class LinkModel {
  constructor(
    public id: string,
    public source: string,
    public target: string,
    public label?: string,
    public params?: any) {}
}
