import {DatasetModel} from './dataset.model';

export class MatchingRequestModel {
  constructor(
    public ontology1: DatasetModel,
    public ontology2: DatasetModel,
    public server: string = null,
    public engine: string = null,
    public refAlignment: string = null
  ) { }
}
