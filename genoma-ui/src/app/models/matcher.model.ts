export class MatcherModel {
  constructor(
    public id: string,
    public name: string,
    public type: string,
    public description: string,
    public inputParams: string,
    public defaultInputParams: any) { }
}
