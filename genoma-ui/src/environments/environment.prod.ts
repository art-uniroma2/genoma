(window as any).global = window
declare var require: any;

export const environment = {
  production: true,
  genomaRest: location.origin + '/api',
  version: require('../../package.json').version
};
