#!/bin/bash

if [ -z "$JAVA_HOME" ]; then
  JAVA=java
else
  JAVA="$JAVA_HOME/bin/java"
fi

JAVA_VERSION=0
for g in `"$JAVA" -version 2>&1 | grep -i version | awk '{print $3}'`; do
    JAVA_VERSION=$g
done

JAVA_VERSION=${JAVA_VERSION//\"/}

if echo $JAVA_VERSION% | sed 's/\.[^.]*$//' | awk '$1 > 1.8' > /dev/null; then
  JAVA_OPTS="$JAVA_OPTS --add-opens java.base/java.lang=ALL-UNNAMED --add-opens java.base/java.util=ALL-UNNAMED --add-opens java.base/java.lang.reflect=ALL-UNNAMED --add-opens java.base/java.text=ALL-UNNAMED  --add-opens java.desktop/java.awt.font=ALL-UNNAMED"
fi

"$JAVA" $JAVA_OPTS -Dserver.port=7575 -jar genoma-alignment-service-2.3.5-SNAPSHOT.jar