package it.uniroma2.art.genoma.service.api;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.validation.Valid;

import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.service.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import io.swagger.annotations.ApiParam;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-06-10T19:01:04.879+02:00[Europe/Berlin]")

@Controller
@RequestMapping("${openapi.alignmentServices.base-path:}")
public class MatchersApiController implements MatchersApi {

	private static final Logger log = LoggerFactory.getLogger(MatchersApiController.class);

	private final NativeWebRequest request;
	private final ObjectMapper objectMapper;

	@org.springframework.beans.factory.annotation.Autowired
	public MatchersApiController(NativeWebRequest request, ObjectMapper objectMapper) {
		this.request = request;
		this.objectMapper = objectMapper;
	}

	@Override
	public Optional<NativeWebRequest> getRequest() {
		return Optional.ofNullable(request);
	}

	@Override
	public ResponseEntity<Matcher> getMatcherByID(
			@ApiParam(value = "Matcher ID", required = true) @PathVariable("id") String id) {
		return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

	}

	@Override
	public ResponseEntity<List<Matcher>> getMatchers() {
		return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
	}

	@Override
	public ResponseEntity<List<Matcher>> searchMatchers(
			@ApiParam(value = "") @Valid @RequestBody(required = false) ScenarioDefinition scenarioDefinition) {
		String accept = request.getHeader("Accept");
		if (accept != null && accept.contains("application/json")) {
			return new ResponseEntity<>(computeSettingsFromScenario(scenarioDefinition), HttpStatus.OK);
		}

		return new ResponseEntity<List<Matcher>>(HttpStatus.NOT_IMPLEMENTED);
	}

	private List<Matcher> computeSettingsFromScenario(ScenarioDefinition scenarioDefinition) {
		JsonNodeFactory nf = JsonNodeFactory.instance;
		ObjectNode matcherSettings = nf.objectNode();
		matcherSettings.put("type", "object");
		matcherSettings.put("title", "Matcher Settings");
		ObjectNode propertiesNode = matcherSettings.putObject("properties");
		Pairing bestPairing = ApiUtil.getBestPairing(scenarioDefinition.getPairings());
		Map<String, Dataset> id2supportDataset = scenarioDefinition.getSupportDatasets().stream()
				.collect(Collectors.toMap(Dataset::getAtId, Function.identity()));
		LexicalizationSet sourceLexicalizationSet = (LexicalizationSet) id2supportDataset
				.get(bestPairing.getSource().getLexicalizationSet());
		LexicalizationSet targetLexicalizationSet = (LexicalizationSet) id2supportDataset
				.get(bestPairing.getTarget().getLexicalizationSet());
		// Options related to the presence of synonymizer
		if(bestPairing.getSynonymizer() != null) {
			propertiesNode.putObject(ApiUtil.SYNONYM_EXPANSION).put("type", "boolean").put("default", false)
					.put("nullable", true).put("description", "whether or not do synonym expansion");
			propertiesNode.putObject(ApiUtil.CONSYNONYMS_MATCHER_WEIGHT).put("type", "number").put("default", 1.0)
					.put("nullable", true).put("description", "weight of cosynomyms matcher");
		}
		// entities weights
		propertiesNode.putObject(ApiUtil.ID_WEIGHT).put("type", "number").put("default", 1.0)
				.put("nullable", false).put("description", "the weight given to URIs");
		propertiesNode.putObject(ApiUtil.LABELS_WEIGHT).put("type", "number").put("default", 1.0)
				.put("nullable", false).put("description", "the weight given to labels");
		// comments are considered only for RDFS lexicalizations
		if(ApiUtil.getLexicalizationFromString(sourceLexicalizationSet.getLexicalizationModel()).equals(MatchingResource.Lexicalization.RDFS) &&
				ApiUtil.getLexicalizationFromString(targetLexicalizationSet.getLexicalizationModel()).equals(MatchingResource.Lexicalization.RDFS)) {
			propertiesNode.putObject(ApiUtil.COMMENTS_WEIGHT).put("type", "number").put("default", 1.0)
					.put("nullable", true).put("description", "the weight given to comments (only for RDF lexicalization");
		}
		// number of entities ti be selected from the target dataset in case of big datasets
		propertiesNode.putObject(ApiUtil.CF_SIZE).put("type", "integer").put("default", 5)
				.put("nullable", false).put("description", "number of documents to be selected from the target dataset using Lucene");

		// string matching options
		propertiesNode.putObject(ApiUtil.STRING_MATCHING).put("type", "boolean").put("default", true)
				.put("nullable", false).put("description", "whether or not execute string matching");
		propertiesNode.putObject(ApiUtil.STRING_MATCHING_WEIGHT).put("type", "number").put("default", 1.0)
				.put("nullable", true).put("description", "weight of string matching");
		propertiesNode.putObject(ApiUtil.STRING_MATCHING_ALGORITHM).put("default", "ISubMatcher")
				.put("nullable", true).put("description", "string matching algorithm")
				.putArray("enum").add("ISubMatcher").add("EditDistanceMatcher").add("StringHammingDistance").add("SubStringCompare");

		// linguitic matching options
		propertiesNode.putObject(ApiUtil.LINGUISTIC_MATCHING).put("type", "boolean").put("default", false)
				.put("nullable", false).put("description", "whether or not execute linguistic matching");
		propertiesNode.putObject(ApiUtil.LINGUISTIC_MATCHING_WEIGHT).put("type", "number").put("default", 1.0)
				.put("nullable", true).put("description", "weight of linguistic matching");

		// aggregation Strategy options
		propertiesNode.putObject(ApiUtil.AGGREGATION_STRATEGY).put("default", "TriangularNormMin")
				.put("nullable", false).put("description", "aggregation strategy to be applied to results coming from different algorithms")
				.putArray("enum").add("TriangularNormMax").add("TriangularNormMin").add("TriangularNormWeightedProducts");

		// filtering strategy options
		propertiesNode.putObject(ApiUtil.FILTERING_STRATEGY).put("default", "HardThreshold")
				.put("nullable", false).put("description", "filtering strategies")
				.putArray("enum").add("HardThreshold").add("DeltaThreshold").add("ProportionalThreshold").add("MediaThreshold").add("MaxOnly");
		propertiesNode.putObject(ApiUtil.FILTERING_THRESHOLD).put("type", "number").put("default", 0.9)
				.put("nullable", false).put("description", "its meaning depends on the filtering strategy adopted");
		Matcher matcher = new Matcher();
		matcher.setId("Matching Architecture");
		matcher.setSettings(matcherSettings);
		return Arrays.asList(matcher);
	}

}
