package it.uniroma2.art.genoma.service.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 * ConceptualizationSetAllOf
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-07-21T15:35:09.281+02:00[Europe/Berlin]")
public class ConceptualizationSetAllOf   {
  @JsonProperty("lexiconDataset")
  private String lexiconDataset;

  @JsonProperty("conceptualDataset")
  private String conceptualDataset;

  @JsonProperty("conceptualizations")
  private Integer conceptualizations;

  @JsonProperty("concepts")
  private Integer concepts;

  @JsonProperty("lexicalEntries")
  private Integer lexicalEntries;

  @JsonProperty("avgSynonymy")
  private Double avgSynonymy;

  @JsonProperty("avgAmbiguity")
  private Double avgAmbiguity;

  public ConceptualizationSetAllOf lexiconDataset(String lexiconDataset) {
    this.lexiconDataset = lexiconDataset;
    return this;
  }

  /**
   * Get lexiconDataset
   * @return lexiconDataset
  */
  @ApiModelProperty(value = "")


  public String getLexiconDataset() {
    return lexiconDataset;
  }

  public void setLexiconDataset(String lexiconDataset) {
    this.lexiconDataset = lexiconDataset;
  }

  public ConceptualizationSetAllOf conceptualDataset(String conceptualDataset) {
    this.conceptualDataset = conceptualDataset;
    return this;
  }

  /**
   * Get conceptualDataset
   * @return conceptualDataset
  */
  @ApiModelProperty(value = "")


  public String getConceptualDataset() {
    return conceptualDataset;
  }

  public void setConceptualDataset(String conceptualDataset) {
    this.conceptualDataset = conceptualDataset;
  }

  public ConceptualizationSetAllOf conceptualizations(Integer conceptualizations) {
    this.conceptualizations = conceptualizations;
    return this;
  }

  /**
   * Get conceptualizations
   * @return conceptualizations
  */
  @ApiModelProperty(value = "")


  public Integer getConceptualizations() {
    return conceptualizations;
  }

  public void setConceptualizations(Integer conceptualizations) {
    this.conceptualizations = conceptualizations;
  }

  public ConceptualizationSetAllOf concepts(Integer concepts) {
    this.concepts = concepts;
    return this;
  }

  /**
   * Get concepts
   * @return concepts
  */
  @ApiModelProperty(value = "")


  public Integer getConcepts() {
    return concepts;
  }

  public void setConcepts(Integer concepts) {
    this.concepts = concepts;
  }

  public ConceptualizationSetAllOf lexicalEntries(Integer lexicalEntries) {
    this.lexicalEntries = lexicalEntries;
    return this;
  }

  /**
   * Get lexicalEntries
   * @return lexicalEntries
  */
  @ApiModelProperty(value = "")


  public Integer getLexicalEntries() {
    return lexicalEntries;
  }

  public void setLexicalEntries(Integer lexicalEntries) {
    this.lexicalEntries = lexicalEntries;
  }

  public ConceptualizationSetAllOf avgSynonymy(Double avgSynonymy) {
    this.avgSynonymy = avgSynonymy;
    return this;
  }

  /**
   * Get avgSynonymy
   * @return avgSynonymy
  */
  @ApiModelProperty(value = "")


  public Double getAvgSynonymy() {
    return avgSynonymy;
  }

  public void setAvgSynonymy(Double avgSynonymy) {
    this.avgSynonymy = avgSynonymy;
  }

  public ConceptualizationSetAllOf avgAmbiguity(Double avgAmbiguity) {
    this.avgAmbiguity = avgAmbiguity;
    return this;
  }

  /**
   * Get avgAmbiguity
   * @return avgAmbiguity
  */
  @ApiModelProperty(value = "")


  public Double getAvgAmbiguity() {
    return avgAmbiguity;
  }

  public void setAvgAmbiguity(Double avgAmbiguity) {
    this.avgAmbiguity = avgAmbiguity;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConceptualizationSetAllOf conceptualizationSetAllOf = (ConceptualizationSetAllOf) o;
    return Objects.equals(this.lexiconDataset, conceptualizationSetAllOf.lexiconDataset) &&
        Objects.equals(this.conceptualDataset, conceptualizationSetAllOf.conceptualDataset) &&
        Objects.equals(this.conceptualizations, conceptualizationSetAllOf.conceptualizations) &&
        Objects.equals(this.concepts, conceptualizationSetAllOf.concepts) &&
        Objects.equals(this.lexicalEntries, conceptualizationSetAllOf.lexicalEntries) &&
        Objects.equals(this.avgSynonymy, conceptualizationSetAllOf.avgSynonymy) &&
        Objects.equals(this.avgAmbiguity, conceptualizationSetAllOf.avgAmbiguity);
  }

  @Override
  public int hashCode() {
    return Objects.hash(lexiconDataset, conceptualDataset, conceptualizations, concepts, lexicalEntries, avgSynonymy, avgAmbiguity);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ConceptualizationSetAllOf {\n");
    
    sb.append("    lexiconDataset: ").append(toIndentedString(lexiconDataset)).append("\n");
    sb.append("    conceptualDataset: ").append(toIndentedString(conceptualDataset)).append("\n");
    sb.append("    conceptualizations: ").append(toIndentedString(conceptualizations)).append("\n");
    sb.append("    concepts: ").append(toIndentedString(concepts)).append("\n");
    sb.append("    lexicalEntries: ").append(toIndentedString(lexicalEntries)).append("\n");
    sb.append("    avgSynonymy: ").append(toIndentedString(avgSynonymy)).append("\n");
    sb.append("    avgAmbiguity: ").append(toIndentedString(avgAmbiguity)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

