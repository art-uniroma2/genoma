package it.uniroma2.art.genoma.service.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;

/**
 * An entity to contact for inquiries about the service
 */
@ApiModel(description = "An entity to contact for inquiries about the service")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-07-21T15:35:09.281+02:00[Europe/Berlin]")
public class ServiceMetadataContact   {
  @JsonProperty("name")
  private String name;

  @JsonProperty("email")
  private String email;

  public ServiceMetadataContact name(String name) {
    this.name = name;
    return this;
  }

  /**
   * The name of the contact
   * @return name
  */
  @ApiModelProperty(required = true, value = "The name of the contact")
  @NotNull


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ServiceMetadataContact email(String email) {
    this.email = email;
    return this;
  }

  /**
   * The email address of the contact
   * @return email
  */
  @ApiModelProperty(required = true, value = "The email address of the contact")
  @NotNull

@javax.validation.constraints.Email
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ServiceMetadataContact serviceMetadataContact = (ServiceMetadataContact) o;
    return Objects.equals(this.name, serviceMetadataContact.name) &&
        Objects.equals(this.email, serviceMetadataContact.email);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, email);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ServiceMetadataContact {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

