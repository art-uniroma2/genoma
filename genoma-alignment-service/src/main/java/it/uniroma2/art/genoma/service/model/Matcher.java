package it.uniroma2.art.genoma.service.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;

/**
 * Matcher
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-07-21T15:35:09.281+02:00[Europe/Berlin]")
public class Matcher   {
  @JsonProperty("id")
  private String id;

  @JsonProperty("description")
  private String description;

  @JsonProperty("settings")
  private Object settings;

  public Matcher id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Identifier of the matcher
   * @return id
  */
  @ApiModelProperty(required = true, value = "Identifier of the matcher")
  @NotNull


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Matcher description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Description of the matcher
   * @return description
  */
  @ApiModelProperty(required = true, value = "Description of the matcher")
  @NotNull


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Matcher settings(Object settings) {
    this.settings = settings;
    return this;
  }

  /**
   * A JSON schema. Its definition is left left under-specified.
   * @return settings
  */
  @ApiModelProperty(value = "A JSON schema. Its definition is left left under-specified.")


  public Object getSettings() {
    return settings;
  }

  public void setSettings(Object settings) {
    this.settings = settings;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Matcher matcher = (Matcher) o;
    return Objects.equals(this.id, matcher.id) &&
        Objects.equals(this.description, matcher.description) &&
        Objects.equals(this.settings, matcher.settings);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, description, settings);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Matcher {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    settings: ").append(toIndentedString(settings)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

