package it.uniroma2.art.genoma.service.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * AlignmentChain
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-07-21T15:35:09.281+02:00[Europe/Berlin]")
public class AlignmentChain   {
  @JsonProperty("score")
  private Double score;

  @JsonProperty("chain")
  @Valid
  private List<String> chain = new ArrayList<>();

  public AlignmentChain score(Double score) {
    this.score = score;
    return this;
  }

  /**
   * the score of this alignment chain
   * @return score
  */
  @ApiModelProperty(required = true, value = "the score of this alignment chain")
  @NotNull


  public Double getScore() {
    return score;
  }

  public void setScore(Double score) {
    this.score = score;
  }

  public AlignmentChain chain(List<String> chain) {
    this.chain = chain;
    return this;
  }

  public AlignmentChain addChainItem(String chainItem) {
    this.chain.add(chainItem);
    return this;
  }

  /**
   * references to the alignments in this chain
   * @return chain
  */
  @ApiModelProperty(required = true, value = "references to the alignments in this chain")
  @NotNull


  public List<String> getChain() {
    return chain;
  }

  public void setChain(List<String> chain) {
    this.chain = chain;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AlignmentChain alignmentChain = (AlignmentChain) o;
    return Objects.equals(this.score, alignmentChain.score) &&
        Objects.equals(this.chain, alignmentChain.chain);
  }

  @Override
  public int hashCode() {
    return Objects.hash(score, chain);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AlignmentChain {\n");
    
    sb.append("    score: ").append(toIndentedString(score)).append("\n");
    sb.append("    chain: ").append(toIndentedString(chain)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

