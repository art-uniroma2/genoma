package it.uniroma2.art.genoma.service.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;

/**
 * PairingHand
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-07-21T15:35:09.281+02:00[Europe/Berlin]")
public class PairingHand   {
  @JsonProperty("lexicalizationSet")
  private String lexicalizationSet;

  public PairingHand lexicalizationSet(String lexicalizationSet) {
    this.lexicalizationSet = lexicalizationSet;
    return this;
  }

  /**
   * the IRI of a support dataset that is a lexicalization set
   * @return lexicalizationSet
  */
  @ApiModelProperty(required = true, value = "the IRI of a support dataset that is a lexicalization set")
  @NotNull


  public String getLexicalizationSet() {
    return lexicalizationSet;
  }

  public void setLexicalizationSet(String lexicalizationSet) {
    this.lexicalizationSet = lexicalizationSet;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PairingHand pairingHand = (PairingHand) o;
    return Objects.equals(this.lexicalizationSet, pairingHand.lexicalizationSet);
  }

  @Override
  public int hashCode() {
    return Objects.hash(lexicalizationSet);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PairingHand {\n");
    
    sb.append("    lexicalizationSet: ").append(toIndentedString(lexicalizationSet)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

