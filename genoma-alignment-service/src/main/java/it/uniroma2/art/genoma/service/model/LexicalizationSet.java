package it.uniroma2.art.genoma.service.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 * LexicalizationSet
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-07-21T15:35:09.281+02:00[Europe/Berlin]")
public class LexicalizationSet extends Dataset  {
  @JsonProperty("lexiconDataset")
  private String lexiconDataset;

  @JsonProperty("referenceDataset")
  private String referenceDataset;

  @JsonProperty("lexicalizationModel")
  private String lexicalizationModel;

  @JsonProperty("lexicalizations")
  private Integer lexicalizations;

  @JsonProperty("references")
  private Integer references;

  @JsonProperty("lexicalEntries")
  private Integer lexicalEntries;

  @JsonProperty("avgNumOfLexicalizations")
  private Double avgNumOfLexicalizations;

  @JsonProperty("percentage")
  private Double percentage;

  @JsonProperty("languageTag")
  private String languageTag;

  @JsonProperty("languageLexvo")
  private String languageLexvo;

  @JsonProperty("languageLOC")
  private String languageLOC;

  public LexicalizationSet lexiconDataset(String lexiconDataset) {
    this.lexiconDataset = lexiconDataset;
    return this;
  }

  /**
   * Get lexiconDataset
   * @return lexiconDataset
  */
  @ApiModelProperty(value = "")


  public String getLexiconDataset() {
    return lexiconDataset;
  }

  public void setLexiconDataset(String lexiconDataset) {
    this.lexiconDataset = lexiconDataset;
  }

  public LexicalizationSet referenceDataset(String referenceDataset) {
    this.referenceDataset = referenceDataset;
    return this;
  }

  /**
   * Get referenceDataset
   * @return referenceDataset
  */
  @ApiModelProperty(value = "")


  public String getReferenceDataset() {
    return referenceDataset;
  }

  public void setReferenceDataset(String referenceDataset) {
    this.referenceDataset = referenceDataset;
  }

  public LexicalizationSet lexicalizationModel(String lexicalizationModel) {
    this.lexicalizationModel = lexicalizationModel;
    return this;
  }

  /**
   * Get lexicalizationModel
   * @return lexicalizationModel
  */
  @ApiModelProperty(value = "")


  public String getLexicalizationModel() {
    return lexicalizationModel;
  }

  public void setLexicalizationModel(String lexicalizationModel) {
    this.lexicalizationModel = lexicalizationModel;
  }

  public LexicalizationSet lexicalizations(Integer lexicalizations) {
    this.lexicalizations = lexicalizations;
    return this;
  }

  /**
   * Get lexicalizations
   * @return lexicalizations
  */
  @ApiModelProperty(value = "")


  public Integer getLexicalizations() {
    return lexicalizations;
  }

  public void setLexicalizations(Integer lexicalizations) {
    this.lexicalizations = lexicalizations;
  }

  public LexicalizationSet references(Integer references) {
    this.references = references;
    return this;
  }

  /**
   * Get references
   * @return references
  */
  @ApiModelProperty(value = "")


  public Integer getReferences() {
    return references;
  }

  public void setReferences(Integer references) {
    this.references = references;
  }

  public LexicalizationSet lexicalEntries(Integer lexicalEntries) {
    this.lexicalEntries = lexicalEntries;
    return this;
  }

  /**
   * Get lexicalEntries
   * @return lexicalEntries
  */
  @ApiModelProperty(value = "")


  public Integer getLexicalEntries() {
    return lexicalEntries;
  }

  public void setLexicalEntries(Integer lexicalEntries) {
    this.lexicalEntries = lexicalEntries;
  }

  public LexicalizationSet avgNumOfLexicalizations(Double avgNumOfLexicalizations) {
    this.avgNumOfLexicalizations = avgNumOfLexicalizations;
    return this;
  }

  /**
   * Get avgNumOfLexicalizations
   * @return avgNumOfLexicalizations
  */
  @ApiModelProperty(value = "")


  public Double getAvgNumOfLexicalizations() {
    return avgNumOfLexicalizations;
  }

  public void setAvgNumOfLexicalizations(Double avgNumOfLexicalizations) {
    this.avgNumOfLexicalizations = avgNumOfLexicalizations;
  }

  public LexicalizationSet percentage(Double percentage) {
    this.percentage = percentage;
    return this;
  }

  /**
   * Get percentage
   * @return percentage
  */
  @ApiModelProperty(value = "")


  public Double getPercentage() {
    return percentage;
  }

  public void setPercentage(Double percentage) {
    this.percentage = percentage;
  }

  public LexicalizationSet languageTag(String languageTag) {
    this.languageTag = languageTag;
    return this;
  }

  /**
   * Get languageTag
   * @return languageTag
  */
  @ApiModelProperty(value = "")


  public String getLanguageTag() {
    return languageTag;
  }

  public void setLanguageTag(String languageTag) {
    this.languageTag = languageTag;
  }

  public LexicalizationSet languageLexvo(String languageLexvo) {
    this.languageLexvo = languageLexvo;
    return this;
  }

  /**
   * Get languageLexvo
   * @return languageLexvo
  */
  @ApiModelProperty(value = "")


  public String getLanguageLexvo() {
    return languageLexvo;
  }

  public void setLanguageLexvo(String languageLexvo) {
    this.languageLexvo = languageLexvo;
  }

  public LexicalizationSet languageLOC(String languageLOC) {
    this.languageLOC = languageLOC;
    return this;
  }

  /**
   * Get languageLOC
   * @return languageLOC
  */
  @ApiModelProperty(value = "")


  public String getLanguageLOC() {
    return languageLOC;
  }

  public void setLanguageLOC(String languageLOC) {
    this.languageLOC = languageLOC;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LexicalizationSet lexicalizationSet = (LexicalizationSet) o;
    return Objects.equals(this.lexiconDataset, lexicalizationSet.lexiconDataset) &&
        Objects.equals(this.referenceDataset, lexicalizationSet.referenceDataset) &&
        Objects.equals(this.lexicalizationModel, lexicalizationSet.lexicalizationModel) &&
        Objects.equals(this.lexicalizations, lexicalizationSet.lexicalizations) &&
        Objects.equals(this.references, lexicalizationSet.references) &&
        Objects.equals(this.lexicalEntries, lexicalizationSet.lexicalEntries) &&
        Objects.equals(this.avgNumOfLexicalizations, lexicalizationSet.avgNumOfLexicalizations) &&
        Objects.equals(this.percentage, lexicalizationSet.percentage) &&
        Objects.equals(this.languageTag, lexicalizationSet.languageTag) &&
        Objects.equals(this.languageLexvo, lexicalizationSet.languageLexvo) &&
        Objects.equals(this.languageLOC, lexicalizationSet.languageLOC) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(lexiconDataset, referenceDataset, lexicalizationModel, lexicalizations, references, lexicalEntries, avgNumOfLexicalizations, percentage, languageTag, languageLexvo, languageLOC, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LexicalizationSet {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    lexiconDataset: ").append(toIndentedString(lexiconDataset)).append("\n");
    sb.append("    referenceDataset: ").append(toIndentedString(referenceDataset)).append("\n");
    sb.append("    lexicalizationModel: ").append(toIndentedString(lexicalizationModel)).append("\n");
    sb.append("    lexicalizations: ").append(toIndentedString(lexicalizations)).append("\n");
    sb.append("    references: ").append(toIndentedString(references)).append("\n");
    sb.append("    lexicalEntries: ").append(toIndentedString(lexicalEntries)).append("\n");
    sb.append("    avgNumOfLexicalizations: ").append(toIndentedString(avgNumOfLexicalizations)).append("\n");
    sb.append("    percentage: ").append(toIndentedString(percentage)).append("\n");
    sb.append("    languageTag: ").append(toIndentedString(languageTag)).append("\n");
    sb.append("    languageLexvo: ").append(toIndentedString(languageLexvo)).append("\n");
    sb.append("    languageLOC: ").append(toIndentedString(languageLOC)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

