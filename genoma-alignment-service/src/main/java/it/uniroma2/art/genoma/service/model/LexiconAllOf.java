package it.uniroma2.art.genoma.service.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 * LexiconAllOf
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-07-21T15:35:09.281+02:00[Europe/Berlin]")
public class LexiconAllOf   {
  @JsonProperty("languageTag")
  private String languageTag;

  @JsonProperty("languageLexvo")
  private String languageLexvo;

  @JsonProperty("languageLOC")
  private String languageLOC;

  @JsonProperty("linguisticCatalog")
  private String linguisticCatalog;

  @JsonProperty("lexicalEntries")
  private Integer lexicalEntries;

  public LexiconAllOf languageTag(String languageTag) {
    this.languageTag = languageTag;
    return this;
  }

  /**
   * Get languageTag
   * @return languageTag
  */
  @ApiModelProperty(value = "")


  public String getLanguageTag() {
    return languageTag;
  }

  public void setLanguageTag(String languageTag) {
    this.languageTag = languageTag;
  }

  public LexiconAllOf languageLexvo(String languageLexvo) {
    this.languageLexvo = languageLexvo;
    return this;
  }

  /**
   * Get languageLexvo
   * @return languageLexvo
  */
  @ApiModelProperty(value = "")


  public String getLanguageLexvo() {
    return languageLexvo;
  }

  public void setLanguageLexvo(String languageLexvo) {
    this.languageLexvo = languageLexvo;
  }

  public LexiconAllOf languageLOC(String languageLOC) {
    this.languageLOC = languageLOC;
    return this;
  }

  /**
   * Get languageLOC
   * @return languageLOC
  */
  @ApiModelProperty(value = "")


  public String getLanguageLOC() {
    return languageLOC;
  }

  public void setLanguageLOC(String languageLOC) {
    this.languageLOC = languageLOC;
  }

  public LexiconAllOf linguisticCatalog(String linguisticCatalog) {
    this.linguisticCatalog = linguisticCatalog;
    return this;
  }

  /**
   * Get linguisticCatalog
   * @return linguisticCatalog
  */
  @ApiModelProperty(value = "")


  public String getLinguisticCatalog() {
    return linguisticCatalog;
  }

  public void setLinguisticCatalog(String linguisticCatalog) {
    this.linguisticCatalog = linguisticCatalog;
  }

  public LexiconAllOf lexicalEntries(Integer lexicalEntries) {
    this.lexicalEntries = lexicalEntries;
    return this;
  }

  /**
   * Get lexicalEntries
   * @return lexicalEntries
  */
  @ApiModelProperty(value = "")


  public Integer getLexicalEntries() {
    return lexicalEntries;
  }

  public void setLexicalEntries(Integer lexicalEntries) {
    this.lexicalEntries = lexicalEntries;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LexiconAllOf lexiconAllOf = (LexiconAllOf) o;
    return Objects.equals(this.languageTag, lexiconAllOf.languageTag) &&
        Objects.equals(this.languageLexvo, lexiconAllOf.languageLexvo) &&
        Objects.equals(this.languageLOC, lexiconAllOf.languageLOC) &&
        Objects.equals(this.linguisticCatalog, lexiconAllOf.linguisticCatalog) &&
        Objects.equals(this.lexicalEntries, lexiconAllOf.lexicalEntries);
  }

  @Override
  public int hashCode() {
    return Objects.hash(languageTag, languageLexvo, languageLOC, linguisticCatalog, lexicalEntries);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LexiconAllOf {\n");
    
    sb.append("    languageTag: ").append(toIndentedString(languageTag)).append("\n");
    sb.append("    languageLexvo: ").append(toIndentedString(languageLexvo)).append("\n");
    sb.append("    languageLOC: ").append(toIndentedString(languageLOC)).append("\n");
    sb.append("    linguisticCatalog: ").append(toIndentedString(linguisticCatalog)).append("\n");
    sb.append("    lexicalEntries: ").append(toIndentedString(lexicalEntries)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

