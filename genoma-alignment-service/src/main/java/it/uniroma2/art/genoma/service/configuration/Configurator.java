package it.uniroma2.art.genoma.service.configuration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import it.uniroma2.art.genoma.DataManagement.AlignmentResultSet;
import it.uniroma2.art.genoma.DataManagement.EnginesSet;

@Component
public class Configurator implements InitializingBean {
	@Autowired
	Environment env;

	List<String> repositoryDirectories = Arrays.asList("cache","custom_matchers","engines","groovy_scripts","libs","logs", "matcherdata", "policies", "results");

	private void initRepository() throws IOException {
		if(!isRepositoryDirectoryValid()) {
			createRepositoryStructure();
		}
		final String resultsetPath = env.getProperty("repository.path") + File.separator + "resultset.xml";
		AlignmentResultSet.loadRepositoryFromFile(resultsetPath);
		//final FileOutputStream fosRes = new FileOutputStream(resultsetPath, true);
		//AlignmentResultSet.init(fosRes);

		final String enginesPath = env.getProperty("repository.path") + File.separator +"engineset.xml";
		EnginesSet.loadRepositoryFromFile(enginesPath);
		//final FileOutputStream fosEng = new FileOutputStream(enginesPath, true);
		//EnginesSet.init(fosEng);
	}

	private void createRepositoryStructure() throws IOException {
		for(String directory: repositoryDirectories) {
			Path path = Paths.get(env.getProperty("repository.path") + File.separator + directory);
			Files.createDirectories(path);
		}

	}

	private boolean isRepositoryDirectoryValid() {
		return Files.exists(Paths.get(env.getProperty("repository.path")));
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		initRepository();
	}

}
