package it.uniroma2.art.genoma.service.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * The task of aligning &#x60;leftDataset&#x60; and &#x60;rightDataset&#x60;
 */
@ApiModel(description = "The task of aligning `leftDataset` and `rightDataset`")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-07-21T15:35:09.281+02:00[Europe/Berlin]")
public class Task   {
  @JsonProperty("id")
  private String id;

  @JsonProperty("leftDataset")
  private String leftDataset;

  @JsonProperty("rightDataset")
  private String rightDataset;

  /**
   * Different stages of a task lifecycle
   */
  public enum StatusEnum {
    SUBMITTED("submitted"),
    
    RUNNING("running"),
    
    FAILED("failed"),
    
    COMPLETED("completed");

    private String value;

    StatusEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static StatusEnum fromValue(String value) {
      for (StatusEnum b : StatusEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("status")
  private StatusEnum status;

  @JsonProperty("progress")
  private JsonNullable<Integer> progress = JsonNullable.undefined();

  @JsonProperty("reason")
  private JsonNullable<TaskReason> reason = JsonNullable.undefined();

  @JsonProperty("submissionTime")
  @org.springframework.format.annotation.DateTimeFormat(iso = org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime submissionTime;

  @JsonProperty("startTime")
  @org.springframework.format.annotation.DateTimeFormat(iso = org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME)
  private JsonNullable<OffsetDateTime> startTime = JsonNullable.undefined();

  @JsonProperty("endTime")
  @org.springframework.format.annotation.DateTimeFormat(iso = org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME)
  private JsonNullable<OffsetDateTime> endTime = JsonNullable.undefined();

  public Task id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Identifier of the task
   * @return id
  */
  @ApiModelProperty(required = true, value = "Identifier of the task")
  @NotNull


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Task leftDataset(String leftDataset) {
    this.leftDataset = leftDataset;
    return this;
  }

  /**
   * The dataset containing the left-hand side of the semantic correspondences to compute
   * @return leftDataset
  */
  @ApiModelProperty(required = true, value = "The dataset containing the left-hand side of the semantic correspondences to compute")
  @NotNull


  public String getLeftDataset() {
    return leftDataset;
  }

  public void setLeftDataset(String leftDataset) {
    this.leftDataset = leftDataset;
  }

  public Task rightDataset(String rightDataset) {
    this.rightDataset = rightDataset;
    return this;
  }

  /**
   * The dataset containing the right-hand side of the semantic correspondences to compute
   * @return rightDataset
  */
  @ApiModelProperty(required = true, value = "The dataset containing the right-hand side of the semantic correspondences to compute")
  @NotNull


  public String getRightDataset() {
    return rightDataset;
  }

  public void setRightDataset(String rightDataset) {
    this.rightDataset = rightDataset;
  }

  public Task status(StatusEnum status) {
    this.status = status;
    return this;
  }

  /**
   * Different stages of a task lifecycle
   * @return status
  */
  @ApiModelProperty(required = true, value = "Different stages of a task lifecycle")
  @NotNull


  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public Task progress(Integer progress) {
    this.progress = JsonNullable.of(progress);
    return this;
  }

  /**
   * the percentage of work done by `running` tasks
   * minimum: 0
   * maximum: 100
   * @return progress
  */
  @ApiModelProperty(value = "the percentage of work done by `running` tasks")

@Min(0) @Max(100) 
  public JsonNullable<Integer> getProgress() {
    return progress;
  }

  public void setProgress(JsonNullable<Integer> progress) {
    this.progress = progress;
  }

  public Task reason(TaskReason reason) {
    this.reason = JsonNullable.of(reason);
    return this;
  }

  /**
   * Get reason
   * @return reason
  */
  @ApiModelProperty(value = "")

  @Valid

  public JsonNullable<TaskReason> getReason() {
    return reason;
  }

  public void setReason(JsonNullable<TaskReason> reason) {
    this.reason = reason;
  }

  public Task submissionTime(OffsetDateTime submissionTime) {
    this.submissionTime = submissionTime;
    return this;
  }

  /**
   * The instant at which the task was submitted
   * @return submissionTime
  */
  @ApiModelProperty(required = true, value = "The instant at which the task was submitted")
  @NotNull

  @Valid

  public OffsetDateTime getSubmissionTime() {
    return submissionTime;
  }

  public void setSubmissionTime(OffsetDateTime submissionTime) {
    this.submissionTime = submissionTime;
  }

  public Task startTime(OffsetDateTime startTime) {
    this.startTime = JsonNullable.of(startTime);
    return this;
  }

  /**
   * The instant at which the execution of the task actually started
   * @return startTime
  */
  @ApiModelProperty(value = "The instant at which the execution of the task actually started")

  @Valid

  public JsonNullable<OffsetDateTime> getStartTime() {
    return startTime;
  }

  public void setStartTime(JsonNullable<OffsetDateTime> startTime) {
    this.startTime = startTime;
  }

  public Task endTime(OffsetDateTime endTime) {
    this.endTime = JsonNullable.of(endTime);
    return this;
  }

  /**
   * The instant at which the execution of the task ended, because of successful completion or a failure
   * @return endTime
  */
  @ApiModelProperty(value = "The instant at which the execution of the task ended, because of successful completion or a failure")

  @Valid

  public JsonNullable<OffsetDateTime> getEndTime() {
    return endTime;
  }

  public void setEndTime(JsonNullable<OffsetDateTime> endTime) {
    this.endTime = endTime;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Task task = (Task) o;
    return Objects.equals(this.id, task.id) &&
        Objects.equals(this.leftDataset, task.leftDataset) &&
        Objects.equals(this.rightDataset, task.rightDataset) &&
        Objects.equals(this.status, task.status) &&
        Objects.equals(this.progress, task.progress) &&
        Objects.equals(this.reason, task.reason) &&
        Objects.equals(this.submissionTime, task.submissionTime) &&
        Objects.equals(this.startTime, task.startTime) &&
        Objects.equals(this.endTime, task.endTime);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, leftDataset, rightDataset, status, progress, reason, submissionTime, startTime, endTime);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Task {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    leftDataset: ").append(toIndentedString(leftDataset)).append("\n");
    sb.append("    rightDataset: ").append(toIndentedString(rightDataset)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    progress: ").append(toIndentedString(progress)).append("\n");
    sb.append("    reason: ").append(toIndentedString(reason)).append("\n");
    sb.append("    submissionTime: ").append(toIndentedString(submissionTime)).append("\n");
    sb.append("    startTime: ").append(toIndentedString(startTime)).append("\n");
    sb.append("    endTime: ").append(toIndentedString(endTime)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

