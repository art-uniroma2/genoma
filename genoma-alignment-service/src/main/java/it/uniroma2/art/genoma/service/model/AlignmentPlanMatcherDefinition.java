package it.uniroma2.art.genoma.service.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;

/**
 * Matcher to use and its settings
 */
@ApiModel(description = "Matcher to use and its settings")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-07-21T15:35:09.281+02:00[Europe/Berlin]")
public class AlignmentPlanMatcherDefinition   {
  @JsonProperty("id")
  private String id;

  @JsonProperty("settings")
  private Object settings;

  public AlignmentPlanMatcherDefinition id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Identifier of the matcher
   * @return id
  */
  @ApiModelProperty(required = true, value = "Identifier of the matcher")
  @NotNull


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public AlignmentPlanMatcherDefinition settings(Object settings) {
    this.settings = settings;
    return this;
  }

  /**
   * An actual settings object. Its definition is left under-specified, as it can be essentially any JSON object
   * @return settings
  */
  @ApiModelProperty(value = "An actual settings object. Its definition is left under-specified, as it can be essentially any JSON object")


  public Object getSettings() {
    return settings;
  }

  public void setSettings(Object settings) {
    this.settings = settings;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AlignmentPlanMatcherDefinition alignmentPlanMatcherDefinition = (AlignmentPlanMatcherDefinition) o;
    return Objects.equals(this.id, alignmentPlanMatcherDefinition.id) &&
        Objects.equals(this.settings, alignmentPlanMatcherDefinition.settings);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, settings);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AlignmentPlanMatcherDefinition {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    settings: ").append(toIndentedString(settings)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

