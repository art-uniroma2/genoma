package it.uniroma2.art.genoma.service.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;

/**
 * Alignment
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-07-21T15:35:09.281+02:00[Europe/Berlin]")
public class Alignment extends Dataset  {
  @JsonProperty("subjectsTarget")
  private String subjectsTarget;

  @JsonProperty("objectsTarget")
  private String objectsTarget;

  public Alignment subjectsTarget(String subjectsTarget) {
    this.subjectsTarget = subjectsTarget;
    return this;
  }

  /**
   * Get subjectsTarget
   * @return subjectsTarget
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getSubjectsTarget() {
    return subjectsTarget;
  }

  public void setSubjectsTarget(String subjectsTarget) {
    this.subjectsTarget = subjectsTarget;
  }

  public Alignment objectsTarget(String objectsTarget) {
    this.objectsTarget = objectsTarget;
    return this;
  }

  /**
   * Get objectsTarget
   * @return objectsTarget
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getObjectsTarget() {
    return objectsTarget;
  }

  public void setObjectsTarget(String objectsTarget) {
    this.objectsTarget = objectsTarget;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Alignment alignment = (Alignment) o;
    return Objects.equals(this.subjectsTarget, alignment.subjectsTarget) &&
        Objects.equals(this.objectsTarget, alignment.objectsTarget) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(subjectsTarget, objectsTarget, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Alignment {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    subjectsTarget: ").append(toIndentedString(subjectsTarget)).append("\n");
    sb.append("    objectsTarget: ").append(toIndentedString(objectsTarget)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

