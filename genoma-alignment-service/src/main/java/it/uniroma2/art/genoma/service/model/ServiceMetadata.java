package it.uniroma2.art.genoma.service.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Metadata about the alignment service
 */
@ApiModel(description = "Metadata about the alignment service")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-07-21T15:35:09.281+02:00[Europe/Berlin]")
public class ServiceMetadata   {
  @JsonProperty("service")
  private String service;

  @JsonProperty("version")
  private String version;

  /**
   * The status of the service
   */
  public enum StatusEnum {
    STARTING("starting"),
    
    ACTIVE("active"),
    
    BUSY("busy"),
    
    SHUTTING_DOWN("shutting down"),
    
    FAILED("failed");

    private String value;

    StatusEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static StatusEnum fromValue(String value) {
      for (StatusEnum b : StatusEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("status")
  private StatusEnum status;

  @JsonProperty("specs")
  @Valid
  private List<String> specs = new ArrayList<>();

  @JsonProperty("contact")
  private ServiceMetadataContact contact;

  @JsonProperty("documentation")
  private URI documentation;

  @JsonProperty("settings")
  private Object settings;

  public ServiceMetadata service(String service) {
    this.service = service;
    return this;
  }

  /**
   * The name of the service
   * @return service
  */
  @ApiModelProperty(required = true, value = "The name of the service")
  @NotNull


  public String getService() {
    return service;
  }

  public void setService(String service) {
    this.service = service;
  }

  public ServiceMetadata version(String version) {
    this.version = version;
    return this;
  }

  /**
   * The version of the service
   * @return version
  */
  @ApiModelProperty(required = true, value = "The version of the service")
  @NotNull


  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public ServiceMetadata status(StatusEnum status) {
    this.status = status;
    return this;
  }

  /**
   * The status of the service
   * @return status
  */
  @ApiModelProperty(required = true, value = "The status of the service")
  @NotNull


  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public ServiceMetadata specs(List<String> specs) {
    this.specs = specs;
    return this;
  }

  public ServiceMetadata addSpecsItem(String specsItem) {
    this.specs.add(specsItem);
    return this;
  }

  /**
   * The specifications implemented by the service. The collection shall contain at least a reference to this alignment services specification.  
   * @return specs
  */
  @ApiModelProperty(required = true, value = "The specifications implemented by the service. The collection shall contain at least a reference to this alignment services specification.  ")
  @NotNull

@Size(min=1) 
  public List<String> getSpecs() {
    return specs;
  }

  public void setSpecs(List<String> specs) {
    this.specs = specs;
  }

  public ServiceMetadata contact(ServiceMetadataContact contact) {
    this.contact = contact;
    return this;
  }

  /**
   * Get contact
   * @return contact
  */
  @ApiModelProperty(value = "")

  @Valid

  public ServiceMetadataContact getContact() {
    return contact;
  }

  public void setContact(ServiceMetadataContact contact) {
    this.contact = contact;
  }

  public ServiceMetadata documentation(URI documentation) {
    this.documentation = documentation;
    return this;
  }

  /**
   * The address of the documentation of the service
   * @return documentation
  */
  @ApiModelProperty(value = "The address of the documentation of the service")

  @Valid

  public URI getDocumentation() {
    return documentation;
  }

  public void setDocumentation(URI documentation) {
    this.documentation = documentation;
  }

  public ServiceMetadata settings(Object settings) {
    this.settings = settings;
    return this;
  }

  /**
   * A JSON schema. Its definition is left left under-specified.
   * @return settings
  */
  @ApiModelProperty(value = "A JSON schema. Its definition is left left under-specified.")


  public Object getSettings() {
    return settings;
  }

  public void setSettings(Object settings) {
    this.settings = settings;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ServiceMetadata serviceMetadata = (ServiceMetadata) o;
    return Objects.equals(this.service, serviceMetadata.service) &&
        Objects.equals(this.version, serviceMetadata.version) &&
        Objects.equals(this.status, serviceMetadata.status) &&
        Objects.equals(this.specs, serviceMetadata.specs) &&
        Objects.equals(this.contact, serviceMetadata.contact) &&
        Objects.equals(this.documentation, serviceMetadata.documentation) &&
        Objects.equals(this.settings, serviceMetadata.settings);
  }

  @Override
  public int hashCode() {
    return Objects.hash(service, version, status, specs, contact, documentation, settings);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ServiceMetadata {\n");
    
    sb.append("    service: ").append(toIndentedString(service)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    specs: ").append(toIndentedString(specs)).append("\n");
    sb.append("    contact: ").append(toIndentedString(contact)).append("\n");
    sb.append("    documentation: ").append(toIndentedString(documentation)).append("\n");
    sb.append("    settings: ").append(toIndentedString(settings)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

