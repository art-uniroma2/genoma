package it.uniroma2.art.genoma.service.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Pairing
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-07-21T15:35:09.281+02:00[Europe/Berlin]")
public class Pairing   {
  @JsonProperty("score")
  private Double score;

  @JsonProperty("source")
  private PairingHand source;

  @JsonProperty("target")
  private PairingHand target;

  @JsonProperty("synonymizer")
  private Synonymizer synonymizer;

  public Pairing score(Double score) {
    this.score = score;
    return this;
  }

  /**
   * Get score
   * @return score
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Double getScore() {
    return score;
  }

  public void setScore(Double score) {
    this.score = score;
  }

  public Pairing source(PairingHand source) {
    this.source = source;
    return this;
  }

  /**
   * Get source
   * @return source
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public PairingHand getSource() {
    return source;
  }

  public void setSource(PairingHand source) {
    this.source = source;
  }

  public Pairing target(PairingHand target) {
    this.target = target;
    return this;
  }

  /**
   * Get target
   * @return target
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public PairingHand getTarget() {
    return target;
  }

  public void setTarget(PairingHand target) {
    this.target = target;
  }

  public Pairing synonymizer(Synonymizer synonymizer) {
    this.synonymizer = synonymizer;
    return this;
  }

  /**
   * Get synonymizer
   * @return synonymizer
  */
  @ApiModelProperty(value = "")

  @Valid

  public Synonymizer getSynonymizer() {
    return synonymizer;
  }

  public void setSynonymizer(Synonymizer synonymizer) {
    this.synonymizer = synonymizer;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Pairing pairing = (Pairing) o;
    return Objects.equals(this.score, pairing.score) &&
        Objects.equals(this.source, pairing.source) &&
        Objects.equals(this.target, pairing.target) &&
        Objects.equals(this.synonymizer, pairing.synonymizer);
  }

  @Override
  public int hashCode() {
    return Objects.hash(score, source, target, synonymizer);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Pairing {\n");
    
    sb.append("    score: ").append(toIndentedString(score)).append("\n");
    sb.append("    source: ").append(toIndentedString(source)).append("\n");
    sb.append("    target: ").append(toIndentedString(target)).append("\n");
    sb.append("    synonymizer: ").append(toIndentedString(synonymizer)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

