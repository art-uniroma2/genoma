package it.uniroma2.art.genoma.service.api;

import java.net.URI;
import java.util.Arrays;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;


import it.uniroma2.art.genoma.service.model.ServiceMetadata;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-06-10T20:58:30.151+02:00[Europe/Berlin]")

@Controller
@RequestMapping("${openapi.alignmentServices.base-path:}")
public class DefaultApiController implements DefaultApi {

	private final NativeWebRequest request;

	@org.springframework.beans.factory.annotation.Autowired
	public DefaultApiController(NativeWebRequest request) {
		this.request = request;
	}

	@Override
	public Optional<NativeWebRequest> getRequest() {
		return Optional.ofNullable(request);
	}

	@Override
	public ResponseEntity<ServiceMetadata> getServiceMetadata() {
		ServiceMetadata serviceMetadata = new ServiceMetadata();
		serviceMetadata.setService("GENOMA REST API");
		serviceMetadata.setVersion("1.0");
		serviceMetadata.setStatus(ServiceMetadata.StatusEnum.ACTIVE);
		serviceMetadata.setDocumentation(URI.create("https://bitbucket.org/art-uniroma2/dome/wiki/Home"));
		serviceMetadata.setSpecs(Arrays.asList("http://art.uniroma2.it/maple/alignment-services-3.1.yaml"));
		
		return new ResponseEntity<>(serviceMetadata, HttpStatus.OK);
	}
}
