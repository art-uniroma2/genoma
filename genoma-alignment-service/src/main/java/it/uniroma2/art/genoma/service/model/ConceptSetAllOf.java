package it.uniroma2.art.genoma.service.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 * ConceptSetAllOf
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-07-21T15:35:09.281+02:00[Europe/Berlin]")
public class ConceptSetAllOf   {
  @JsonProperty("concepts")
  private Long concepts;

  public ConceptSetAllOf concepts(Long concepts) {
    this.concepts = concepts;
    return this;
  }

  /**
   * Get concepts
   * @return concepts
  */
  @ApiModelProperty(value = "")


  public Long getConcepts() {
    return concepts;
  }

  public void setConcepts(Long concepts) {
    this.concepts = concepts;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConceptSetAllOf conceptSetAllOf = (ConceptSetAllOf) o;
    return Objects.equals(this.concepts, conceptSetAllOf.concepts);
  }

  @Override
  public int hashCode() {
    return Objects.hash(concepts);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ConceptSetAllOf {\n");
    
    sb.append("    concepts: ").append(toIndentedString(concepts)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

