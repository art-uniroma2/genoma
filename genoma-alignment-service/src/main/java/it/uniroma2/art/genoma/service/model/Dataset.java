package it.uniroma2.art.genoma.service.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Dataset
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-07-21T15:35:09.281+02:00[Europe/Berlin]")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "@type", visible = true)
@JsonSubTypes({
  @JsonSubTypes.Type(value = VoidDataset.class, name = "http://rdfs.org/ns/void#Dataset"),
  @JsonSubTypes.Type(value = Alignment.class, name = "http://semanticturkey.uniroma2.it/ns/mdr#Alignment"),
  @JsonSubTypes.Type(value = ConceptualizationSet.class, name = "http://www.w3.org/ns/lemon/lime#ConceptualizationSet"),
  @JsonSubTypes.Type(value = LexicalizationSet.class, name = "http://www.w3.org/ns/lemon/lime#LexicalizationSet"),
  @JsonSubTypes.Type(value = Lexicon.class, name = "http://www.w3.org/ns/lemon/lime#Lexicon"),
  @JsonSubTypes.Type(value = ConceptSet.class, name = "http://www.w3.org/ns/lemon/ontolex#ConceptSet"),
})

public class Dataset   {
  @JsonProperty("@id")
  private String atId;

  @JsonProperty("@type")
  private String atType;

  @JsonProperty("uriSpace")
  private String uriSpace;

  @JsonProperty("sparqlEndpoint")
  private DataService sparqlEndpoint;

  @JsonProperty("conformsTo")
  private String conformsTo;

  public Dataset atId(String atId) {
    this.atId = atId;
    return this;
  }

  /**
   * Get atId
   * @return atId
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getAtId() {
    return atId;
  }

  public void setAtId(String atId) {
    this.atId = atId;
  }

  public Dataset atType(String atType) {
    this.atType = atType;
    return this;
  }

  /**
   * Get atType
   * @return atType
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getAtType() {
    return atType;
  }

  public void setAtType(String atType) {
    this.atType = atType;
  }

  public Dataset uriSpace(String uriSpace) {
    this.uriSpace = uriSpace;
    return this;
  }

  /**
   * Get uriSpace
   * @return uriSpace
  */
  @ApiModelProperty(value = "")


  public String getUriSpace() {
    return uriSpace;
  }

  public void setUriSpace(String uriSpace) {
    this.uriSpace = uriSpace;
  }

  public Dataset sparqlEndpoint(DataService sparqlEndpoint) {
    this.sparqlEndpoint = sparqlEndpoint;
    return this;
  }

  /**
   * Get sparqlEndpoint
   * @return sparqlEndpoint
  */
  @ApiModelProperty(value = "")

  @Valid

  public DataService getSparqlEndpoint() {
    return sparqlEndpoint;
  }

  public void setSparqlEndpoint(DataService sparqlEndpoint) {
    this.sparqlEndpoint = sparqlEndpoint;
  }

  public Dataset conformsTo(String conformsTo) {
    this.conformsTo = conformsTo;
    return this;
  }

  /**
   * Get conformsTo
   * @return conformsTo
  */
  @ApiModelProperty(value = "")


  public String getConformsTo() {
    return conformsTo;
  }

  public void setConformsTo(String conformsTo) {
    this.conformsTo = conformsTo;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Dataset dataset = (Dataset) o;
    return Objects.equals(this.atId, dataset.atId) &&
        Objects.equals(this.atType, dataset.atType) &&
        Objects.equals(this.uriSpace, dataset.uriSpace) &&
        Objects.equals(this.sparqlEndpoint, dataset.sparqlEndpoint) &&
        Objects.equals(this.conformsTo, dataset.conformsTo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(atId, atType, uriSpace, sparqlEndpoint, conformsTo);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Dataset {\n");
    
    sb.append("    atId: ").append(toIndentedString(atId)).append("\n");
    sb.append("    atType: ").append(toIndentedString(atType)).append("\n");
    sb.append("    uriSpace: ").append(toIndentedString(uriSpace)).append("\n");
    sb.append("    sparqlEndpoint: ").append(toIndentedString(sparqlEndpoint)).append("\n");
    sb.append("    conformsTo: ").append(toIndentedString(conformsTo)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

