package it.uniroma2.art.genoma.service.api;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.validation.Valid;

import it.uniroma2.art.genoma.ComposerAggregator.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.openapitools.jackson.nullable.JsonNullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiParam;
import it.uniroma2.art.genoma.ComposerAggregator.MatcherConfiguration.FileType;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource.Lexicalization;
import it.uniroma2.art.genoma.DataManagement.AlignmentResultSet;
import it.uniroma2.art.genoma.DataManagement.Result;
import it.uniroma2.art.genoma.service.model.AlignmentPlan;
import it.uniroma2.art.genoma.service.model.Dataset;
import it.uniroma2.art.genoma.service.model.LexicalizationSet;
import it.uniroma2.art.genoma.service.model.Pairing;
import it.uniroma2.art.genoma.service.model.ScenarioDefinition;
import it.uniroma2.art.genoma.service.model.Task;
import it.uniroma2.art.genoma.service.model.TaskReason;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-06-10T19:01:04.879+02:00[Europe/Berlin]")

@Controller
@RequestMapping("${openapi.alignmentServices.base-path:}")
public class TasksApiController implements TasksApi {

	private static final Logger log = LoggerFactory.getLogger(TasksApiController.class);

	private final NativeWebRequest request;
	private final ObjectMapper objectMapper;
	private final TaskExecutor taskExecutor;
	private final Environment env;

	@org.springframework.beans.factory.annotation.Autowired
	public TasksApiController(NativeWebRequest request, ObjectMapper objectMapper, Environment env,
			TaskExecutor taskExecutor) {
		this.request = request;
		this.objectMapper = objectMapper;
		this.env = env;
		this.taskExecutor = taskExecutor;
	}

	@Override
	public Optional<NativeWebRequest> getRequest() {
		return Optional.ofNullable(request);
	}

	@Override
	public ResponseEntity<Void> deleteTaskByID(
			@ApiParam(value = "Task ID", required = true) @PathVariable("id") String id) {
		try {
			AlignmentResultSet.removeResult(id);
			removeFiles(id, env.getProperty("repository.path") + "/results/");
			removeFiles(id, env.getProperty("repository.path") + "/logs/");
			AlignmentResultSet.saveRepositoryToFile(null);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (IOException e) {
			log.error("Failed to save repository to file after deleting a task", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ResponseEntity<Resource> downloadAlignment(
			@ApiParam(value = "Task ID", required = true) @PathVariable("id") String id) {
		Result result = AlignmentResultSet.getResult(id);
		if (result == null || !Objects.equals(result.getStatus(), "Completed")) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(new UrlResource(result.getAlignmentFile()), HttpStatus.OK);
		}
	}

	@Override
	public ResponseEntity<Task> getTaskByID(
			@ApiParam(value = "Task ID", required = true) @PathVariable("id") String id) {
		String accept = request.getHeader("Accept");
		if (accept != null && accept.contains("application/json")) {
			Result result = AlignmentResultSet.getResult(id);
			if (result != null) {
				return new ResponseEntity<>(convertGenomaResultToTask(result), HttpStatus.OK);
			} else {
				new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}

		}

		return new ResponseEntity<Task>(HttpStatus.NOT_IMPLEMENTED);
	}

	@Override
	public ResponseEntity<List<Task>> getTasks() {
		String accept = request.getHeader("Accept");
		if (accept != null && accept.contains("application/json")) {
			final Set<String> keySet = AlignmentResultSet.getKeySet();
			final List<Task> tasks = new ArrayList<>(keySet.size());
			for (final String key : keySet) {
				Result result = AlignmentResultSet.getResult(key);
				Task task = convertGenomaResultToTask(result);
				tasks.add(task);
			}
			return new ResponseEntity<List<Task>>(tasks, HttpStatus.OK);
		}

		return new ResponseEntity<List<Task>>(HttpStatus.NOT_IMPLEMENTED);
	}

	@Override
	public ResponseEntity<Task> submitTask(
			@ApiParam(value = "") @Valid @RequestBody(required = false) AlignmentPlan alignmentPlan) {
		String accept = request.getHeader("Accept");
		if (accept != null && accept.contains("application/json")) {
			try {
				ScenarioDefinition taskReport = alignmentPlan.getScenarioDefinition();

				checkTaskSubmission(alignmentPlan);
				final File resourcesDirectory = new File(env.getProperty("repository.path"));
				final String basePath = resourcesDirectory.getAbsolutePath();
				Map<String, Dataset> id2supportDataset = taskReport.getSupportDatasets().stream()
						.collect(Collectors.toMap(Dataset::getAtId, Function.identity()));

				Pairing bestPairing = ApiUtil.getBestPairing(taskReport.getPairings());

				if (bestPairing == null) {
					throw new IllegalArgumentException("No pairing of lexicalization sets");
				}
				LexicalizationSet sourceLexicalizationSet = (LexicalizationSet) id2supportDataset
						.get(bestPairing.getSource().getLexicalizationSet());
				LexicalizationSet targetLexicalizationSet = (LexicalizationSet) id2supportDataset
						.get(bestPairing.getTarget().getLexicalizationSet());

				String engine = basePath + File.separator + "engines" + File.separator + env.getProperty("maple.engine");
				Dataset synonymizer = null;

				if (bestPairing.getSynonymizer() != null) {
					engine = basePath + File.separator+ "engines" + File.separator + env.getProperty("maple.engine.withSynonymizer");
					synonymizer = bestPairing.getSynonymizer() != null
							? id2supportDataset.get(bestPairing.getSynonymizer().getLexicon())
							: null;
				}

				final Result res = new Result(taskReport.getLeftDataset().getAtId(),
						taskReport.getRightDataset().getAtId(), "file:///" + engine, "file:///" + basePath);
				final String idalignmentjob = res.getAlignmentId();
				AlignmentResultSet.addResult(idalignmentjob, res);
				//Map<FileType, OutputStream> outFiles = prepareOutputFiles(idalignmentjob, basePath);
				Map<FileType, String> outFilesMap = prepareOutputFiles(idalignmentjob, basePath);
				AlignmentResultSet.saveRepositoryToFile(null);

				final MatcherConfiguration mc = new MatcherConfiguration(idalignmentjob,
						new MatchingResource(taskReport.getLeftDataset().getAtId(),
								ApiUtil.convertDataService(taskReport.getLeftDataset().getSparqlEndpoint()),
								ApiUtil.getLexicalizationFromString(sourceLexicalizationSet.getLexicalizationModel()),
								sourceLexicalizationSet.getLanguageTag(),
								synonymizer != null ? ApiUtil.convertDataService(synonymizer.getSparqlEndpoint()) : null),
						new MatchingResource(taskReport.getRightDataset().getAtId(),
								ApiUtil.convertDataService(taskReport.getRightDataset().getSparqlEndpoint()),
								ApiUtil.getLexicalizationFromString(targetLexicalizationSet.getLexicalizationModel()),
								targetLexicalizationSet.getLanguageTag(),
								synonymizer != null ? ApiUtil.convertDataService(synonymizer.getSparqlEndpoint()) : null),
						null, null, null, outFilesMap);
				if(alignmentPlan.getMatcherDefinition() != null && alignmentPlan.getMatcherDefinition().getSettings() != null) {
					mc.setGraph(ApiUtil.composeMatchingGraphFromSettings((Map<String,Object>)alignmentPlan.getMatcherDefinition().getSettings()));
				} else {
					mc.setGraph(ApiUtil.composeDefaultEngine(bestPairing.getSynonymizer() != null));
				}
				startMatching(mc);

				return new ResponseEntity<>(
						this.convertGenomaResultToTask(AlignmentResultSet.getResult(idalignmentjob)),
						HttpStatus.CREATED);
			} catch (IllegalArgumentException e) {
				throw e;
			} catch (Exception e) {
				log.error("An exception was thrown when queueing a new task", e);
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		}

		return new ResponseEntity<Task>(HttpStatus.NOT_IMPLEMENTED);
	}

	private void removeFiles(String id, String folder) {
		final List<File> files = (List<File>) FileUtils.listFiles(new File(folder),
				new WildcardFileFilter(id + ".*"), null);
		for (final File file : files) {
			if (!file.delete()) {
				System.err.println("Can't remove " + file.getAbsolutePath());
			}
		}
	}

	private Task convertGenomaResultToTask(Result result) {
		Task task = new Task();
		task.setId(result.getAlignmentId());
		task.setLeftDataset(result.getOntology1());
		task.setRightDataset(result.getOntology2());

		String status = result.getStatus();

		if (status == null || status.isEmpty()) {
			task.setStatus(Task.StatusEnum.SUBMITTED);
		} else if (status.equals("In progress")) {
			task.setStatus(Task.StatusEnum.RUNNING);
			task.setProgress(JsonNullable.of(0));
		} else if (status.endsWith("%")) {
			task.setStatus(Task.StatusEnum.RUNNING);
			task.setProgress(JsonNullable.of(Integer.parseInt(status.substring(0, status.length() - 1))));
		} else if (status.startsWith("Error")) {
			task.setStatus(Task.StatusEnum.FAILED);
			int indexOfColon = status.indexOf(":");
			TaskReason reason = new TaskReason();

			if (indexOfColon == -1) {
				reason.setMessage("Unknown error");
			} else {
				reason.setMessage(status.substring(indexOfColon + 1));
			}
			task.setReason(JsonNullable.of(reason));
		} else if (status.equals("Completed")) {
			task.setStatus(Task.StatusEnum.COMPLETED);
		}

		// TODO handle unmapped statuses

		Date startDate = result.getStartDate();
		if (startDate != null) {
			OffsetDateTime offsetDateTime = convertDateToOffsetDateTime(startDate);
			task.setSubmissionTime(offsetDateTime);
			task.setStartTime(JsonNullable.of(offsetDateTime));
		}

		if (result.getEndDate() != null) {
			task.setEndTime(JsonNullable.of(convertDateToOffsetDateTime(result.getEndDate())));
		}
		return task;
	}

	private OffsetDateTime convertDateToOffsetDateTime(Date date) {
		java.time.OffsetDateTime offsetDateTime = date.toInstant().atOffset(ZoneOffset.UTC);
		return OffsetDateTime.parse(offsetDateTime.toString());
	}

	private void checkTaskSubmission(AlignmentPlan alignmentPlan) throws Exception {
		ScenarioDefinition scenarioDefinition = alignmentPlan.getScenarioDefinition();

		if (scenarioDefinition.getLeftDataset() == null || scenarioDefinition.getRightDataset() == null) {
			throw new Exception("Both datasets have to be not null");
		}
	}


	private Map<FileType, String> prepareOutputFiles(String idalignmentjob, String basePath)
			throws IOException {
		final Map<FileType, String> outFiles = new HashMap<>();
		String filePath = basePath + File.separator + "results" + File.separator + idalignmentjob + ".rdf";
		try (FileOutputStream fileOutputStream = new FileOutputStream(filePath)) {
			outFiles.put(MatcherConfiguration.FileType.RES_FILE, filePath);
		}
		filePath = basePath + File.separator + "logs" + File.separator + idalignmentjob + ".log";
		try (FileOutputStream fileOutputStream = new FileOutputStream(filePath)) {
			outFiles.put(MatcherConfiguration.FileType.LOG_FILE, filePath);
		}
		filePath = basePath + File.separator + "results" + File.separator+ idalignmentjob + ".xml";
		try (FileOutputStream fileOutputStream = new FileOutputStream(filePath)) {
			outFiles.put(MatcherConfiguration.FileType.GRAPH_FILE, filePath);
		}
		/*
		outFiles.put(MatcherConfiguration.FileType.RES_FILE,
				new FileOutputStream(basePath + File.separator + "results" + File.separator + idalignmentjob + ".rdf"));
		outFiles.put(MatcherConfiguration.FileType.LOG_FILE,
				new FileOutputStream(basePath + File.separator + "logs" + File.separator + idalignmentjob + ".log"));
		outFiles.put(MatcherConfiguration.FileType.GRAPH_FILE,
				new FileOutputStream(basePath + File.separator + "results" + File.separator+ idalignmentjob + ".xml"));

		 */
		return outFiles;
	}


	private void startMatching(MatcherConfiguration mc) throws IOException {
		final Composer composer = new Composer(mc);
		taskExecutor.execute(new Runnable() {
			@Override
			public void run() {
				try {
					composer.startMatching(null);
				} catch (final InterruptedException | IOException e) {
					composer.addLog(e.getMessage());
				}
			}
		});
	}


}
