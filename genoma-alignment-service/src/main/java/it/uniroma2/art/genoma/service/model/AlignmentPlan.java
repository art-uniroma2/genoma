package it.uniroma2.art.genoma.service.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Description of a task submission
 */
@ApiModel(description = "Description of a task submission")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-07-21T15:35:09.281+02:00[Europe/Berlin]")
public class AlignmentPlan   {
  @JsonProperty("scenarioDefinition")
  private ScenarioDefinition scenarioDefinition;

  @JsonProperty("settings")
  private Object settings;

  @JsonProperty("matcherDefinition")
  private AlignmentPlanMatcherDefinition matcherDefinition;

  public AlignmentPlan scenarioDefinition(ScenarioDefinition scenarioDefinition) {
    this.scenarioDefinition = scenarioDefinition;
    return this;
  }

  /**
   * Get scenarioDefinition
   * @return scenarioDefinition
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public ScenarioDefinition getScenarioDefinition() {
    return scenarioDefinition;
  }

  public void setScenarioDefinition(ScenarioDefinition scenarioDefinition) {
    this.scenarioDefinition = scenarioDefinition;
  }

  public AlignmentPlan settings(Object settings) {
    this.settings = settings;
    return this;
  }

  /**
   * An actual settings object. Its definition is left under-specified, as it can be essentially any JSON object
   * @return settings
  */
  @ApiModelProperty(value = "An actual settings object. Its definition is left under-specified, as it can be essentially any JSON object")


  public Object getSettings() {
    return settings;
  }

  public void setSettings(Object settings) {
    this.settings = settings;
  }

  public AlignmentPlan matcherDefinition(AlignmentPlanMatcherDefinition matcherDefinition) {
    this.matcherDefinition = matcherDefinition;
    return this;
  }

  /**
   * Get matcherDefinition
   * @return matcherDefinition
  */
  @ApiModelProperty(value = "")

  @Valid

  public AlignmentPlanMatcherDefinition getMatcherDefinition() {
    return matcherDefinition;
  }

  public void setMatcherDefinition(AlignmentPlanMatcherDefinition matcherDefinition) {
    this.matcherDefinition = matcherDefinition;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AlignmentPlan alignmentPlan = (AlignmentPlan) o;
    return Objects.equals(this.scenarioDefinition, alignmentPlan.scenarioDefinition) &&
        Objects.equals(this.settings, alignmentPlan.settings) &&
        Objects.equals(this.matcherDefinition, alignmentPlan.matcherDefinition);
  }

  @Override
  public int hashCode() {
    return Objects.hash(scenarioDefinition, settings, matcherDefinition);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AlignmentPlan {\n");
    
    sb.append("    scenarioDefinition: ").append(toIndentedString(scenarioDefinition)).append("\n");
    sb.append("    settings: ").append(toIndentedString(settings)).append("\n");
    sb.append("    matcherDefinition: ").append(toIndentedString(matcherDefinition)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

