package it.uniroma2.art.genoma.service.exception;

public class BaseException extends Exception{
    String msg;

    public BaseException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
