package it.uniroma2.art.genoma.service.api;

import it.uniroma2.art.genoma.ComposerAggregator.DataService;
import it.uniroma2.art.genoma.ComposerAggregator.Graph;
import it.uniroma2.art.genoma.ComposerAggregator.GraphArch;
import it.uniroma2.art.genoma.ComposerAggregator.MatcherNode;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.service.exception.BaseException;
import it.uniroma2.art.genoma.service.model.Pairing;
import org.springframework.web.context.request.NativeWebRequest;

import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class ApiUtil {
	public static String SYNONYM_EXPANSION = "synonymExpansion";
	public static String CONSYNONYMS_MATCHER_WEIGHT = "cosynonymsMatcherWeight";
	public static String ID_WEIGHT = "idWeight";
	public static String LABELS_WEIGHT = "labelsWeight";
	public static String COMMENTS_WEIGHT = "commentsWeight";
	public static String CF_SIZE = "cfsize";
	public static String STRING_MATCHING = "stringMatching";
	public static String STRING_MATCHING_WEIGHT = "stringMatchingWeight";
	public static String STRING_MATCHING_ALGORITHM = "stringMatchingAlgorithm";
	public static String LINGUISTIC_MATCHING = "linguisticMatching";
	public static String LINGUISTIC_MATCHING_WEIGHT = "linguisticMatchingWeight";
	public static String AGGREGATION_STRATEGY = "aggregationStrategy";
	public static String FILTERING_STRATEGY = "filteringStrategy";
	public static String FILTERING_THRESHOLD = "filteringThreshold";

	public static void setExampleResponse(NativeWebRequest req, String contentType, String example) {
		try {
			HttpServletResponse res = req.getNativeResponse(HttpServletResponse.class);
			res.setCharacterEncoding("UTF-8");
			res.addHeader("Content-Type", contentType);
			res.getWriter().print(example);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static Pairing getBestPairing(List<Pairing> pairings) {
		return pairings.stream().max(Comparator.comparing(Pairing::getScore)).orElse(null);
	}

	public static MatchingResource.Lexicalization getLexicalizationFromString(final String uri) {
		if (uri.equals(MatchingResource.Lexicalization.ONTOLEX.getUri())) {
			return MatchingResource.Lexicalization.ONTOLEX;
		} else if (uri.equals(MatchingResource.Lexicalization.SKOS.getUri())) {
			return MatchingResource.Lexicalization.SKOS;
		} else if (uri.equals(MatchingResource.Lexicalization.SKOS_XL.getUri())) {
			return MatchingResource.Lexicalization.SKOS_XL;
		} else {
			return MatchingResource.Lexicalization.RDFS;
		}
	}

	public static Graph composeMatchingGraphFromSettings(Map<String, Object> settings) throws BaseException {
		Graph engine = new Graph();
		// check not nullable fields
		if (settings.get(ApiUtil.CF_SIZE) != null) {
			MatcherNode sme = new MatcherNode(0, "SimilarityMatrixExtractor", 1.0,
					"-align=none " + "-cfsize=" + settings.get(ApiUtil.CF_SIZE));
			engine.addNode(sme);
		} else {
			throw new BaseException("CF size cannot be null");
		}
		String weights = null;
		if (settings.get(ApiUtil.ID_WEIGHT) != null && settings.get(ApiUtil.LABELS_WEIGHT) != null) {
			weights = "-iw=" + settings.get(ApiUtil.ID_WEIGHT) + " -lw="
					+ settings.get(ApiUtil.LABELS_WEIGHT);
			String commentsWeight = settings.get(ApiUtil.COMMENTS_WEIGHT) != null
					? String.valueOf((double) settings.get(ApiUtil.COMMENTS_WEIGHT))
					: "0.0";
			weights += " -cw=" + commentsWeight;
		} else {
			throw new BaseException("Id weight and labels weight cannot be null");
		}

		if (settings.get(ApiUtil.STRING_MATCHING) != null
				&& (boolean) settings.get(ApiUtil.STRING_MATCHING)) {
			String stringMatcherOptions = weights + " "
					+ isMatcherDisabled((Boolean) settings.get(ApiUtil.STRING_MATCHING));
			if (settings.get(ApiUtil.STRING_MATCHING_ALGORITHM) != null
					&& settings.get(ApiUtil.STRING_MATCHING_WEIGHT) != null) {
				MatcherNode stringMatcher = new MatcherNode(1,
						(String) settings.get(ApiUtil.STRING_MATCHING_ALGORITHM),
						Double.parseDouble("" + settings.get(ApiUtil.STRING_MATCHING_WEIGHT)),
						stringMatcherOptions);
				engine.addNode(stringMatcher);
			} else {
				throw new BaseException(
						"String Matching Algorithm and String Matching weight have to be specified if string matching is enabled");
			}
		} else {
			String stringMatcherOptions = weights + " -disabled";
			MatcherNode stringMatcher = new MatcherNode(1, "ISubMatcher", 1.0, stringMatcherOptions);
			engine.addNode(stringMatcher);
		}
		if (settings.get(ApiUtil.LINGUISTIC_MATCHING) != null
				&& (boolean) settings.get(ApiUtil.LINGUISTIC_MATCHING)) {
			String linguisticMatcherOptions = weights + " "
					+ isMatcherDisabled((Boolean) settings.get(ApiUtil.LINGUISTIC_MATCHING));
			if (settings.get(ApiUtil.LINGUISTIC_MATCHING_WEIGHT) != null) {
				MatcherNode linguisticMatcher = new MatcherNode(2, "VirtualDocMatcher",
						Double.parseDouble("" + settings.get(ApiUtil.LINGUISTIC_MATCHING_WEIGHT)),
						linguisticMatcherOptions);
				engine.addNode(linguisticMatcher);
			} else {
				throw new BaseException(
						"Linguistic Matching Weight has to be specified if linguistic matching is enabled");
			}
		} else {
			String linguisticMatcherOptions = weights + " -disabled";
			MatcherNode linguisticMatcher = new MatcherNode(2, "VirtualDocMatcher", 1.0,
					linguisticMatcherOptions);
			engine.addNode(linguisticMatcher);
		}
		if (settings.get(ApiUtil.SYNONYM_EXPANSION) != null
				&& (boolean) settings.get(ApiUtil.SYNONYM_EXPANSION)) {
			String cosynonymsMatcherOptions = weights + " "
					+ isMatcherDisabled((Boolean) settings.get(ApiUtil.SYNONYM_EXPANSION));
			if (settings.get(ApiUtil.CONSYNONYMS_MATCHER_WEIGHT) != null) {
				MatcherNode cosynonymsMatcher = new MatcherNode(3, "CosynonymsMatcher",
						Double.parseDouble("" + settings.get(ApiUtil.CONSYNONYMS_MATCHER_WEIGHT)),
						cosynonymsMatcherOptions);
				engine.addNode(cosynonymsMatcher);
			} else {
				throw new BaseException("Cosynomys weight cannot be null if synonymizer is enabled");
			}
		} else {
			String cosynonymsMatcherOptions = weights + " -disabled";
			MatcherNode cosynonymsMatcher = new MatcherNode(3, "CosynonymsMatcher", 1.0,
					cosynonymsMatcherOptions);
			engine.addNode(cosynonymsMatcher);
		}
		if (settings.get(ApiUtil.AGGREGATION_STRATEGY) != null) {
			MatcherNode junctionPoint = new MatcherNode(4, "JunctionPoint", 1.0,
					"-ms=" + settings.get(ApiUtil.AGGREGATION_STRATEGY));
			engine.addNode(junctionPoint);
			MatcherNode junctionPoint2 = new MatcherNode(5, "JunctionPoint", 1.0,
					"-ms=" + settings.get(ApiUtil.AGGREGATION_STRATEGY));
			engine.addNode(junctionPoint2);
		} else {
			throw new BaseException("Aggregation Strategy cannot be null");
		}
		if (settings.get(ApiUtil.FILTERING_STRATEGY) != null
				&& settings.get(ApiUtil.FILTERING_THRESHOLD) != null) {
			String filteringOptions = "-filter=" + settings.get(ApiUtil.FILTERING_STRATEGY) + "-th="
					+ (double) settings.get(ApiUtil.FILTERING_THRESHOLD);
			MatcherNode alignmentExtractor = new MatcherNode(6, "AlignmentExtractor", 1.0, filteringOptions);
			engine.addNode(alignmentExtractor);
		} else {
			throw new BaseException("Filtering strategy and threshold cannot be null");
		}

		engine.addArch(new GraphArch(0, 1));
		engine.addArch(new GraphArch(0, 2));
		engine.addArch(new GraphArch(0, 3));
		engine.addArch(new GraphArch(1, 4));
		engine.addArch(new GraphArch(2, 4));
		engine.addArch(new GraphArch(4, 5));
		engine.addArch(new GraphArch(3, 5));
		engine.addArch(new GraphArch(5, 6));
		return engine;
	}

	public static Graph composeDefaultEngine(boolean withSynonimizer) {
		Graph engine = new Graph();
		MatcherNode sme = new MatcherNode(0, "SimilarityMatrixExtractor", 1.0, "-align=none " + "-cfsize=5");
		engine.addNode(sme);
		String stringMatcherOptions = "-iw=0.0 -lw=1.0 -cw=1.0";
		MatcherNode stringMatcher = new MatcherNode(1, "ISubMatcher", 1.0, stringMatcherOptions);
		engine.addNode(stringMatcher);
		if (withSynonimizer) {
			String cosynonymsMatcherOptions = "-iw=1.0 -lw=1.0 -cw=1.0";
			MatcherNode cosynonymsMatcher = new MatcherNode(2, "CosynonymsMatcher", 1.0,
					cosynonymsMatcherOptions);
			engine.addNode(cosynonymsMatcher);
			MatcherNode junctionPoint = new MatcherNode(3, "JunctionPoint", 1.0, "-ms=TriangularNormMax");
			engine.addNode(junctionPoint);
			String filteringOptions = "-filter=HardThreshold -th=0.6";
			MatcherNode alignmentExtractor = new MatcherNode(4, "AlignmentExtractor", 1.0, filteringOptions);
			engine.addNode(alignmentExtractor);
			engine.addArch(new GraphArch(0, 1));
			engine.addArch(new GraphArch(0, 2));
			engine.addArch(new GraphArch(1, 3));
			engine.addArch(new GraphArch(3, 4));
		} else {
			String filteringOptions = "-filter=HardThreshold -th=0.6";
			MatcherNode alignmentExtractor = new MatcherNode(2, "AlignmentExtractor", 1.0, filteringOptions);
			engine.addNode(alignmentExtractor);
			engine.addArch(new GraphArch(0, 1));
			engine.addArch(new GraphArch(1, 2));
		}
		return engine;
	}

	private static String isMatcherDisabled(Boolean field) {
		if (field != null && !field)
			return "-disabled";
		else
			return "";
	}

	public static DataService convertDataService(
			it.uniroma2.art.genoma.service.model.DataService sparqlEndpoint) {
		return new DataService(sparqlEndpoint.getEndpointURL(), sparqlEndpoint.getUsername(),
				sparqlEndpoint.getPassword());
	}
}
