package it.uniroma2.art.genoma.service.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;

/**
 * AlignmentAllOf
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-07-21T15:35:09.281+02:00[Europe/Berlin]")
public class AlignmentAllOf   {
  @JsonProperty("subjectsTarget")
  private String subjectsTarget;

  @JsonProperty("objectsTarget")
  private String objectsTarget;

  public AlignmentAllOf subjectsTarget(String subjectsTarget) {
    this.subjectsTarget = subjectsTarget;
    return this;
  }

  /**
   * Get subjectsTarget
   * @return subjectsTarget
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getSubjectsTarget() {
    return subjectsTarget;
  }

  public void setSubjectsTarget(String subjectsTarget) {
    this.subjectsTarget = subjectsTarget;
  }

  public AlignmentAllOf objectsTarget(String objectsTarget) {
    this.objectsTarget = objectsTarget;
    return this;
  }

  /**
   * Get objectsTarget
   * @return objectsTarget
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getObjectsTarget() {
    return objectsTarget;
  }

  public void setObjectsTarget(String objectsTarget) {
    this.objectsTarget = objectsTarget;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AlignmentAllOf alignmentAllOf = (AlignmentAllOf) o;
    return Objects.equals(this.subjectsTarget, alignmentAllOf.subjectsTarget) &&
        Objects.equals(this.objectsTarget, alignmentAllOf.objectsTarget);
  }

  @Override
  public int hashCode() {
    return Objects.hash(subjectsTarget, objectsTarget);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AlignmentAllOf {\n");
    
    sb.append("    subjectsTarget: ").append(toIndentedString(subjectsTarget)).append("\n");
    sb.append("    objectsTarget: ").append(toIndentedString(objectsTarget)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

