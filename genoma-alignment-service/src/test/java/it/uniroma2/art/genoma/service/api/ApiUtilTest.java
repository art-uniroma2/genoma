package it.uniroma2.art.genoma.service.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.uniroma2.art.genoma.ComposerAggregator.Composer;
import it.uniroma2.art.genoma.ComposerAggregator.DataService;
import it.uniroma2.art.genoma.ComposerAggregator.Graph;
import it.uniroma2.art.genoma.ComposerAggregator.MatcherConfiguration;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.DataManagement.AlignmentResultSet;
import it.uniroma2.art.genoma.DataManagement.Result;
import it.uniroma2.art.genoma.service.model.AlignmentPlan;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ApiUtilTest {

    private static final String ONTO1 = "101.rdf";
    private static final String ONTO2 = "103.rdf";
    private static final String ALREF = "refalign103.rdf";

    @Test
    public void composeMatchingGraphFromSettings() throws Exception {
        Map<String, Object> settings = new HashMap<>();
        settings.put(ApiUtil.CF_SIZE, 10);
        settings.put(ApiUtil.ID_WEIGHT, 0.5);
        settings.put(ApiUtil.LABELS_WEIGHT, 1.0);
        settings.put(ApiUtil.COMMENTS_WEIGHT, null);
        settings.put(ApiUtil.STRING_MATCHING, true);
        settings.put(ApiUtil.STRING_MATCHING_ALGORITHM, "ISubMatcher");
        settings.put(ApiUtil.STRING_MATCHING_WEIGHT, 1.0);
        settings.put(ApiUtil.CONSYNONYMS_MATCHER_WEIGHT, null);
        settings.put(ApiUtil.SYNONYM_EXPANSION, false);
        settings.put(ApiUtil.LINGUISTIC_MATCHING, true);
        settings.put(ApiUtil.LINGUISTIC_MATCHING_WEIGHT, 1);
        settings.put(ApiUtil.AGGREGATION_STRATEGY, "TriangularNormMin");
        settings.put(ApiUtil.FILTERING_STRATEGY, "HardThreshold");
        settings.put(ApiUtil.FILTERING_THRESHOLD, 0.96);
        Graph engine = ApiUtil.composeMatchingGraphFromSettings(settings);
        assertEquals("[0 SimilarityMatrixExtractor, 1 ISubMatcher, 2 VirtualDocMatcher, 3 CosynonymsMatcher, 4 JunctionPoint, 5 JunctionPoint, 6 AlignmentExtractor]",engine.getNodes().toString());
        assertEquals("-align=none -cfsize=10", engine.getNodes().get(0).getOptions());
        assertEquals("-iw=0.5 -lw=1.0 -cw=0.0 ", engine.getNodes().get(1).getOptions());
        assertEquals("-iw=0.5 -lw=1.0 -cw=0.0 ", engine.getNodes().get(2).getOptions());
        assertEquals("-iw=0.5 -lw=1.0 -cw=0.0 -disabled", engine.getNodes().get(3).getOptions());
        assertEquals("-ms=TriangularNormMin", engine.getNodes().get(4).getOptions());
        assertEquals("-ms=TriangularNormMin", engine.getNodes().get(5).getOptions());
        assertEquals("-filter=HardThreshold-th=0.96", engine.getNodes().get(6).getOptions());
        assertEquals(engine.getNodes().size(),7);
        assertEquals("0-1",engine.getArches().get(0).getIdNodeFrom() + "-" +engine.getArches().get(0).getidNodeTo());
        assertEquals("0-2",engine.getArches().get(1).getIdNodeFrom() + "-" +engine.getArches().get(1).getidNodeTo());
        assertEquals("0-3",engine.getArches().get(2).getIdNodeFrom() + "-" +engine.getArches().get(2).getidNodeTo());
        assertEquals("1-4",engine.getArches().get(3).getIdNodeFrom() + "-" +engine.getArches().get(3).getidNodeTo());
        assertEquals("2-4",engine.getArches().get(4).getIdNodeFrom() + "-" +engine.getArches().get(4).getidNodeTo());
        assertEquals("4-5",engine.getArches().get(5).getIdNodeFrom() + "-" +engine.getArches().get(5).getidNodeTo());
        assertEquals("3-5",engine.getArches().get(6).getIdNodeFrom() + "-" +engine.getArches().get(6).getidNodeTo());
        assertEquals("5-6",engine.getArches().get(7).getIdNodeFrom() + "-" +engine.getArches().get(7).getidNodeTo());
        assertEquals(8, engine.getArches().size());
    }

    @Test
    public void composeDefaultEngine() throws Exception {

        Graph engine = ApiUtil.composeDefaultEngine(true);
        assertEquals("[0 SimilarityMatrixExtractor, 1 ISubMatcher, 2 CosynonymsMatcher, 3 JunctionPoint, 4 AlignmentExtractor]",engine.getNodes().toString());
        assertEquals("-align=none -cfsize=5", engine.getNodes().get(0).getOptions());
        assertEquals("-iw=0.0 -lw=1.0 -cw=1.0", engine.getNodes().get(1).getOptions());
        assertEquals("-iw=1.0 -lw=1.0 -cw=1.0", engine.getNodes().get(2).getOptions());
        assertEquals("-ms=TriangularNormMax", engine.getNodes().get(3).getOptions());
        assertEquals("-filter=HardThreshold -th=0.6", engine.getNodes().get(4).getOptions());
        assertEquals(engine.getNodes().size(),5);
        assertEquals("0-1",engine.getArches().get(0).getIdNodeFrom() + "-" +engine.getArches().get(0).getidNodeTo());
        assertEquals("0-2",engine.getArches().get(1).getIdNodeFrom() + "-" +engine.getArches().get(1).getidNodeTo());
        assertEquals("1-3",engine.getArches().get(2).getIdNodeFrom() + "-" +engine.getArches().get(2).getidNodeTo());
        assertEquals("3-4",engine.getArches().get(3).getIdNodeFrom() + "-" +engine.getArches().get(3).getidNodeTo());
        assertEquals(4, engine.getArches().size());

        engine = ApiUtil.composeDefaultEngine(false);
        assertEquals("[0 SimilarityMatrixExtractor, 1 ISubMatcher, 2 AlignmentExtractor]",engine.getNodes().toString());
        assertEquals("-align=none -cfsize=5", engine.getNodes().get(0).getOptions());
        assertEquals("-iw=0.0 -lw=1.0 -cw=1.0", engine.getNodes().get(1).getOptions());
        assertEquals("-filter=HardThreshold -th=0.6", engine.getNodes().get(2).getOptions());
        assertEquals(engine.getNodes().size(),3);
        assertEquals("0-1",engine.getArches().get(0).getIdNodeFrom() + "-" +engine.getArches().get(0).getidNodeTo());
        assertEquals("1-2",engine.getArches().get(1).getIdNodeFrom() + "-" +engine.getArches().get(1).getidNodeTo());
        assertEquals(2, engine.getArches().size());
    }

    @Test
    public void composeMatchingGraphFromSettingsUsingJSON() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        AlignmentPlan alignmentPlan;
        try (InputStream is = this.getClass().getResourceAsStream("/alignment-plan-with-matcher-def.json")) {
            alignmentPlan = objectMapper.readValue(is, AlignmentPlan.class);
        }
        Graph engine = ApiUtil.composeMatchingGraphFromSettings((Map<String, Object>) alignmentPlan.getMatcherDefinition().getSettings());
        assertEquals("[0 SimilarityMatrixExtractor, 1 ISubMatcher, 2 VirtualDocMatcher, 3 CosynonymsMatcher, 4 JunctionPoint, 5 JunctionPoint, 6 AlignmentExtractor]",engine.getNodes().toString());
        assertEquals("-align=none -cfsize=5", engine.getNodes().get(0).getOptions());
        assertEquals("-iw=1 -lw=1 -cw=0.0 ", engine.getNodes().get(1).getOptions());
        assertEquals("-iw=1 -lw=1 -cw=0.0 -disabled", engine.getNodes().get(2).getOptions());
        assertEquals("-iw=1 -lw=1 -cw=0.0 -disabled", engine.getNodes().get(3).getOptions());
        assertEquals("-ms=TriangularNormMin", engine.getNodes().get(4).getOptions());
        assertEquals("-ms=TriangularNormMin", engine.getNodes().get(5).getOptions());
        assertEquals("-filter=HardThreshold-th=0.9", engine.getNodes().get(6).getOptions());
        assertEquals(engine.getNodes().size(),7);
        assertEquals("0-1",engine.getArches().get(0).getIdNodeFrom() + "-" +engine.getArches().get(0).getidNodeTo());
        assertEquals("0-2",engine.getArches().get(1).getIdNodeFrom() + "-" +engine.getArches().get(1).getidNodeTo());
        assertEquals("0-3",engine.getArches().get(2).getIdNodeFrom() + "-" +engine.getArches().get(2).getidNodeTo());
        assertEquals("1-4",engine.getArches().get(3).getIdNodeFrom() + "-" +engine.getArches().get(3).getidNodeTo());
        assertEquals("2-4",engine.getArches().get(4).getIdNodeFrom() + "-" +engine.getArches().get(4).getidNodeTo());
        assertEquals("4-5",engine.getArches().get(5).getIdNodeFrom() + "-" +engine.getArches().get(5).getidNodeTo());
        assertEquals("3-5",engine.getArches().get(6).getIdNodeFrom() + "-" +engine.getArches().get(6).getidNodeTo());
        assertEquals("5-6",engine.getArches().get(7).getIdNodeFrom() + "-" +engine.getArches().get(7).getidNodeTo());
        assertEquals(8, engine.getArches().size());
        String log = executeTest(engine, ONTO1, ONTO2,null, null,
                MatchingResource.Lexicalization.RDFS, MatchingResource.Lexicalization.RDFS, ALREF, "en");
    }

    @Test
    public void testMatchingExecutionWithCustomSettings() throws Exception {
        Map<String, Object> settings = new HashMap<>();
        settings.put(ApiUtil.CF_SIZE, 5);
        settings.put(ApiUtil.ID_WEIGHT, 1.0);
        settings.put(ApiUtil.LABELS_WEIGHT, 0.8);
        settings.put(ApiUtil.COMMENTS_WEIGHT, 0.6);
        settings.put(ApiUtil.STRING_MATCHING, true);
        settings.put(ApiUtil.STRING_MATCHING_ALGORITHM, "ISubMatcher");
        settings.put(ApiUtil.STRING_MATCHING_WEIGHT, 1.0);
        settings.put(ApiUtil.CONSYNONYMS_MATCHER_WEIGHT, null);
        settings.put(ApiUtil.SYNONYM_EXPANSION, false);
        settings.put(ApiUtil.LINGUISTIC_MATCHING, true);
        settings.put(ApiUtil.LINGUISTIC_MATCHING_WEIGHT, 1.0);
        settings.put(ApiUtil.AGGREGATION_STRATEGY, "TriangularNormMin");
        settings.put(ApiUtil.FILTERING_STRATEGY, "HardThreshold");
        settings.put(ApiUtil.FILTERING_THRESHOLD, 0.96);
        Graph engine = ApiUtil.composeMatchingGraphFromSettings(settings);
        final String expectedLog = "[2020-06-10 05:42:13.452]: http://oaei.ontologymatching.org/2011/benchmarks/101/onto.rdf#book vs http://oaei.ontologymatching.org/2011/benchmarks/103/onto.rdf#book rel type:= grade:1.0\n" +
                "[2020-06-10 05:42:13.452]: http://oaei.ontologymatching.org/2011/benchmarks/101/onto.rdf#humanCreator vs http://oaei.ontologymatching.org/2011/benchmarks/103/onto.rdf#humanCreator rel type:= grade:1.0\n" +
                "[2020-06-10 05:42:13.623]: Found:96\n" +
                "[2020-06-10 05:42:13.623]: Existing:97\n" +
                "[2020-06-10 05:42:13.623]: Correct:96\n" +
                "[2020-06-10 05:42:13.623]: Precision:1.0\n" +
                "[2020-06-10 05:42:13.623]: Recall:0.9896907216494846\n" +
                "[2020-06-10 05:42:13.624]: F-Measure:0.9948186528497409";
        String log = executeTest(engine, ONTO1, ONTO2,null, null,
                MatchingResource.Lexicalization.RDFS, MatchingResource.Lexicalization.RDFS, ALREF, "en");
        assertEquals(expectedLog.split("Found:")[1].split("\n")[0], log.toString().split("Found:")[1].split("\n")[0]);
        assertEquals(expectedLog.split("Correct:")[1].split("\n")[0],
                log.split("Correct:")[1].split("\n")[0]);
        assertEquals(expectedLog.split("Precision:")[1].split("\n")[0],
                log.split("Precision:")[1].split("\n")[0]);
        assertEquals(expectedLog.split("Recall:")[1].split("\n")[0], log.toString().split("Recall:")[1].split("\n")[0]);
    }

    @Test
    public void testMatchingExecutionWithStringHammingDistance() throws Exception {
        Map<String, Object> settings = new HashMap<>();
        settings.put(ApiUtil.CF_SIZE, 5);
        settings.put(ApiUtil.ID_WEIGHT, 1.0);
        settings.put(ApiUtil.LABELS_WEIGHT, 0.8);
        settings.put(ApiUtil.COMMENTS_WEIGHT, 0.6);
        settings.put(ApiUtil.STRING_MATCHING, true);
        settings.put(ApiUtil.STRING_MATCHING_ALGORITHM, "StringHammingDistance");
        settings.put(ApiUtil.STRING_MATCHING_WEIGHT, 1.0);
        settings.put(ApiUtil.CONSYNONYMS_MATCHER_WEIGHT, null);
        settings.put(ApiUtil.SYNONYM_EXPANSION, false);
        settings.put(ApiUtil.LINGUISTIC_MATCHING, true);
        settings.put(ApiUtil.LINGUISTIC_MATCHING_WEIGHT, 1.0);
        settings.put(ApiUtil.AGGREGATION_STRATEGY, "TriangularNormMin");
        settings.put(ApiUtil.FILTERING_STRATEGY, "HardThreshold");
        settings.put(ApiUtil.FILTERING_THRESHOLD, 0.96);
        Graph engine = ApiUtil.composeMatchingGraphFromSettings(settings);
        final String expectedLog = "[2020-06-10 05:42:13.452]: http://oaei.ontologymatching.org/2011/benchmarks/101/onto.rdf#book vs http://oaei.ontologymatching.org/2011/benchmarks/103/onto.rdf#book rel type:= grade:1.0\n" +
                "[2020-06-10 05:42:13.452]: http://oaei.ontologymatching.org/2011/benchmarks/101/onto.rdf#humanCreator vs http://oaei.ontologymatching.org/2011/benchmarks/103/onto.rdf#humanCreator rel type:= grade:1.0\n" +
                "[2020-06-10 05:42:13.623]: Found:96\n" +
                "[2020-06-10 05:42:13.623]: Existing:97\n" +
                "[2020-06-10 05:42:13.623]: Correct:96\n" +
                "[2020-06-10 05:42:13.623]: Precision:1.0\n" +
                "[2020-06-10 05:42:13.623]: Recall:0.9896907216494846\n" +
                "[2020-06-10 05:42:13.624]: F-Measure:0.9948186528497409";

        String log = executeTest(engine, ONTO1, ONTO2,null, null,
                MatchingResource.Lexicalization.RDFS, MatchingResource.Lexicalization.RDFS, ALREF, "en");
        assertEquals(expectedLog.split("Found:")[1].split("\n")[0], log.toString().split("Found:")[1].split("\n")[0]);
        assertEquals(expectedLog.split("Correct:")[1].split("\n")[0],
                log.split("Correct:")[1].split("\n")[0]);
        assertEquals(expectedLog.split("Precision:")[1].split("\n")[0],
                log.split("Precision:")[1].split("\n")[0]);
        assertEquals(expectedLog.split("Recall:")[1].split("\n")[0], log.toString().split("Recall:")[1].split("\n")[0]);
    }

    @Test
    public void testMatchingExecutionWithDefaultSettings() throws Exception {
        Graph engine = ApiUtil.composeDefaultEngine(false);
        final String expectedLog = "[2020-06-30 08:26:41.431]: http://oaei.ontologymatching.org/2011/benchmarks/101/onto.rdf#humanCreator vs http://oaei.ontologymatching.org/2011/benchmarks/103/onto.rdf#humanCreator rel type:= grade:1.0\n" +
                "[2020-06-30 08:26:41.431]: http://oaei.ontologymatching.org/2011/benchmarks/101/onto.rdf#humanCreator vs http://oaei.ontologymatching.org/2011/benchmarks/103/onto.rdf#author rel type:= grade:0.6243320915380521\n" +
                "[2020-06-30 08:26:41.605]: Found:148\n" +
                "[2020-06-30 08:26:41.605]: Existing:97\n" +
                "[2020-06-30 08:26:41.605]: Correct:96\n" +
                "[2020-06-30 08:26:41.605]: Precision:0.6486486486486487\n" +
                "[2020-06-30 08:26:41.605]: Recall:0.9896907216494846\n" +
                "[2020-06-30 08:26:41.605]: F-Measure:0.7836734693877551\n" +
                "Total Time:0:0:10.85\n";
        String log = executeTest(engine, ONTO1, ONTO2,null, null,
                MatchingResource.Lexicalization.RDFS, MatchingResource.Lexicalization.RDFS, ALREF, "en");
        System.out.println(log);
        assertEquals(expectedLog.split("Found:")[1].split("\n")[0], log.toString().split("Found:")[1].split("\n")[0]);
        assertEquals(expectedLog.split("Correct:")[1].split("\n")[0],
                log.split("Correct:")[1].split("\n")[0]);
        assertEquals(expectedLog.split("Precision:")[1].split("\n")[0],
                log.split("Precision:")[1].split("\n")[0]);
        assertEquals(expectedLog.split("Recall:")[1].split("\n")[0], log.toString().split("Recall:")[1].split("\n")[0]);
    }

    private String executeTest(Graph engine, String onto1, String onto2, String sprqlEndpoint1, String sprqlEndpoint2,
                               MatchingResource.Lexicalization lexOnto1, MatchingResource.Lexicalization lexOnto2,
                               String alrefName, String language) throws IOException, InterruptedException {
        final File resourcesDirectory = new File("src/test/resources");
        final String basePath = resourcesDirectory.getAbsolutePath();
        final String o1 = basePath + "/" + onto1;
        final String o2 = basePath + "/" + onto2;
        final String alref = basePath + "/" + alrefName;
        final Map<MatcherConfiguration.FileType, OutputStream> outFiles = new HashMap<>();
        try (ByteArrayOutputStream baosRes = new ByteArrayOutputStream();
                ByteArrayOutputStream baosLog = new ByteArrayOutputStream();
                ByteArrayOutputStream baosGraph = new ByteArrayOutputStream()) {
            outFiles.put(MatcherConfiguration.FileType.RES_FILE, baosRes);
            outFiles.put(MatcherConfiguration.FileType.LOG_FILE, baosLog);
            outFiles.put(MatcherConfiguration.FileType.GRAPH_FILE, baosGraph);
            final Result res = new Result(o1, o2, "file:///" + engine, "file:///" + basePath);
            final String idalignmentjob = res.getAlignmentId();
            try (final ByteArrayOutputStream repository = new ByteArrayOutputStream()) {
                //AlignmentResultSet.init(repository);
                AlignmentResultSet.addResult(idalignmentjob, res);
                AlignmentResultSet.saveRepositoryToFile(repository);
                final MatcherConfiguration mc = new MatcherConfiguration(idalignmentjob,
                        new MatchingResource("file:///" + o1, new DataService(sprqlEndpoint1), lexOnto1, language, null),
                        new MatchingResource("file:///" + o2, new DataService(sprqlEndpoint2), lexOnto2, language, null),
                        null, alref, outFiles, null);
                mc.setGraph(engine);
                final Composer composer = new Composer(mc);

                composer.startMatching(repository);
                final ByteArrayOutputStream os = (ByteArrayOutputStream) outFiles.get(MatcherConfiguration.FileType.LOG_FILE);
                return os.toString();
            }
        }
    }
}