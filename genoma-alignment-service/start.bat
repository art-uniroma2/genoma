@echo off

if JAVA_HOME == "" (
  set JAVA=java
) else (
  set JAVA=%JAVA_HOME%/bin/java
)

setlocal enabledelayedexpansion

set JAVA_VERSION=0
for /f "tokens=3" %%g in ('"%JAVA%" -version 2^>^&1 ^| findstr /i version') do (
    set JAVA_VERSION=%%g
)
set JAVA_VERSION=%JAVA_VERSION:"=%

setlocal disabledelayedexpansion

if "%JAVA_VERSION%" GTR "1.8" (
  set JAVA_OPTS=%JAVA_OPTS% --add-opens java.base/java.lang=ALL-UNNAMED --add-opens java.base/java.util=ALL-UNNAMED --add-opens java.base/java.lang.reflect=ALL-UNNAMED --add-opens java.base/java.text=ALL-UNNAMED  --add-opens java.desktop/java.awt.font=ALL-UNNAMED
)

"%JAVA%" %JAVA_OPTS% -Dserver.port=7575 -jar genoma-alignment-service-2.3.5-SNAPSHOT.jar