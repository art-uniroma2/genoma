package it.uniroma2.art.genoma.ComposerAggregator;

import groovy.lang.GroovyShell;
import groovy.lang.Script;
import it.uniroma2.art.genoma.ComposerAggregator.MatcherConfiguration.FileType;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource.Lexicalization;
import it.uniroma2.art.genoma.CustomExceptions.RMIServerNotAvailableException;
import it.uniroma2.art.genoma.DataManagement.AlignmentResultSet;
import it.uniroma2.art.genoma.DataManagement.Result;
import org.codehaus.groovy.control.CompilationFailedException;
import org.junit.Ignore;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ComposerTest {
	private static final String EASY_ENGINE = "EngineEasy.xml";
	private static final String EASY_ENGINE_FOR_SKOS = "EngineEasyForSKOS.xml";
	private static final String ONTOCHORD_ENGINE = "OntoChord.xml";
	private static final String COSYNONYMS_ENGINE = "CosynonymsEngine.xml";
	private static final String FALCON_AO = "FalconAO.xml";
	private static final String CUSTOM_MATCHER_ENGINE = "EngineWithCustomMatcher.xml";
	private static final String ONTO1 = "101.rdf";
	private static final String ONTO2 = "103.rdf";
	private static final String ONTO3 = "304.rdf";
	private static final String ONTO1_SKOSXL = "eurovoc-euvoc-rich.rdf";
	private static final String ONTO2_SKOSXL = "teseo-skos-xl-redux.rdf";
	private static final String ONTO3_SKOSXL = "EurovocCUT-export.n3";
	private static final String ONTO4_SKOSXL = "FISMA_corporate_taxonomy.rdf";
	private static final String ONTO1_SKOS = "ilo glossary.rdf";
	private static final String ONTO2_SKOS = "ilo thesaurus.rdf";
	private static final String ALREF = "refalign103.rdf";
	private static final String ALREF2 = "refalign304.rdf";

	@Test
	public void testEasyEngine() throws IOException, InterruptedException {
		final String expectedLog = "[2018-03-13 17:47:43.073]: http://oaei.ontologymatching.org/2011/benchmarks/101/onto.rdf#school vs http://oaei.ontologymatching.org/2011/benchmarks/103/onto.rdf#school rel type:= grade:2.0\n"
				+ "[2018-03-13 17:47:43.074]: http://www.w3.org/1999/02/22-rdf-syntax-ns#rest vs http://oaei.ontologymatching.org/2011/benchmarks/103/onto.rdf#address rel type:= grade:1.3815201192250373\n"
				+ "[2018-03-13 17:47:43.139]: Found:112\n" + "[2018-03-13 17:47:43.139]: Existing:97\n"
				+ "[2018-03-13 17:47:43.139]: Correct:96\n"
				+ "[2018-03-13 17:47:43.139]: Precision:0.8571428571428571\n"
				+ "[2018-03-13 17:47:43.139]: Recall:0.9896907216494846\n"
				+ "[2018-03-13 17:47:43.139]: F-Measure:0.49113924050632907\n"
				+ "[2018-03-13 17:47:44.042]: *****************************************************************\n"
				+ "[2018-03-13 17:47:44.042]: Graph Extractor\n"
				+ "[2018-03-13 17:47:44.042]: *****************************************************************\n"
				+ "Total Time:0:0:5.767";
		String log = executeTest(EASY_ENGINE, ONTO1, ONTO2,null, null,
				Lexicalization.RDFS, Lexicalization.RDFS, ALREF, "en");
		assertEquals(expectedLog.split("Found:")[1].split("\n")[0], log.split("Found:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Correct:")[1].split("\n")[0],
				log.split("Correct:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Precision:")[1].split("\n")[0],
				log.split("Precision:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Recall:")[1].split("\n")[0], log.split("Recall:")[1].split("\n")[0]);
	}


	@Test
	public void testEasyEngineSKOS() throws IOException, RMIServerNotAvailableException, InterruptedException {
		String log = executeTest(EASY_ENGINE_FOR_SKOS, ONTO1_SKOS, ONTO2_SKOS,null, null,
				Lexicalization.SKOS, Lexicalization.SKOS, null, "en");
		assert log.contains("http://publications.europa.eu/ilo/gloss#00011 vs http://publications.europa.eu/ilo/taxa#13.03.2.ghn rel type:= grade:1.0");
	}

	@Ignore("Ignored because it requires too much time to be completed")
	@Test
	public void testFalconAOSKOS() throws IOException, InterruptedException {
		final String expectedLog = "[2018-03-13 17:47:43.073]: http://oaei.ontologymatching.org/2011/benchmarks/101/onto.rdf#school vs http://oaei.ontologymatching.org/2011/benchmarks/103/onto.rdf#school rel type:= grade:2.0\n"
				+ "[2018-03-13 17:47:43.074]: http://www.w3.org/1999/02/22-rdf-syntax-ns#rest vs http://oaei.ontologymatching.org/2011/benchmarks/103/onto.rdf#address rel type:= grade:1.3815201192250373\n"
				+ "[2018-03-13 17:47:43.139]: Found:298\n" + "[2018-03-13 17:47:43.139]: Existing:97\n"
				+ "[2018-03-13 17:47:43.139]: Correct:97\n"
				+ "[2018-03-13 17:47:43.139]: Precision:0.32550335570469796\n"
				+ "[2018-03-13 17:47:43.139]: Recall:1.0\n"
				+ "[2018-03-13 17:47:43.139]: F-Measure:0.49113924050632907\n"
				+ "[2018-03-13 17:47:44.042]: *****************************************************************\n"
				+ "[2018-03-13 17:47:44.042]: Graph Extractor\n"
				+ "[2018-03-13 17:47:44.042]: *****************************************************************\n"
				+ "Total Time:0:0:5.767";
		final File resourcesDirectory = new File("src/test/resources");
		final String basePath = resourcesDirectory.getAbsolutePath();
		final String engine = basePath + "/" + FALCON_AO;
		final String o1 = basePath + "/" + ONTO1_SKOS;
		final String o2 = basePath + "/" + ONTO2_SKOS;
		final String alref = null;
		final Map<FileType, OutputStream> outFiles = new HashMap<>();
		try (ByteArrayOutputStream baosRes = new ByteArrayOutputStream();
			 ByteArrayOutputStream baosLog = new ByteArrayOutputStream();
			 ByteArrayOutputStream baosGraph = new ByteArrayOutputStream()) {
			outFiles.put(MatcherConfiguration.FileType.RES_FILE, baosRes);
			outFiles.put(MatcherConfiguration.FileType.LOG_FILE, baosLog);
			outFiles.put(MatcherConfiguration.FileType.GRAPH_FILE, baosGraph);


			final Result res = new Result(o1, o2, "file:///" + engine, "file:///" + basePath);
			final String idalignmentjob = res.getAlignmentId();
			try (final ByteArrayOutputStream repository = new ByteArrayOutputStream()) {
				//AlignmentResultSet.init(repository);
				AlignmentResultSet.addResult(idalignmentjob, res);
				AlignmentResultSet.saveRepositoryToFile(repository);
				final MatcherConfiguration mc = new MatcherConfiguration(idalignmentjob,
						new MatchingResource("file:///" + o1, null,
								Lexicalization.SKOS, "en", null),
						new MatchingResource("file:///" + o2, null,
								Lexicalization.SKOS, "en", null),
						null, alref, outFiles, null);
				mc.loadArchitecture(engine);
				final Composer composer = new Composer(mc);

				composer.startMatching(repository);
				final ByteArrayOutputStream os = (ByteArrayOutputStream) outFiles.get(FileType.LOG_FILE);
				System.out.println(os.toString());
				try (final FileOutputStream fos = new FileOutputStream("result.txt");) {
					fos.write(os.toByteArray());
				}
			}
		}
	}

	@Ignore("Ignored because it uses an external dataset")
	@Test
	public void testEasyEngineSKOSXL() throws IOException, RMIServerNotAvailableException, InterruptedException {
		String log = executeTest(EASY_ENGINE_FOR_SKOS, ONTO1_SKOSXL, ONTO2_SKOSXL,
				"http://localhost:7200/repositories/Eurovoc-CUT", "http://localhost:7200/repositories/Teseo-CUT",
				Lexicalization.SKOS_XL, Lexicalization.SKOS_XL, null, "it");
		assert log.contains("http://eurovoc.europa.eu/4000 vs http://www.senato.it/teseo/tes/00001919 rel type:= grade:0.5025872442839953");
		assert log.contains("http://eurovoc.europa.eu/2628 vs http://www.senato.it/teseo/tes/00000099 rel type:= grade:0.7272727272727273");
		assert log.contains("http://eurovoc.europa.eu/3049 vs http://www.senato.it/teseo/tes/00002344 rel type:= grade:0.5758473512427819");
	}

	@Ignore("Ignored because it uses an external dataset")
	@Test
	public void testEasyEngineSKOSXL2() throws IOException, RMIServerNotAvailableException, InterruptedException {
		String log = executeTest(EASY_ENGINE_FOR_SKOS, ONTO3_SKOSXL, ONTO4_SKOSXL,
				"http://localhost:7200/repositories/Eurovoc-CUT2", "http://localhost:7200/repositories/Fisma",
				Lexicalization.SKOS_XL, Lexicalization.SKOS_XL, null, "en");
		assert log.contains("http://eurovoc.europa.eu/164 vs http://FinReg/Contractual_Agent rel type:= grade:0.8888888888888888");
	}

	@Ignore("Ignored because it uses an external dataset")
	@Test
	public void testEasyEngineOntolex() throws IOException, RMIServerNotAvailableException, InterruptedException {
		final String expectedLog = "[2018-03-13 17:47:43.073]: http://oaei.ontologymatching.org/2011/benchmarks/101/onto.rdf#school vs http://oaei.ontologymatching.org/2011/benchmarks/103/onto.rdf#school rel type:= grade:2.0\n"
				+ "[2018-03-13 17:47:43.074]: http://www.w3.org/1999/02/22-rdf-syntax-ns#rest vs http://oaei.ontologymatching.org/2011/benchmarks/103/onto.rdf#address rel type:= grade:1.3815201192250373\n"
				+ "[2018-03-13 17:47:43.139]: Found:298\n" + "[2018-03-13 17:47:43.139]: Existing:97\n"
				+ "[2018-03-13 17:47:43.139]: Correct:97\n"
				+ "[2018-03-13 17:47:43.139]: Precision:0.32550335570469796\n"
				+ "[2018-03-13 17:47:43.139]: Recall:1.0\n"
				+ "[2018-03-13 17:47:43.139]: F-Measure:0.49113924050632907\n"
				+ "[2018-03-13 17:47:44.042]: *****************************************************************\n"
				+ "[2018-03-13 17:47:44.042]: Graph Extractor\n"
				+ "[2018-03-13 17:47:44.042]: *****************************************************************\n"
				+ "Total Time:0:0:5.767";
		String log = executeTest(EASY_ENGINE_FOR_SKOS, ONTO1_SKOSXL, ONTO2_SKOSXL,
				"http://localhost:7200/repositories/Eurovoc-CUT", "http://localhost:7200/repositories/WordnetEurovocIT",
				Lexicalization.SKOS_XL, Lexicalization.SKOS_XL, null, "en");

		assertEquals(expectedLog.split("Found:")[1].split("\n")[0], log.split("Found:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Correct:")[1].split("\n")[0],
				log.split("Correct:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Precision:")[1].split("\n")[0],
				log.split("Precision:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Recall:")[1].split("\n")[0], log.split("Recall:")[1].split("\n")[0]);
	}


	@Test
	public void testOntoChord() throws IOException, InterruptedException {
		final String expectedLog = "[2020-07-04 06:15:46.232]: http://oaei.ontologymatching.org/2011/benchmarks/101/onto.rdf#humanCreator vs http://oaei.ontologymatching.org/2011/benchmarks/103/onto.rdf#humanCreator rel type:= grade:1.0\n" +
				"[2020-07-04 06:15:46.409]: Found:149\n" +
				"[2020-07-04 06:15:46.409]: Existing:97\n" +
				"[2020-07-04 06:15:46.41]: Correct:96\n" +
				"[2020-07-04 06:15:46.41]: Precision:0.6442953020134228\n" +
				"[2020-07-04 06:15:46.41]: Recall:0.9896907216494846\n" +
				"[2020-07-04 06:15:46.41]: F-Measure:0.7804878048780488\n" +
				"Total Time:0:0:19.744";
		String log = executeTest(ONTOCHORD_ENGINE, ONTO1, ONTO2,
				null, null,
				Lexicalization.RDFS, Lexicalization.RDFS, ALREF, "en");
		assertEquals(expectedLog.split("Found:")[1].split("\n")[0], log.split("Found:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Correct:")[1].split("\n")[0],
				log.split("Correct:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Precision:")[1].split("\n")[0],
				log.split("Precision:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Recall:")[1].split("\n")[0], log.split("Recall:")[1].split("\n")[0]);
	}

	@Ignore("Ignored because it uses an external resource")
	@Test
	public void testCosynonymsEngine() throws IOException, RMIServerNotAvailableException, InterruptedException {
		final String expectedLog = "[2020-12-10 09:54:59.238]: http://oaei.ontologymatching.org/2011/benchmarks/101/onto.rdf#publisher vs http://oaei.ontologymatching.org/2011/benchmarks/304/onto.rdf#howPublished rel type:= grade:0.9152827918170878\n" +
				"[2020-12-10 09:54:59.238]: http://oaei.ontologymatching.org/2011/benchmarks/101/onto.rdf#humanCreator vs http://oaei.ontologymatching.org/2011/benchmarks/304/onto.rdf#humanCreator rel type:= grade:1.0\n" +
				"[2020-12-10 09:54:59.265]: Found:64\n" +
				"[2020-12-10 09:54:59.265]: Existing:76\n" +
				"[2020-12-10 09:54:59.265]: Correct:50\n" +
				"[2020-12-10 09:54:59.265]: Precision:0.78125\n" +
				"[2020-12-10 09:54:59.265]: Recall:0.6578947368421053\n" +
				"[2020-12-10 09:54:59.265]: F-Measure:0.7142857142857143\n" +
				"Total Time:0:0:5.391";
		String log = executeTest(COSYNONYMS_ENGINE, ONTO1, ONTO3,
				null, null,
				Lexicalization.RDFS, Lexicalization.RDFS, ALREF2, "en");
		System.out.println(log);
		assertEquals(expectedLog.split("Found:")[1].split("\n")[0], log.split("Found:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Correct:")[1].split("\n")[0],
				log.split("Correct:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Precision:")[1].split("\n")[0],
				log.split("Precision:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Recall:")[1].split("\n")[0], log.split("Recall:")[1].split("\n")[0]);
	}

	@Test
	public void testFalconAO() throws IOException, InterruptedException {
		final String expectedLog = "[2020-06-10 05:42:13.452]: http://oaei.ontologymatching.org/2011/benchmarks/101/onto.rdf#book vs http://oaei.ontologymatching.org/2011/benchmarks/103/onto.rdf#book rel type:= grade:1.0\n" +
				"[2020-06-10 05:42:13.452]: http://oaei.ontologymatching.org/2011/benchmarks/101/onto.rdf#humanCreator vs http://oaei.ontologymatching.org/2011/benchmarks/103/onto.rdf#humanCreator rel type:= grade:1.0\n" +
				"[2020-06-10 05:42:13.623]: Found:96\n" +
				"[2020-06-10 05:42:13.623]: Existing:97\n" +
				"[2020-06-10 05:42:13.623]: Correct:96\n" +
				"[2020-06-10 05:42:13.623]: Precision:1.0\n" +
				"[2020-06-10 05:42:13.623]: Recall:0.9896907216494846\n" +
				"[2020-06-10 05:42:13.624]: F-Measure:0.9948186528497409";
		String log = executeTest(FALCON_AO, ONTO1, ONTO2,null, null,
				Lexicalization.RDFS, Lexicalization.RDFS, ALREF, "en");
		assertEquals(expectedLog.split("Found:")[1].split("\n")[0], log.split("Found:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Correct:")[1].split("\n")[0],
				log.split("Correct:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Precision:")[1].split("\n")[0],
				log.split("Precision:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Recall:")[1].split("\n")[0], log.split("Recall:")[1].split("\n")[0]);
	}

	@Test
	public void testCustomMatcher() throws IOException, InterruptedException {
		final String expectedLog = "[2018-06-20 00:07:43.311]: http://oaei.ontologymatching.org/2011/benchmarks/101/onto.rdf#publisher vs http://oaei.ontologymatching.org/2011/benchmarks/103/onto.rdf#publisher rel type:= grade:1.0\n"
				+ "[2018-06-20 00:07:43.311]: http://oaei.ontologymatching.org/2011/benchmarks/101/onto.rdf#school vs http://oaei.ontologymatching.org/2011/benchmarks/103/onto.rdf#school rel type:= grade:1.0\n"
				+ "[2018-06-20 00:07:43.378]: Found:222\n" + "[2018-06-20 00:07:43.378]: Existing:96\r\n"
				+ "[2018-06-20 00:07:43.378]: Correct:96\n"
				+ "[2018-06-20 00:07:43.378]: Precision:0.43243243243243246\n"
				+ "[2018-06-20 00:07:43.378]: Recall:0.9896907216494846\n"
				+ "[2018-06-20 00:07:43.379]: F-Measure:0.5950920245398773\n"
				+ "[2018-06-20 00:07:44.294]: *****************************************************************\n"
				+ "[2018-06-20 00:07:44.295]: Graph Extractor\r\n"
				+ "[2018-06-20 00:07:44.295]: *****************************************************************\r\n"
				+ "Total Time:0:0:6.648";
		String log = executeTest(CUSTOM_MATCHER_ENGINE, ONTO1, ONTO2,null, null,
				Lexicalization.RDFS, Lexicalization.RDFS, ALREF, "en");
		assertEquals(expectedLog.split("Found:")[1].split("\n")[0],log.split("Found:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Correct:")[1].split("\n")[0],
				log.split("Correct:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Precision:")[1].split("\n")[0],
				log.split("Precision:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Recall:")[1].split("\n")[0], log.split("Recall:")[1].split("\n")[0]);
	}

	@Test
	public void testCustomMatcherWithSKOS() throws IOException, InterruptedException {
		final String expectedLog = "[2020-07-04 07:10:49.189]: http://publications.europa.eu/ilo/gloss#00046 vs http://publications.europa.eu/ilo/taxa#13.05.2.gmz rel type:= grade:0.5833333333333334\n" +
				"[2020-07-04 07:10:49.385]: Found:670\n" +
				"[2020-07-04 07:10:49.385]: Existing:97\n" +
				"[2020-07-04 07:10:49.385]: Correct:0\n" +
				"[2020-07-04 07:10:49.385]: Precision:0.0\n" +
				"[2020-07-04 07:10:49.386]: Recall:0.0\n" +
				"[2020-07-04 07:10:49.386]: F-Measure:NaN\n" +
				"Total Time:0:1:36.571";
		String log = executeTest(CUSTOM_MATCHER_ENGINE, ONTO1_SKOS, ONTO2_SKOS,null, null,
				Lexicalization.SKOS, Lexicalization.SKOS, ALREF, "en");
		assertEquals(expectedLog.split("Found:")[1].split("\n")[0], log.split("Found:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Correct:")[1].split("\n")[0],
				log.split("Correct:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Precision:")[1].split("\n")[0],
				log.split("Precision:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Recall:")[1].split("\n")[0], log.split("Recall:")[1].split("\n")[0]);
	}

	@Test
	public void testGroovyScript() throws CompilationFailedException, IOException {
		final File resourcesDirectory = new File("src/test/resources");
		final String basePath = resourcesDirectory.getAbsolutePath();
		final GroovyShell shell = new GroovyShell();
		final Script script = shell.parse(new File(basePath + "/dice_coefficient.groovy"));
		final String test = "test";
		script.invokeMethod("parseOptions", test);
	}

	@Test
	public void testSubjects() throws IOException, RMIServerNotAvailableException, InterruptedException {
		String log = executeTest(EASY_ENGINE_FOR_SKOS, ONTO1_SKOSXL, ONTO2_SKOSXL,
				"http://localhost:7200/repositories/Eurovoc-CUT", "http://localhost:7200/repositories/subjects",
				Lexicalization.SKOS_XL, Lexicalization.SKOS, null, "en");
	}

	@Test
	public void testGND() throws IOException, RMIServerNotAvailableException, InterruptedException {
		String log = executeTest(EASY_ENGINE_FOR_SKOS, ONTO1_SKOSXL, ONTO2_SKOSXL,
				"http://localhost:7200/repositories/Eurovoc-CUT", "http://localhost:7200/repositories/GND",
				Lexicalization.SKOS_XL, Lexicalization.RDFS, null, "en");
	}

	@Test
	public void testEasyEngineGneratedByUI() throws IOException, InterruptedException {
		final String expectedLog = "[2018-03-13 17:47:43.073]: http://oaei.ontologymatching.org/2011/benchmarks/101/onto.rdf#school vs http://oaei.ontologymatching.org/2011/benchmarks/103/onto.rdf#school rel type:= grade:2.0\n"
				+ "[2018-03-13 17:47:43.074]: http://www.w3.org/1999/02/22-rdf-syntax-ns#rest vs http://oaei.ontologymatching.org/2011/benchmarks/103/onto.rdf#address rel type:= grade:1.3815201192250373\n"
				+ "[2018-03-13 17:47:43.139]: Found:275\n" + "[2018-03-13 17:47:43.139]: Existing:97\n"
				+ "[2018-03-13 17:47:43.139]: Correct:96\n"
				+ "[2018-03-13 17:47:43.139]: Precision:0.3490909090909091\n"
				+ "[2018-03-13 17:47:43.139]: Recall:0.9896907216494846\n"
				+ "[2018-03-13 17:47:43.139]: F-Measure:0.49113924050632907\n"
				+ "[2018-03-13 17:47:44.042]: *****************************************************************\n"
				+ "[2018-03-13 17:47:44.042]: Graph Extractor\n"
				+ "[2018-03-13 17:47:44.042]: *****************************************************************\n"
				+ "Total Time:0:0:5.767";
		String log = executeTest("test2.xml", ONTO1, ONTO2,null, null,
				Lexicalization.RDFS, Lexicalization.RDFS, ALREF, "en");
		assertEquals(expectedLog.split("Found:")[1].split("\n")[0], log.split("Found:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Correct:")[1].split("\n")[0],
				log.split("Correct:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Precision:")[1].split("\n")[0],
				log.split("Precision:")[1].split("\n")[0]);
		assertEquals(expectedLog.split("Recall:")[1].split("\n")[0], log.split("Recall:")[1].split("\n")[0]);
	}

	private String executeTest(String engineName, String onto1, String onto2, String sprqlEndpoint1, String sprqlEndpoint2, Lexicalization lexOnto1, Lexicalization lexOnto2, String alrefName, String language) throws IOException, InterruptedException {
		final File resourcesDirectory = new File("src/test/resources");
		final String basePath = resourcesDirectory.getAbsolutePath();
		final String engine = basePath + "/" + engineName;
		final String o1 = basePath + "/" + onto1;
		final String o2 = basePath + "/" + onto2;
		final String alref = basePath + "/" + alrefName;
		final Map<FileType, OutputStream> outFiles = new HashMap<>();
		try (ByteArrayOutputStream baosRes = new ByteArrayOutputStream();
			 ByteArrayOutputStream baosLog = new ByteArrayOutputStream();
			 ByteArrayOutputStream baosGraph = new ByteArrayOutputStream()) {
			outFiles.put(MatcherConfiguration.FileType.RES_FILE, baosRes);
			outFiles.put(MatcherConfiguration.FileType.LOG_FILE, baosLog);
			outFiles.put(MatcherConfiguration.FileType.GRAPH_FILE, baosGraph);

			final Result res = new Result(o1, o2, "file:///" + engine, "file:///" + basePath);
			final String idalignmentjob = res.getAlignmentId();
			try (final ByteArrayOutputStream repository = new ByteArrayOutputStream()) {
				//AlignmentResultSet.init(repository);
				AlignmentResultSet.addResult(idalignmentjob, res);
				AlignmentResultSet.saveRepositoryToFile(repository);
				final MatcherConfiguration mc = new MatcherConfiguration(idalignmentjob,
						new MatchingResource("file:///" + o1, new DataService(sprqlEndpoint1), lexOnto1, language, null),
						new MatchingResource("file:///" + o2, new DataService(sprqlEndpoint2), lexOnto2, language, null),
						null, alref, outFiles, null);
				mc.loadArchitecture(engine);
				final Composer composer = new Composer(mc);

				composer.startMatching(repository);
				final ByteArrayOutputStream os = (ByteArrayOutputStream) outFiles.get(FileType.LOG_FILE);
				return os.toString();
			}
		}
	}



}
