package it.uniroma2.art.genoma.graphdb;

import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.http.HTTPRepository;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
@Ignore
public class GraphDbTest {

	// GraphDB
	private static final String GRAPHDB_SERVER = "http://localhost:7200/";
	private static final String REPOSITORY_ID = "genoma";
	// Why This Failure marker
	private static final Marker WTF_MARKER = MarkerFactory.getMarker("WTF");
	private static String strQuery = "SELECT ?subject ?predicate ?object  WHERE {?subject ?predicate ?object .}";

	@Test
	public void test() {
		RepositoryConnection repositoryConnection = null;
		try {
			repositoryConnection = getRepositoryConnection();
			query(repositoryConnection);
		} catch (final Throwable t) {
			System.out.println(t.getMessage());
		} finally {
			repositoryConnection.close();
		}
	}

	private static RepositoryConnection getRepositoryConnection() {
		final Repository repository = new HTTPRepository(GRAPHDB_SERVER, REPOSITORY_ID);
		repository.initialize();
		final RepositoryConnection repositoryConnection = repository.getConnection();
		return repositoryConnection;
	}

	private static void query(final RepositoryConnection repositoryConnection) {
		final TupleQuery tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, strQuery);
		try (TupleQueryResult result = tupleQuery.evaluate()) {
			while (result.hasNext()) {
				final BindingSet bindingSet = result.next();
				final Resource subject = (Resource) bindingSet.getValue("subject");
				final Resource predicate = (Resource) bindingSet.getValue("predicate");
				final Resource object = (Resource) bindingSet.getValue("object");
				System.out.println(subject.stringValue() + " " + predicate.stringValue() + " " + object.stringValue());
			}
		} catch (final QueryEvaluationException qee) {
			System.out.println(qee.getStackTrace().toString());
		}
	}
}
