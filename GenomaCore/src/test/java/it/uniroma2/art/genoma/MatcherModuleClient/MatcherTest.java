package it.uniroma2.art.genoma.MatcherModuleClient;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.automaton.TooComplexToDeterminizeException;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.junit.Ignore;
import org.junit.Test;

public class MatcherTest {

	@Ignore
	@Test
	public void testRemoteSPARQLEndPoint() {
		final String strQuery = "select (count(distinct ?s) as ?count) where {?s a <http://www.w3.org/2004/02/skos/core#Concept> .} LIMIT 100";
		final SPARQLRepository sparql = new SPARQLRepository("http://localhost:7200/repositories/Eurovoc");
		sparql.initialize();
		try (final RepositoryConnection conn = sparql.getConnection()) {
			final TupleQuery query = conn.prepareTupleQuery(QueryLanguage.SPARQL, strQuery);
			try (final TupleQueryResult rs = query.evaluate()) {
				while (rs.hasNext()) {
					System.out.println(rs.next());
				}
			}
		} finally {
			sparql.shutDown();
		}
	}

	@Test
	public void testLucene() throws IOException {
		final Map<String, List<String>> doc = new HashMap<>();
		doc.put("http://test", Arrays.asList("areonautica militare", "sdfs", "pippo"));
		final Directory memoryIndex = createMemoryIndex(doc);
		final List<Document> candidates = fuzzySearch(memoryIndex, Arrays.asList("militare"));
		assertEquals(1, candidates.size());
	}

	private Directory createMemoryIndex(final Map<String, List<String>> map) throws IOException {
		final Directory memoryIndex = new RAMDirectory();
		final StandardAnalyzer analyzer = new StandardAnalyzer();

		final IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);

		final IndexWriter writer = new IndexWriter(memoryIndex, indexWriterConfig);

		for (final String key : map.keySet()) {

			final Document document = new Document();
			document.add(new TextField("key", key, Store.YES));
			document.add(new TextField("label", String.join(",", map.get(key)), Store.YES));

			writer.addDocument(document);

		}
		writer.close();
		return memoryIndex;
	}

	private List<Document> fuzzySearch(final Directory memoryIndex, final List<String> terms) throws IOException {
		final List<Document> documents = new ArrayList<>();
		for (final String termString : terms) {
			for (final String token : termString.split(" ")) {
				final Term term = new Term("label", token);
				final Query query = new FuzzyQuery(term);
				final IndexReader indexReader = DirectoryReader.open(memoryIndex);
				final IndexSearcher searcher = new IndexSearcher(indexReader);
				try {
					final TopDocs topDocs = searcher.search(query, 30);
					for (final ScoreDoc scoreDoc : topDocs.scoreDocs) {
						documents.add(searcher.doc(scoreDoc.doc));
					}
				} catch (final TooComplexToDeterminizeException e) {
					e.printStackTrace();
				}
			}
		}
		return documents;
	}

}
