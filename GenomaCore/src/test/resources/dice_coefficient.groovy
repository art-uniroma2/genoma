import groovy.json.internal.LazyMap;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


	def double similarityFunction(String l1, String l2, String c1, String c2,
			String n1, String n2, double labelWeight, double commentWeight, double idWeight) {
		return Math.max(diceCoefficientOptimized(l1, l2) * labelWeight,
				Math.max(diceCoefficientOptimized(c1, c2) * commentWeight, diceCoefficientOptimized(n1, n2) * idWeight));
	}

	def double similarityFunction(String l1, String l2, String n1, String n2, double labelWeight, double commentWeight, double idWeight)  {
		return Math.max(diceCoefficientOptimized(l1, l2) * labelWeight, diceCoefficientOptimized(n1, n2) * idWeight);
	}

	def double similarityFunction(String n1, String n2, double labelWeight, double commentWeight, double idWeight) { 
		return diceCoefficientOptimized(n1, n2) * idWeight;
	}


  def double diceCoefficientOptimized(String s, String t)
{
	if(s == null || t == null)
		return 0.0;
	Set<String> nx = new HashSet<String>();
	Set<String> ny = new HashSet<String>();

	for (int i=0; i < s.length()-1; i++) {
		char x1 = s.charAt(i);
		char x2 = s.charAt(i+1);
		String tmp = "" + x1 + x2;
		nx.add(tmp);
	}
	for (int j=0; j < t.length()-1; j++) {
		char y1 = t.charAt(j);
		char y2 = t.charAt(j+1);
		String tmp = "" + y1 + y2;
		ny.add(tmp);
	}

	Set<String> intersection = new HashSet<String>(nx);
	intersection.retainAll(ny);
	double totcombigrams = intersection.size();

	return (2*totcombigrams) / (nx.size()+ny.size());
}

	def LazyMap parseOptions(String options) {
		LazyMap result = new LazyMap();
		final String[] tmp = options.split("-");
		for (final String element : tmp) {
			if (element.contains("iw")) {
				result.put("iw", Double.parseDouble(
						element.substring(element.lastIndexOf("iw=") + ("iw=").length(), element.length()).trim()));
			} else if (element.contains("lw")) {
				result.put("lw", Double.parseDouble(
						element.substring(element.lastIndexOf("lw=") + ("lw=").length(), element.length()).trim()));
			} else if (element.contains("cw")) {
				result.put("cw", Double.parseDouble(
						element.substring(element.lastIndexOf("cw=") + ("cw=").length(), element.length()).trim()));
			} else if (element.contains("incI=true")) {
				result.put("incI",true);
			} else if (element.contains("incE=true")) {
				result.put("incE", true);
			}

		}
		return result;
	}
