/**
 * Copyright 2010 Websoft research group, Nanjing University, PR China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.uniroma2.art.genoma.MatcherModuleClient.gmo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.eclipse.rdf4j.common.iteration.Iterations;
import org.eclipse.rdf4j.model.BNode;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.genoma.AlignmentTools.OntoResource;
import it.uniroma2.art.genoma.MatcherModuleClient.vdoc.NeighborSet;
import it.uniroma2.art.genoma.MatcherModuleClient.vdoc.TokenNodeSet;
import it.uniroma2.art.genoma.MatcherModuleClient.vdoc.Tokenizer;

/**
 * @author Wei Hu
 * @see http://ws.nju.edu.cn
 */
public class GmoParser {
	private TokenNodeSet tokenNodes = new TokenNodeSet();
	private NeighborSet neighbors = new NeighborSet();
	private Tokenizer tokenizer = new Tokenizer();
	private Collection<OntoResource> nodeList = new ArrayList<>();
	private Collection<Quadruple> quadList = new ArrayList();

	GmoParser(final RepositoryConnection conn) {
		parseNamedClasses(conn);
		parseProperties(conn);
		parseInstances(conn);
		parseStatements(conn);
	}

	public Collection<OntoResource> getNodeList() {
		return nodeList;
	}

	public Collection<Quadruple> getQuadrupleList() {
		return quadList;
	}

	private TupleQueryResult getCategoryNodesFromOntology(final RepositoryConnection conn, final String entityType) {
		final String queryString = "SELECT DISTINCT ?entity ?entityLabel ?entityComment " + "\nWHERE {" + "\n?entity <"
				+ RDF.TYPE.stringValue() + "> <" + entityType + ">. " + "\nOPTIONAL{ ?entity <"
				+ RDFS.LABEL.stringValue() + "> ?entityLabel. " + "\nFILTER(lang(?entityLabel) = \"en\")} ."
				+ "\nOPTIONAL{?entity <" + RDFS.COMMENT.stringValue() + "> ?entityComment."
				+ "\nFILTER(lang(?entityComment) = \"en\") }" + "\n}";
		final TupleQuery query1 = conn.prepareTupleQuery(queryString);
		query1.setIncludeInferred(false);
        return query1.evaluate(); // it should be closed by the calling method
	}

	private TupleQueryResult getInstancesOfClass(final RepositoryConnection model, final String Class) {
		final String prefix = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ "\nPREFIX owl: <http://www.w3.org/2002/07/owl#>"
				+ "\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n";
		// String queryString = prefix + "SELECT DISTINCT ?instance WHERE {?class
		// rdfs:subClassOf* <"+Class+"> . ?instance rdf:type ?class .?instance
		// <"+RDFS.LABEL+"> ?instanceLabel .?instance <"+RDFS.COMMENT+">
		// ?instanceComment. FILTER(lang(?instanceLabel) = \"en\").
		// FILTER(lang(?instanceComment) = \"en\")}";
		final String queryString = prefix + "SELECT DISTINCT ?instance ?instanceLabel ?class " + "WHERE {"
				+ "\n?class rdfs:subClassOf* <" + Class + "> . " + "\n?instance rdf:type ?class .?instance <"
				+ RDFS.LABEL.stringValue() + "> ?instanceLabel}";
		final TupleQuery query1 = model.prepareTupleQuery(queryString);
		query1.setIncludeInferred(false);
        return query1.evaluate(); // it should be closed by the calling method
	}

	private TupleQueryResult getAllStatements(final RepositoryConnection conn) {
		final String queryString = "SELECT DISTINCT ?subject ?predicate ?object WHERE {?subject ?predicate ?object}";
		final TupleQuery query1 = conn.prepareTupleQuery(queryString);
		query1.setIncludeInferred(false);
        return query1.evaluate(); // it should be closed by the calling method
	}

	private void parseEntity(final RepositoryConnection conn, final String entityType, final int category) {
		try (final TupleQueryResult it = getCategoryNodesFromOntology(conn, entityType)) {
			while (it.hasNext()) {
				final BindingSet tuplebinding = it.next();
				final Value value = tuplebinding.getBinding("entity").getValue();
				if (value instanceof BNode) {
					// nodeList.add(new
					// OntoResource(uri.asBNode().getID(),uri.asBNode().getNominalValue(),null,null,
					// OntoResource.BLANK, OntoResource.ONTOLOGY_LEVEL));
					// TODO check if the RDF4J is correct
					final BNode bnode = (BNode) value;
					nodeList.add(new OntoResource(bnode.getID(), bnode.getID(), null, null, OntoResource.BLANK,
							OntoResource.ONTOLOGY_LEVEL));
				} else {
					if (!IsUriInLocalNamespace(conn, (IRI) value))
						continue;
					Value lab = null;
					if (tuplebinding.hasBinding("entityLabel")) {
						lab = tuplebinding.getBinding("entityLabel").getValue();
					}
					Value com = null;
					if (tuplebinding.hasBinding("entityComment")) {
						com = tuplebinding.getBinding("entityComment").getValue();
					}

					final IRI iri = (IRI) value;
					if (lab == null) {
						nodeList.add(new OntoResource(iri.stringValue(), iri.getLocalName(), null, null, category,
								OntoResource.ONTOLOGY_LEVEL));
					} else if (com == null) {
						// lab.stringValue() return its label, since it is a Literal
						nodeList.add(new OntoResource(iri.stringValue(), iri.getLocalName(), lab.stringValue(), null,
								category, OntoResource.ONTOLOGY_LEVEL));
					} else {
						// lab.stringValue() return its label, since it is a Literal
						nodeList.add(new OntoResource(iri.stringValue(), iri.getLocalName(), lab.stringValue(),
								com.stringValue(), category, OntoResource.ONTOLOGY_LEVEL));
					}
				}
			}
		}
	}

	private void parseNamedClasses(final RepositoryConnection conn) {
		parseEntity(conn, RDFS.CLASS.stringValue(), OntoResource.CLASS);
		parseEntity(conn, OWL.CLASS.stringValue(), OntoResource.CLASS);
	}

	private void parseProperties(final RepositoryConnection conn) {
		parseEntity(conn, OWL.DATATYPEPROPERTY.stringValue(), OntoResource.PROPERTY);
		parseEntity(conn, OWL.OBJECTPROPERTY.stringValue(), OntoResource.PROPERTY);
	}

	private void parseInstances(final RepositoryConnection conn) {
		TupleQueryResult it1 = null;
		try {
			it1 = getCategoryNodesFromOntology(conn, RDFS.CLASS.stringValue());
			if (!it1.hasNext()) {
				it1.close(); // it is closed, since a new TupleQueryResult is going to be associated to it
				it1 = getCategoryNodesFromOntology(conn, OWL.CLASS.stringValue());
			}
			while (it1.hasNext()) {
				final BindingSet tuplebinding1 = it1.next();
				final Value value1 = tuplebinding1.getBinding("entity").getValue();
				if (value1 instanceof BNode)
					continue;
				try (final TupleQueryResult it2 = getInstancesOfClass(conn, value1.stringValue())) {
					while (it2.hasNext()) {
						final BindingSet tuplebinding = it2.next();
						final Value value2 = tuplebinding.getBinding("instance").getValue();
						final Value lab = tuplebinding.getBinding("instanceLabel").getValue();
						// ARTNode com = tuplebinding.getBinding("instanceComment").getBoundValue();
						final IRI iri = (IRI) value2;
						final OntoResource instance = new OntoResource(iri.stringValue(), iri.getLocalName(), lab.stringValue(),
								null, OntoResource.INSTANCE, OntoResource.INSTANCE_LEVEL);
						if (!IsInCollection(instance, nodeList)) {
							nodeList.add(instance);
						}
					}
				}
			}
		} finally {
			if (it1 != null) {
				// it is manually closed, and not in a try-with-resources, since it1 could have been assigned to a different
				// TupleQueryResult than its first assignment (in this case, the first assignment has been already
				// manually closed)
				it1.close();
			}
		}

	}

	private void parseStatements(final RepositoryConnection conn) {
		try (TupleQueryResult it = getAllStatements(conn)) {
			while (it.hasNext()) {
				final BindingSet tuplebinding = it.next();
				final Value subject = tuplebinding.getBinding("subject").getValue();
				final Value predicate = tuplebinding.getBinding("predicate").getValue();
				final Value object = tuplebinding.getBinding("object").getValue();
				OntoResource sub = null, pred = null, obj = null;
				if (!(subject instanceof BNode)) {
					sub = getResourceFromList(subject.stringValue());
					final IRI subjIri = (IRI) subject;
					if (sub == null) {
						if (isOnLanguageLevel(subject)) {
							sub = new OntoResource(subjIri.stringValue(), subjIri.getLocalName(), null, null,
									OntoResource.PROPERTY, OntoResource.LANGUAGE_LEVEL);
						} else {
							sub = new OntoResource(subjIri.stringValue(), subjIri.getLocalName(), null, null,
									OntoResource.PROPERTY, OntoResource.EXTERNAL);
						}
					}
				} else {
					sub = getResourceFromList(((BNode) subject).getID());
					if (sub == null) {
						final BNode bnode = (BNode) subject;
						sub = new OntoResource(bnode.getID(), bnode.getID(), null, null, OntoResource.BLANK,
								OntoResource.ONTOLOGY_LEVEL);
					}
				}

				pred = getResourceFromList(predicate.stringValue());
				if (pred == null) {
					final IRI predIri = (IRI) pred;
					if (isOnLanguageLevel(predicate)) {
						pred = new OntoResource(predIri.stringValue(), predIri.getLocalName(), null, null,
								OntoResource.PROPERTY, OntoResource.LANGUAGE_LEVEL);
					} else {
						pred = new OntoResource(predIri.stringValue(), predIri.getLocalName(), null, null,
								OntoResource.PROPERTY, OntoResource.EXTERNAL);
					}
				}
				if (object instanceof IRI) {
					obj = getResourceFromList(object.stringValue());
				} else if (object instanceof Literal) {
					final Literal literalObj = (Literal) object;
					obj = new OntoResource(literalObj.getLabel(), literalObj.getLabel(), literalObj.getLabel(), null,
							OntoResource.LITERAL, OntoResource.ONTOLOGY_LEVEL);
				} else if (object instanceof BNode) {
					final BNode bnodeObj = (BNode) object;
					obj = new OntoResource(bnodeObj.getID(), bnodeObj.getID(), null, null, OntoResource.BLANK,
							OntoResource.ONTOLOGY_LEVEL);
				}
				if (obj == null) {
					final IRI iriObj = (IRI) object;
					if (isOnLanguageLevel(object)) {
						obj = new OntoResource(iriObj.stringValue(), iriObj.getLocalName(), null, null,
								OntoResource.PROPERTY, OntoResource.LANGUAGE_LEVEL);
					} else if (IsUriInLocalNamespace(conn, iriObj)) {
						obj = new OntoResource(iriObj.stringValue(), iriObj.getLocalName(), null, null,
								OntoResource.PROPERTY, OntoResource.ONTOLOGY_LEVEL);
					} else {
						obj = new OntoResource(iriObj.stringValue(), iriObj.getLocalName(), null, null,
								OntoResource.PROPERTY, OntoResource.EXTERNAL);
					}
				}
				if (!((sub.getLevel() == OntoResource.LANGUAGE_LEVEL || sub.getLevel() == OntoResource.INSTANCE
						|| sub.getLevel() == OntoResource.EXTERNAL)
						&& (pred.getLevel() == OntoResource.LANGUAGE_LEVEL || pred.getLevel() == OntoResource.EXTERNAL)
						&& (obj.getLevel() == OntoResource.LANGUAGE_LEVEL || obj.getLevel() == OntoResource.EXTERNAL))) {
					nodeList.add(new OntoResource(tuplebinding.toString(), sub.getURIString(), pred.getURIString(),
							obj.getURIString(), OntoResource.STATEMENT, OntoResource.ONTOLOGY_LEVEL));
					quadList.add(new Quadruple(tuplebinding.toString(), sub, pred, obj));
					if (!IsInCollection(sub, nodeList))
						nodeList.add(sub);
					if (!IsInCollection(pred, nodeList))
						nodeList.add(pred);
					if (!IsInCollection(obj, nodeList))
						nodeList.add(obj);
				}

			}
		}
	}

	private OntoResource getResourceFromList(final String uri) {
		final Iterator<OntoResource> it = nodeList.iterator();
		while (it.hasNext()) {
			final OntoResource temp = it.next();
			if (temp.getURIString().equals(uri))
				return temp;
		}
		return null;
	}

	private boolean IsUriInLocalNamespace(final RepositoryConnection conn, final IRI uri) {
		final String namespaceFromUri = uri.getNamespace();
		// TODO check if the using the prefix "my" as local is the right thing to do,
		// since also the prefix null ("")
		// is used sometimes
		if (conn.getNamespace("my") == null) {
			final Iterator<Namespace> iter = Iterations.asList(conn.getNamespaces()).iterator();
			while (iter.hasNext()) {
				final Namespace namespaceStruct = iter.next();
				final String namespace = namespaceStruct.getName();
				if (namespace.equals(namespaceFromUri)) {
					return false;
				}
			}
			// OLD version
			// Map<String, String> map = conn.getNamespacePrefixMapping();
			/*
			 * for (Map.Entry<String, String> entry : map.entrySet()) { if
			 * (uri.stringValue().contains(entry.getValue())) return false; }
			 */
			return true;
		} else {
			return conn.getNamespace("my").equals(namespaceFromUri);
			// OLD version
			// return uri.stringValue().contains(conn.getNamespace("my"));
		}
	}

	private boolean isOnLanguageLevel(final Value uri) {
		if (uri.stringValue().contains(Constants.DC_NS)) {
			return true;
		} else if (uri.stringValue().contains(Constants.FOAF_NS)) {
			return true;
		} else if (uri.stringValue().contains(Constants.ICAL_NS)) {
			return true;
		} else if (uri.stringValue().contains(Constants.OWL_NS)) {
			return true;
		} else if (uri.stringValue().contains(Constants.RDFS_NS)) {
			return true;
		} else if (uri.stringValue().contains(Constants.RDF_NS)) {
			return true;
		} else if (uri.stringValue().contains(Constants.XSD_NS)) {
			return true;
		} else {
			return false;
		}
	}

	private boolean IsInCollection(final OntoResource e, final Collection<OntoResource> c) {
		final Iterator<OntoResource> it = c.iterator();
		while (it.hasNext()) {
			final OntoResource o = it.next();
			if (o.getURIString().equals(e.getURIString()))
				return true;
		}
		return false;
	}

}
