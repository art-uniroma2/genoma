/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.DataManagement;

/**
 *
 * @author Roberto
 */
public class Engine {
	private String name;
	private String description;
	private String id;

	public Engine(final String name, final String desc, final String id) {
		this.name = name;
		this.description = desc;
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public String getDescription() {
		return this.description;
	}

	public String getId() {
		return this.id;
	}
}
