/**
 * Copyright 2010 Websoft research group, Nanjing University, PR China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.uniroma2.art.genoma.MatcherModuleClient.vdoc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.AlignmentTools.OntoResource;
import it.uniroma2.art.genoma.MatcherModuleClient.MatcherReturn;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Wei Hu
 */
//this class has been modified by replacing the various prints of System.out or System.err with a Logger
public class VDocMatcher // implements AbstractMatcher
{
	private ArrayList<Object> classDocList = new ArrayList<Object>();
	private ArrayList<Object> propertyDocList = new ArrayList<Object>();
	private ArrayList<Object> instanceDocList = new ArrayList<Object>();
	private ArrayList<Object> wordList = new ArrayList<Object>();
	private ArrayList<Collection<EntityAlignment>> InputMatrix;
	private int classSeparator = 0;
	private int propertySeparator = 0;
	private int instanceSeparator = 0;
	private TokenNodeSet tns1 = null, tns2 = null;
	private NeighborSet ns1 = null, ns2 = null;
	private RepositoryConnection connA = null, connB = null;
	private NamedMatrix cbm = null, csm = null, cpm = null, com = null;
	private NamedMatrix pbm = null, psm = null, ppm = null, pom = null;
	private NamedMatrix ibm = null, ism = null, ipm = null, iom = null;
	private NamedMatrix namedClassMatrix = null;
	private NamedMatrix namedPropertyMatrix = null;
	private NamedMatrix namedInstanceMatrix = null;
	private MatcherReturn result;
	private MatchingResource onto1;
	private MatchingResource onto2;

	public VDocMatcher(final RepositoryConnection modelA, final RepositoryConnection modelB,
					   final ArrayList<Collection<EntityAlignment>> im, final MatcherReturn res, MatchingResource onto1, MatchingResource onto2) {
		this.connA = modelA;
		this.connB = modelB;
		this.InputMatrix = im;
		this.result = res;
		this.onto1 = onto1;
		this.onto2 = onto2;
	}

	private void initWords(final TokenNodeSet tns, final HashMap<String, WordFrequency> words) {
		for (final Iterator<TokenNode> iter = tns.iterator(); iter.hasNext();) {
			final TokenNode node = iter.next();
			if (node.getNodeCategory() == OntoResource.CLASS && !(node.getNode().getCategory() == OntoResource.BLANK)) {
				classDocList.add(node.getNode());
			} else if ((node.getNodeCategory() == OntoResource.OBJECTPROPERTY
					|| node.getNodeCategory() == OntoResource.DATATYPEPROPERTY)
					&& !(node.getNode().getCategory() == OntoResource.BLANK)) {
				propertyDocList.add(node.getNode());
			} else if (node.getNodeCategory() == OntoResource.INSTANCE
					&& !(node.getNode().getCategory() == OntoResource.BLANK)) {
				instanceDocList.add(node.getNode());
			}
			if (Parameters.localnameWeight > 0) {
				final HashMap<String, WordFrequency> t = node.getLocalName();
				for (final WordFrequency wf : t.values()) {
					words.put(wf.getWord(), wf);
				}
			}
			if (Parameters.labelWeight > 0) {
				final HashMap<String, WordFrequency> t = node.getLabel();
				for (final WordFrequency wf : t.values()) {
					words.put(wf.getWord(), wf);
				}
			}
			if (Parameters.commentWeight > 0) {
				final HashMap<String, WordFrequency> t = node.getComment();
				for (final WordFrequency wf : t.values()) {
					words.put(wf.getWord(), wf);
				}
			}
		}
	}

	public void init() throws Exception {
		final HashMap<String, WordFrequency> words = new HashMap<String, WordFrequency>();

		final VDocParser parser1 = new VDocParser(InputMatrix, 1, onto1);
		parser1.run(connA);
		tns1 = parser1.getTokenNodes();
		ns1 = parser1.getNeighbors();
		initWords(tns1, words);

		classSeparator = classDocList.size();
		propertySeparator = propertyDocList.size();
		instanceSeparator = instanceDocList.size();

		final VDocParser parser2 = new VDocParser(this.InputMatrix, 2, onto2);
		parser2.run(connB);
		tns2 = parser2.getTokenNodes();
		ns2 = parser2.getNeighbors();
		initWords(tns2, words);

		for (final WordFrequency wordFrequency : words.values()) {
			final String word = wordFrequency.getWord();
			wordList.add(word);
		}

		cbm = new NamedMatrix(wordList, classDocList);
		csm = new NamedMatrix(wordList, classDocList);
		cpm = new NamedMatrix(wordList, classDocList);
		com = new NamedMatrix(wordList, classDocList);
		pbm = new NamedMatrix(wordList, propertyDocList);
		psm = new NamedMatrix(wordList, propertyDocList);
		ppm = new NamedMatrix(wordList, propertyDocList);
		pom = new NamedMatrix(wordList, propertyDocList);
		ibm = new NamedMatrix(wordList, instanceDocList);
		ism = new NamedMatrix(wordList, instanceDocList);
		ipm = new NamedMatrix(wordList, instanceDocList);
		iom = new NamedMatrix(wordList, instanceDocList);

		final HashMap<String, Neighbor> nl1 = ns1.getNeighbors();
		for (final Neighbor neighbor : nl1.values()) {
			final String uri = neighbor.getURI();
			final TokenNode node = tns1.get(uri);
			if (node.getNodeCategory() == OntoResource.CLASS && !(node.getNode().getCategory() == OntoResource.BLANK)) {
				addTokenNodeToWordList(classDocList, node, node, cbm);
				if (Parameters.inclNeighbor) {
					final HashMap<String, TokenNode> sm = neighbor.getSubjects();
					for (final TokenNode n : sm.values()) {
						addTokenNodeToWordList(classDocList, node, n, com);
					}
					final HashMap<String, TokenNode> pm = neighbor.getPredicates();
					for (final TokenNode n : pm.values()) {
						addTokenNodeToWordList(classDocList, node, n, cpm);
					}
					final HashMap<String, TokenNode> om = neighbor.getObjects();
					for (final TokenNode n : om.values()) {
						addTokenNodeToWordList(classDocList, node, n, csm);
					}
				}
			} else if ((node.getNodeCategory() == OntoResource.OBJECTPROPERTY
					|| node.getNodeCategory() == OntoResource.DATATYPEPROPERTY)
					&& !(node.getNode().getCategory() == OntoResource.BLANK)) {
				addTokenNodeToWordList(propertyDocList, node, node, pbm);
				if (Parameters.inclNeighbor) {
					final HashMap<String, TokenNode> sm = neighbor.getSubjects();
					for (final TokenNode n : sm.values()) {
						addTokenNodeToWordList(propertyDocList, node, n, pom);
					}
					final HashMap<String, TokenNode> pm = neighbor.getPredicates();
					for (final TokenNode n : pm.values()) {
						addTokenNodeToWordList(propertyDocList, node, n, ppm);
					}
					final HashMap<String, TokenNode> om = neighbor.getObjects();
					for (final TokenNode n : om.values()) {
						addTokenNodeToWordList(propertyDocList, node, n, psm);
					}
				}
			} else if (node.getNodeCategory() == OntoResource.INSTANCE
					&& !(node.getNode().getCategory() == OntoResource.BLANK)) {
				addTokenNodeToWordList(instanceDocList, node, node, ibm);
				if (Parameters.inclNeighbor) {
					final HashMap<String, TokenNode> sm = neighbor.getSubjects();
					for (final TokenNode n : sm.values()) {
						addTokenNodeToWordList(instanceDocList, node, n, iom);
					}
					final HashMap<String, TokenNode> pm = neighbor.getPredicates();
					for (final TokenNode n : pm.values()) {
						addTokenNodeToWordList(instanceDocList, node, n, ipm);
					}
					final HashMap<String, TokenNode> om = neighbor.getObjects();
					for (final TokenNode n : om.values()) {
						addTokenNodeToWordList(instanceDocList, node, n, ism);
					}
				}
			}
		}

		final HashMap<String, Neighbor> nl2 = ns2.getNeighbors();
		for (final Neighbor neighbor : nl2.values()) {
			final String uri = neighbor.getURI();
			final TokenNode node = tns2.get(uri);
			if (node.getNodeCategory() == OntoResource.CLASS && !(node.getNode().getCategory() == OntoResource.BLANK)) {
				addTokenNodeToWordList(classDocList, node, node, cbm);
				if (Parameters.inclNeighbor) {
					final HashMap<String, TokenNode> sm = neighbor.getSubjects();
					for (final TokenNode n : sm.values()) {
						addTokenNodeToWordList(classDocList, node, n, com);
					}
					final HashMap<String, TokenNode> pm = neighbor.getPredicates();
					for (final TokenNode n : pm.values()) {
						addTokenNodeToWordList(classDocList, node, n, cpm);
					}
					final HashMap<String, TokenNode> om = neighbor.getObjects();
					for (final TokenNode n : om.values()) {
						addTokenNodeToWordList(classDocList, node, n, csm);
					}
				}
			} else if ((node.getNodeCategory() == OntoResource.OBJECTPROPERTY
					|| node.getNodeCategory() == OntoResource.DATATYPEPROPERTY)
					&& !(node.getNode().getCategory() == OntoResource.BLANK)) {
				addTokenNodeToWordList(propertyDocList, node, node, pbm);
				if (Parameters.inclNeighbor) {
					final HashMap<String, TokenNode> sm = neighbor.getSubjects();
					for (final TokenNode n : sm.values()) {
						addTokenNodeToWordList(propertyDocList, node, n, pom);
					}
					final HashMap<String, TokenNode> pm = neighbor.getPredicates();
					for (final TokenNode n : pm.values()) {
						addTokenNodeToWordList(propertyDocList, node, n, ppm);
					}
					final HashMap<String, TokenNode> om = neighbor.getObjects();
					for (final TokenNode n : om.values()) {
						addTokenNodeToWordList(propertyDocList, node, n, psm);
					}
				}
			} else if (node.getNodeCategory() == OntoResource.INSTANCE
					&& !(node.getNode().getCategory() == OntoResource.BLANK)) {
				addTokenNodeToWordList(instanceDocList, node, node, ibm);
				if (Parameters.inclNeighbor) {
					final HashMap<String, TokenNode> sm = neighbor.getSubjects();
					for (final TokenNode n : sm.values()) {
						addTokenNodeToWordList(instanceDocList, node, n, iom);
					}
					final HashMap<String, TokenNode> pm = neighbor.getPredicates();
					for (final TokenNode n : pm.values()) {
						addTokenNodeToWordList(instanceDocList, node, n, ipm);
					}
					final HashMap<String, TokenNode> om = neighbor.getObjects();
					for (final TokenNode n : om.values()) {
						addTokenNodeToWordList(instanceDocList, node, n, ism);
					}
				}
			}
		}
	}

	private void addTokenNodeToWordList(final ArrayList<Object> docList, final TokenNode node, final TokenNode n,
			final NamedMatrix m) {
		int col = -1;
		for (int k = 0, size = docList.size(); k < size; k++) {
			if (docList.get(k).toString().equals(node.getNode().toString())) {
				col = k;
			}
		}
		if (col == -1) {
			return;
		}
		final HashMap<String, WordFrequency> localname = n.getLocalName();
		final HashMap<String, WordFrequency> label = n.getLabel();
		final HashMap<String, WordFrequency> comment = n.getComment();
		if (Parameters.localnameWeight > 0 && localname != null) {
			for (final WordFrequency wf : localname.values()) {
				final int row = wordList.indexOf(wf.getWord());
				final double value = m.get(row, col) + wf.getFrequency() * Parameters.localnameWeight;
				m.set(row, col, value);
			}
		}
		if (Parameters.labelWeight > 0 && label != null) {
			for (final WordFrequency wf : label.values()) {
				final int row = wordList.indexOf(wf.getWord());
				final double value = m.get(row, col) + wf.getFrequency() * Parameters.labelWeight;
				m.set(row, col, value);
			}
		}
		if (Parameters.commentWeight > 0 && comment != null) {
			for (final WordFrequency wf : comment.values()) {
				final int row = wordList.indexOf(wf.getWord());
				final double value = m.get(row, col) + wf.getFrequency() * Parameters.commentWeight;
				m.set(row, col, value);
			}
		}
	}

	private BasicMatrix calcSimilarity(final int numRows, final int numColumns, final NamedMatrix bm,
			final NamedMatrix sm, final NamedMatrix pm, final NamedMatrix om) {
		final BasicMatrix b = new BasicMatrix(numRows, numColumns);
		final BasicMatrix s = new BasicMatrix(numRows, numColumns);
		final BasicMatrix p = new BasicMatrix(numRows, numColumns);
		final BasicMatrix o = new BasicMatrix(numRows, numColumns);

		final double sumColumns[] = new double[numColumns];
		double maxDoc = numColumns;
		for (int j = 0; j < numColumns; j++) {
			sumColumns[j] = bm.sumEntriesInColumn(j);
		}
		for (int i = 0; i < numRows; i++) {
			final double docTimes = bm.countNonZeroEntriesInRow(i);
			for (int j = 0; j < numColumns; j++) {
				final double maxTerm = sumColumns[j];
				final double termTimes = bm.get(i, j);
				final double weight = getTermWeight(termTimes, docTimes, maxTerm, maxDoc);
				if (weight != 0) {
					final double value = b.get(i, j) + weight;
					b.set(i, j, value);
				}
			}
		}
		maxDoc = numColumns;
		for (int j = 0; j < numColumns; j++) {
			sumColumns[j] = sm.sumEntriesInColumn(j);
		}
		for (int i = 0; i < numRows; i++) {
			final double docTimes = sm.countNonZeroEntriesInRow(i);
			for (int j = 0; j < numColumns; j++) {
				final double maxTerm = sumColumns[j];
				final double termTimes = sm.get(i, j);
				final double weight = getTermWeight(termTimes, docTimes, maxTerm, maxDoc);
				if (weight != 0) {
					final double value = s.get(i, j) + weight;
					s.set(i, j, value);
				}
			}
		}
		maxDoc = numColumns;
		for (int j = 0; j < numColumns; j++) {
			sumColumns[j] = pm.sumEntriesInColumn(j);
		}
		for (int i = 0; i < numRows; i++) {
			final double docTimes = pm.countNonZeroEntriesInRow(i);
			for (int j = 0; j < numColumns; j++) {
				final double maxTerm = sumColumns[j];
				final double termTimes = pm.get(i, j);
				final double weight = getTermWeight(termTimes, docTimes, maxTerm, maxDoc);
				if (weight != 0) {
					final double value = p.get(i, j) + weight;
					p.set(i, j, value);
				}
			}
		}
		maxDoc = numColumns;
		for (int j = 0; j < numColumns; j++) {
			sumColumns[j] = om.sumEntriesInColumn(j);
		}
		for (int i = 0; i < numRows; i++) {
			final double docTimes = om.countNonZeroEntriesInRow(i);
			for (int j = 0; j < numColumns; j++) {
				final double maxTerm = sumColumns[j];
				final double termTimes = om.get(i, j);
				final double weight = getTermWeight(termTimes, docTimes, maxTerm, maxDoc);
				if (weight != 0) {
					final double value = o.get(i, j) + weight;
					o.set(i, j, value);
				}
			}
		}

		final BasicMatrix temp = b.times(Parameters.basicWeight).copyAdd(s.times(Parameters.subjectWeight)
				.copyAdd(p.times(Parameters.predicateWeight).copyAdd(o.times(Parameters.objectWeight))));
		return temp.cosine();
	}

	public Collection<EntityAlignment> match() throws Exception {
		init();
		final Collection<EntityAlignment> OutputSimMatrix = new ArrayList();
		final int numRows = wordList.size();

		final int classNumColumns = classDocList.size();
		final BasicMatrix classMatrix = calcSimilarity(numRows, classNumColumns, cbm, csm, cpm, com);
		final ArrayList<Object> classRowList = new ArrayList<Object>();
		final ArrayList<Object> classColumnList = new ArrayList<Object>();

		// from here on, it could be eliminated. Let's see it next time :-)
		for (int i = 0, n = classDocList.size(); i < n; i++) {
			if (i < classSeparator) {
				classRowList.add(classDocList.get(i));
			} else {
				classColumnList.add(classDocList.get(i));
			}
		}
		namedClassMatrix = new NamedMatrix(classRowList, classColumnList);
		// till here
		int count = 0;
		for (int i = 0, m = namedClassMatrix.numRows(); i < m; i++) {
			for (int j = 0, n = namedClassMatrix.numColumns(); j < n; j++) {
				final double value = classMatrix.get(i, j + classSeparator);
				namedClassMatrix.set(i, j, value);
				if (this.InputMatrix.isEmpty()) {
					OutputSimMatrix.add(new EntityAlignment((OntoResource) namedClassMatrix.getRow(i),
							(OntoResource) namedClassMatrix.getColumn(j), '=', value));
					result.addLog(((OntoResource) namedClassMatrix.getRow(i)).getLocalName() + "-"
							+ ((OntoResource) namedClassMatrix.getColumn(j)).getLocalName() + ":" + value);
				} else {
					final EntityAlignment al = findAlignment((OntoResource) namedClassMatrix.getRow(i),
							(OntoResource) namedClassMatrix.getColumn(j));
					if(al == null)
						continue;
					count++;
					OutputSimMatrix.add(new EntityAlignment(al.getEntity1(), al.getEntity2(), '=',
							value + al.getSimilarityValue()));
					result.addLog(al.getEntity1().getLocalName() + "-" + al.getEntity2().getLocalName()
							+ String.valueOf(value + al.getSimilarityValue()));
					//System.out.println("Computed " + count);
					Logger.getLogger(VDocMatcher.class.getName()).log(Level.INFO, "Computed " + count);
				}
			}
		}

		final int propertyNumColumns = propertyDocList.size();
		final BasicMatrix propertyMatrix = calcSimilarity(numRows, propertyNumColumns, pbm, psm, ppm, pom);
		final ArrayList<Object> propertyRowList = new ArrayList<Object>();
		final ArrayList<Object> propertyColumnList = new ArrayList<Object>();
		for (int i = 0, n = propertyDocList.size(); i < n; i++) {
			if (i < propertySeparator) {
				propertyRowList.add(propertyDocList.get(i));
			} else {
				propertyColumnList.add(propertyDocList.get(i));
			}
		}
		namedPropertyMatrix = new NamedMatrix(propertyRowList, propertyColumnList);
		for (int i = 0, m = namedPropertyMatrix.numRows(); i < m; i++) {
			for (int j = 0, n = namedPropertyMatrix.numColumns(); j < n; j++) {
				final double value = propertyMatrix.get(i, j + propertySeparator);
				namedPropertyMatrix.set(i, j, value);
				if (this.InputMatrix.isEmpty()) {
					OutputSimMatrix.add(new EntityAlignment((OntoResource) namedPropertyMatrix.getRow(i),
							(OntoResource) namedPropertyMatrix.getColumn(j), '=', value));
					result.addLog(((OntoResource) namedPropertyMatrix.getRow(i)).getLocalName() + "-"
							+ ((OntoResource) namedPropertyMatrix.getColumn(j)).getLocalName() + ":" + value);
				} else {
					final EntityAlignment al = findAlignment((OntoResource) namedPropertyMatrix.getRow(i),
							(OntoResource) namedPropertyMatrix.getColumn(j));
					if(al == null)
						continue;
					count++;
					OutputSimMatrix.add(new EntityAlignment(al.getEntity1(), al.getEntity2(), '=',
							value + al.getSimilarityValue()));
					result.addLog(al.getEntity1().getLocalName() + "-" + al.getEntity2().getLocalName()
							+ String.valueOf(value + al.getSimilarityValue()));
					//System.out.println("Computed " + count);
					Logger.getLogger(VDocMatcher.class.getName()).log(Level.INFO, "Computed " + count);
				}
			}
		}

		if (Parameters.inclInstMatch) {
			// System.out.println("Ci sono:"+instanceDocList.size());
			final int instanceNumColumns = instanceDocList.size();
			final BasicMatrix instanceMatrix = calcSimilarity(numRows, instanceNumColumns, ibm, ism, ipm, iom);
			final ArrayList<Object> instanceRowList = new ArrayList<Object>();
			final ArrayList<Object> instanceColumnList = new ArrayList<Object>();
			for (int i = 0, n = instanceDocList.size(); i < n; i++) {
				if (i < instanceSeparator) {
					instanceRowList.add(instanceDocList.get(i));
				} else {
					instanceColumnList.add(instanceDocList.get(i));
				}
			}
			namedInstanceMatrix = new NamedMatrix(instanceRowList, instanceColumnList);
			for (int i = 0, m = namedInstanceMatrix.numRows(); i < m; i++) {
				for (int j = 0, n = namedInstanceMatrix.numColumns(); j < n; j++) {
					final double value = instanceMatrix.get(i, j + instanceSeparator);
					namedInstanceMatrix.set(i, j, value);
					if (this.InputMatrix.isEmpty()) {
						OutputSimMatrix.add(new EntityAlignment((OntoResource) namedInstanceMatrix.getRow(i),
								(OntoResource) namedInstanceMatrix.getColumn(j), '=', value));
						result.addLog(((OntoResource) namedInstanceMatrix.getRow(i)).getLabel() + "-"
								+ ((OntoResource) namedInstanceMatrix.getColumn(j)).getLabel() + ":" + value);
						// System.out.println(((OntoResource)namedInstanceMatrix.getRow(i)).getLabel()+"-"+((OntoResource)namedInstanceMatrix.getColumn(j)).getLabel()+":"+value);
					} else {
						final EntityAlignment al = findAlignment((OntoResource) namedInstanceMatrix.getRow(i),
								(OntoResource) namedInstanceMatrix.getColumn(j));
						OutputSimMatrix.add(new EntityAlignment(al.getEntity1(), al.getEntity2(), '=',
								value + al.getSimilarityValue()));
						result.addLog(al.getEntity1().getLabel() + "-" + al.getEntity2().getLabel()
								+ String.valueOf(value + al.getSimilarityValue()));
					}
				}
			}

		}
		return OutputSimMatrix;
	}

	private double getTermWeight(final double termtimes, final double doctimes, final double maxterm,
			final double maxdoc) {
		if (termtimes == 0) {
			return 0;
		}
		final double tf = termtimes / maxterm;
		final double idf = (1 + Math.log(maxdoc / doctimes) / Math.log(2)) / 2;
		return (tf * idf);
	}

	public NamedMatrix getClassMatrix() {
		return namedClassMatrix;
	}

	public NamedMatrix getPropertyMatrix() {
		return namedPropertyMatrix;
	}

	public NamedMatrix getInstanceMatrix() {
		return namedInstanceMatrix;
	}

	public MatcherReturn getResult() {
		return this.result;
	}

	private EntityAlignment findAlignment(final OntoResource left, final OntoResource right) {
		final Iterator<EntityAlignment> it = InputMatrix.get(0).iterator();
		EntityAlignment al = null;
		while (it.hasNext()) {
			al = it.next();
			if (al.getEntity1().getURIString().equals(left.getURIString())
					&& al.getEntity2().getURIString().equals(right.getURIString())) {
				return al;
			}
		}
		return al;
	}

	/*
	 * public static void main(String args[]) { OntDocumentManager mgr = new
	 * OntDocumentManager(); mgr.setProcessImports(false); OntModelSpec spec = new
	 * OntModelSpec(OntModelSpec.OWL_MEM); spec.setDocumentManager(mgr); OntModel
	 * model1 = ModelFactory.createOntologyModel(spec, null); String op1 =
	 * "file:./sample/101.owl"; model1.read(op1); OntModel model2 =
	 * ModelFactory.createOntologyModel(spec, null); String op2 =
	 * "file:./sample/304.owl"; model2.read(op2); RBGModel rbgModelString1 =
	 * RBGModelFactory.createModel("VDOC_MODEL");
	 * rbgModelString1.setOntModel(model1); RBGModel rbgModelString2 =
	 * RBGModelFactory.createModel("VDOC_MODEL");
	 * rbgModelString2.setOntModel(model2); VDocMatcher vdocMatcher = new
	 * VDocMatcher(rbgModelString1, rbgModelString2); vdocMatcher.match(); Alignment
	 * align = vdocMatcher.getAlignment(); AlignmentWriter2 writer = new
	 * AlignmentWriter2(align, "./sample/falcon.rdf"); writer.write("onto1",
	 * "onto2", "uri1", "uri2"); AlignmentReader2 reader = new AlignmentReader2(op1,
	 * op2, "./sample/refalign.rdf"); Alignment ref = reader.read(); Evaluator
	 * evaluator = new Evaluator(); evaluator.compare(align, ref); }
	 */
}
