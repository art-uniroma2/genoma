/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.MatcherModuleClient;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.codec.digest.DigestUtils;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.AlignmentTools.OntoResource;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.ComposerAggregator.TripleStore;
import it.uniroma2.art.genoma.CustomExceptions.InvalidMatchingParamsException;

/**
 *
 * @author Roberto Enea
 */
public class GraphExtractor extends Matcher {

	private boolean includeClasses;
	private boolean includeProperties;
	private boolean includeInstances;
	private double threshold;

	public GraphExtractor(final MatchingResource ontology1, final MatchingResource ontology2,
			final TripleStore tripleStore, final ArrayList<Collection<EntityAlignment>> SimMatrix,
			final String options) {
		super(ontology1, ontology2, tripleStore, SimMatrix, options);
		matcherId = "Graph Extractor";
		this.alignmentId = DigestUtils.shaHex(this.ontology1.getUri() + this.ontology2.getUri() + this.matcherId);
		this.includeClasses = false;
		this.includeProperties = false;
		this.includeInstances = false;
	}

	@Override
	protected void matchingAlgorithm() {
		try {
			Logger.getLogger(GraphExtractor.class.getName()).log(Level.INFO, matcherId);
			result.addLog("*****************************************************************");
			result.addLog(matcherId);
			result.addLog("*****************************************************************");
			parseOptions();
			OutputSimMatrix = filterMatrix(InputSimMatrix.get(0));
			// normalizeSimMatrix();
			deleteDuplicate();
			result.setSimMatrix(OutputSimMatrix);
		} catch (final InvalidMatchingParamsException e) {
			Logger.getLogger(GraphExtractor.class.getName()).log(Level.SEVERE, e.getMessage());
			result.setError();
			result.setErrorMessage(e.getMessage());
			result.addLog(e.getMessage());
		}
	}

	@Override
	protected void parseOptions() throws InvalidMatchingParamsException {
		final String[] tmp = options.split("-");
		for (final String element : tmp) {
			if (element.contains("IncCL=true")) {
				includeClasses = true;
			} else if (element.contains("IncPR=true")) {
				includeProperties = true;
			} else if (element.contains("IncIN=true")) {
				includeInstances = true;
			} else if (element.contains("thd")) {
				threshold = Double.parseDouble(
						element.substring(element.lastIndexOf("thd=") + ("thd=").length(), element.length()).trim());
			}
		}
	}

	private Collection<EntityAlignment> filterMatrix(final Collection<EntityAlignment> in) {
		final Collection<EntityAlignment> res = new ArrayList<>();
		if (includeClasses && includeProperties && includeInstances) {
			res.addAll(in);
			return res;
		}
		final Iterator<EntityAlignment> it = in.iterator();
		while (it.hasNext()) {
			final EntityAlignment al = it.next();
			if (al.getSimilarityValue() < threshold) {
				al.setSimilarityValue(0.0);
			}
			if (includeClasses && al.getEntity1().getCategory() == OntoResource.CLASS)
				res.add(al);
			else if (includeProperties && (al.getEntity1().getCategory() == OntoResource.PROPERTY
					|| al.getEntity1().getCategory() == OntoResource.DATATYPEPROPERTY
					|| al.getEntity1().getCategory() == OntoResource.OBJECTPROPERTY))
				res.add(al);
			else if (includeInstances && al.getEntity1().getCategory() == OntoResource.INSTANCE)
				res.add(al);

		}
		return res;
	}

	private void deleteDuplicate() {
		final LinkedHashMap<String, EntityAlignment> res = new LinkedHashMap<>();
		final Iterator<EntityAlignment> it = OutputSimMatrix.iterator();
		while (it.hasNext()) {
			final EntityAlignment al = it.next();
			if (al.getEntity1().getCategory() == al.getEntity2().getCategory()) {
				res.put(al.getAlignmentId(), al);
			}
		}
		OutputSimMatrix.clear();
		OutputSimMatrix.addAll(res.values());
	}

}
