/**
 * Copyright 2010 Websoft research group, Nanjing University, PR China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.uniroma2.art.genoma.MatcherModuleClient.gmo;

import it.uniroma2.art.genoma.AlignmentTools.OntoResource;

/**
 * @author Wei Hu & Ningsheng Jian
 * @see http://ws.nju.edu.cn
 */
public class Quadruple {
	private String statement = null;
	private OntoResource subject = null;
	private OntoResource predicate = null;
	private OntoResource object = null;

	public Quadruple(final String stat, final OntoResource s, final OntoResource p, final OntoResource o) {
		statement = stat;
		subject = s;
		predicate = p;
		object = o;
	}

	public String getStatement() {
		return statement;
	}

	public OntoResource getSubject() {
		return subject;
	}

	public OntoResource getPredicate() {
		return predicate;
	}

	public OntoResource getObject() {
		return object;
	}

	public void setStatement(final String stat) {
		statement = stat;
	}

	public void setSubject(final OntoResource s) {
		subject = s;
	}

	public void setPredicate(final OntoResource p) {
		predicate = p;
	}

	public void setObject(final OntoResource o) {
		object = o;
	}

	@Override
	public String toString() {
		return statement.toString();
	}
}
