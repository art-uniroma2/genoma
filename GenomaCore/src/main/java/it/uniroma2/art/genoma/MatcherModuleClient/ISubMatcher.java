/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.MatcherModuleClient;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.codec.digest.DigestUtils;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.ComposerAggregator.TripleStore;

/**
 *
 * @author Roberto Enea
 */

public class ISubMatcher extends Matcher {

	public ISubMatcher(final MatchingResource ontology1, final MatchingResource ontology2,
			final TripleStore tripleStore, final ArrayList<Collection<EntityAlignment>> SimMatrix,
			final String options) {
		super(ontology1, ontology2, tripleStore, SimMatrix, options);
		matcherId = "ISub Matcher";
		this.alignmentId = DigestUtils.shaHex(this.ontology1.getUri() + this.ontology2.getUri() + this.matcherId);
	}

	@Override
	protected double similarityFunction(final String l1, final String l2, final String c1, final String c2,
			final String n1, final String n2) {
		return Math.max(iSubFunction(l1, l2) * labelWeight,
				Math.max(iSubFunction(c1, c2) * commentWeight, iSubFunction(n1, n2) * idWeight));
	}

	@Override
	protected double similarityFunction(final String l1, final String l2, final String n1, final String n2) {
		return Math.max(iSubFunction(l1, l2) * labelWeight, iSubFunction(n1, n2) * idWeight);
	}

	@Override
	protected double similarityFunction(final String n1, final String n2) {
		return iSubFunction(n1, n2) * idWeight;
	}

	private double score(final String st1, final String st2) {
		if (st1 == null || st2 == null || st1.length() == 0 || st2.length() == 0) {
			return -1;
		}
		String s1 = st1.toLowerCase();
		String s2 = st2.toLowerCase();
		s1 = normalizeString(s1, '.');
		s2 = normalizeString(s2, '.');
		s1 = normalizeString(s1, '_');
		s2 = normalizeString(s2, '_');
		s1 = normalizeString(s1, ' ');
		s2 = normalizeString(s2, ' ');
		int l1 = s1.length(), l2 = s2.length();
		final int L1 = l1, L2 = l2;
		if ((L1 == 0) && (L2 == 0)) {
			return 0;
		}
		if ((L1 == 0) || (L2 == 0)) {
			return 1;
		}
		double common = 0;
		int best = 2;
		while (s1.length() > 0 && s2.length() > 0 && best != 0) {
			best = 0;
			l1 = s1.length();
			l2 = s2.length();
			int i = 0, j = 0;
			int startS1 = 0, endS1 = 0;
			int startS2 = 0, endS2 = 0;
			int p = 0;
			for (i = 0; (i < l1) && (l1 - i > best); i++) {
				j = 0;
				while (l2 - j > best) {
					int k = i;
					for (; (j < l2) && (s1.charAt(k) != s2.charAt(j)); j++) {
					}
					if (j != l2) {
						p = j;
						for (j++, k++; (j < l2) && (k < l1) && (s1.charAt(k) == s2.charAt(j)); j++, k++) {
						}
						if (k - i > best) {
							best = k - i;
							startS1 = i;
							endS1 = k;
							startS2 = p;
							endS2 = j;
						}
					}
				}
			}
			char[] newString = new char[s1.length() - (endS1 - startS1)];
			j = 0;
			for (i = 0; i < s1.length(); i++) {
				if (i >= startS1 && i < endS1) {
					continue;
				}
				newString[j++] = s1.charAt(i);
			}
			s1 = new String(newString);
			newString = new char[s2.length() - (endS2 - startS2)];
			j = 0;
			for (i = 0; i < s2.length(); i++) {
				if (i >= startS2 && i < endS2) {
					continue;
				}
				newString[j++] = s2.charAt(i);
			}
			s2 = new String(newString);
			if (best > 2) {
				common += best;
			} else {
				best = 0;
			}
		}
		double commonality = 0;
		final double scaledCommon = 2 * common / (L1 + L2);
		commonality = scaledCommon;
		final double winklerImprovement = winklerImprovement(st1, st2, commonality);
		double dissimilarity = 0;
		final double rest1 = L1 - common;
		final double rest2 = L2 - common;
		double unmatchedS1 = rest1 / L1;
		double unmatchedS2 = rest2 / L2;
		final double suma = unmatchedS1 + unmatchedS2;
		final double product = unmatchedS1 * unmatchedS2;
		final double p = 0.6;
		if ((suma - product) == 0) {
			dissimilarity = 0;
		} else {
			dissimilarity = (product) / (p + (1 - p) * (suma - product));
		}
		return commonality - dissimilarity + winklerImprovement;
	}

	private double winklerImprovement(final String s1, final String s2, final double commonality) {
		int i;
		final int n = Math.min(s1.length(), s2.length());
		for (i = 0; i < n; i++) {
			if (s1.charAt(i) != s2.charAt(i)) {
				break;
			}
		}
		final double commonPrefixLength = Math.min(4, i);
		final double winkler = commonPrefixLength * 0.1 * (1 - commonality);
		return winkler;
	}

	private String normalizeString(final String str, final char remo) {
		final StringBuffer strBuf = new StringBuffer();
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) != remo) {
				strBuf.append(str.charAt(i));
			}
		}
		return strBuf.toString();
	}

	private double iSubFunction(final String s1, final String s2) {
		if(s1 == null || s2 == null) {
			return -1;
		}
		final int s1l = s1.length(), s2l = s2.length();
		if (s1l <= 2 || s2l <= 2) {
			return -1;
		} else {
			return score(s1, s2);
		}
	}

}
