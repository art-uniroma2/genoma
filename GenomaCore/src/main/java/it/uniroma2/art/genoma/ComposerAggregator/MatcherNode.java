/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.ComposerAggregator;

import java.util.ArrayList;
import java.util.Collection;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;

/**
 *
 * @author Roberto Enea
 */
public class MatcherNode {
	private int id; /*
					 * identificativo univoco del nodo all'interno dell'architettura
					 */
	private String MatcherType; /* identificativo univoco del tipo di matcher */
	private ArrayList<Collection<EntityAlignment>> InputSimMatrix;
	private Collection<EntityAlignment> OutputSimMatrix;
	private double weight;
	private String options;
	private String log;

	public MatcherNode(final int id, final String mt, final double wt) {
		this.OutputSimMatrix = new ArrayList<EntityAlignment>();
		this.id = id;
		this.MatcherType = mt;
		this.weight = wt;
		this.options = "";
		this.InputSimMatrix = new ArrayList<Collection<EntityAlignment>>();
	}

	public MatcherNode(final int id, final String mt, final double wt, final String options) {
		this.OutputSimMatrix = new ArrayList<EntityAlignment>();
		this.id = id;
		this.MatcherType = mt;
		this.weight = wt;
		this.options = options;
		this.InputSimMatrix = new ArrayList<Collection<EntityAlignment>>();
	}

	public String getMatcherType() {
		return this.MatcherType;
	}

	public int getId() {
		return this.id;
	}

	public Collection<EntityAlignment> getOutputSimMatrix() {
		return this.OutputSimMatrix;
	}

	public ArrayList<Collection<EntityAlignment>> getInputSimMatrix() {
		return this.InputSimMatrix;
	}

	public void setOutputSimMatrix(final Collection<EntityAlignment> sm) {
		this.OutputSimMatrix = sm;
	}

	public void setInputSimMatrix(final ArrayList<Collection<EntityAlignment>> sm) {
		this.InputSimMatrix = sm;
	}

	public void addToInputSimMatrix(final Collection<EntityAlignment> sm) {
		this.InputSimMatrix.add(sm);
	}

	public double getWeight() {
		return this.weight;
	}

	public String getLog() {
		return this.log;
	}

	public void setLog(final String l) {
		this.log = l;
	}

	@Override
	public String toString() {
		// return "Node:"+ Integer.toString(id) + " MatcherType:" + MatcherType;
		return Integer.toString(id) + " " + MatcherType;
	}

	public String getOptions() {
		return this.options;
	}

	public void setOptions(final String options) {
		this.options = options;
	}

}
