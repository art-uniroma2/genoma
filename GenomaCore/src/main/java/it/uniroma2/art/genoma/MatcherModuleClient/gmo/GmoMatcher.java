/**
 * Copyright 2010 Websoft research group, Nanjing University, PR China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.uniroma2.art.genoma.MatcherModuleClient.gmo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.eclipse.rdf4j.repository.RepositoryConnection;
//import nju.websoft.falcon.model.Node;
//import nju.websoft.falcon.model.NodeCategory;
//import nju.websoft.falcon.model.Quadruple;
//import nju.websoft.falcon.output.AlignmentSelector;
//import nju.websoft.falcon.output.Alignment;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.AlignmentTools.OntoResource;
import it.uniroma2.art.genoma.MatcherModuleClient.MatcherReturn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Wei Hu & Ningsheng Jian
 * @see http://ws.nju.edu.cn
 */
public class GmoMatcher {
	private static final Logger logger = LoggerFactory.getLogger(GmoMatcher.class);
	private GmoParser parserA = null;
	private GmoParser parserB = null;
	private ExternalMatch external = null;
	private NamedMatrix Pk = null, Ck = null, Ik = null;
	private NamedMatrix EPba = null, ECba = null, EIba = null;
	private NamedMatrix Sk = null;
	private NamedMatrix Aeps = null, Aecs = null, Aeis = null;
	private NamedMatrix Aps = null, Acs = null, Ais = null;
	private NamedMatrix Aep = null, Aec = null, Aei = null;
	private NamedMatrix Apop = null, Acop = null, Aiop = null;
	private NamedMatrix Beps = null, Becs = null, Beis = null;
	private NamedMatrix Bps = null, Bcs = null, Bis = null;
	private NamedMatrix Bep = null, Bec = null, Bei = null;
	private NamedMatrix Bpop = null, Bcop = null, Biop = null;
	private ArrayList<Collection<EntityAlignment>> InputMatrix;
	private MatcherReturn result;

	public GmoMatcher(final RepositoryConnection connA, final RepositoryConnection connB,
			final ArrayList<Collection<EntityAlignment>> im, final MatcherReturn res) {
		parserA = new GmoParser(connA);
		parserB = new GmoParser(connB);
		InputMatrix = im;
		result = res;
	}

	public void setExternalMatch(final ExternalMatch ext) {
		external = ext;
	}

	private void initMatrix() {
		final ArrayList<Object> nodesAep = new ArrayList<Object>();
		final ArrayList<Object> nodesAec = new ArrayList<Object>();
		final ArrayList<Object> nodesAei = new ArrayList<Object>();

		final ArrayList<Object> nodesAop = new ArrayList<Object>();
		final ArrayList<Object> nodesAoc = new ArrayList<Object>();
		final ArrayList<Object> nodesAoi = new ArrayList<Object>();
		final ArrayList<Object> nodesAstmt = new ArrayList<Object>();

		final ArrayList<Object> nodesBep = new ArrayList<Object>();
		final ArrayList<Object> nodesBec = new ArrayList<Object>();
		final ArrayList<Object> nodesBei = new ArrayList<Object>();

		final ArrayList<Object> nodesBop = new ArrayList<Object>();
		final ArrayList<Object> nodesBoc = new ArrayList<Object>();
		final ArrayList<Object> nodesBoi = new ArrayList<Object>();
		final ArrayList<Object> nodesBstmt = new ArrayList<Object>();

		final Iterator<OntoResource> nodesA = parserA.getNodeList().iterator();
		while (nodesA.hasNext()) {
			final OntoResource node = nodesA.next();
			switch (NodeCategory.getCategory(node)) {
			case Constants.STMT:
				if (node.getLevel() == OntoResource.EXTERNAL) {
					break;
				} else {
					nodesAstmt.add(node);
				}
				break;
			case Constants.EXTERNAL_PROPERTY:
				nodesAep.add(node);
				break;
			case Constants.EXTERNAL_CLASS:
				nodesAec.add(node);
				break;
			case Constants.EXTERNAL_INSTANCE:
				nodesAei.add(node);
				break;
			case Constants.ONTOLOGY_PROPERTY:
				nodesAop.add(node);
				break;
			case Constants.ONTOLOGY_CLASS:
				nodesAoc.add(node);
				break;
			case Constants.ONTOLOGY_INSTANCE:
				if (Parameters.inclInstMatch == false) {
					nodesAei.add(node);
				} else {
					nodesAoi.add(node);
				}
				break;
			default:
				break;
			}
		}
		final Iterator<OntoResource> nodesB = parserB.getNodeList().iterator();
		while (nodesB.hasNext()) {
			final OntoResource node = (OntoResource) nodesB.next();
			switch (NodeCategory.getCategory(node)) {
			case Constants.STMT:
				if (node.getLevel() == OntoResource.EXTERNAL) {
					break;
				} else {
					nodesBstmt.add(node);
				}
				break;
			case Constants.EXTERNAL_PROPERTY:
				nodesBep.add(node);
				break;
			case Constants.EXTERNAL_CLASS:
				nodesBec.add(node);
				break;
			case Constants.EXTERNAL_INSTANCE:
				nodesBei.add(node);
				break;
			case Constants.ONTOLOGY_PROPERTY:
				nodesBop.add(node);
				break;
			case Constants.ONTOLOGY_CLASS:
				nodesBoc.add(node);
				break;
			case Constants.ONTOLOGY_INSTANCE:
				if (Parameters.inclInstMatch == false) {
					nodesBei.add(node);
				} else {
					nodesBoi.add(node);
				}
				break;
			default:
				break;
			}
		}

		Aeps = new NamedMatrix(nodesAep, nodesAstmt);
		Aecs = new NamedMatrix(nodesAec, nodesAstmt);
		Aeis = new NamedMatrix(nodesAei, nodesAstmt);

		Aep = new NamedMatrix(nodesAstmt, nodesAep);
		Aec = new NamedMatrix(nodesAstmt, nodesAec);
		Aei = new NamedMatrix(nodesAstmt, nodesAei);

		Aps = new NamedMatrix(nodesAop, nodesAstmt);
		Acs = new NamedMatrix(nodesAoc, nodesAstmt);
		Ais = new NamedMatrix(nodesAoi, nodesAstmt);

		Apop = new NamedMatrix(nodesAstmt, nodesAop);
		Acop = new NamedMatrix(nodesAstmt, nodesAoc);
		Aiop = new NamedMatrix(nodesAstmt, nodesAoi);

		Beps = new NamedMatrix(nodesBep, nodesBstmt);
		Becs = new NamedMatrix(nodesBec, nodesBstmt);
		Beis = new NamedMatrix(nodesBei, nodesBstmt);

		Bep = new NamedMatrix(nodesBstmt, nodesBep);
		Bec = new NamedMatrix(nodesBstmt, nodesBec);
		Bei = new NamedMatrix(nodesBstmt, nodesBei);

		Bps = new NamedMatrix(nodesBop, nodesBstmt);
		Bcs = new NamedMatrix(nodesBoc, nodesBstmt);
		Bis = new NamedMatrix(nodesBoi, nodesBstmt);

		Bpop = new NamedMatrix(nodesBstmt, nodesBop);
		Bcop = new NamedMatrix(nodesBstmt, nodesBoc);
		Biop = new NamedMatrix(nodesBstmt, nodesBoi);

		initAdjacencyMatrix();

		EPba = new NamedMatrix(nodesBep, nodesAep);
		ECba = new NamedMatrix(nodesBec, nodesAec);
		EIba = new NamedMatrix(nodesBei, nodesAei);

		initExternalSim();

		Pk = new NamedMatrix(nodesBop, nodesAop);
		Ck = new NamedMatrix(nodesBoc, nodesAoc);
		Ik = new NamedMatrix(nodesBoi, nodesAoi);

		for (int i = 0; i < nodesBop.size(); i++) {
			for (int j = 0; j < nodesAop.size(); j++) {
				Pk.set(i, j, 1);
			}
		}
		for (int i = 0; i < nodesBoc.size(); i++) {
			for (int j = 0; j < nodesAoc.size(); j++) {
				Ck.set(i, j, 1);
			}
		}
		for (int i = 0; i < nodesBoi.size(); i++) {
			for (int j = 0; j < nodesAoi.size(); j++) {
				Ik.set(i, j, 1);
			}
		}
		Sk = new NamedMatrix(nodesBstmt, nodesAstmt);

		for (int i = 0; i < nodesBstmt.size(); i++) {
			for (int j = 0; j < nodesAstmt.size(); j++) {
				Sk.set(i, j, 1);
			}
		}

	}

	private void initAdjacencyMatrix() {
		final Iterator<Quadruple> quadruplesA = parserA.getQuadrupleList().iterator();
		while (quadruplesA.hasNext()) {
			final Quadruple quadruple = quadruplesA.next();
			final OntoResource subject = quadruple.getSubject();
			final OntoResource predicate = quadruple.getPredicate();
			final OntoResource object = quadruple.getObject();
			final String stmt = quadruple.getStatement();
			if ((subject.getLevel() == OntoResource.EXTERNAL || subject.getCategory() == OntoResource.INSTANCE
					|| subject.getLevel() == OntoResource.LANGUAGE_LEVEL)
					&& (predicate.getLevel() == OntoResource.EXTERNAL
							|| predicate.getLevel() == OntoResource.LANGUAGE_LEVEL)
					&& (object.getLevel() == OntoResource.EXTERNAL
							|| object.getLevel() == OntoResource.LANGUAGE_LEVEL)) {
				continue;
			}
			switch (NodeCategory.getCategory(subject)) {
			case Constants.EXTERNAL_PROPERTY:
				Aeps.set(subject, stmt, 1);
				break;
			case Constants.EXTERNAL_CLASS:
				Aecs.set(subject, stmt, 1);
				break;
			case Constants.EXTERNAL_INSTANCE:
				Aeis.set(subject, stmt, 1);
				break;
			case Constants.ONTOLOGY_PROPERTY:
				Aps.set(subject, stmt, 1);
				break;
			case Constants.ONTOLOGY_CLASS:
				Acs.set(subject, stmt, 1);
				break;
			case Constants.ONTOLOGY_INSTANCE:
				if (Parameters.inclInstMatch == false) {
					Aeis.set(subject, stmt, 1);
				} else {
					Ais.set(subject, stmt, 1);
				}
				break;
			default:
				break;
			}
			switch (NodeCategory.getCategory(predicate)) {
			case Constants.EXTERNAL_PROPERTY:
				Aep.set(stmt, predicate, 1);
				break;
			case Constants.EXTERNAL_CLASS:
				Aec.set(stmt, predicate, 1);
				break;
			case Constants.EXTERNAL_INSTANCE:
				Aei.set(stmt, predicate, 1);
				break;
			case Constants.ONTOLOGY_PROPERTY:
				Apop.set(stmt, predicate, 1);
				break;
			case Constants.ONTOLOGY_CLASS:
				Acop.set(stmt, predicate, 1);
				break;
			case Constants.ONTOLOGY_INSTANCE:
				if (Parameters.inclInstMatch == false) {
					Aei.set(stmt, predicate, 1);
				} else {
					Aiop.set(stmt, predicate, 1);
				}
				break;
			default:
				break;
			}
			switch (NodeCategory.getCategory(object)) {
			case Constants.EXTERNAL_PROPERTY:
				Aep.set(stmt, object, 1);
				break;
			case Constants.EXTERNAL_CLASS:
				Aec.set(stmt, object, 1);
				break;
			case Constants.EXTERNAL_INSTANCE:
				Aei.set(stmt, object, 1);
				break;
			case Constants.ONTOLOGY_PROPERTY:
				Apop.set(stmt, object, 1);
				break;
			case Constants.ONTOLOGY_CLASS:
				Acop.set(stmt, object, 1);
				break;
			case Constants.ONTOLOGY_INSTANCE:
				if (Parameters.inclInstMatch == false) {
					Aei.set(stmt, object, 1);
				} else {
					Aiop.set(stmt, object, 1);
				}
				break;
			default:
				break;
			}
		}

		final Iterator<Quadruple> quadruplesB = parserB.getQuadrupleList().iterator();
		while (quadruplesB.hasNext()) {
			final Quadruple quadruple = quadruplesB.next();
			final OntoResource subject = quadruple.getSubject();
			final OntoResource predicate = quadruple.getPredicate();
			final OntoResource object = quadruple.getObject();
			final String stmt = quadruple.getStatement();
			if ((subject.getLevel() == OntoResource.EXTERNAL || subject.getCategory() == OntoResource.INSTANCE
					|| subject.getLevel() == OntoResource.LANGUAGE_LEVEL)
					&& (predicate.getLevel() == OntoResource.EXTERNAL
							|| predicate.getLevel() == OntoResource.LANGUAGE_LEVEL)
					&& (object.getLevel() == OntoResource.EXTERNAL
							|| object.getLevel() == OntoResource.LANGUAGE_LEVEL)) {
				continue;
			}
			switch (NodeCategory.getCategory(subject)) {
			case Constants.EXTERNAL_PROPERTY:
				Beps.set(subject, stmt, 1);
				break;
			case Constants.EXTERNAL_CLASS:
				Becs.set(subject, stmt, 1);
				break;
			case Constants.EXTERNAL_INSTANCE:
				Beis.set(subject, stmt, 1);
				break;
			case Constants.ONTOLOGY_PROPERTY:
				Bps.set(subject, stmt, 1);
				break;
			case Constants.ONTOLOGY_CLASS:
				Bcs.set(subject, stmt, 1);
				break;
			case Constants.ONTOLOGY_INSTANCE:
				if (Parameters.inclInstMatch == false) {
					Beis.set(subject, stmt, 1);
				} else {
					Bis.set(subject, stmt, 1);
				}
				break;
			default:
				break;
			}
			switch (NodeCategory.getCategory(predicate)) {
			case Constants.EXTERNAL_PROPERTY:
				Bep.set(stmt, predicate, 1);
				break;
			case Constants.EXTERNAL_CLASS:
				Bec.set(stmt, predicate, 1);
				break;
			case Constants.EXTERNAL_INSTANCE:
				Bei.set(stmt, predicate, 1);
				break;
			case Constants.ONTOLOGY_PROPERTY:
				Bpop.set(stmt, predicate, 1);
				break;
			case Constants.ONTOLOGY_CLASS:
				Bcop.set(stmt, predicate, 1);
				break;
			case Constants.ONTOLOGY_INSTANCE:
				if (Parameters.inclInstMatch == false) {
					Bei.set(stmt, predicate, 1);
				} else {
					Biop.set(stmt, predicate, 1);
				}
				break;
			default:
				break;
			}
			switch (NodeCategory.getCategory(object)) {
			case Constants.EXTERNAL_PROPERTY:
				Bep.set(stmt, object, 1);
				break;
			case Constants.EXTERNAL_CLASS:
				Bec.set(stmt, object, 1);
				break;
			case Constants.EXTERNAL_INSTANCE:
				Bei.set(stmt, object, 1);
				break;
			case Constants.ONTOLOGY_PROPERTY:
				Bpop.set(stmt, object, 1);
				break;
			case Constants.ONTOLOGY_CLASS:
				Bcop.set(stmt, object, 1);
				break;
			case Constants.ONTOLOGY_INSTANCE:
				if (Parameters.inclInstMatch == false) {
					Bei.set(stmt, object, 1);
				} else {
					Biop.set(stmt, object, 1);
				}
				break;
			default:
				break;
			}
		}
	}

	private void initExternalSim() {
		final ArrayList<?> nodesBep = EPba.getRowList();
		final ArrayList<?> nodesAep = EPba.getColList();
		final ArrayList<?> nodesBec = ECba.getRowList();
		final ArrayList<?> nodesAec = ECba.getColList();
		final ArrayList<?> nodesBei = EIba.getRowList();
		final ArrayList<?> nodesAei = EIba.getColList();

		if (!InputMatrix.isEmpty()) {
			external = new ExternalMatch(InputMatrix);
		} else {
			external = new ExternalMatch();
		}
		for (int i = 0; i < nodesBep.size(); i++) {
			for (int j = 0; j < nodesAep.size(); j++) {
				final double sim = external.getSimilarity((OntoResource) nodesBep.get(i),
						(OntoResource) nodesAep.get(j));
				if (sim != 0) {
					EPba.set(i, j, sim);
				}
			}
		}
		for (int i = 0; i < nodesBec.size(); i++) {
			for (int j = 0; j < nodesAec.size(); j++) {
				final double sim = external.getSimilarity((OntoResource) nodesBec.get(i),
						(OntoResource) nodesAec.get(j));
				if (sim != 0) {
					ECba.set(i, j, sim);
				}
			}
		}
		for (int i = 0; i < nodesBei.size(); i++) {
			for (int j = 0; j < nodesAei.size(); j++) {
				final double sim = external.getSimilarity((OntoResource) nodesBei.get(i),
						(OntoResource) nodesAei.get(j));
				if (sim != 0) {
					EIba.set(i, j, sim);
				}
			}
		}
	}

	public Collection<EntityAlignment> match() {
		initMatrix();
		final Collection<EntityAlignment> OutputSimMatrix = new ArrayList();
		double square = 0;
		double norm = 1;

		for (int i = 0; i < Parameters.iterTimes; i++) {
			final BasicMatrix tempPk = new BasicMatrix(Pk);
			final BasicMatrix tempCk = new BasicMatrix(Ck);
			final BasicMatrix tempIk = new BasicMatrix(Ik);
			final BasicMatrix tempSk = new BasicMatrix(Sk);

			Pk.setMatrix(Bps.multi(Sk).multi(Aps.trans()).copyAdd(Bpop.trans().multi(Sk).multi(Apop)));
			Ck.setMatrix(Bcs.multi(Sk).multi(Acs.trans()).copyAdd(Bcop.trans().multi(Sk).multi(Acop)));
			Ik.setMatrix(Bis.multi(Sk).multi(Ais.trans()).copyAdd(Biop.trans().multi(Sk).multi(Aiop)));

			/*
			 * Sk = block11 + block12 + block13 + block21 + block22 + block23 + block31 +
			 * block32 + block33 + block41 + block42 + block43
			 */
			final BasicMatrix block11 = Beps.trans().multi(EPba).multi(Aeps);
			final BasicMatrix block12 = Becs.trans().multi(ECba).multi(Aecs);
			final BasicMatrix block13 = Beis.trans().multi(EIba).multi(Aeis);
			final BasicMatrix block21 = Bep.multi(EPba).multi(Aep.trans());
			final BasicMatrix block22 = Bec.multi(ECba).multi(Aec.trans());
			final BasicMatrix block23 = Bei.multi(EIba).multi(Aei.trans());
			final BasicMatrix block31 = Bpop.multi(tempPk).multi(Apop.trans());
			final BasicMatrix block32 = Bcop.multi(tempCk).multi(Acop.trans());
			final BasicMatrix block33 = Biop.multi(tempIk).multi(Aiop.trans());
			final BasicMatrix block41 = Bps.trans().multi(tempPk).multi(Aps);
			final BasicMatrix block42 = Bcs.trans().multi(tempCk).multi(Acs);
			final BasicMatrix block43 = Bis.trans().multi(tempIk).multi(Ais);
			Sk.setMatrix(block11.copyAdd(block12).copyAdd(block13).copyAdd(block21).copyAdd(block22).copyAdd(block23)
					.copyAdd(block31).copyAdd(block32).copyAdd(block33).copyAdd(block41).copyAdd(block42)
					.copyAdd(block43));

			square = Pk.sumEntriesSquare() + Ck.sumEntriesSquare() + Ik.sumEntriesSquare() + Sk.sumEntriesSquare();

			norm = 1 / Math.sqrt(square);

			Pk.setMatrix(Pk.times(norm));
			Ck.setMatrix(Ck.times(norm));
			Ik.setMatrix(Ik.times(norm));
			Sk.setMatrix(Sk.times(norm));

			if ((i % 2) == 1 && Sk.distance(tempSk) < Parameters.convergence) {
				break;
			}
		}

		Pk.norm();
		Ck.norm();
		Ik.norm();
		Sk.norm();
		int rows = 0, columns = 0;
		ArrayList<Object> rowList = Ck.getRowList(), colList = Ck.getColList();
		ArrayList<Object> newRowList = new ArrayList<Object>();
		ArrayList<Object> newColList = new ArrayList<Object>();
		for (int i = 0; i < rowList.size(); i++) {
			final OntoResource node = (OntoResource) rowList.get(i);
			if (!(node.getCategory() == OntoResource.BLANK)) {
				rows++;
				newRowList.add(node);
			}
		}
		for (int j = 0; j < colList.size(); j++) {
			final OntoResource node = (OntoResource) colList.get(j);
			if (!(node.getCategory() == OntoResource.BLANK)) {
				columns++;
				newColList.add(node);
			}
		}
		final NamedMatrix namedClassMatrix = new NamedMatrix(newRowList, newColList);
		for (int i = 0; i < rows; i++) {
			final int r = Ck.getRowIndex(newRowList.get(i));
			for (int j = 0; j < columns; j++) {
				final int c = Ck.getColumnIndex(newColList.get(j));
				namedClassMatrix.set(i, j, Ck.get(r, c));
				OutputSimMatrix.add(new EntityAlignment((OntoResource) namedClassMatrix.getColumn(j),
						(OntoResource) namedClassMatrix.getRow(i), '=', Ck.get(r, c)));
				result.addLog(((OntoResource) namedClassMatrix.getColumn(j)).getLocalName() + "-"
						+ ((OntoResource) namedClassMatrix.getRow(i)).getLocalName() + ":" + Ck.get(r, c));
			}
		}
		Ck = null;
		int rowsP = 0, columnsP = 0;
		final ArrayList<Object> rowListP = Pk.getRowList(), colListP = Pk.getColList();
		final ArrayList<Object> newRowListP = new ArrayList<Object>();
		final ArrayList<Object> newColListP = new ArrayList<Object>();
		for (int i = 0; i < rowListP.size(); i++) {
			final OntoResource node = (OntoResource) rowListP.get(i);
			if (!(node.getCategory() == OntoResource.BLANK)) {
				rowsP++;
				newRowListP.add(node);
			}
		}
		for (int j = 0; j < colListP.size(); j++) {
			final OntoResource node = (OntoResource) colListP.get(j);
			if (!(node.getCategory() == OntoResource.BLANK)) {
				columnsP++;
				newColListP.add(node);
			}
		}
		final NamedMatrix namedPropertyMatrix = new NamedMatrix(newRowListP, newColListP);
		for (int i = 0; i < rowsP; i++) {
			final int r = Pk.getRowIndex(newRowListP.get(i));
			for (int j = 0; j < columnsP; j++) {
				final int c = Pk.getColumnIndex(newColListP.get(j));
				namedPropertyMatrix.set(i, j, Pk.get(r, c));
				OutputSimMatrix.add(new EntityAlignment((OntoResource) namedPropertyMatrix.getColumn(j),
						(OntoResource) namedPropertyMatrix.getRow(i), '=', Pk.get(r, c)));
				result.addLog(((OntoResource) namedPropertyMatrix.getColumn(j)).getLocalName() + "-"
						+ ((OntoResource) namedPropertyMatrix.getRow(i)).getLocalName() + ":" + Pk.get(r, c));
			}
		}
		Pk = null;
		if (Parameters.inclInstMatch) {
			rows = 0;
			columns = 0;
			rowList = Ik.getRowList();
			colList = Ik.getColList();
			newRowList = new ArrayList<Object>();
			newColList = new ArrayList<Object>();
			for (int i = 0; i < rowList.size(); i++) {
				final OntoResource node = (OntoResource) rowList.get(i);
				if (!(node.getCategory() == OntoResource.BLANK)) {
					rows++;
					newRowList.add(node);
				}
			}
			for (int j = 0; j < colList.size(); j++) {
				final OntoResource node = (OntoResource) colList.get(j);
				if (!(node.getCategory() == OntoResource.BLANK)) {
					columns++;
					newColList.add(node);
				}
			}
			final NamedMatrix namedInstanceMatrix = new NamedMatrix(newRowList, newColList);
			for (int i = 0; i < rows; i++) {
				final int r = Ik.getRowIndex(newRowList.get(i));
				for (int j = 0; j < columns; j++) {
					final int c = Ik.getColumnIndex(newColList.get(j));
					namedInstanceMatrix.set(i, j, Ik.get(r, c));
					OutputSimMatrix.add(new EntityAlignment((OntoResource) namedInstanceMatrix.getColumn(j),
							(OntoResource) namedInstanceMatrix.getRow(i), '=', Ik.get(r, c)));
					result.addLog(((OntoResource) namedInstanceMatrix.getColumn(j)).getLocalName() + "-"
							+ ((OntoResource) namedInstanceMatrix.getRow(i)).getLocalName() + ":" + Ik.get(r, c));
				}
			}
		}
		Ik = null;
		Sk = null;
		EPba = null;
		ECba = null;
		EIba = null;
		Aeps = null;
		Aecs = null;
		Aeis = null;
		Aps = null;
		Acs = null;
		Ais = null;
		Aep = null;
		Aec = null;
		Aei = null;
		Apop = null;
		Acop = null;
		Aiop = null;
		Beps = null;
		Becs = null;
		Beis = null;
		Bps = null;
		Bcs = null;
		Bis = null;
		Bep = null;
		Bec = null;
		Bei = null;
		Bpop = null;
		Bcop = null;
		Biop = null;
		System.gc();
		return OutputSimMatrix;
	}

	public MatcherReturn getResult() {
		return this.result;
	}

}
