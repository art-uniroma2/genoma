/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.DataManagement;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;
import it.uniroma2.art.genoma.ComposerAggregator.Graph;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.LinkedHashMap;

/**
 *
 * @author Roberto
 */
public class EnginesSet {
	private static String filePath;
	//private static OutputStream enginesSet;
	private static LinkedHashMap<String, Engine> enginesRepository = new LinkedHashMap<>();

	public synchronized static Collection<Engine> getEngines() {
		return EnginesSet.enginesRepository.values();
	}

	public synchronized static void addEngine(final Engine engine) throws IOException {
		final String key = DigestUtils.shaHex(engine.getId());
		if (EnginesSet.enginesRepository.containsKey(key))
			EnginesSet.enginesRepository.remove(key);
		EnginesSet.enginesRepository.put(key, engine);
	}

	/*
	public synchronized static void init(final OutputStream engSet) {
		enginesSet = engSet;
	}
	 */

	public synchronized static Engine getEngine(final String engine) {
		final String key = DigestUtils.shaHex(engine);
		return EnginesSet.enginesRepository.get(key);
	}

	public synchronized  static Graph getEngineGraph(String enginePath) throws IOException {
		XStream xstream = new XStream();

		xstream.allowTypesByWildcard(new String[] { "it.uniroma2.art.genoma.**" });
		Graph graph;
		try (FileInputStream fis = new FileInputStream(enginePath)) {
			graph = (Graph) xstream.fromXML(IOUtils.toString(fis));
		}
		return graph;
	}

	/*
	public synchronized static OutputStream getEnginesSetOutputStream() {
		return EnginesSet.enginesSet;
	}
	 */

	public synchronized static boolean loadRepositoryFromFile(final String filePath) {
		EnginesSet.filePath = filePath;
		try {
			XStream xstream = new XStream();

			xstream.allowTypesByWildcard(new String[] { "it.uniroma2.art.genoma.**" });
			try (FileInputStream fis = new FileInputStream(filePath)) {
				EnginesSet.enginesRepository = (LinkedHashMap<String, Engine>) xstream
						.fromXML(IOUtils.toString(fis));
			}
			return true;
		} catch (final IOException | XStreamException e) {
			enginesRepository = new LinkedHashMap<>();
			return false;
		}
	}

	public synchronized static void saveRepositoryToFile() throws IOException {
		//DataManagementUtils.saveRepositoryToFile(enginesRepository, enginesSet, filePath);
		DataManagementUtils.saveRepositoryToFile(enginesRepository, null, filePath);
	}
}
