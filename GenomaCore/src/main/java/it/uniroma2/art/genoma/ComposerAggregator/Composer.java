/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.ComposerAggregator;

import com.thoughtworks.xstream.XStream;
import it.uniroma2.art.genoma.AlignmentTools.AlignmentWriter;
import it.uniroma2.art.genoma.AlignmentTools.ChordGraphWriter;
import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.ComposerAggregator.MatcherConfiguration.FileType;
import it.uniroma2.art.genoma.CustomExceptions.MatchingException;
import it.uniroma2.art.genoma.DataManagement.AlignmentResultSet;
import it.uniroma2.art.genoma.MatcherModuleClient.MatcherDictionary;
import it.uniroma2.art.genoma.MatcherModuleClient.MatcherReturn;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Roberto Enea
 */
public class Composer {

	private MatcherConfiguration matcherConfiguration;
	private final int[][] adjMatrix;
	private final ExecutorService pool;
	//private String log;
	private StringBuilder logSB;
	private long startTime;
	private long endTime;
	private int graphIndex = -1;
	private XStream xstream;

	public Composer(final MatcherConfiguration mc) {
		this.matcherConfiguration = mc;
		adjMatrix = matcherConfiguration.getGraph().getAdjacencyMatrix();
		pool = Executors.newFixedThreadPool(matcherConfiguration.getGraph().getNodes().size());
		graphIndex = -1;
		xstream = new XStream();
		logSB = new StringBuilder();
		/*-
		xstream.allowTypesByWildcard(new String[] {
			    "it.uniroma2.art.genoma.**"
			});-*/
	}

	private void printAdjMatrix() {
		final int size = matcherConfiguration.getGraph().getNodes().size();
		for (int i = 0; i < size; i++) {
			Logger.getLogger(Composer.class.getName()).info("");
			for (int j = 0; j < size; j++)
				Logger.getLogger(Composer.class.getName()).info(String.valueOf(adjMatrix[i][j]));
		}
	}

	public void startMatching(ByteArrayOutputStream resultSet) throws InterruptedException, IOException {
		final int size = matcherConfiguration.getGraph().getNodes().size();
		int progress = 0;
		int last = 0;
		try {
			setStartTime(resultSet);
			setStatus("In progress");
			Logger.getLogger(Composer.class.getName()).log(Level.INFO, "Starting Alignment task "+matcherConfiguration.getAlignmentId());
			do {
				for (int i = 0; i < size; i++) {
					if (!AreIncomingArchsExecuted(matcherConfiguration.getGraph().getNodes().get(i).getId()))
						continue;
					aggregationCheck(matcherConfiguration.getGraph().getNodes().get(i).getId());
					pool.execute(new MatcherThread(matcherConfiguration.getGraph().getNodes().get(i), resultSet));
					if (matcherConfiguration.getGraph().getNodes().get(i).getMatcherType().equals("GraphExtractor")) {
						graphIndex = i;
					}
					setExecutingFlag(i);
					last = i;
					progress++;
					Thread.sleep(1000);
					//printAdjMatrix();
				}

				if (IsError()) {
					throw new MatchingException("Error during matching. Check the logs");
				}
				setStatus(String.valueOf(Math.round(progress * 100 / size)) + "%");
			} while (!isAdjMatrixEmpty());

			if (this.graphIndex > -1) {
				final ChordGraphWriter cgw = new ChordGraphWriter(
						matcherConfiguration.getGraph().getNodes().get(graphIndex).getOutputSimMatrix(), 0.05);
				cgw.write(matcherConfiguration, FileType.GRAPH_FILE,
						matcherConfiguration.getOntology1().getUri(), matcherConfiguration.getOntology2().getUri(),
						matcherConfiguration.getOntology1().getUri(), matcherConfiguration.getOntology2().getUri());
			} else {
				setGraphFileToNull(resultSet);
			}
			saveAlignmentToFile(matcherConfiguration, FileType.RES_FILE,
					matcherConfiguration.getGraph().getNodes().get(last).getOutputSimMatrix());
			setStatus("Completed");
		} catch (final MatchingException e) {
			Logger.getLogger(Composer.class.getName()).log(Level.SEVERE, e.getMessage());
			setGraphFileToNull(resultSet);
			setStatus("Error");
		} catch (final IOException ex) {
			Logger.getLogger(Composer.class.getName()).log(Level.SEVERE, ex.getMessage());
			setStatus("Error:" + ex.getMessage());
		} finally {
			setEndTime(resultSet);
			Logger.getLogger(Composer.class.getName()).log(Level.INFO, "Completed Alignment task "
					+matcherConfiguration.getAlignmentId()+" in "+getTotalTime());
			try(OutputStream os = matcherConfiguration.getOutFile(FileType.LOG_FILE)) {
				saveLogToFile(os);
			}
			AlignmentResultSet.saveRepositoryToFile(resultSet);
			pool.shutdown();
			pool.awaitTermination(100, TimeUnit.SECONDS);
		}
	}

	private void setStatus(final String p) {
		//final String test = matcherConfiguration.getAlignmentId();
		AlignmentResultSet.getResult(matcherConfiguration.getAlignmentId()).setStatus(p);
	}

	private void aggregationCheck(final int id) {
		final int size = matcherConfiguration.getGraph().getNodes().size();
		final int[][] tempAdjMatrix = matcherConfiguration.getGraph().getAdjacencyMatrix();
		final ArrayList<Collection<EntityAlignment>> l = new ArrayList<Collection<EntityAlignment>>();
		for (int i = 0; i < size; i++) {
			if (tempAdjMatrix[i][id] == 1
					&& !matcherConfiguration.getGraph().getNodes().get(i).getOutputSimMatrix().isEmpty()) {
				l.add(matcherConfiguration.getGraph().getNodes().get(i).getOutputSimMatrix());
			}
		}
		if (l.isEmpty()) {
			return;
		} else {
			matcherConfiguration.getGraph().getNodes().get(id).setInputSimMatrix(l);
		}

	}

	private boolean AreIncomingArchsExecuted(final int nodeId) {
		final int size = matcherConfiguration.getGraph().getNodes().size();
		for (int i = 0; i < size; i++) {
			if (adjMatrix[i][nodeId] != 0)
				return false;
		}
		return true;
	}

	private void setExecutingFlag(final int id) {
		adjMatrix[id][id] = -1;
	}

	private boolean isAdjMatrixEmpty() {
		final int size = matcherConfiguration.getGraph().getNodes().size();
		for (int i = 0; i < size; i++) {
			if (adjMatrix[i][i] != -2)
				return false;
		}
		return true;
	}

	private boolean IsError() {
		final int size = matcherConfiguration.getGraph().getNodes().size();
		for (int i = 0; i < size; i++) {
			if (adjMatrix[i][i] == -3)
				return true;
		}
		return false;
	}

	public void saveConfiguration(final String filename) throws IOException {
		try (final FileWriter out = new FileWriter(filename)) {
			final String xml = xstream.toXML(this.matcherConfiguration);
			out.write(xml);
		}
	}

	public void loadConfiguration(final String filename) throws IOException {
		final File xml = new File(filename);
		this.matcherConfiguration = (MatcherConfiguration) xstream.fromXML(xml);
	}

	public void addLog(final String l) {
		//this.log += l;
		this.logSB.append(l);
	}

	public void saveLogToFile(final OutputStream os) {
		for (int i = 0; i < matcherConfiguration.getGraph().getNodes().size(); i++) {
			addLog(matcherConfiguration.getGraph().getNodes().get(i).getLog());
		}
		addLog("\nTotal Time:" + getTotalTime());
		try {
			//os.write(this.log.getBytes());
			os.write(this.logSB.toString().getBytes());
			//os.close();
		} catch (final IOException ex) {
			Logger.getLogger(Composer.class.getName()).log(Level.SEVERE, ex.getMessage());
		}
	}

	public void saveAlignmentToFile(MatcherConfiguration matcherConfiguration, FileType fileType,
									final Collection<EntityAlignment> align)
			throws FileNotFoundException, IOException {
		final AlignmentWriter aw = new AlignmentWriter(align);
		aw.write(matcherConfiguration, fileType,
				matcherConfiguration.getOntology1().getUri(), matcherConfiguration.getOntology2().getUri(),
				matcherConfiguration.getOntology1().getUri(), matcherConfiguration.getOntology2().getUri());
	}

	private void setStartTime(ByteArrayOutputStream resultSet) throws IOException {
		startTime = System.currentTimeMillis();
		setStartDateOnDB(resultSet);
	}

	private void setEndTime(ByteArrayOutputStream resultSet) throws IOException {
		endTime = System.currentTimeMillis();
		setEndDateOnDB(resultSet);
	}

	private void setGraphFileToNull(ByteArrayOutputStream resultSet) throws IOException {
		//matcherConfiguration.getOutFile(FileType.GRAPH_FILE).close();
		AlignmentResultSet.getResult(matcherConfiguration.getAlignmentId()).setGraphFile(null);
		AlignmentResultSet.saveRepositoryToFile(resultSet);
	}

	private long getTotalTimeInMs() {
		return endTime - startTime;
	}

	private String getTotalTime() {
		long res = getTotalTimeInMs();
		final double hours = Math.floor(res / 3600000.0);
		res = res % 3600000;
		final double minutes = Math.floor(res / 60000.0);
		res = res % 60000;
		final double seconds = Math.floor(res / 1000.0);
		res = res % 1000;
		return (int) hours + ":" + (int) minutes + ":" + (int) seconds + "." + (int) res;
	}

	private void setStartDateOnDB(ByteArrayOutputStream resultSet) throws IOException {
		final Date date = new Date();
		AlignmentResultSet.getResult(matcherConfiguration.getAlignmentId()).setStartDate(date);
		AlignmentResultSet.saveRepositoryToFile(resultSet);
	}

	private void setEndDateOnDB(ByteArrayOutputStream resultSet) throws IOException {
		final Date date = new Date();
		AlignmentResultSet.getResult(matcherConfiguration.getAlignmentId()).setEndDate(date);
		AlignmentResultSet.saveRepositoryToFile(resultSet);
	}

	private class MatcherThread extends Thread {
		private MatcherNode node;

		// this ByteArrayOutputStream is usually null and it is used mainly for the tests, to avoid the need to
		// use a dedicated file as the outputstream
		private ByteArrayOutputStream byteArrayOutputStream;

		public MatcherThread(final MatcherNode mn, ByteArrayOutputStream byteArrayOutputStream) {
			this.node = mn;
			this.byteArrayOutputStream = byteArrayOutputStream;
		}

		@Override
		public void run() {
			final Collection<EntityAlignment> SimMatrix = new ArrayList<EntityAlignment>();
			MatcherReturn result = null;
			try {

				result = MatcherDictionary.getMatcher(node, matcherConfiguration).execute();
				if (result.IsError())
					throw new MatchingException(result.getErrorMessage());
				if (result.getSimMatrix() != null && !result.getSimMatrix().isEmpty()) {
					final Iterator it = result.getSimMatrix().iterator();
					while (it.hasNext()) {
						final EntityAlignment al = (EntityAlignment) it.next();
						SimMatrix.add(new EntityAlignment(al.getEntity1(), al.getEntity2(), al.getRelationType(),
								al.getSimilarityValue() * node.getWeight()));
					}
				}
				node.setOutputSimMatrix(SimMatrix);
				node.setLog(result.getLog());

				adjMatrix[node.getId()][node.getId()] = -2;
				for (int i = 0; i < matcherConfiguration.getGraph().getNodes().size(); i++) {
					if (i != node.getId()) {
						adjMatrix[node.getId()][i] = 0;
					}
				}
			} catch (final Exception ex) {
				Logger.getLogger(Composer.class.getName()).log(Level.SEVERE, ex.getMessage());
				this.interrupt();
				adjMatrix[node.getId()][node.getId()] = -3;
				if (result != null) {
					node.setLog(result.getLog());
				}
			} finally {
				if (result != null) {
					result.clearLog();
				}
				try {
					AlignmentResultSet.saveRepositoryToFile(byteArrayOutputStream);
				} catch (final IOException ex) {
					Logger.getLogger(Composer.class.getName()).log(Level.SEVERE, ex.getMessage());
				}
			}

		}
	}

}
