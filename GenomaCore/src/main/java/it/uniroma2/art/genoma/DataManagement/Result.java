/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.DataManagement;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author Roberto
 */
public class Result {
	private String ontology1;
	private String ontology2;
	private URL alignmentFile;
	private URL graphFile;
	private URL logFile;
	private URL engine;
	private String alignmentId;
	private String status;
	private Date startTime;
	private Date endTime;

	public Result(final String o1, final String o2, final String en, final String basepath)
			throws MalformedURLException {
		this.ontology1 = o1;
		this.ontology2 = o2;
		this.engine = new URL(en);
		this.status = null;
		this.startTime = null;
		this.endTime = null;
		this.alignmentId = DigestUtils
				.shaHex(this.ontology1.toString() + this.ontology2.toString() + this.engine.toString() + Math.random());
		this.alignmentFile = new URL(basepath + "/results/" + this.alignmentId + ".rdf");
		this.graphFile = new URL(basepath + "results/" + this.alignmentId + ".xml");
		this.logFile = new URL(basepath + "/logs/" + this.alignmentId + ".log");
	}

	public String getOntology1() {
		return this.ontology1;
	}

	public String getOntology2() {
		return this.ontology2;
	}

	public URL getAlignmentFile() {
		return this.alignmentFile;
	}

	public URL getLogFile() {
		return this.logFile;
	}

	public URL getGraphFile() {
		return this.graphFile;
	}

	public boolean graphFileExists() {
		final File f = new File(this.graphFile.getFile());
		final boolean exist = f.exists();
		return exist;
	}

	public URL getEngine() {
		return this.engine;
	}

	public String getStatus() {
		return this.status;
	}

	public Date getStartDate() {
		return this.startTime;
	}

	public Date getEndDate() {
		return this.endTime;
	}

	public void setStatus(final String st) {
		this.status = st;
	}

	public void setStartDate(final Date sd) {
		this.startTime = sd;
	}

	public void setEndDate(final Date ed) {
		this.endTime = ed;
	}

	public void setGraphFile(final URL file) {
		this.graphFile = file;
	}

	public String getAlignmentId() {
		return this.alignmentId;
	}

}
