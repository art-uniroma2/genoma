/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package it.uniroma2.art.genoma.ComposerAggregator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Roberto Enea
 */
public class Evaluator {
	private static final Logger log = LoggerFactory.getLogger(Evaluator.class);
	private final double precision;
	private final double recall;
	private final double fMeasure;
	private final int found;
	private final int existing;
	private final int correct;

	Collection<EntityAlignment> correctAl;
	Collection<EntityAlignment> notCorrectAl;

	public Evaluator(final Collection<EntityAlignment> foundAl, final Collection<EntityAlignment> refal,
			final double alpha) {
		found = foundAl.size();
		existing = refal.size();
		correctAl = new ArrayList<>();
		notCorrectAl = new ArrayList<>();
		final Iterator<EntityAlignment> itAl = foundAl.iterator();
		boolean alfound;
		while (itAl.hasNext()) {
			alfound = false;
			final EntityAlignment al1 = itAl.next();
			final Iterator<EntityAlignment> itRef = refal.iterator();
			while (itRef.hasNext()) {
				final EntityAlignment al2 = itRef.next();
				if (al1.getAlignmentId().equals(al2.getAlignmentId())) {
					correctAl.add(al2);
					alfound = true;
					break;
				}
			}
			if (!alfound)
				notCorrectAl.add(al1);
		}
		correct = correctAl.size();
		precision = (double) correct / found;
		recall = (double) correct / existing;
		fMeasure = (1 + alpha) * (precision * recall) / (alpha * precision + recall);
		refal.removeAll(correctAl);
		final Iterator<EntityAlignment> it = refal.iterator();
		while (it.hasNext()) {
			final EntityAlignment al = it.next();
			log.info(al.getEntity1().getURIString() + "-" + al.getEntity2().getURIString());
		}

		final Iterator<EntityAlignment> it2 = notCorrectAl.iterator();
		while (it2.hasNext()) {
			final EntityAlignment al = it2.next();
			log.info(al.getEntity1().getURIString() + "-" + al.getEntity2().getURIString());
		}
	}

	public double getPrecision() {
		return precision;
	}

	public double getRecall() {
		return recall;
	}

	public double getFMeasure() {
		return fMeasure;
	}

	public int getFound() {
		return found;
	}

	public int getExisting() {
		return existing;
	}

	public int getCorrect() {
		return correct;
	}
}
