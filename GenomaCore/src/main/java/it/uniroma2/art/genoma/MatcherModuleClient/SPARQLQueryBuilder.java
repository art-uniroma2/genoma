package it.uniroma2.art.genoma.MatcherModuleClient;

import java.util.Arrays;
import java.util.List;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.model.vocabulary.SKOS;
import org.eclipse.rdf4j.model.vocabulary.SKOSXL;

import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource.Lexicalization;
import it.uniroma2.art.genoma.ontolex.ONTOLEX;

public class SPARQLQueryBuilder {
	private static final String[] SKOS_LABELS = { "concept", "label" };
	private static final String[] SKOSXL_LABELS = { "concept", "literalForm" };
	private static final String[] RDF_LABELS = { "concept", "label" };
	private static final String[] ONTOLEX_LABELS = { "resource", "writtenRep" };

	public static enum SubType {
		OWL, RDFS
	}

	// @formatter:off
	private static final String RDF_QUERY = "SELECT DISTINCT ?concept ?prop ?label" // +
			+ "	WHERE {" //
			+ "	?concept a <%s> ." //
			+ "	?concept ?prop ?label ." //
			+ "	FILTER(?prop = <" + RDFS.LABEL + "> || ?prop = <" + RDFS.COMMENT + "> ) ." //
			+ "	FILTER(lang(?label) = \"%s\") ." //
			+ "	} ORDER BY ?concept DESC(?prop)";
	private static final String SKOS_QUERY = "SELECT DISTINCT ?concept ?prop ?label" // +
			+ "	WHERE {" //
			+ "	?concept a <%s> ." //
			+ "	?concept ?prop ?label ." //
			+ "	FILTER(?prop = <" + SKOS.PREF_LABEL + "> || ?prop = <" + SKOS.ALT_LABEL + "> ) ." //
			+ "	FILTER(lang(?label) = \"%s\") ." //
			+ "	} ORDER BY ?concept ?prop";
	private static final String SKOSXL_QUERY = " SELECT DISTINCT ?concept ?prop ?xlabel ?literalForm" //
			+ "	WHERE {" //
			+ "	?concept a <%s> ." //
			+ "	?concept ?prop ?xlabel ." //
			+ "	FILTER(?prop = <" + SKOSXL.PREF_LABEL + "> || ?prop = <" + SKOSXL.ALT_LABEL + "> ) ." //
			+ "	?xlabel <" + SKOSXL.LITERAL_FORM + "> ?literalForm." //
			+ "	FILTER(lang(?literalForm) = \"%s\") ." //
			+ "	} ORDER BY ?concept ?prop";
	// @formatter:on

	// construct the complex path from a resource to a LexicalEntry
	private static final String directResToLexicalEntry = getUriForSPARQLQuery(ONTOLEX.IS_DENOTED_BY) + "|^"
			+ getUriForSPARQLQuery(ONTOLEX.DENOTES) + "|" + getUriForSPARQLQuery(ONTOLEX.IS_EVOKED_BY) + "|^"
			+ getUriForSPARQLQuery(ONTOLEX.EVOKES);
	private static final String doubleStepResToLexicalEntry = "(" + getUriForSPARQLQuery(ONTOLEX.LEXICALIZED_SENSE) + "|^"
			+ getUriForSPARQLQuery(ONTOLEX.IS_LEXICALIZED_SENSE_OF) + "|^" + getUriForSPARQLQuery(ONTOLEX.REFERENCE)
			+ "|" + getUriForSPARQLQuery(ONTOLEX.IS_REFERENCE_OF) + ")" + "/(^" + getUriForSPARQLQuery(ONTOLEX.SENSE)
			+ "|" + getUriForSPARQLQuery(ONTOLEX.IS_SENSE_OF) + ")";
	private static final String allResToLexicalEntry = " (" + directResToLexicalEntry + "|" + doubleStepResToLexicalEntry + ") ";

	private static final String canonicalFormOrOtherForm = " (" + getUriForSPARQLQuery(ONTOLEX.CANONICAL_FORM) + " | "
			+ getUriForSPARQLQuery(ONTOLEX.OTHER_FORM) + ") ";

	public static String getQueryFromLexicalization(final Lexicalization lexicalization, final String entityType,
			final String language) throws Exception {
		switch (lexicalization) {
		case RDFS:
			if (entityType == null)
				return String.format(RDF_QUERY, RDFS.CLASS.stringValue(), language);
			else
				return String.format(RDF_QUERY, entityType, language);
		case SKOS:
			if (entityType == null)
				return String.format(SKOS_QUERY, SKOS.CONCEPT.stringValue(), language);
			else
				return String.format(SKOS_QUERY, entityType, language);
		case SKOS_XL:
			if (entityType == null)
				return String.format(SKOSXL_QUERY, SKOS.CONCEPT.stringValue(), language);
			else
				return String.format(SKOSXL_QUERY, entityType, language);
		case ONTOLEX:
			// see for more details https://www.w3.org/2016/05/ontolex/#core

			// first get all the LexicalConcept (or Ontology Entity) connected to the
			// LexicalEntry that are
			// then connected to the form (the Literal). Take also the LexicalEntry and
			// store them in a List.
			// Then get just the LexicalEntry and index only those not present in the List
			// constructued in
			// the first query

			// @formatter:off
			return "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>"
					+ "\nPREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#>"
					+ "\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
					+ "\nSELECT ?resource ?resourceType ?predicate ?writtenRep ?lang ?lexicalEntry"
					+ "\nWHERE{"
					+ "\n?lexicalEntry a " + getUriForSPARQLQuery(ONTOLEX.LEXICAL_ENTRY) + "."
					+ "\n?resource" + allResToLexicalEntry + "?lexicalEntry ."
					+ "\n?lexicalEntry " + canonicalFormOrOtherForm + " ?ontoForm ."
					+ "\n?lexicalEntry ?predicate ?ontoForm ."
					+ "\n?ontoForm " + getUriForSPARQLQuery(ONTOLEX.WRITTEN_REP) + " ?writtenRep ."
					+ "\nFILTER(isLiteral(?writtenRep)) "
					+ "\nFILTER(lang(?writtenRep) = \"" + language + "\") ." //
					// + "\nFILTER(isIRI(?resource)) "
					// + "\n?resource a ?resourceType ."
					+ "\n}";
			// @formatter:on
		}
		throw new Exception(lexicalization.toString() + "is not a valid lexicalization");
	}

	public static String getClassFromLexicalization(final Lexicalization lexicalization, final SubType subType)
			throws Exception {
		switch (lexicalization) {
		case RDFS:
			if (subType.equals(SubType.OWL))
				return OWL.CLASS.stringValue();
			else
				return RDFS.CLASS.stringValue();
		case SKOS:
		case SKOS_XL:
			return SKOS.CONCEPT.stringValue();
		case ONTOLEX:
			return null;
		}
		throw new Exception(lexicalization.toString() + "is not a valid lexicalization");
	}

	public static List<String> getBindingLabels(final Lexicalization lexicalization) throws Exception {
		switch (lexicalization) {
		case RDFS:
			return Arrays.asList(RDF_LABELS);
		case SKOS:
			return Arrays.asList(SKOS_LABELS);
		case SKOS_XL:
			return Arrays.asList(SKOSXL_LABELS);
		case ONTOLEX:
			return Arrays.asList(ONTOLEX_LABELS);
		}
		throw new Exception(lexicalization.toString() + "is not a valid lexicalization");
	}

	public static String getQueryToExtractResourcesFromWrittenRep(String writtenRep, String languageTag) {
		// @formatter:off
		return  "{SELECT ?resource"
				+ "\nWHERE{"
				+ "\n?lexicalEntry a " + getUriForSPARQLQuery(ONTOLEX.LEXICAL_ENTRY) + "."
				+ "\n?resource" + allResToLexicalEntry + "?lexicalEntry ."
				+ "\n?lexicalEntry " + canonicalFormOrOtherForm + " ?ontoForm ."
				+ "\n?lexicalEntry ?predicate ?ontoForm ."
				+ "\n?ontoForm " + getUriForSPARQLQuery(ONTOLEX.WRITTEN_REP) + " \"" + writtenRep + "\"@" + languageTag + " ."
				+ "\n}"
				+ "\n}";
		// @formatter:on
	}

	public static String getQueryForSynomymsFromWrittenRep(String writtenRep, String languageTag) {
		// @formatter:off
		return "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>"
				+ "\nPREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#>"
				+ "\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ "\nSELECT DISTINCT ?writtenRep" + "\nWHERE{"
				+ "\n?lexicalEntry a " + getUriForSPARQLQuery(ONTOLEX.LEXICAL_ENTRY) + "."
				+ "\n?resource" + allResToLexicalEntry + "?lexicalEntry ."
				+ getQueryToExtractResourcesFromWrittenRep(writtenRep, languageTag)
				+ "\n?lexicalEntry " + canonicalFormOrOtherForm + " ?ontoForm ."
				+ "\n?lexicalEntry ?predicate ?ontoForm ."
				+ "\n?ontoForm " + getUriForSPARQLQuery(ONTOLEX.WRITTEN_REP) + " ?writtenRep ."
				+ "\nFILTER(isLiteral(?writtenRep)) " + "\nFILTER(lang(?writtenRep) = \"" + languageTag + "\") ." //
				+ "\n}";
		// @formatter:on
	}

	private static String getUriForSPARQLQuery(IRI resource) {
		return "<" + resource.toString() + ">";
	}

}
