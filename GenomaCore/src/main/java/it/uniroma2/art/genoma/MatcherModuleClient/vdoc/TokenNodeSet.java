/**
 * Copyright 2010 Websoft research group, Nanjing University, PR China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.uniroma2.art.genoma.MatcherModuleClient.vdoc;

import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * @author Wei Hu
 * @see http://ws.nju.edu.cn
 */
//this class has been modified by replacing the various prints of System.out or System.err with a Logger
public class TokenNodeSet {
	private HashMap<String, TokenNode> nodes = new HashMap<String, TokenNode>();

	public Iterator<TokenNode> iterator() {
		return nodes.values().iterator();
	}

	public void add(final TokenNode node) {
		nodes.put(node.getURI(), node);
	}

	public TokenNode get(final String uri) {
		return nodes.get(uri);
	}

	public boolean contains(final String uri) {
		return nodes.containsKey(uri);
	}

	public int size() {
		return nodes.size();
	}

	public void show() {
		for (final TokenNode node : nodes.values()) {
			//System.out.println(node.getURI());
			Logger.getLogger(TokenNodeSet.class.getName()).log(Level.INFO, node.getURI());
			HashMap<String, WordFrequency> t = node.getLocalName();
			if (t != null) {
				for (final WordFrequency wf : t.values()) {
					//System.out.println("->[localname]->" + wf.getWord() + ", " + wf.getFrequency());
					Logger.getLogger(TokenNodeSet.class.getName()).log(Level.INFO, "->[localname]->" + wf.getWord() + ", " + wf.getFrequency());
				}
			}
			t = node.getLabel();
			if (t != null) {
				for (final WordFrequency wf : t.values()) {
					//System.out.println("->[label]->" + wf.getWord() + ", " + wf.getFrequency());
					Logger.getLogger(TokenNodeSet.class.getName()).log(Level.INFO, "->[label]->" + wf.getWord() + ", " + wf.getFrequency());
				}
			}
			t = node.getComment();
			if (t != null) {
				for (final WordFrequency wf : t.values()) {
					//System.out.println("->[comment]->" + wf.getWord() + ", " + wf.getFrequency());
					Logger.getLogger(TokenNodeSet.class.getName()).log(Level.INFO, "->[comment]->" + wf.getWord() + ", " + wf.getFrequency());
				}
			}
			//System.out.println();
			Logger.getLogger(TokenNodeSet.class.getName()).log(Level.INFO, "");
		}
		//System.out.println();
		Logger.getLogger(TokenNodeSet.class.getName()).log(Level.INFO, "");
	}
}
