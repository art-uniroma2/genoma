package it.uniroma2.art.genoma.ComposerAggregator;

import java.net.URL;

public class DataService {
	private String endpointURL;
	private String username;
	private String password;

	public DataService(String endpointURL) {
		this(endpointURL, null, null);
	}

	public DataService(String endpointURL, /* Nullable */ String username, /* Nullable */ String password) {
		this.endpointURL = endpointURL;
		this.username = username;
		this.password = password;
	}

	public String getEndpointURL() {
		return endpointURL;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

}
