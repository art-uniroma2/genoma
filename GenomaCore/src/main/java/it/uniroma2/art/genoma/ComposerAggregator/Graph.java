/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.ComposerAggregator;

/**
 *
 * @author Roberto Enea
 */
import java.util.ArrayList;

public class Graph {

	private ArrayList<MatcherNode> Nodes;
	private ArrayList<GraphArch> Arches;

	public Graph() {
		Nodes = new ArrayList();
		Arches = new ArrayList();
	}

	public void addNode(final MatcherNode n) {
		Nodes.add(n.getId(), n);
	}

	public void addArch(final GraphArch a) {
		Arches.add(a);
	}

	public void removeNode(final int id) {
		Nodes.remove(id);
		for (int i = 0; i < Arches.size(); i++) {
			if (Arches.get(i).getIdNodeFrom() == id || Arches.get(i).getidNodeTo() == id) {
				Arches.remove(id);
			}
		}
	}

	public MatcherNode getMatcherNode(final int id) {
		return Nodes.get(id);
	}

	public int[][] getAdjacencyMatrix() {
		final int[][] adjMatrix = new int[Nodes.size()][Nodes.size()];
		for (int i = 0; i < Arches.size(); i++) {
			adjMatrix[Arches.get(i).getIdNodeFrom()][Arches.get(i).getidNodeTo()] = 1;
		}
		return adjMatrix;
	}

	public ArrayList<MatcherNode> getNodes() {
		return Nodes;
	}

	public ArrayList<GraphArch> getArches() {
		return Arches;
	}

}
