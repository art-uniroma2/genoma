/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.MatcherModuleClient;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.AlignmentTools.OntoResource;
import it.uniroma2.art.genoma.ComposerAggregator.DataService;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource.Lexicalization;
import it.uniroma2.art.genoma.ComposerAggregator.TripleStore;
import it.uniroma2.art.genoma.CustomExceptions.InvalidMatchingParamsException;
import it.uniroma2.art.genoma.MatcherModuleClient.SPARQLQueryBuilder.SubType;
import it.uniroma2.art.genoma.ontolex.ONTOLEX;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.automaton.TooComplexToDeterminizeException;
import org.eclipse.rdf4j.model.BNode;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.model.vocabulary.SKOS;
import org.eclipse.rdf4j.query.Binding;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.http.HTTPRepository;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Roberto Enea
 */
public abstract class Matcher implements Serializable {
	private static final long serialVersionUID = 227L;
	protected Collection<EntityAlignment> OutputSimMatrix;
	protected ArrayList<Collection<EntityAlignment>> InputSimMatrix;
	protected final MatchingResource ontology1;
	protected final MatchingResource ontology2;
	protected final TripleStore tripleStore;
	protected String alignmentId;
	protected final String options;
	protected String matcherId;
	protected MatcherReturn result;
	protected double idWeight;
	protected double labelWeight;
	protected double commentWeight;
	protected boolean inclInstances;
	protected boolean inclExt;
	protected int cFSize;
	protected boolean disabled;

	public Matcher(final MatchingResource ontology1, final MatchingResource ontology2, final TripleStore tripleStore,
			final ArrayList<Collection<EntityAlignment>> SimMatrix, final String options) {
		this.ontology1 = ontology1;
		this.ontology2 = ontology2;
		if (SimMatrix != null) {
			this.InputSimMatrix = SimMatrix;
		} else {
			this.InputSimMatrix = new ArrayList<>();
		}
		this.OutputSimMatrix = new ArrayList<>();
		this.options = options;
		this.result = new MatcherReturn(this.OutputSimMatrix, "");
		this.tripleStore = tripleStore;
		idWeight = -1;
		labelWeight = -1;
		commentWeight = -1;
		cFSize = 5;
		inclInstances = false;
		inclExt = false;
	}

	public MatcherReturn execute() {
		matchingAlgorithm();
		return this.result;
	}

	protected void matchingAlgorithm() {
		RepositoryConnection conn1 = null, conn2 = null;
		try {
			parseOptions();
			if(disabled) {
				result.setSimMatrix(null);
				return;
			}
			final Lexicalization lexicalization1 = ontology1.getLexicalization();
			final Lexicalization lexicalization2 = ontology2.getLexicalization();
			if (InputSimMatrix.isEmpty()) {
				final ArrayList<RepositoryConnection> res = initModels();
				conn1 = res.get(0);
				conn2 = res.get(1);
				if (lexicalization1.equals(Lexicalization.RDFS) && lexicalization2.equals(Lexicalization.RDFS)) {
					compareEntities(conn1, conn2, RDFS.CLASS.stringValue());
					compareEntities(conn1, conn2, OWL.CLASS.stringValue());
					compareEntities(conn1, conn2, OWL.DATATYPEPROPERTY.stringValue());
					compareEntities(conn1, conn2, OWL.OBJECTPROPERTY.stringValue());
					if (inclInstances) {
						compareInstances(conn1, conn2);
					}
				} else {
					compareEntities(conn1, conn2, SKOS.CONCEPT.stringValue());
				}
			} else {
				compareAllEntities();
			}
			normalizeSimMatrix();
			result.setSimMatrix(OutputSimMatrix);
		} catch (final Exception ex) {
			Logger.getLogger(Matcher.class.getName()).log(Level.SEVERE, null, ex);
			result.setError();
			result.setErrorMessage(ex.getMessage());
			result.addLog(ex.getMessage());
		} finally {
			if(conn1 != null && conn2 != null) {
				conn1.close();
				conn2.close();
			}
		}
	}

	protected void parseOptions() throws InvalidMatchingParamsException {
		final String[] tmp = options.split("-");
		for (final String element : tmp) {
			if (element.contains("iw")) {
				idWeight = Double.parseDouble(
						element.substring(element.lastIndexOf("iw=") + ("iw=").length()).trim());
			} else if (element.contains("lw")) {
				labelWeight = Double.parseDouble(
						element.substring(element.lastIndexOf("lw=") + ("lw=").length()).trim());
			} else if (element.contains("cw")) {
				commentWeight = Double.parseDouble(
						element.substring(element.lastIndexOf("cw=") + ("cw=").length()).trim());
			} else if (element.contains("cfsize")) {
				cFSize = Integer.parseInt(element
						.substring(element.lastIndexOf("cfsize=") + ("cfsize=").length()).trim());
			} else if (element.contains("incI=true")) {
				inclInstances = true;
			} else if (element.contains("incE=true")) {
				inclExt = true;
			} else if (element.contains("disabled")) {
				disabled = true;
			}

		}
		if (idWeight == -1)
			throw new InvalidMatchingParamsException("No id Weight Value");
		if (labelWeight == -1)
			throw new InvalidMatchingParamsException("No Label Weight Found");
		if (commentWeight == -1)
			throw new InvalidMatchingParamsException("No Comment Weight Found");
	}

	protected void compareInstances(final RepositoryConnection conn1, final RepositoryConnection conn2)
			throws Exception {
		try (final TupleQueryResult it1 = getCategoryNodesFromOntologyOnLexicalization(conn1, ontology1)) {
			while (it1.hasNext()) {
				final BindingSet tuplebinding1 = it1.next();
				final Value uri1 = tuplebinding1.getBinding("entity").getValue();
				try (final TupleQueryResult it2 = getInstancesOfClass(conn1, uri1.stringValue())) {
					while (it2.hasNext()) {
						final BindingSet tuplebinding2 = it2.next();
						final Value uri2 = tuplebinding2.getBinding("instance").getValue();
						final Value lab1 = tuplebinding2.getBinding("instanceLabel").getValue();
						final String n1 = ((IRI) uri2).getLocalName();
						try (final TupleQueryResult it3 = getCategoryNodesFromOntologyOnLexicalization(conn2, ontology2)) {
							while (it3.hasNext()) {
								final BindingSet tuplebinding3 = it3.next();
								final Value uri3 = tuplebinding3.getBinding("entity").getValue();
								try (final TupleQueryResult it4 = getInstancesOfClass(conn2, uri3.stringValue())) {
									while (it4.hasNext()) {
										final BindingSet tuplebinding4 = it4.next();
										final Value uri4 = tuplebinding4.getBinding("instance").getValue();
										final Value lab2 = tuplebinding4.getBinding("instanceLabel").getValue();
										final String n2 = ((IRI) uri4).getLocalName();
										final double alignment_grade = similarityFunction(lab1.stringValue(), lab2.stringValue(), n1,
												n2);
										OutputSimMatrix.add(new EntityAlignment(
												new OntoResource(uri2.stringValue(), n1, lab1.stringValue(), null,
														OntoResource.INSTANCE),
												new OntoResource(uri4.stringValue(), n2, lab2.stringValue(), null,
														OntoResource.INSTANCE),
												'=', alignment_grade));
										// System.out.println("Equal:" + lab1.stringValue() + "-" + lab2.stringValue() +
										// '\n');
										result.addLog(lab1.stringValue() + "-" + lab2.stringValue() + ":" + alignment_grade);
									}
								}
							}
						}
					}
				}
			}
		}

	}

	protected Map<String, List<String>> populateMap(final String entityType, final RepositoryConnection conn,
													final MatchingResource ontology) throws Exception {
		final Map<String, List<String>> map = new HashMap<>();
		// SPARQL section
		// @formatter:off

		result.addLog("ontology.getLanguage() : "+ontology.getLanguage());
		result.addLog("ontology.getLexicalization() : "+ontology.getLexicalization());

		final String queryString = SPARQLQueryBuilder.getQueryFromLexicalization(ontology.getLexicalization(),
				entityType, ontology.getLanguage());
		final List<String> bindingVariables = SPARQLQueryBuilder.getBindingLabels(ontology.getLexicalization());

		result.addLog("queryString: "+queryString);
		result.addLog("bindingVariables: "+bindingVariables);

		// @formatter:on
		final TupleQuery query = conn.prepareTupleQuery(queryString);
		query.setIncludeInferred(false);
		try (final TupleQueryResult it = query.evaluate()) {
			while (it.hasNext()) {
				final BindingSet tuplebinding = it.next();
				// in labels there is the resource uri (as the first element) and the label value
				final List<String> labels = new ArrayList<>();
				for (final String bindingVariable : bindingVariables) {
					final Binding binding = tuplebinding.getBinding(bindingVariable);
					if (binding != null) {
						final Value label = binding.getValue();
						if (label instanceof BNode) {
							continue;
						}
						if (label instanceof IRI) {
							if (!IsUriInLocalNamespace(conn, (IRI) label) && !inclExt) {
								continue;
							}
						}
						labels.add(label.stringValue());
					}
				}
				if (labels.size() > 1) {
					final String keyLabel = labels.get(0);
					if (map.get(keyLabel) == null) {
						labels.remove(0);
						map.put(keyLabel, labels);
					} else {
						labels.remove(0);
						map.get(keyLabel).addAll(labels);
					}
				}
			}
		}
		result.addLog("After populateMap, map.size(): "+map.size());
		return map;
	}

	protected void compareEntities(final RepositoryConnection conn1, final RepositoryConnection conn2,
			final String entityType) throws Exception {
		result.addLog("entityType: "+entityType);
		// SPARQL section
		final int category = getCategoryFromEntityType(entityType);
		// @formatter:off
		final Map<String, List<String>> map1 = populateMap(entityType, conn1, ontology1);
		if(map1.isEmpty()) {
			result.addLog("Error: no labels collected");
			//throw new Exception("Error: no labels collected");
		}
		result.addLog("map1.size(): "+map1.size());
		final Map<String, List<String>> map2 = populateMap(entityType, conn2, ontology2);
		if(map2.isEmpty()) {
			result.addLog("Error: no labels collected");
			//throw new Exception("Error: no labels collected");
		}
		result.addLog("map2.size(): "+map2.size());
		final boolean ontologyComparison = ontology1.getLexicalization().equals(Lexicalization.RDFS) && //
				ontology2.getLexicalization().equals(Lexicalization.RDFS);
		if (map1.keySet().size() < 100 && map2.keySet().size() < 100) {
			_compareEntities(map1, map2, category, ontologyComparison);
		} else {
			compareEntitiesWithCandidateFiltering(map1, map2, category, ontologyComparison);
		}
	}

	private void _compareEntities(final Map<String, List<String>> map1, final Map<String, List<String>> map2,
			final int category, final boolean ontologyComparison) {
		for (final String element1 : map1.keySet()) {
			for (final String element2 : map2.keySet()) {
				elementsComparison(element1, element2, map1, map2, category, ontologyComparison);
			}
		}
	}

	protected void compareEntitiesWithCandidateFiltering(final Map<String, List<String>> map1,
			final Map<String, List<String>> map2, final int category, final boolean ontologyComparison)
			throws Exception {
		int sizeMap1 = getMapSize(map1);
		int sizeMap2 = getMapSize(map2);
		// put in localSmallerMap the smaller between map1 and map2 and put in localBiggerMap the bigger between map1 and map2
		final Map<String, List<String>> localSmallerMap = sizeMap2 > sizeMap1 ? map1 : map2;
		final Map<String, List<String>> localBiggerMap = sizeMap1 > sizeMap2 ? map1 : map2;
		//final Directory memoryIndex = createMemoryIndex(localMap2);
		final int totalSize = localBiggerMap.keySet().size();
		int counter = 0;
		// start the comparison between each element of the bigger dataset (always in localBiggerMap) and the elements of the
		// smaller dataset (localSmallerMap) that have been stored in a lucene index
		//System.out.println("Starting Computing "+totalSize+" entities"); // foe DEBUG
		Logger.getLogger(Matcher.class.getName()).log(Level.INFO, "Starting Computing "+totalSize+" entities");
		result.addLog("Starting Computing "+totalSize+" entities");


		//create an in-memory lucene index for the smaller-dataset which will be used to extract the candidate elements
		// to be compared to each element of the bigger dataset
		try (final Directory memoryIndex = createMemoryIndex(localSmallerMap);
			final IndexReader indexReader = DirectoryReader.open(memoryIndex)) {
			final IndexSearcher searcher = new IndexSearcher(indexReader);
			//start the comparison between each element of the bigger dataset with the elements from the smaller dataset
			for (final String elementFromBigger : localBiggerMap.keySet()) {
				final List<Document> candidatesFromSmaller = fuzzySearch(searcher, localBiggerMap.get(elementFromBigger));
				//iterate over the candidates, from the smaller datasets (these candidates are the ones that were retrived
				// using lucene giving, as input for the search, one element at a time, from the bigger)
				for (final Document candidateFromSmaller : candidatesFromSmaller) {
					final String elementFromSmaller = candidateFromSmaller.get("key");
					// the first passed element ALWAYS belong to the first dataset, the second element to the second dataset
					elementsComparison(sizeMap1 > sizeMap2 ? elementFromBigger : elementFromSmaller,
							sizeMap1 > sizeMap2 ? elementFromSmaller : elementFromBigger,
							map1, map2, category, ontologyComparison);
				}
				counter++;
				//System.out.println("Computed:" + counter + " entities on " + totalSize);
			}
		}
	}

	protected void elementsComparison(final String element1, final String element2,
			final Map<String, List<String>> map1, final Map<String, List<String>> map2, final int category,
			final boolean ontologyComparison) {
		double alignment_grade = 0;
		String localName1, localName2;
		if (element1.lastIndexOf("#") > -1) {
			localName1 = element1.substring(element1.lastIndexOf("#") + 1 );
		} else {
			localName1 = element1.substring(element1.lastIndexOf("/") + 1 );
		}
		if (element2.lastIndexOf("#") > -1) {
			localName2 = element2.substring(element2.lastIndexOf("#") + 1 );
		} else {
			localName2 = element2.substring(element2.lastIndexOf("/") + 1 );
		}
		final List<String> el1Tokens = map1.get(element1);
		final List<String> el2Tokens = map2.get(element2);

		if (!ontologyComparison) {
			alignment_grade = similarityFunction(localName1, localName2, el1Tokens, el2Tokens);
			OutputSimMatrix.add(new EntityAlignment(new OntoResource(element1, localName1, el1Tokens, category),
					new OntoResource(element2, localName2, el2Tokens, category), '=', alignment_grade));
		} else {
			if (el1Tokens.size() >= 2 && el2Tokens.size() >= 2) {
				alignment_grade = similarityFunction(el1Tokens.get(0), el2Tokens.get(0), el1Tokens.get(1),
						el2Tokens.get(1), localName1, localName2);
				OutputSimMatrix.add(new EntityAlignment(
						new OntoResource(element1, localName1, el1Tokens.get(0), el1Tokens.get(1), category),
						new OntoResource(element2, localName2, el2Tokens.get(0), el2Tokens.get(1), category), '=',
						alignment_grade));
			} else if (!el1Tokens.isEmpty() && !el2Tokens.isEmpty()) {
				alignment_grade = similarityFunction(el1Tokens.get(0), el2Tokens.get(0), localName1, localName2);
				OutputSimMatrix.add(new EntityAlignment(
						new OntoResource(element1, localName1, map1.get(element1).get(0), null, category),
						new OntoResource(element2, localName2, map2.get(element2).get(0), null, category), '=',
						alignment_grade));
			} else {
				alignment_grade = similarityFunction(localName1, localName2);
				OutputSimMatrix.add(new EntityAlignment(new OntoResource(element1, localName1, null, null, category),
						new OntoResource(element2, localName2, null, null, category), '=', alignment_grade));
			}
		}

		StringBuilder sbTemp = new StringBuilder();
		sbTemp.append(localName1).append("(").append(String.join(",", el1Tokens)).append(")-")
				.append(localName2).append("(").append(String.join(",", el2Tokens))
				.append("):").append(alignment_grade);
		result.addLog(sbTemp.toString());

		//result.addLog(localName1 + "(" + String.join(",", el1Tokens) + ")-" + localName2 + "("
		//		+ String.join(",", el2Tokens) + "):" + alignment_grade);
	}

	protected Directory createMemoryIndex(final Map<String, List<String>> map) throws IOException {
		// in the future, RAMDirectory should be replaced with MMapDirectory (but a path should be provided, so a major
		// changes is required)
		final Directory memoryIndex = new RAMDirectory();
		try (final StandardAnalyzer analyzer = new StandardAnalyzer()) {
			final IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);

			final IndexWriter writer = new IndexWriter(memoryIndex, indexWriterConfig);

			for (final String key : map.keySet()) {

				final Document document = new Document();

				document.add(new TextField("key", key, Store.YES));
				document.add(new TextField("label", String.join(",", map.get(key)), Store.YES));

				writer.addDocument(document);

			}
			writer.close();
		}
		return memoryIndex;
	}

	protected List<Document> fuzzySearch(final IndexSearcher searcher, final List<String> terms) throws IOException {
		final List<Document> documents = new ArrayList<>();
		for (final String termString : terms) {
			for (final String token : termString.split(" ")) {
				final Term term = new Term("label", token);
				final Query query = new FuzzyQuery(term);
				try {
					final TopDocs topDocs = searcher.search(query, cFSize);
					for (final ScoreDoc scoreDoc : topDocs.scoreDocs) {
						documents.add(searcher.doc(scoreDoc.doc));
					}
				} catch (final TooComplexToDeterminizeException e) {
					e.printStackTrace();
				}
			}
		}
		return documents;
	}

	protected TupleQueryResult getCategoryNodesFromOntology(final RepositoryConnection model,
			final Lexicalization lexicalization, final String language, final SubType subType) throws Exception {
		// @formatter:off
		final String entityType = SPARQLQueryBuilder.getClassFromLexicalization(lexicalization, subType);
		final String queryString = SPARQLQueryBuilder.getQueryFromLexicalization(lexicalization, entityType, language);
		// @formatter:on
		final TupleQuery query1 = model.prepareTupleQuery(queryString);
		query1.setIncludeInferred(false);
        return query1.evaluate(); // it should be closed by the calling method
	}

	protected TupleQueryResult getCategoryNodesFromOntologyOnLexicalization(final RepositoryConnection conn,
			final MatchingResource ontology) throws Exception {
		TupleQueryResult it = null;
		if (ontology.getLexicalization() == Lexicalization.RDFS) {
			it = getCategoryNodesFromOntology(conn, ontology.getLexicalization(), ontology.getLanguage(), SubType.RDFS);
			if (!it.hasNext()) {
				it.close(); // close "it" since a new TupleQueryResult is created and associated to "it"
				it = getCategoryNodesFromOntology(conn, ontology.getLexicalization(), ontology.getLanguage(),
						SubType.OWL);
			}
		} else if (ontology1.getLexicalization() == Lexicalization.SKOS
				|| ontology1.getLexicalization() == Lexicalization.SKOS_XL) {
			it = getCategoryNodesFromOntology(conn, ontology.getLexicalization(), ontology.getLanguage(), null);
		}
		return it; // it should be closed by the calling method
	}

	protected TupleQueryResult getInstancesOfClass(final RepositoryConnection conn, final String Class) {
		// @formatter:off
		final String prefix = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
				+ "\nPREFIX owl: <http://www.w3.org/2002/07/owl#>\n"
				+ "\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n";
		final String queryString = prefix + "SELECT DISTINCT ?instance ?instanceLabel ?class " + "\nWHERE {"
				+ "\n?class rdfs:subClassOf* <" + Class + "> . " + "\n?instance rdf:type ?class ." + "\n?instance <"
				+ RDFS.LABEL + "> ?instanceLabel" + "\n}";
		// @formatter:on
		final TupleQuery query1 = conn.prepareTupleQuery(queryString);
		query1.setIncludeInferred(false);
        return query1.evaluate(); // it should be closed by the calling method
	}

	protected void compareAllEntities() {
		result.addLog("Input Matrix not null");
		final Iterator<EntityAlignment> it = InputSimMatrix.iterator().next().iterator();
		int count = 0;
		//System.out.println("Started Computing " + InputSimMatrix.get(0).size()+" elements"); // for DEBUG
		Logger.getLogger(Matcher.class.getName()).log(Level.INFO, "Starting Computing " + InputSimMatrix.get(0).size()+" elements");
		while (it.hasNext()) {
			final EntityAlignment al = it.next();
			double alignment_grade = 0;
			final boolean ontologyComparison = ontology1.getLexicalization().equals(Lexicalization.RDFS) && //
					ontology2.getLexicalization().equals(Lexicalization.RDFS);
			if (!ontologyComparison) {
				alignment_grade = similarityFunction(al.getEntity1().getLocalName(), al.getEntity2().getLocalName(),
						al.getEntity1().getLabels(), al.getEntity2().getLabels());
				OutputSimMatrix.add(new EntityAlignment(al.getEntity1(),
						al.getEntity2(), '=', alignment_grade));
				count++;
				//System.out.println("Computed " + count + " on " + InputSimMatrix.get(0).size());
			} else {
				alignment_grade = similarityFunction(al.getEntity1().getLabel(), al.getEntity2().getLabel(),
						al.getEntity1().getComment(), al.getEntity2().getComment(), al.getEntity1().getLocalName(),
						al.getEntity2().getLocalName());
				OutputSimMatrix.add(new EntityAlignment(al.getEntity1(), al.getEntity2(), '=',
						alignment_grade + al.getSimilarityValue()));
				//System.out.println("Alignment:" + al.getEntity1().getLabel() + "-" + al.getEntity2().getLabel() + '\n');
				result.addLog(al.getEntity1().getLabel() + "-" + al.getEntity2().getLabel()
						+ ":" + (alignment_grade + al.getSimilarityValue()));
			}
		}
	}

	protected double similarityFunction(final String l1, final String l2, final List<String> labels1,
			final List<String> labels2) {
		double max = 0;
		for (final String label1 : labels1) {
			for (final String label2 : labels2) {
				final double alignmentGrade = similarityFunction(label1, label2, l1, l2);
				if (alignmentGrade > max)
					max = alignmentGrade;
			}
		}
		return max;
	}

	protected double similarityFunction(final String l1, final String l2, final String c1, final String c2,
			final String n1, final String n2) {
		return 0;
	}

	protected double similarityFunction(final String l1, final String l2, final String n1, final String n2) {
		return 0;
	}

	protected double similarityFunction(final String n1, final String n2) {
		return 0;
	}

	protected boolean createDirs(final String folder) {
		final File theDir = new File(folder);
		if (!theDir.exists()) {
			//System.out.println("creating directory: " + folder);
			Logger.getLogger(Matcher.class.getName()).log(Level.INFO, null, "creating directory: " + folder);
			result.addLog("creating directory: " + folder);
			return theDir.mkdir();
		}
		return true;
	}

	protected ArrayList<RepositoryConnection> initModels() throws IOException {
		result.addLog("*****************************************************************");
		result.addLog(matcherId);
		result.addLog("*****************************************************************");
		//System.out.println("\nInitializing Models...");
		Logger.getLogger(Matcher.class.getName()).log(Level.INFO, "\nInitializing Models...");
		result.addLog("\nInitializing Models...\n");
		RepositoryConnection conn1 = null, conn2 = null;
		final ArrayList<RepositoryConnection> res = new ArrayList<>();

		final String baseuri = "http://test.it/GenomaWeb";

		if (this.tripleStore != null) {
			final String tripleStoreUrl = this.tripleStore.getUrl();
			//System.out.println("\nRemote connection to " + tripleStoreUrl + "\n");
			Logger.getLogger(Matcher.class.getName()).log(Level.INFO, "\nRemote connection to " + tripleStoreUrl + "\n");
			final Repository repository1 = new HTTPRepository(tripleStoreUrl, this.ontology1.getUri());
			final Repository repository2 = new HTTPRepository(tripleStoreUrl, this.ontology2.getUri());
			repository1.initialize();
			repository2.initialize();

			conn1 = repository1.getConnection();

			conn2 = repository2.getConnection();

			// TODO: do we need to add the vocabularies (RDF, RDFS, OWL, SKOS, SKOSXL ??? )
		} else {
			// create 2 local connection
			DataService ontology1Endpoint = this.ontology1.getSparqlEndpoint();
			if (ontology1Endpoint.getEndpointURL() != null) {
				final SPARQLRepository sparql = buildSPARQLEndpoint(ontology1Endpoint);
				sparql.initialize();
				conn1 = sparql.getConnection();
			} else {
				conn1 = loadOntologyFromFile(this.ontology1, baseuri);
			}

			DataService ontology2Endpoint = this.ontology2.getSparqlEndpoint();
			if (ontology2Endpoint.getEndpointURL() != null) {
				final SPARQLRepository sparql = buildSPARQLEndpoint(ontology2Endpoint);
				sparql.initialize();
				conn2 = sparql.getConnection();
			} else {
				conn2 = loadOntologyFromFile(this.ontology2, baseuri);
			}

			// TODO: do we need to add the vocabularies (RDF, RDFS, OWL, SKOS, SKOSXL ??? )
		}
		res.add(conn1);
		res.add(conn2);
		return res;
	}

	protected SPARQLRepository buildSPARQLEndpoint(DataService dataService) throws RepositoryException {
		final SPARQLRepository sparql = new SPARQLRepository(dataService.getEndpointURL());
		String username = dataService.getUsername();
		String password = dataService.getPassword();
		if (StringUtils.isNoneBlank(username, password)) {
			sparql.setUsernameAndPassword(username, password);
		}
		sparql.initialize();
		return sparql;
	}

	private RepositoryConnection loadOntologyFromFile(MatchingResource ontology, String baseuri) throws IOException {
		final Repository rep = new SailRepository(new MemoryStore());
		rep.initialize();
		RepositoryConnection conn = rep.getConnection();
		final String ontologyUri = ontology.getUri();
		//System.out.println("Loading ontology:" + ontologyUri);
		Logger.getLogger(Matcher.class.getName()).log(Level.INFO, "Loading ontology:" + ontologyUri);
		result.addLog("Loading ontology:" + ontologyUri);

		// the graph is the baseuri
		conn.add(new URL(ontologyUri), null, RDFFormat.RDFXML,
				SimpleValueFactory.getInstance().createIRI(baseuri));
		return conn;
	}

	/*
	protected void saveSimMatrixToXml(final String filename) throws IOException {
		try (final FileWriter out = new FileWriter(filename)) {
			XStream xstream = new XStream();
			xstream.allowTypesByWildcard(new String[] { "it.uniroma2.art.genoma.**" });
			final String xml = xstream.toXML(result.getSimMatrix());
			out.write(xml);
		}
	}
	 */

	/*
	protected void loadSimMatrixFromXml(final String filename) {
		final File xml = new File(filename);
		XStream xstream = new XStream();
		xstream.allowTypesByWildcard(new String[] { "it.uniroma2.art.genoma.**" });
		this.OutputSimMatrix = (Collection<EntityAlignment>) xstream.fromXML(xml);
	}
	 */

	/*
	protected boolean ontologyAlignmentExists(final String alignmentId) {
		final File f = new File("alignments/" + alignmentId + "_c.xml");
		if (f.exists()) {
			return true;
		} else {
			return false;
		}
	}
	 */

	/*
	 * protected boolean checkAlignmentTempFilePresence(String alignmentId) { File f
	 * = new File("temp_alignments/"+alignmentId+"_t.xml"); if(f.exists()){ return
	 * true; } else { return false; } }
	 */

	protected MatchingResource getOntology1() {
		return this.ontology1;
	}

	protected MatchingResource getOntology2() {
		return this.ontology2;
	}

	protected TripleStore getTripleStore() {
		return this.tripleStore;
	}

	protected String getOptions() {
		return this.options;
	}

	protected double[] getSimMatrixMaxValue() {
		final double[] res = new double[2];
		double max = 0.0;
		double min = 1.0;
		// get max value
        for (EntityAlignment align : this.OutputSimMatrix) {
            if (align.getSimilarityValue() > max) {
                max = align.getSimilarityValue();
            }
            if (align.getSimilarityValue() < min) {
                min = align.getSimilarityValue();
            }
        }
		res[0] = min;
		res[1] = max;
		return res;
	}

	protected void normalizeSimMatrix() {
		final double[] limits = getSimMatrixMaxValue();
		if(limits[0] == limits[1])
			return;
		final Collection<EntityAlignment> temp = new ArrayList<EntityAlignment>();
        for (EntityAlignment align : this.OutputSimMatrix) {
            temp.add(new EntityAlignment(align.getEntity1(), align.getEntity2(), align.getRelationType(),
                    (align.getSimilarityValue() - limits[0]) / (limits[1] - limits[0])));
        }
		this.OutputSimMatrix = temp;
	}

	protected int getCategoryFromEntityType(final String et) {
		if (et == null)
			return OntoResource.UNDEFINED;
		else if (et.equals(RDFS.CLASS.stringValue()) || et.equals(OWL.CLASS.stringValue())
				|| et.equals(SKOS.CONCEPT.stringValue()) || et.equals(ONTOLEX.CONCEPT.stringValue()))
			return OntoResource.CLASS;
		else if (et.equals(OWL.DATATYPEPROPERTY.stringValue()))
			return OntoResource.DATATYPEPROPERTY;
		else if (et.equals(OWL.OBJECTPROPERTY.stringValue()))
			return OntoResource.OBJECTPROPERTY;
		else
			return OntoResource.INSTANCE;
	}

	protected boolean IsUriInLocalNamespace(final RepositoryConnection conn, final IRI uri) {
		if (conn.getNamespace("my") == null) {
			return true;
		} else {
            return uri.stringValue().contains(conn.getNamespace("my"));
		}
	}

	protected int getMapSize(Map<String, List<String>> map) {
		try {
			//System.out.println("Index Size: " + map.size());
			Logger.getLogger(Matcher.class.getName()).log(Level.INFO, "Index Size: " + map.size());
			try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
				ObjectOutputStream oos = new ObjectOutputStream(baos);
				oos.writeObject(map);
				return baos.size();
			}
		} catch (IOException e) {
			return 0;
		}

	}

}
