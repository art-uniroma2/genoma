/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.MatcherModuleClient;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.codec.digest.DigestUtils;
import org.dom4j.DocumentException;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.ComposerAggregator.Evaluator;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.ComposerAggregator.RefAlignmentReader;
import it.uniroma2.art.genoma.ComposerAggregator.TripleStore;
import it.uniroma2.art.genoma.CustomExceptions.InvalidMatchingParamsException;

/**
 *
 * @author Roberto Enea
 */
public class AlignmentExtractor extends Matcher {

	private String threshold;
	private String filter;
	private InputStream refAlignment;

	public AlignmentExtractor(final MatchingResource ontology1, final MatchingResource ontology2,
			final TripleStore tripleStore, final ArrayList<Collection<EntityAlignment>> SimMatrix, final String options,
			final String refalpath) {
		super(ontology1, ontology2, tripleStore, SimMatrix, options);
		matcherId = "Alignment Extractor";
		this.alignmentId = DigestUtils.shaHex(this.ontology1.getUri() + this.ontology2.getUri() + this.matcherId);
		this.threshold = null;
		try {
			if (refalpath != null)
				refAlignment = new FileInputStream(refalpath);
		} catch (final FileNotFoundException e) {
			try {
				refAlignment = new URL(refalpath).openStream();
			} catch (final MalformedURLException e1) {
				refAlignment = null;
			} catch (final IOException e1) {
				refAlignment = null;
			}
		}
	}

	@Override
	protected void matchingAlgorithm() {
		try {
			result.addLog("*****************************************************************");
			result.addLog(matcherId);
			result.addLog("*****************************************************************");
			parseOptions();
			OutputSimMatrix = FilteringDictionary.applyFiltering(InputSimMatrix.get(0), filter, threshold);
			// normalizeSimMatrix();
			deleteDuplicate();
			result.setSimMatrix(OutputSimMatrix);
			showSimilarityMatrixInLogs();
		} catch (final InvalidMatchingParamsException e) {
			//removed the third parameter from the .log and, which was the "e" and passing only the e.getMessage()
			Logger.getLogger(AlignmentExtractor.class.getName()).log(Level.SEVERE, e.getMessage());
			result.setError();
			result.setErrorMessage(e.getMessage());
			result.addLog(e.getMessage());
		} catch (final DocumentException e) {
			//removed the third parameter from the .log and, which was the "e" and passing only the e.getMessage()
			Logger.getLogger(AlignmentExtractor.class.getName()).log(Level.SEVERE, e.getMessage());
			result.setError();
			result.setErrorMessage(e.getMessage());
			result.addLog(e.getMessage());
		} catch (final MalformedURLException e) {
			//removed the third parameter from the .log and, which was the "e" and passing only the e.getMessage()
			Logger.getLogger(AlignmentExtractor.class.getName()).log(Level.SEVERE, e.getMessage());
			result.setError();
			result.setErrorMessage(e.getMessage());
			result.addLog(e.getMessage());
		} catch (final IndexOutOfBoundsException e) {
			//removed the third parameter from the .log and, which was the "e" and passing only the e.getMessage()
			Logger.getLogger(AlignmentExtractor.class.getName()).log(Level.SEVERE, e.getMessage());
			result.setError();
			result.setErrorMessage(e.getMessage());
			result.addLog(e.getMessage());
		}
	}

	@Override
	protected void parseOptions() throws InvalidMatchingParamsException {
		final String[] tmp = options.split("-");
		for (final String element : tmp) {
			if (element.contains("filter")) {
				filter = element.substring(element.lastIndexOf("filter=") + ("filter=").length(), element.length())
						.trim();
			} else if (element.contains("th")) {
				threshold = element.substring(element.lastIndexOf("th=") + ("th=").length(), element.length()).trim();
			}
		}
		if (threshold == null)
			throw new InvalidMatchingParamsException("No Threshold Value");
		if (filter == null)
			throw new InvalidMatchingParamsException("No Filter Found");
	}

	private void showSimilarityMatrixInLogs() throws DocumentException, MalformedURLException {
		result.addLog("*************************************************************");
		result.addLog("*************************************************************");
		result.addLog("FINAL RESULTS");
		result.addLog("*************************************************************");
		result.addLog("*************************************************************");
        for (EntityAlignment temp : this.OutputSimMatrix) {
            result.addLog(temp.getEntity1().getURIString() + " vs " + temp.getEntity2().getURIString() + " rel type:"
                    + temp.getRelationType() + " grade:" + temp.getSimilarityValue());
        }
		if (!(refAlignment == null)) {
			final Evaluator ev = new Evaluator(this.OutputSimMatrix, RefAlignmentReader.read(refAlignment), 1.0);
			result.addLog("Found:" + ev.getFound());
			result.addLog("Existing:" + ev.getExisting());
			result.addLog("Correct:" + ev.getCorrect());
			result.addLog("Precision:" + ev.getPrecision());
			result.addLog("Recall:" + ev.getRecall());
			result.addLog("F-Measure:" + ev.getFMeasure());
		}
	}

	private void deleteDuplicate() {
		final Collection<EntityAlignment> duplicateList = new ArrayList<>();
		Iterator<EntityAlignment> it = OutputSimMatrix.iterator();
		while (it.hasNext()) {
			final EntityAlignment al = it.next();
			int count = 0;
			final Iterator<EntityAlignment> it2 = OutputSimMatrix.iterator();
			while (it2.hasNext()) {
				final EntityAlignment al2 = it2.next();
				if (al.getAlignmentId().equals(al2.getAlignmentId())) {
					count++;
					if (count > 1)
						duplicateList.add(al2);
				}
			}
		}
		it = duplicateList.iterator();
		while (it.hasNext()) {
			OutputSimMatrix.remove(it.next());
		}
	}

}
