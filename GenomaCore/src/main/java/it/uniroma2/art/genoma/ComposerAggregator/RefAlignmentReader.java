/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package it.uniroma2.art.genoma.ComposerAggregator;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.AlignmentTools.OntoResource;

/**
 *
 * @author Roberto Enea
 */
public class RefAlignmentReader {

	public static Collection<EntityAlignment> read(final InputStream refAlignment)
			throws DocumentException, MalformedURLException {
		final Collection<EntityAlignment> alignment = new ArrayList<>();
		final SAXReader reader = new SAXReader();
		final Document doc = reader.read(refAlignment);
		final Element root = doc.getRootElement();
		final Element align = root.element("Alignment");
		final Iterator<?> iter = align.elementIterator("map");
		if (!iter.hasNext()) {
			return null;
		}
		while (iter.hasNext()) {
			final Element e = ((Element) iter.next()).element("Cell");
			if (e == null)
				continue;
			final String s1 = e.element("entity1").attributeValue("resource");
			final String s2 = e.element("entity2").attributeValue("resource");
			final OntoResource e1 = new OntoResource(s1, null);
			final OntoResource e2 = new OntoResource(s2, null);
			final double sim = Double.parseDouble(e.elementText("measure"));
			final String rel = e.elementText("relation");
			final EntityAlignment al = new EntityAlignment(e1, e2, rel.charAt(0), sim);
			alignment.add(al);
		}
		return alignment;
	}
}
