/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.MatcherModuleClient;

import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.codec.digest.DigestUtils;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.ComposerAggregator.TripleStore;
import it.uniroma2.art.genoma.CustomExceptions.InvalidMatchingParamsException;
import it.uniroma2.art.genoma.MatcherModuleClient.vdoc.Parameters;
import it.uniroma2.art.genoma.MatcherModuleClient.vdoc.VDocMatcher;

/**
 *
 * @author Roberto Enea
 */

public class VirtualDocMatcher extends Matcher {

	public VirtualDocMatcher(final MatchingResource ontology1, final MatchingResource ontology2,
			final TripleStore tripleStore, final ArrayList<Collection<EntityAlignment>> SimMatrix,
			final String options) {
		super(ontology1, ontology2, tripleStore, SimMatrix, options);
		matcherId = "Virtual Document Matcher";
		this.alignmentId = DigestUtils.shaHex(this.ontology1.getUri() + this.ontology2.getUri() + this.matcherId);
		Parameters.inclExt = false;
	}

	@Override
	protected void matchingAlgorithm() {
		try {
			RepositoryConnection conn1 = null, conn2 = null;
			parseOptions();
			if(disabled) {
				result.setSimMatrix(null);
				return;
			}
			if(this.InputSimMatrix.isEmpty()) {
				final ArrayList<RepositoryConnection> res = initModels();
				conn1 = res.get(0);
				conn2 = res.get(1);
			}
			final VDocMatcher vdoc = new VDocMatcher(conn1, conn2, this.InputSimMatrix, result, ontology1, ontology2);
			this.OutputSimMatrix = vdoc.match();
			normalizeSimMatrix();
			result = vdoc.getResult();
			result.setSimMatrix(OutputSimMatrix);
		} catch (final Exception ex) {
			Logger.getLogger(VirtualDocMatcher.class.getName()).log(Level.SEVERE, null, ex);
			result.setError();
			result.setErrorMessage(ex.getMessage());
			result.addLog(ex.getMessage());
		}
	}

	@Override
	protected void parseOptions() throws InvalidMatchingParamsException {
		super.parseOptions();
		final String[] tmp = options.split("-");
		for (final String element : tmp) {
			if (element.contains("incN")) {
				Parameters.inclNeighbor = true;
			} else if (element.contains("incBN")) {
				Parameters.inclBlankNode = true;
			} else if (element.contains("stopwords")) {
				Parameters.stopwordsAddress = element
						.substring(element.lastIndexOf("stopwords:") + ("stopwords:").length()).trim()
						.replace("@", "-");
			} else if (element.contains("disabled")) {
				disabled = true;
			}
		}
	}

}
