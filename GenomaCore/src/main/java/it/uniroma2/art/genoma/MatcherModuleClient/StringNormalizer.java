/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.MatcherModuleClient;

import java.text.Normalizer;
import java.text.Normalizer.Form;

/**
 *
 * @author Roberto Enea
 */
public class StringNormalizer {

	public static String caseNormalisation(final String text) {
		return text.toLowerCase();
	}

	public static String diacriticSuppression(final String text) {
		final String decomposed = Normalizer.normalize(text, Form.NFD);
		// removing diacritics
		final String removed = decomposed.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
		return removed;
	}

	public static String blankNormalisation(final String text) {
		return text.replaceAll("\\s+", " ");
	}

	public static String linkSuppression(final String text) {
		return text.replaceAll("\\W", " ").replaceAll("_", " ");
	}

	public static String digitSuppression(final String text) {
		return text.replaceAll("\\d", "");
	}

	public static String punctuationElimination(final String text) {
		return text.replaceAll("\\p{P}", "");
	}

}
