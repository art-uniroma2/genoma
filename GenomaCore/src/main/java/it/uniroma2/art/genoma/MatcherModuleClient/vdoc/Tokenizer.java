/**
 * Copyright 2010 Websoft research group, Nanjing University, PR China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.uniroma2.art.genoma.MatcherModuleClient.vdoc;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import org.tartarus.snowball.SnowballProgram;

/**
 * @author Wei Hu & Gong Cheng
 *
 */
public class Tokenizer {
	private static Map<String, String> languages;
	private final String delimiters = " \t\n\r\f~!@#$%^&*()_+|`-=\\{}[]:\";'<>?,./'1234567890";

	static {
		languages = new HashMap<>();
		languages.put("en", "English");
		languages.put("dk", "Danish");
		languages.put("nl", "Dutch");
		languages.put("fi", "Finnish");
		languages.put("fr", "French");
		languages.put("de", "German2");
		languages.put("hu", "Hungarian");
		languages.put("it", "Italian");
		languages.put("no", "Norwegian");
		languages.put("pt", "Portuguese");
		languages.put("ro", "Romanian");
		languages.put("ru", "Russian");
		languages.put("es", "Spanish");
		languages.put("se", "Swedish");
		languages.put("tr", "Turkish");
	}

	public String[] split(String source, String language) {
		final ArrayList<String> listForNumber = new ArrayList<String>();
		flag3: for (int i = 0; i < source.length(); i++) {
			char thisChar = source.charAt(i);
			final StringBuffer thisNumber = new StringBuffer();
			boolean hasDigit = false;
			if (Character.isDigit(thisChar)) {
				thisNumber.append(thisChar);
				for (++i; i < source.length(); i++) {
					thisChar = source.charAt(i);
					if ((thisChar == '.') && !hasDigit) {
						thisNumber.append(thisChar);
						hasDigit = true;
					} else if (Character.isDigit(thisChar)) {
						thisNumber.append(thisChar);
					} else {
						if (thisNumber.length() != 0) {
							listForNumber.add(thisNumber.toString());
							continue flag3;
						}
					}
				}
				if (thisNumber.length() != 0) {
					listForNumber.add(thisNumber.toString());
				}
			}
		}

		int positionOfDot;
		final StringBuffer tempSource = new StringBuffer(source);
		while ((positionOfDot = tempSource.indexOf(".")) != -1) {
			tempSource.deleteCharAt(positionOfDot);
		}
		source = tempSource.toString();
		final StringTokenizer stringTokenizer = new StringTokenizer(source, delimiters);

		final ArrayList<String> list = new ArrayList<String>();
		final ArrayList<String> listForAllUpperCase = new ArrayList<String>();

		flag0: while (stringTokenizer.hasMoreTokens()) {
			String token = stringTokenizer.nextToken();
			boolean allUpperCase = true;
			for (int i = 0; i < token.length(); i++) {
				if (!Character.isUpperCase(token.charAt(i))) {
					allUpperCase = false;
				}
			}
			if (allUpperCase) {
				listForAllUpperCase.add(token);
				continue flag0;
			}
			int index = 0;
			flag1: while (index < token.length()) {
				flag2: while (true) {
					index++;
					if ((index == token.length()) || !Character.isLowerCase(token.charAt(index))) {
						break flag2;
					}
				}
				list.add(token.substring(0, index).toLowerCase());
				token = token.substring(index);
				index = 0;
				continue flag1;
			}
		}

		try {

			final Class<?> stemClass = Class.forName("org.tartarus.snowball.ext." + getLanguageStemmerFromLanguage(language) + "Stemmer");
			final SnowballProgram stemmer = (SnowballProgram) stemClass.newInstance();
			final Method stemMethod = stemClass.getMethod("stem", new Class[0]);
			final Object[] emptyArgs = new Object[0];
			for (int i = 0; i < list.size(); i++) {
				stemmer.setCurrent(list.get(i));
				stemMethod.invoke(stemmer, emptyArgs);
				list.set(i, stemmer.getCurrent());
			}
		} catch (final ClassNotFoundException e) {
			e.printStackTrace();
		} catch (final InstantiationException e) {
			e.printStackTrace();
		} catch (final NoSuchMethodException e) {
			e.printStackTrace();
		} catch (final IllegalAccessException e) {
			e.printStackTrace();
		} catch (final InvocationTargetException e) {
			e.printStackTrace();
		}

		for (int i = 0; i < listForAllUpperCase.size(); i++) {
			list.add(listForAllUpperCase.get(i));
		}
		for (int i = 0; i < listForNumber.size(); i++) {
			list.add(listForNumber.get(i));
		}

		final String[] array = new String[list.size()];
		final Iterator<String> iter = list.iterator();
		int index = 0;
		while (iter.hasNext()) {
			array[index] = iter.next().toLowerCase();
			index++;
		}
		return array;
	}

	private String getLanguageStemmerFromLanguage(String language) {
		return languages.get(language);
	}
}
