/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.MatcherModuleClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.codec.digest.DigestUtils;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.ComposerAggregator.TripleStore;
import it.uniroma2.art.genoma.CustomExceptions.InvalidMatchingParamsException;
import it.uniroma2.art.genoma.MatcherModuleClient.gmo.GmoMatcher;
import it.uniroma2.art.genoma.MatcherModuleClient.gmo.Parameters;

/**
 *
 * @author Roberto Enea
 */

public class GraphMatcher extends Matcher {

	public GraphMatcher(final MatchingResource ontology1, final MatchingResource ontology2,
			final TripleStore tripleStore, final ArrayList<Collection<EntityAlignment>> SimMatrix,
			final String options) {
		super(ontology1, ontology2, tripleStore, SimMatrix, options);
		matcherId = "Graph GMO Matcher";
		this.alignmentId = DigestUtils.shaHex(this.ontology1.getUri() + this.ontology2.getUri() + this.matcherId);
	}

	@Override
	protected void matchingAlgorithm() {
		try {
			parseOptions();
			final ArrayList<RepositoryConnection> res = initModels();
			final RepositoryConnection conn1 = res.get(0);
			final RepositoryConnection conn2 = res.get(1);
			final GmoMatcher gmo = new GmoMatcher(conn1, conn2, this.InputSimMatrix, result);
			this.OutputSimMatrix = gmo.match();
			normalizeSimMatrix();
			result = gmo.getResult();
			result.setSimMatrix(OutputSimMatrix);
		} catch (final InvalidMatchingParamsException | IOException ex) {
			Logger.getLogger(GraphMatcher.class.getName()).log(Level.SEVERE, ex.getMessage());
			result.setError();
			result.setErrorMessage(ex.getMessage());
			result.addLog(ex.getMessage());
		}
	}

	@Override
	protected void parseOptions() throws InvalidMatchingParamsException {
		final String[] tmp = options.split("-");
		for (final String element : tmp) {
			if (element.contains("it")) {
				Parameters.iterTimes = Integer.parseInt(
						element.substring(element.lastIndexOf("it=") + ("it=").length(), element.length()).trim());
			} else if (element.contains("co")) {
				Parameters.convergence = Double.parseDouble(
						element.substring(element.lastIndexOf("co=") + ("co=").length(), element.length()).trim());
			} else if (element.contains("incI=true")) {
				Parameters.inclInstMatch = true;
			}
		}
	}

}
