/**
 * Copyright 2010 Websoft research group, Nanjing University, PR China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.uniroma2.art.genoma.MatcherModuleClient.gmo;

import java.util.ArrayList;

import it.uniroma2.art.genoma.AlignmentTools.OntoResource;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Wei Hu
 * @see http://ws.nju.edu.cn
 */
//this class has been modified by replacing the various prints of System.out or System.err with a Logger
public class NamedMatrix extends BasicMatrix {
	private ArrayList<Object> rowList = null, columnList = null;

	public NamedMatrix(final int numRows, final int numColumns) {
		super(numRows, numColumns);
	}

	public NamedMatrix(final ArrayList<Object> rlist, final ArrayList<Object> clist) {
		super(rlist.size(), clist.size());
		rowList = rlist;
		columnList = clist;
	}

	public NamedMatrix(final NamedMatrix matrix) {
		super(matrix.numRows, matrix.numColumns);
		rowList = matrix.getRowList();
		columnList = matrix.getColList();
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numColumns; j++) {
				set(i, j, matrix.get(i, j));
			}
		}
	}

	public void setRowList(final ArrayList<Object> rlist) {
		rowList = rlist;
	}

	public void setColumnList(final ArrayList<Object> clist) {
		columnList = clist;
	}

	public ArrayList<Object> getRowList() {
		return rowList;
	}

	public ArrayList<Object> getColList() {
		return columnList;
	}

	public double get(final OntoResource row, final OntoResource col) {
		int i = -1;
		for (int k = 0, n = numRows; k < n; k++) {
			if (((OntoResource) rowList.get(k)).getURIString().equals(row.getURIString())) {
				i = k;
			}
		}
		int j = -1;
		for (int k = 0, n = numColumns; k < n; k++) {
			if (((OntoResource) columnList.get(k)).getURIString().equals(col.getURIString())) {
				j = k;
			}
		}
		if (i < 0 || j < 0) {
			System.err.println("getError: Cannot find such row or column.");
			return -1;
		} else {
			return get(i, j);
		}

	}

	public double get(final String row, final OntoResource col) {
		int i = -1;
		for (int k = 0, n = numRows; k < n; k++) {
			if (((OntoResource) rowList.get(k)).getURIString().equals(row)) {
				i = k;
			}
		}
		int j = -1;
		for (int k = 0, n = numColumns; k < n; k++) {
			if (((OntoResource) columnList.get(k)).getURIString().equals(col.getURIString())) {
				j = k;
			}
		}
		if (i < 0 || j < 0) {
			System.err.println("getError: Cannot find such row or column.");
			return -1;
		} else {
			return get(i, j);
		}

	}

	public double get(final OntoResource row, final String col) {
		int i = -1;
		for (int k = 0, n = numRows; k < n; k++) {
			if (((OntoResource) rowList.get(k)).getURIString().equals(row.getURIString())) {
				i = k;
			}
		}
		int j = -1;
		for (int k = 0, n = numColumns; k < n; k++) {
			if (((OntoResource) columnList.get(k)).getURIString().equals(col)) {
				j = k;
			}
		}
		if (i < 0 || j < 0) {
			System.err.println("getError: Cannot find such row or column.");
			return -1;
		} else {
			return get(i, j);
		}

	}

	public void set(final OntoResource row, final OntoResource col, final double value) {
		int i = -1;
		for (int k = 0, n = numRows; k < n; k++) {
			if (((OntoResource) rowList.get(k)).getURIString().equals(row.getURIString())) {
				i = k;
			}
		}
		int j = -1;
		for (int k = 0, n = numColumns; k < n; k++) {
			if (((OntoResource) columnList.get(k)).getURIString().equals(col.getURIString())) {
				j = k;
			}
		}
		if (i < 0 || j < 0) {
			System.err.println("setError: Cannot find such row or column.");
			return;
		} else {
			set(i, j, value);
		}
	}

	public void set(final String row, final OntoResource col, final double value) {
		int i = -1;
		for (int k = 0, n = numRows; k < n; k++) {
			if (((OntoResource) rowList.get(k)).getURIString().equals(row)) {
				i = k;
			}
		}
		int j = -1;
		for (int k = 0, n = numColumns; k < n; k++) {
			if (((OntoResource) columnList.get(k)).getURIString().equals(col.getURIString())) {
				j = k;
			}
		}
		if (i < 0 || j < 0) {
			System.err.println("setError: Cannot find such row or column.");
			return;
		} else {
			set(i, j, value);
		}
	}

	public void set(final OntoResource row, final String col, final double value) {
		int i = -1;
		for (int k = 0, n = numRows; k < n; k++) {
			if (((OntoResource) rowList.get(k)).getURIString().equals(row.getURIString())) {
				i = k;
			}
		}
		int j = -1;
		for (int k = 0, n = numColumns; k < n; k++) {
			if (((OntoResource) columnList.get(k)).getURIString().equals(col)) {
				j = k;
			}
		}
		if (i < 0 || j < 0) {
			System.err.println("setError: Cannot find such row or column.");
			return;
		} else {
			set(i, j, value);
		}
	}

	public Object getRow(final int row) {
		if (row < 0 || row >= numRows) {
			System.err.println("getRowError: Index is out of bound.");
			return null;
		} else {
			return rowList.get(row);
		}
	}

	public Object getColumn(final int col) {
		if (col < 0 || col >= numColumns) {
			System.err.println("getColumnError: Index is out of bound.");
			return null;
		} else {
			return columnList.get(col);
		}
	}

	public int getRowIndex(final Object row) {
		int index = -1;
		for (int k = 0, n = numRows; k < n; k++) {
			if (rowList.get(k).toString().equals(row.toString())) {
				index = k;
			}
		}
		return index;
	}

	public int getColumnIndex(final Object col) {
		int index = -1;
		for (int k = 0, n = numColumns; k < n; k++) {
			if (columnList.get(k).toString().equals(col.toString())) {
				index = k;
			}
		}
		return index;
	}

	public void replaceRow(final Object oldRow, final Object newRow) {
		final int i = getRowIndex(oldRow);
		final int j = getRowIndex(newRow);
		if (i == -1 || j == -1) {
			return;
		}
		for (int k = 0; k < numColumns; k++) {
			set(i, k, get(j, k));
		}
	}

	public void replaceCol(final Object oldCol, final Object newCol) {
		final int i = getColumnIndex(oldCol);
		final int j = getColumnIndex(newCol);
		if (i == -1 || j == -1) {
			return;
		}
		for (int k = 0; k < numRows; k++) {
			set(k, i, get(k, j));
		}
	}

	@Override
	public void show() {
		for (int i = 0; i < numRows; i++) {
			//System.out.println("rowList[" + i + "]:" + rowList.get(i));
			Logger.getLogger(NamedMatrix.class.getName()).log(Level.INFO, "rowList[" + i + "]:" + rowList.get(i));
		}
		for (int j = 0; j < numColumns; j++) {
			//System.out.println("colList[" + j + "]:" + columnList.get(j));
			Logger.getLogger(NamedMatrix.class.getName()).log(Level.INFO, "colList[" + j + "]:" + columnList.get(j));
		}
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numColumns; j++) {
				//System.out.print(get(i, j) + " ");
				Logger.getLogger(NamedMatrix.class.getName()).log(Level.INFO, get(i, j) + " ");
			}
			//System.out.println();
			Logger.getLogger(NamedMatrix.class.getName()).log(Level.INFO, "");
		}
	}
}
