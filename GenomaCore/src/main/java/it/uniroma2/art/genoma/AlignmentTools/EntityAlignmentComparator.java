/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.AlignmentTools;

import java.util.Comparator;

/**
 *
 * @author Roberto Enea
 */
public class EntityAlignmentComparator implements Comparator<EntityAlignment> {
	@Override
	public int compare(final EntityAlignment o1, final EntityAlignment o2) {
		return o1.getAlignmentId().compareTo(o2.getAlignmentId());
	}
}
