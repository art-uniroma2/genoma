/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.MatcherModuleClient;

import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.codec.digest.DigestUtils;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.ComposerAggregator.TripleStore;
import it.uniroma2.art.genoma.CustomExceptions.IllegalSimMatrixSizeException;
import it.uniroma2.art.genoma.CustomExceptions.InvalidMatchingParamsException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Roberto Enea
 */
public class JunctionPoint extends Matcher {
	private String matchingStrategy;

	public JunctionPoint(final MatchingResource ontology1, final MatchingResource ontology2,
			final TripleStore tripleStore, final ArrayList<Collection<EntityAlignment>> SimMatrix,
			final String options) {
		super(ontology1, ontology2, tripleStore, SimMatrix, options);
		matcherId = "Junction Point";
		this.alignmentId = DigestUtils.shaHex(this.ontology1.getUri() + this.ontology2.getUri() + this.matcherId);
		this.matchingStrategy = null;
	}

	@Override
	protected void matchingAlgorithm() {
		try {
			//System.out.println(matcherId);
			Logger.getLogger(JunctionPoint.class.getName()).log(Level.INFO, matcherId);
			result.addLog("*****************************************************************");
			result.addLog(matcherId);
			result.addLog("*****************************************************************");
			parseOptions();
			if (this.matchingStrategy != null) {
				result.setSimMatrix(calcOutputMatrix(this.matchingStrategy));
			}
		} catch (final IllegalSimMatrixSizeException e) {
			Logger.getLogger(JunctionPoint.class.getName()).log(Level.SEVERE, null, e);
			result.setError();
			result.setErrorMessage(e.getMessage());
			result.addLog(e.getMessage());
		} catch (final InvalidMatchingParamsException ex) {
			Logger.getLogger(JunctionPoint.class.getName()).log(Level.SEVERE, null, ex);
			result.setError();
			result.setErrorMessage(ex.getMessage());
			result.addLog(ex.getMessage());
		} catch (final IndexOutOfBoundsException e) {
			Logger.getLogger(JunctionPoint.class.getName()).log(Level.SEVERE, null, e);
			result.setError();
			result.setErrorMessage(e.getMessage());
			result.addLog(e.getMessage());
		}
	}

	@Override
	protected void parseOptions() throws InvalidMatchingParamsException {
		if (this.getOptions().contains("-ms=")) {
			this.matchingStrategy = options.substring(options.lastIndexOf("-ms=") + ("-ms=").length(),
					options.length());
		} else {
			throw new InvalidMatchingParamsException("Matching Strategy Parameter not present");
		}
	}

	private Collection<EntityAlignment> calcOutputMatrix(final String ms)
			throws IllegalSimMatrixSizeException, InvalidMatchingParamsException {
		return AggregationStrategyDictionary.applyMatchingStrategy(InputSimMatrix, ms);
	}

}
