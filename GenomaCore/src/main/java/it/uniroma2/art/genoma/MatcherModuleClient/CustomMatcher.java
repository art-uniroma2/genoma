package it.uniroma2.art.genoma.MatcherModuleClient;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.codec.digest.DigestUtils;
import org.codehaus.groovy.control.CompilationFailedException;
import org.codehaus.groovy.runtime.metaclass.MissingMethodExceptionNoStack;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.model.vocabulary.SKOS;
import org.eclipse.rdf4j.model.vocabulary.SKOSXL;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import groovy.json.internal.LazyMap;
import groovy.lang.GroovyShell;
import groovy.lang.Script;
import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource.Lexicalization;
import it.uniroma2.art.genoma.ComposerAggregator.TripleStore;
import it.uniroma2.art.genoma.CustomExceptions.InvalidMatchingParamsException;

public class CustomMatcher extends Matcher {
	private Script groovyScript;
	private String strOptions;
	private LazyMap options;

	public CustomMatcher(final MatchingResource ontology1, final MatchingResource ontology2,
			final TripleStore tripleStore, final ArrayList<Collection<EntityAlignment>> SimMatrix,
			final String options) {
		super(ontology1, ontology2, tripleStore, SimMatrix, options);
		this.strOptions = options;
		this.groovyScript = null;
		this.options = new LazyMap();
		try {
			parseOptions();
		} catch (final InvalidMatchingParamsException e) {
			Logger.getLogger(StringEqualityMatcher.class.getName()).log(Level.SEVERE, e.getMessage());
		}
		this.alignmentId = DigestUtils.shaHex(this.ontology1.getUri() + this.ontology2.getUri() + this.matcherId);

	}

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void matchingAlgorithm() {
		if(disabled) {
			result.setSimMatrix(null);
			return;
		}
		try {
			OutputSimMatrix = (Collection<EntityAlignment>) this.groovyScript.invokeMethod("matchingAlgorithm", null);
		} catch (final MissingMethodExceptionNoStack e) {
			OutputSimMatrix = null;
		}
		final Lexicalization lexicalization = ontology1.getLexicalization();
		if (OutputSimMatrix == null) {
			this.OutputSimMatrix = new ArrayList();
			try {
				RepositoryConnection conn1 = null, conn2 = null;
				final ArrayList<RepositoryConnection> res = initModels();
				conn1 = res.get(0);
				conn2 = res.get(1);
				Lexicalization lexicalization1 = ontology1.getLexicalization();
				Lexicalization lexicalization2 = ontology1.getLexicalization();
				if (InputSimMatrix.isEmpty()) {
					if (lexicalization1.equals(Lexicalization.RDFS) && lexicalization2.equals(Lexicalization.RDFS)) {
						compareEntities(conn1, conn2, RDFS.CLASS.stringValue());
						compareEntities(conn1, conn2, OWL.CLASS.stringValue());
						compareEntities(conn1, conn2, OWL.DATATYPEPROPERTY.stringValue());
						compareEntities(conn1, conn2, OWL.OBJECTPROPERTY.stringValue());
						if (inclInstances) {
							compareInstances(conn1, conn2);
						}
					} else {
						compareEntities(conn1, conn2, null);
					}
				} else {
					compareAllEntities();
				}
			} catch (final Exception ex) {
				Logger.getLogger(StringEqualityMatcher.class.getName()).log(Level.SEVERE, ex.getMessage());
				result.setError();
				result.setErrorMessage(ex.getMessage());
				result.addLog(ex.getMessage());
			}
		}
		normalizeSimMatrix();
		result.setSimMatrix(OutputSimMatrix);
	}

	@Override
	protected double similarityFunction(final String l1, final String l2, final String c1, final String c2,
			final String n1, final String n2) {
		final double alignment_grade = (double) this.groovyScript.invokeMethod("similarityFunction",
				new Object[] { l1, l2, c1, c2, n1, n2, (double) this.options.get("lw"), (double) this.options.get("cw"),
						(double) this.options.get("iw") });
		return alignment_grade;
	}

	@Override
	protected double similarityFunction(final String l1, final String l2, final String n1, final String n2) {
		final double alignment_grade = (double) this.groovyScript.invokeMethod("similarityFunction",
				new Object[] { l1, l2, n1, n2, (double) this.options.get("lw"), (double) this.options.get("cw"),
						(double) this.options.get("iw") });
		return alignment_grade;
	}

	@Override
	protected double similarityFunction(final String n1, final String n2) {
		final double alignment_grade = (double) this.groovyScript.invokeMethod("similarityFunction",
				new Object[] { n1, n2, (double) this.options.get("lw"), (double) this.options.get("cw"),
						(double) this.options.get("iw") });
		return alignment_grade;
	}

	@Override
	protected void parseOptions() throws InvalidMatchingParamsException {
		String script = null;
		final String[] tmp = this.strOptions.split("-");
		for (final String element : tmp) {
			if (element.contains("script")) {
				script = element.substring(element.lastIndexOf("script=") + ("script=").length(), element.length())
						.trim().replace("@", "-");
				this.matcherId = script;
			}
		}
		try {
			final GroovyShell shell = new GroovyShell();
			this.groovyScript = shell.parse(new File(script));
		} catch (final CompilationFailedException e) {
			Logger.getLogger(CustomMatcher.class.getName()).log(Level.SEVERE,  e.getMessage());
			result.setError();
			result.setErrorMessage(e.getMessage());
			result.addLog(e.getMessage());
		} catch (final IOException e) {
			Logger.getLogger(StringEqualityMatcher.class.getName()).log(Level.SEVERE, e.getMessage());
		}
		this.options = (LazyMap) this.groovyScript.invokeMethod("parseOptions", this.strOptions);

	}

}
