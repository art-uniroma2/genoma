/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.CustomExceptions;

/**
 *
 * @author Roberto Enea
 */
public class UnableToUploadFileException extends Exception {
	// Parameterless Constructor
	public UnableToUploadFileException() {
	}

	// Constructor that accepts a message
	public UnableToUploadFileException(final String message) {
		super(message);
	}
}
