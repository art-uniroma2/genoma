/**
 * Copyright 2010 Websoft research group, Nanjing University, PR China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.uniroma2.art.genoma.MatcherModuleClient.vdoc;

import java.util.HashMap;

/**
 * @author Wei Hu
 * @see http://ws.nju.edu.cn
 */
public class Neighbor {
	private String uri = "";
	private HashMap<String, TokenNode> subjects = new HashMap<String, TokenNode>();
	private HashMap<String, TokenNode> predicates = new HashMap<String, TokenNode>();
	private HashMap<String, TokenNode> objects = new HashMap<String, TokenNode>();

	public Neighbor(final String u) {
		uri = u;
	}

	public void setUri(final String u) {
		uri = u;
	}

	public void addSubject(final TokenNode node) {
		subjects.put(node.getURI(), node);
	}

	public void addPredicate(final TokenNode node) {
		predicates.put(node.getURI(), node);
	}

	public void addObject(final TokenNode node) {
		objects.put(node.getURI(), node);
	}

	public String getURI() {
		return uri;
	}

	public HashMap<String, TokenNode> getSubjects() {
		return subjects;
	}

	public HashMap<String, TokenNode> getPredicates() {
		return predicates;
	}

	public HashMap<String, TokenNode> getObjects() {
		return objects;
	}
}
