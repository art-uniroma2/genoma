package it.uniroma2.art.genoma.MatcherModuleClient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.query.Binding;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.ComposerAggregator.TripleStore;

public class LinguisticMatcher extends Matcher {

	protected RepositoryConnection conn1, conn2;

	private Map<String, List<String>> synonymsMap;

	public LinguisticMatcher(MatchingResource ontology1, MatchingResource ontology2, TripleStore tripleStore,
			ArrayList<Collection<EntityAlignment>> SimMatrix, String options) {
		super(ontology1, ontology2, tripleStore, SimMatrix, options);
		conn1 = initLinguisticResource(ontology1);
		conn2 = initLinguisticResource(ontology2);
		synonymsMap = new HashMap<>();
	}

	protected RepositoryConnection initLinguisticResource(MatchingResource ontology) {
		if(ontology.getSynonymizerSPARQLEndpoint() != null) {
			final SPARQLRepository sparql = buildSPARQLEndpoint(ontology.getSynonymizerSPARQLEndpoint());
			sparql.initialize();
			RepositoryConnection conn = sparql.getConnection();
			return conn;
		} else {
			return null;
		}

	}

	protected List<String> getAllSynonyms(String term, String languageTag, RepositoryConnection conn) {
		if(term.length() == 1)
			return Collections.emptyList();
		else if(conn == null) {
			return Arrays.asList(term);
		}
		if(synonymsMap.get(term) == null) {
		final String queryString = SPARQLQueryBuilder.getQueryForSynomymsFromWrittenRep(term.replace("\n", ""), languageTag);
		final List<String> bindingVariables = Arrays.asList("writtenRep");
		final List<String> labels = new ArrayList<>();
		final TupleQuery query = conn.prepareTupleQuery(queryString);
		query.setIncludeInferred(false);
		try (final TupleQueryResult it = query.evaluate()) {
			while (it.hasNext()) {
				final BindingSet tuplebinding = it.next();
				for (final String bindingVariable : bindingVariables) {
					final Binding binding = tuplebinding.getBinding(bindingVariable);
					if (binding != null) {
						final Value label = binding.getValue();
						labels.add(label.stringValue());
					}
				}
			}
		}
		synonymsMap.put(term, labels);
		return labels;
		} else {
			return synonymsMap.get(term);
		}
	}

	protected List<String> getAllSynonymsOfCompoundWords(final String s, final String languageTag, RepositoryConnection conn) {
		if (synonymsMap.get(s) == null) {
			final String[] r = s.split("(?=\\p{Lu})");
			List<String> AllSynonyms = null;
			for (final String r1 : r) {
				final List<String> tempSynonyms = getAllSynonyms(r1, languageTag, conn);
				if (AllSynonyms != null && tempSynonyms != null) {
					AllSynonyms.addAll(tempSynonyms);
				} else if (AllSynonyms == null) {
					AllSynonyms = getAllSynonyms(r1, languageTag, conn);
				}
			}
			synonymsMap.put(s, AllSynonyms);
			return AllSynonyms;
		} else {
			return synonymsMap.get(s);
		}
	}

}
