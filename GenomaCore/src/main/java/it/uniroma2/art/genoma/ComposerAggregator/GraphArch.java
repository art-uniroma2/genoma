/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.ComposerAggregator;

/**
 *
 * @author Roberto Enea
 */
public class GraphArch {
	private int idNodeFrom;
	private int idNodeTo;

	public GraphArch(final int from, final int to) {
		this.idNodeFrom = from;
		this.idNodeTo = to;
	}

	public int getIdNodeFrom() {
		return this.idNodeFrom;
	}

	public int getidNodeTo() {
		return this.idNodeTo;
	}

}
