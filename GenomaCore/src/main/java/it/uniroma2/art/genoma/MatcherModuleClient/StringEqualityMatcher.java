/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.MatcherModuleClient;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.codec.digest.DigestUtils;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.ComposerAggregator.TripleStore;

/**
 *
 * @author Roberto Enea
 */

public class StringEqualityMatcher extends Matcher {

	public StringEqualityMatcher(final MatchingResource ontology1, final MatchingResource ontology2,
			final TripleStore tripleStore, final ArrayList<Collection<EntityAlignment>> SimMatrix,
			final String options) {
		super(ontology1, ontology2, tripleStore, SimMatrix, options);
		matcherId = "String Equality Matcher";
		this.alignmentId = DigestUtils.shaHex(this.ontology1.getUri() + this.ontology2.getUri() + this.matcherId);
	}

	@Override
	protected double similarityFunction(final String l1, final String l2, final String c1, final String c2,
			final String n1, final String n2) {
		return Math.max(equalityFunction(l1, l2) * labelWeight,
				Math.max(equalityFunction(c1, c2) * commentWeight, equalityFunction(n1, n2) * idWeight));
	}

	@Override
	protected double similarityFunction(final String l1, final String l2, final String n1, final String n2) {
		return Math.max(equalityFunction(l1, l2) * labelWeight, equalityFunction(n1, n2) * idWeight);
	}

	@Override
	protected double similarityFunction(final String n1, final String n2) {
		return equalityFunction(n1, n2) * idWeight;
	}

	private double equalityFunction(final String s1, final String s2) {
		if (s1 == null || s2 == null)
			return 0.0;
		if (s1.equals(s2)) {
			return 1.0;
		} else {
			return 0.0;
		}
	}

}
