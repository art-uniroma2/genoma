/**
 * Copyright 2010 Websoft research group, Nanjing University, PR China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.uniroma2.art.genoma.MatcherModuleClient.gmo;

import it.uniroma2.art.genoma.AlignmentTools.OntoResource;

/**
 * @author Wei Hu & Ningsheng Jian
 * @see http://ws.nju.edu.cn
 */
public class DataLiteralSim {
	public double getSimilarity(final OntoResource left, final OntoResource right) {
		return DataLiteralSim.getDataLiteralSim(left.getLabel(), right.getLabel());
	}

	public static double getDataLiteralSim(final String left, final String right) {
		if (left.equals(right)) {
			return 1;
		}
		return 0;
	}
}
