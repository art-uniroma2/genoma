/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package it.uniroma2.art.genoma.MatcherModuleClient;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;

/**
 *
 * @author Roberto Enea
 * @author Andrea Turbati
 */
public class MatcherReturn implements Serializable {
	private Collection<EntityAlignment> SimOutputMatrix;
	//private String log;
	StringBuilder logSB;
	private boolean error;
	private String errorMessage;

	public MatcherReturn(final Collection<EntityAlignment> om, final String l) {
		this.SimOutputMatrix = om;
		//this.log = l;
		logSB = new StringBuilder(l);
		this.error = false;
	}

	public Collection<EntityAlignment> getSimMatrix() {
		return this.SimOutputMatrix;
	}

	public void setSimMatrix(final Collection<EntityAlignment> m) {
		this.SimOutputMatrix = m;
	}

	public String getLog() {
		//return this.log;
		return logSB.toString();
	}

	public void clearLog() {
		// clear the content of the StringBuilder
		logSB.setLength(0); // setting to 0, it is cleared
	}

	/*
	 * public void resetLog() { this.log = ""; }
	 */

	public void addLog(final String l) {
		final java.util.Date date = new java.util.Date();
		//this.log += "\n[" + (new Timestamp(date.getTime())) + "]: " + l;
		logSB.append("\n[").append((new Timestamp(date.getTime()))).append("]: ").append(l);
	}

	public boolean IsError() {
		return error;
	}

	public void setError() {
		this.error = true;
	}

	public void setErrorMessage(final String em) {
		this.errorMessage = em;
	}

	public String getErrorMessage() {
		return this.errorMessage;
	}

}
