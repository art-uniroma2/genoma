package it.uniroma2.art.genoma.DataManagement;

import com.thoughtworks.xstream.XStream;

import java.io.*;
import java.util.LinkedHashMap;

public class DataManagementUtils {

    /*
    public static synchronized void saveRepositoryToFile(Object repository,
                                                         OutputStream resultSet, String filePath) throws IOException {
        XStream xstream = new XStream();
        xstream.allowTypesByWildcard(new String[] { "it.uniroma2.art.genoma.**" });
        final String xml = xstream.toXML(repository);
        if (resultSet instanceof FileOutputStream) {
            resultSet = new FileOutputStream(filePath);
        } else if (resultSet instanceof ByteArrayOutputStream) {
            ((ByteArrayOutputStream) resultSet).reset();
        }
        resultSet.write(xml.getBytes());
        resultSet.flush();
    }
     */

    public static synchronized void saveRepositoryToFile(Object repository,
                                                         ByteArrayOutputStream resultSet, String filePath) throws IOException {
        XStream xstream = new XStream();
        xstream.allowTypesByWildcard(new String[] { "it.uniroma2.art.genoma.**" });
        final String xml = xstream.toXML(repository);
        if (resultSet!=null) {
            resultSet.reset();
            resultSet.write(xml.getBytes());
            resultSet.flush();
        } else {
            try (FileOutputStream fos = new FileOutputStream(filePath)) {
                fos.write(xml.getBytes());
                fos.flush();
            }
        }
    }

}
