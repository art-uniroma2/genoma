/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.MatcherModuleClient;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.codec.digest.DigestUtils;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.ComposerAggregator.TripleStore;

/**
 *
 * @author Roberto Enea
 */

public class EditDistanceMatcher extends Matcher {

	public EditDistanceMatcher(final MatchingResource ontology1, final MatchingResource ontology2,
			final TripleStore tripleStore, final ArrayList<Collection<EntityAlignment>> SimMatrix,
			final String options) {
		super(ontology1, ontology2, tripleStore, SimMatrix, options);
		matcherId = "Edit Distance Matcher";
		this.alignmentId = DigestUtils.shaHex(this.ontology1.getUri() + this.ontology2.getUri() + this.matcherId);
	}

	@Override
	protected double similarityFunction(final String l1, final String l2, final String c1, final String c2,
			final String n1, final String n2) {
		return Math.max(editDistanceFunction(l1, l2) * labelWeight,
				Math.max(editDistanceFunction(c1, c2) * commentWeight, editDistanceFunction(n1, n2) * idWeight));
	}

	@Override
	protected double similarityFunction(final String l1, final String l2, final String n1, final String n2) {
		return Math.max(editDistanceFunction(l1, l2) * labelWeight, editDistanceFunction(n1, n2) * idWeight);
	}

	@Override
	protected double similarityFunction(final String n1, final String n2) {
		return editDistanceFunction(n1, n2) * idWeight;
	}

	private double editDistanceFunction(final String s1, final String s2) {
		if (s1 == null || s2 == null)
			return 0.0;
		final int edit = getEditDistance(s1, s2);
		final double sim = 1 / Math.exp(edit / (double) (s1.length() + s2.length() - edit));
		return sim;
	}

	private int getEditDistance(final String s, final String t) {
		int d[][];
		int n;
		int m;
		int i;
		int j;
		char s_i;
		char t_j;
		int cost;

		n = s.length();
		m = t.length();
		if (n == 0) {
			return m;
		}
		if (m == 0) {
			return n;
		}
		d = new int[n + 1][m + 1];
		for (i = 0; i <= n; i++) {
			d[i][0] = i;

		}
		for (j = 0; j <= m; j++) {
			d[0][j] = j;

		}
		for (i = 1; i <= n; i++) {
			s_i = s.charAt(i - 1);
			for (j = 1; j <= m; j++) {
				t_j = t.charAt(j - 1);
				if (s_i == t_j) {
					cost = 0;
				} else {
					cost = 1;
				}
				d[i][j] = Minimum(d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + cost);
			}
		}
		return d[n][m];

	}

	private static int Minimum(final int a, final int b, final int c) {
		int mi;
		mi = a;
		if (b < mi) {
			mi = b;
		}
		if (c < mi) {
			mi = c;
		}
		return mi;
	}

}
