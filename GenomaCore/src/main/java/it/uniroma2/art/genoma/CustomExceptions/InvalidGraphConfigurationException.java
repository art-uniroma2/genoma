/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.CustomExceptions;

/**
 *
 * @author Roberto Enea
 */
public class InvalidGraphConfigurationException extends Exception {
	// Parameterless Constructor
	public InvalidGraphConfigurationException() {
	}

	// Constructor that accepts a message
	public InvalidGraphConfigurationException(final String message) {
		super(message);
	}
}
