/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.MatcherModuleClient;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.CustomExceptions.InvalidMatchingParamsException;

/**
 *
 * @author Roberto Enea
 */
public class FilteringDictionary {
	public static final String[] filteringList = { "HardThreshold", "DeltaThreshold", "ProportionalThreshold",
			"MediaThreshold", "MaxOnly" };

	public static Collection<EntityAlignment> applyFiltering(final Collection<EntityAlignment> x, final String fm,
			final String param) throws InvalidMatchingParamsException {
		if (fm.equals(filteringList[0])) {
			return hardThreshold(x, param);
		} else if (fm.equals(filteringList[1])) {
			return deltaThreshold(x, param);
		} else if (fm.equals(filteringList[2])) {
			return proportionalThreshold(x, param);
		} else if (fm.equals(filteringList[3])) {
			return mediaThreshold(x, param);
		} else if (fm.equals(filteringList[4])) {
			return maxOnly(x, param);
		}
		throw new InvalidMatchingParamsException("The filtering method does not exist in genoma's db." + fm);
	}

	private static Collection<EntityAlignment> hardThreshold(final Collection<EntityAlignment> x,
			final String threshold) {
		final double th = Double.parseDouble(threshold);
		final Iterator<EntityAlignment> it = x.iterator();
		final Collection<EntityAlignment> res = new ArrayList<>();
		while (it.hasNext()) {
			final EntityAlignment al = it.next();
			if (al.getSimilarityValue() > th) {
				res.add(new EntityAlignment(al.getEntity1(), al.getEntity2(), al.getRelationType(),
						al.getSimilarityValue()));
			}
		}
		return res;
	}

	private static Collection<EntityAlignment> deltaThreshold(final Collection<EntityAlignment> x, final String delta) {
		final double th = findMax(x) - Double.parseDouble(delta);
		final Iterator<EntityAlignment> it = x.iterator();
		final Collection<EntityAlignment> res = new ArrayList<>();
		while (it.hasNext()) {
			final EntityAlignment al = it.next();
			if (al.getSimilarityValue() > th) {
				res.add(new EntityAlignment(al.getEntity1(), al.getEntity2(), al.getRelationType(),
						al.getSimilarityValue()));
			}
		}
		return res;
	}

	private static Collection<EntityAlignment> proportionalThreshold(final Collection<EntityAlignment> x,
			final String perc) {
		final double th = findMax(x) * Double.parseDouble(perc);
		final Iterator<EntityAlignment> it = x.iterator();
		final Collection<EntityAlignment> res = new ArrayList<>();
		while (it.hasNext()) {
			final EntityAlignment al = it.next();
			if (al.getSimilarityValue() > th) {
				res.add(new EntityAlignment(al.getEntity1(), al.getEntity2(), al.getRelationType(),
						al.getSimilarityValue()));
			}
		}
		return res;
	}

	private static Collection<EntityAlignment> mediaThreshold(final Collection<EntityAlignment> x, final String perc) {
		final double th = getMedia(x) * (1 + Double.parseDouble(perc));
		final Iterator<EntityAlignment> it = x.iterator();
		final Collection<EntityAlignment> res = new ArrayList<>();
		while (it.hasNext()) {
			final EntityAlignment al = it.next();
			if (al.getSimilarityValue() > th) {
				res.add(new EntityAlignment(al.getEntity1(), al.getEntity2(), al.getRelationType(),
						al.getSimilarityValue()));
			}
		}
		return res;
	}

	private static Collection<EntityAlignment> maxOnly(final Collection<EntityAlignment> x, final String thd) {
		final double th = Double.parseDouble(thd);
		final Map<String, EntityAlignment> result = new HashMap<>();
		final Iterator<EntityAlignment> it = x.iterator();
		final Collection<EntityAlignment> res = new ArrayList<>();
		while (it.hasNext()) {
			final EntityAlignment al = it.next();
			if (al.getSimilarityValue() > th) {
				if (result.get(al.getEntity1().getURIString()) == null) {
					result.put(al.getEntity1().getURIString(), al);
				} else if (result.get(al.getEntity1().getURIString()).getSimilarityValue() < al.getSimilarityValue()) {
					result.put(al.getEntity1().getURIString(), al);
				}
			}
		}
		res.addAll(result.values());
		return res;
	}

	private static double findMax(final Collection<EntityAlignment> x) {
		double res = 0.0;
		final Iterator<EntityAlignment> it = x.iterator();
		while (it.hasNext()) {
			final EntityAlignment al = it.next();
			if (res < al.getSimilarityValue()) {
				res = al.getSimilarityValue();
			}
		}
		return res;
	}

	private static double getMedia(final Collection<EntityAlignment> x) {
		double res = 0.0;
		final Iterator<EntityAlignment> it = x.iterator();
		while (it.hasNext()) {
			final EntityAlignment al = it.next();
			res += al.getSimilarityValue();
		}
		return res / x.size();
	}
}
