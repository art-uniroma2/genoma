/**
 * Copyright 2010 Websoft research group, Nanjing University, PR China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.uniroma2.art.genoma.MatcherModuleClient.vdoc;

/**
 * @author Wei Hu
 * @see http://ws.nju.edu.cn
 */
public class Parameters {
	public static boolean inclBlankNode = false;
	public static boolean inclNeighbor = false;
	public static boolean inclInstMatch = false;
	public static boolean inclExt = false;
	public static String stopwordsAddress = "";

	public static double localnameWeight = 1.0;
	public static double labelWeight = 0.5;
	public static double commentWeight = 0.25;

	public final static double initBlankNode = 1;
	public final static double iterValue = 2;

	public static double basicWeight = 1.0;
	public static double subjectWeight = 0.1;
	public static double predicateWeight = 0.1;
	public static double objectWeight = 0.1;
	public final static double threshold = 0;
}
