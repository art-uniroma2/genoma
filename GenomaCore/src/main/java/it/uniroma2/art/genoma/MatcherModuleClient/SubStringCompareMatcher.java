/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.MatcherModuleClient;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.codec.digest.DigestUtils;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.ComposerAggregator.TripleStore;

/**
 *
 * @author Roberto Enea
 */

public class SubStringCompareMatcher extends Matcher {

	public SubStringCompareMatcher(final MatchingResource ontology1, final MatchingResource ontology2,
			final TripleStore tripleStore, final ArrayList<Collection<EntityAlignment>> SimMatrix,
			final String options) {
		super(ontology1, ontology2, tripleStore, SimMatrix, options);
		matcherId = "SubString Compare Matcher";
		this.alignmentId = DigestUtils.shaHex(this.ontology1.getUri() + this.ontology2.getUri() + this.matcherId);
	}

	@Override
	protected double similarityFunction(final String l1, final String l2, final String c1, final String c2,
			final String n1, final String n2) {
		return Math.max(subStringCompare(l1, l2) * labelWeight,
				Math.max(subStringCompare(c1, c2) * commentWeight, subStringCompare(n1, n2) * idWeight));
	}

	@Override
	protected double similarityFunction(final String l1, final String l2, final String n1, final String n2) {
		return Math.max(subStringCompare(l1, l2) * labelWeight, subStringCompare(n1, n2) * idWeight);
	}

	@Override
	protected double similarityFunction(final String n1, final String n2) {
		return subStringCompare(n1, n2) * idWeight;
	}

	private double subStringCompare(String s1, String s2) {
		if (s1 == null || s2 == null)
			return 0.0;
		s1 = StringNormalizer.caseNormalisation(s1);
		s2 = StringNormalizer.caseNormalisation(s2);
		double res = 0.0;
		double max_length = 0;
		for (int i = 0; i < s1.length() - 2; i++) {
			for (int j = i + 2; j < s1.length(); j++) {
				if (s2.contains(s1.subSequence(i, j)) && j + 1 - i > max_length) {
					max_length = j - i;
				}
			}
		}
		for (int i = 0; i < s2.length() - 2; i++) {
			for (int j = i + 2; j < s2.length(); j++) {
				if (s1.contains(s2.subSequence(i, j)) && j + 1 - i > max_length) {
					max_length = j + 1 - i;
				}
			}
		}
		res = 2 * max_length / (s1.length() + s2.length());
		return res;
	}

}
