/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.MatcherModuleClient;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.codec.digest.DigestUtils;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.ComposerAggregator.TripleStore;

/**
 *
 * @author Roberto Enea
 */

public class StringHammingDistanceMatcher extends Matcher {

	public StringHammingDistanceMatcher(final MatchingResource ontology1, final MatchingResource ontology2,
			final TripleStore tripleStore, final ArrayList<Collection<EntityAlignment>> SimMatrix,
			final String options) {
		super(ontology1, ontology2, tripleStore, SimMatrix, options);
		matcherId = "String Hamming Distance Matcher";
		this.alignmentId = DigestUtils.shaHex(this.ontology1.getUri() + this.ontology2.getUri() + this.matcherId);
	}

	@Override
	protected double similarityFunction(final String l1, final String l2, final String c1, final String c2,
			final String n1, final String n2) {
		return Math.max(hammingDistance(l1, l2) * labelWeight,
				Math.max(hammingDistance(c1, c2) * commentWeight, hammingDistance(n1, n2) * idWeight));
	}

	@Override
	protected double similarityFunction(final String l1, final String l2, final String n1, final String n2) {
		return Math.max(hammingDistance(l1, l2) * labelWeight, hammingDistance(n1, n2) * idWeight);
	}

	@Override
	protected double similarityFunction(final String n1, final String n2) {
		return hammingDistance(n1, n2) * idWeight;
	}

	private double hammingDistance(final String s1, final String s2) {
		double res = 0.0;
		if(s1 == null || s2 == null) {
			return res;
		}
		for (int i = 0; i < Math.min(s1.length(), s2.length()); i++) {
			if (s1.charAt(i) != s2.charAt(i)) {
				res++;
			}
		}
		res = (res + Math.abs(s1.length() - s2.length())) / Math.max(s1.length(), s2.length());
		return (1.0 - res);
	}

}
