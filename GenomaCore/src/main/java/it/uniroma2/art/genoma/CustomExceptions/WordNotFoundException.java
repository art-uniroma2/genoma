/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.CustomExceptions;

/**
 *
 * @author Roberto Enea
 */
public class WordNotFoundException extends Exception {
	// Parameterless Constructor
	public WordNotFoundException() {
	}

	// Constructor that accepts a message
	public WordNotFoundException(final String message) {
		super(message);
	}
}
