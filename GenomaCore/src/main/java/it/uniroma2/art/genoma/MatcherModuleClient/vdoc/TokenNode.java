/**
 * Copyright 2010 Websoft research group, Nanjing University, PR China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.uniroma2.art.genoma.MatcherModuleClient.vdoc;

import java.util.HashMap;

import it.uniroma2.art.genoma.AlignmentTools.OntoResource;

/**
 * @author Wei Hu
 * @see http://ws.nju.edu.cn
 */
public class TokenNode {
	private OntoResource node;
	private HashMap<String, WordFrequency> localname = null;
	private HashMap<String, WordFrequency> label = null;
	private HashMap<String, WordFrequency> comment = null;

	public TokenNode(final OntoResource n) {
		node = n;
	}

	public TokenNode(final OntoResource n, final HashMap<String, WordFrequency> name,
			final HashMap<String, WordFrequency> lbl, final HashMap<String, WordFrequency> cmt) {
		node = n;
		if (name != null) {
			localname = name;
		}
		if (lbl != null) {
			label = lbl;
		}
		if (cmt != null) {
			comment = cmt;
		}
	}

	public OntoResource getNode() {
		return node;
	}

	public String getURI() {
		return node.getURIString();
	}

	public int getNodeCategory() {
		return node.getCategory();
	}

	public HashMap<String, WordFrequency> getLocalName() {
		return localname;
	}

	public HashMap<String, WordFrequency> getLabel() {
		return label;
	}

	public HashMap<String, WordFrequency> getComment() {
		return comment;
	}

	public void setLocalName(final HashMap<String, WordFrequency> name) {
		if (name != null) {
			localname = name;
		}
	}

	public void setLabel(final HashMap<String, WordFrequency> lbl) {
		if (lbl != null) {
			label = lbl;
		}
	}

	public void setComment(final HashMap<String, WordFrequency> cmt) {
		if (cmt != null) {
			comment = cmt;
		}
	}
}
