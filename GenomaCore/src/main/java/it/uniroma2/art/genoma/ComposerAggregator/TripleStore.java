package it.uniroma2.art.genoma.ComposerAggregator;

public class TripleStore {
	private String url;
	private String username;
	private String password;

	public TripleStore(final String url, final String username, final String password) {
		this.url = url;
		this.username = username;
		this.password = password;
	}

	public String getUrl() {
		return this.url;
	}

	public String getUsername() {
		return this.username;
	}

	public String getPassword() {
		return this.password;
	}

}
