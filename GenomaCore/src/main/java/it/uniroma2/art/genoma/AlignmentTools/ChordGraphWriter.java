/**
 * Copyright 2010 Websoft research group, Nanjing University, PR China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.uniroma2.art.genoma.AlignmentTools;

import it.uniroma2.art.genoma.ComposerAggregator.MatcherConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

/**
 * @author Roberto Enea
 *
 */
public class ChordGraphWriter {

	private static final Logger log = LoggerFactory.getLogger(ChordGraphWriter.class);
	private ArrayList<EntityAlignment> simMatrix = null;
	// private RandomAccessFile raf = null;
	//private OutputStream raf;
	private String filepath = null;
	private ArrayList<String> writeList = null;
	private int[][] matrix;
	//private String matrixString;
	private StringBuilder matrixStringSB;
	//private String names;
	private StringBuilder namesSB;
	private int respondents;
	private final double emptyPerc;
	private int emptyStroke;
	private int onto1Size;
	private int onto2Size;

	public ChordGraphWriter(final Collection<EntityAlignment> as, final double ep)
			throws FileNotFoundException {
		simMatrix = (ArrayList<EntityAlignment>) as;
		writeList = new ArrayList<>();
		emptyPerc = ep;
		respondents = 0;
		onto1Size = 0;
		onto2Size = 0;
		matrixStringSB = new StringBuilder();
		namesSB = new StringBuilder();
	}

	public void write(MatcherConfiguration matcherConfiguration, MatcherConfiguration.FileType fileType,
						final String onto1, final String onto2, final String uri1, final String uri2)
			throws IOException {
		writeNS();
		writeStart(onto1, onto2, uri1, uri2);
		onto1Size = getOnto1Size(simMatrix, OntoResource.CLASS);
		onto2Size = getOnto2Size(simMatrix, OntoResource.CLASS);
		if (onto1Size != 0 && onto2Size != 0) {
			populateMatrixAndNames(OntoResource.CLASS);
			writeMatrixString(OntoResource.CLASS);
			writeNames(OntoResource.CLASS);
			writeMatrix(OntoResource.CLASS);
			writeEnd(OntoResource.CLASS);
		}
		onto1Size = getOnto1Size(simMatrix, OntoResource.DATATYPEPROPERTY);
		onto2Size = getOnto2Size(simMatrix, OntoResource.DATATYPEPROPERTY);
		if (onto1Size != 0 && onto2Size != 0) {
			populateMatrixAndNames(OntoResource.DATATYPEPROPERTY);
			writeMatrixString(OntoResource.DATATYPEPROPERTY);
			writeNames(OntoResource.DATATYPEPROPERTY);
			writeMatrix(OntoResource.DATATYPEPROPERTY);
			writeEnd(OntoResource.DATATYPEPROPERTY);
		}
		onto1Size = getOnto1Size(simMatrix, OntoResource.OBJECTPROPERTY);
		onto2Size = getOnto2Size(simMatrix, OntoResource.OBJECTPROPERTY);
		if (onto1Size != 0 && onto2Size != 0) {
			populateMatrixAndNames(OntoResource.OBJECTPROPERTY);
			writeMatrixString(OntoResource.OBJECTPROPERTY);
			writeNames(OntoResource.OBJECTPROPERTY);
			writeMatrix(OntoResource.OBJECTPROPERTY);
			writeEnd(OntoResource.OBJECTPROPERTY);
		}
		onto1Size = getOnto1Size(simMatrix, OntoResource.INSTANCE);
		onto2Size = getOnto2Size(simMatrix, OntoResource.INSTANCE);
		if (onto1Size != 0 && onto2Size != 0) {
			populateMatrixAndNames(OntoResource.INSTANCE);
			writeMatrixString(OntoResource.INSTANCE);
			writeNames(OntoResource.INSTANCE);
			writeMatrix(OntoResource.INSTANCE);
			writeEnd(OntoResource.INSTANCE);
		}
		writeCloseAlignment();
		//writeToFile(matcherConfiguration, fileType);
	}

	public void writeNS() {
		final String temp = "<?xml version='1.0' encoding='utf-8'?>\n";
		writeList.add(temp);
	}

	public void writeStart(final String onto1, final String onto2, final String uri1, final String uri2) {
		final String temp = "<AlignmentGraph>\n" + "  <onto1>" + onto1 + "</onto1>\n" + "  <onto2>" + onto2
				+ "</onto2>\n" + "  <uri1>" + uri1 + "</uri1>\n" + "  <uri2>" + uri2 + "</uri2>";
		writeList.add(temp);
	}

	private void writeMatrix(final int entityType) {
		String temp = "";
		switch (entityType) {
		case OntoResource.CLASS:
			//temp = "<classmatrix>" + matrixString + "</classmatrix>";
			temp = "<classmatrix>" + matrixStringSB.toString() + "</classmatrix>";
			break;
		case OntoResource.DATATYPEPROPERTY:
			//temp = "<datatypepropertymatrix>" + matrixString + "</datatypepropertymatrix>";
			temp = "<datatypepropertymatrix>" + matrixStringSB.toString() + "</datatypepropertymatrix>";
			break;
		case OntoResource.OBJECTPROPERTY:
			//temp = "<objectpropertymatrix>" + matrixString + "</objectpropertymatrix>";
			temp = "<objectpropertymatrix>" + matrixStringSB.toString() + "</objectpropertymatrix>";
			break;
		case OntoResource.INSTANCE:
			//temp = "<instancematrix>" + matrixString + "</instancematrix>";
			temp = "<instancematrix>" + matrixStringSB.toString() + "</instancematrix>";
			break;
		}
		writeList.add(temp);
	}

	private void writeNames(final int entityType) {
		String temp = "";
		switch (entityType) {
		case OntoResource.CLASS:
			//temp = "<classnames>" + names + "</classnames>";
			temp = "<classnames>" + namesSB.toString() + "</classnames>";
			break;
		case OntoResource.DATATYPEPROPERTY:
			//temp = "<datatypepropertynames>" + names + "</datatypepropertynames>";
			temp = "<datatypepropertynames>" + namesSB.toString() + "</datatypepropertynames>";
			break;
		case OntoResource.OBJECTPROPERTY:
			//temp = "<objectpropertynames>" + names + "</objectpropertynames>";
			temp = "<objectpropertynames>" + namesSB.toString() + "</objectpropertynames>";
			break;
		case OntoResource.INSTANCE:
			//temp = "<instancenames>" + names + "</instancenames>";
			temp = "<instancenames>" + namesSB.toString() + "</instancenames>";
			break;
		}
		writeList.add(temp);
	}

	public void writeEnd(final int entityType) {
		String temp = "";
		switch (entityType) {
		case OntoResource.CLASS:
			temp = "<classrespondents>" + respondents + "</classrespondents>";
			break;
		case OntoResource.DATATYPEPROPERTY:
			temp = "<datatypepropertyrespondents>" + respondents + "</datatypepropertyrespondents>";
			break;
		case OntoResource.OBJECTPROPERTY:
			temp = "<objectpropertyrespondents>" + respondents + "</objectpropertyrespondents>";
			break;
		case OntoResource.INSTANCE:
			temp = "<instancerespondents>" + respondents + "</instancerespondents>";
			break;
		}
		// temp += "<emptyperc>"+emptyPerc+"</emptyperc>";
		// temp += " </AlignmentGraph>\n";
		writeList.add(temp);
	}

	public void writeCloseAlignment() {
		String temp = "<emptyperc>" + emptyPerc + "</emptyperc>";
		temp += "  </AlignmentGraph>\n";
		writeList.add(temp);
	}

	public void writeToFile(MatcherConfiguration matcherConfiguration, MatcherConfiguration.FileType fileType) throws IOException {
		try(OutputStream os = matcherConfiguration.getOutFile(fileType)) {
            for (String string : writeList) {
                final String t = string + "\r\n";
                os.write(t.getBytes());
            }
		}
	}

	private int populateMatrixAndNames(final int entityType) {
		//String rows = "";
		//String columns = "";
		StringBuilder rowsSB = new StringBuilder();
		StringBuilder columnsSB = new StringBuilder();
		//names = "[";
		namesSB = new StringBuilder();
		namesSB.append("[");
		respondents = 0;
		int j = -1;
		if (onto1Size == 0 || onto2Size == 0) {
			return 0;
		}
		final int matrixSize = onto1Size + onto2Size + 2;
		matrix = new int[matrixSize][matrixSize];
		boolean columnsoff = false;
		for (int i = 0; i < simMatrix.size(); i++) {
			if (simMatrix.get(i).getEntity1().getCategory() != entityType
					|| simMatrix.get(i).getEntity2().getCategory() != entityType) {
				continue;
			}
			j++;
			respondents += (int) Math.round(simMatrix.get(i).getSimilarityValue() * 100);
			int x, y;
			x = (int) Math.floor(j / onto2Size);
			// if(x >= onto1Size) x++;
			y = onto1Size + 1 + j % onto2Size;
			matrix[x][y] = (int) Math.round(simMatrix.get(i).getSimilarityValue() * 100);
			matrix[y][x] = (int) Math.round(simMatrix.get(i).getSimilarityValue() * 100);
			if (j == 0) {
				//rows += "\"" + simMatrix.get(i).getEntity1().getLocalName() + "\"";
				rowsSB.append("\"").append(simMatrix.get(i).getEntity1().getLocalName()).append("\"");
				//columns += ",\"" + simMatrix.get(i).getEntity2().getLocalName() + "\"";
				columnsSB.append(",\"").append(simMatrix.get(i).getEntity2().getLocalName()).append("\"");
			} else {
				if (!simMatrix.get(i).getEntity1().getURIString()
						.equals(simMatrix.get(i - 1).getEntity1().getURIString())) {
					//rows += ",\"" + simMatrix.get(i).getEntity1().getLocalName() + "\"";
					rowsSB.append(",\"").append(simMatrix.get(i).getEntity1().getLocalName()).append("\"");
					columnsoff = true;
				} else {
					if (!columnsoff) {
						//columns += ",\"" + simMatrix.get(i).getEntity2().getLocalName() + "\"";
						columnsSB.append(",\"").append(simMatrix.get(i).getEntity2().getLocalName()).append("\"");
					}
				}
			}
		}
		//names += rows + ",\"\"" + columns + ",\"\"" + "]";
		namesSB.append(rowsSB).append(",\"\"").append(columnsSB).append(",\"\"").append("]");
		emptyStroke = (int) Math.round(respondents * emptyPerc);
		matrix[onto1Size][onto1Size + onto2Size + 1] = emptyStroke;
		matrix[onto1Size + onto2Size + 1][onto1Size] = emptyStroke;
		return matrixSize;
	}

	private int getOnto2Size(final ArrayList<EntityAlignment> sm, final int entityType) {
		final HashSet onto2 = new HashSet();
		for (int i = 1; i < sm.size(); i++) {
			if (simMatrix.get(i).getEntity2().getCategory() != entityType) {
				continue;
			}
			onto2.add(sm.get(i).getEntity2().getURIString());
		}
		return onto2.size();
	}

	private int getOnto1Size(final ArrayList<EntityAlignment> sm, final int entityType) {
		final HashSet onto1 = new HashSet();
		for (int i = 1; i < sm.size(); i++) {
			if (simMatrix.get(i).getEntity1().getCategory() != entityType) {
				continue;
			}
			onto1.add(sm.get(i).getEntity1().getURIString());
		}
		return onto1.size();
	}

	private void writeMatrixString(final int entityType) {
		final String onto2ZeroStrip = getZeroStrip(onto2Size + 1, 2);
		final String onto1ZeroStrip = getZeroStrip(onto1Size + 1, 1);
		final int matrixSize = onto1Size + onto2Size + 2;
		//matrixString = "[";
		matrixStringSB = new StringBuilder();
		matrixStringSB.append("[");
		for (int i = 0; i < onto1Size + 1; i++) {
			//matrixString += "[" + onto1ZeroStrip;
			matrixStringSB.append("[").append(onto1ZeroStrip);
			for (int j = onto1Size + 1; j < matrixSize; j++) {
				//matrixString += matrix[i][j];
				matrixStringSB.append(matrix[i][j]);
				if (j < (matrixSize - 1)) {
					//matrixString += ",";
					matrixStringSB.append(",");
				}
			}
			//matrixString += "],";
			matrixStringSB.append("],");
		}
		for (int i = onto1Size + 1; i < matrixSize; i++) {
			//matrixString += "[";
			matrixStringSB.append("[");
			for (int j = 0; j < onto1Size + 1; j++) {
				//matrixString += matrix[i][j];
				matrixStringSB.append(matrix[i][j]);
				if (j < matrixSize - 1) {
					//matrixString += ",";
					matrixStringSB.append(",");
				}
			}
			//matrixString += onto2ZeroStrip + "]";
			matrixStringSB.append(onto2ZeroStrip).append("]");
			if (i < (matrixSize - 1)) {
				//matrixString += ",";
				matrixStringSB.append(",");
			}
		}
		//matrixString += "]";
		matrixStringSB.append("]");
		log.info("Sim Matrix Size:" + simMatrix.size());
		log.info("Size first ontology:" + onto1Size);
		log.info("Size second ontology:" + onto2Size);
		log.info("first half:" + (matrixSize));
		log.info("second half:" + (onto2Size + 1 + onto1Size + 1));
	}

	private String getZeroStrip(final int size, final int ontology) {
		//String res = "";
		StringBuilder resSB = new StringBuilder();
		int i = 0;
		if (ontology == 2) {
			//res = "0";
			resSB.append("0");
			i = 1;
		}
		for (; i < size; i++) {
			if (ontology == 1) {
				//res += "0,";
				resSB.append("0,");
			} else if (ontology == 2) {
				//res += ",0";
				resSB.append(",0");
			}
		}
		//return res;
		return resSB.toString();
	}
}
