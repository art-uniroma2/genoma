/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.MatcherModuleClient;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.codec.digest.DigestUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.common.collect.ObjectArrays;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.ComposerAggregator.TripleStore;
import it.uniroma2.art.genoma.CustomExceptions.InvalidMatchingParamsException;
import it.uniroma2.art.genoma.CustomExceptions.WordNotFoundException;

/**
 *
 * @author Roberto Enea
 */

public class SynonymsMatcher extends LinguisticMatcher {
	

	public SynonymsMatcher(final MatchingResource ontology1, final MatchingResource ontology2,
			final TripleStore tripleStore, final ArrayList<Collection<EntityAlignment>> SimMatrix,
			final String options) {
		super(ontology1, ontology2, tripleStore, SimMatrix, options);
		matcherId = "Synonyms Matcher";
		this.alignmentId = DigestUtils.shaHex(this.ontology1.getUri() + this.ontology2.getUri() + this.matcherId);
	}

	@Override
	protected double similarityFunction(final String l1, final String l2, final String n1, final String n2) {
		return Math.max(synonymySimilarity(l1, l2, ontology1.getLanguage()) * labelWeight,
				synonymySimilarity(n1, n2, ontology1.getLanguage()) * idWeight);
	}

	protected double similarityFunction(final String n1, final String n2, final String languageTag) {
		return synonymySimilarity(n1, n2, languageTag) * idWeight;
	}


	private double synonymySimilarity(final String s1, final String s2, final String languageTag) {
		if (s1 == null || s2 == null)
			return 0.0;
		final List<String> s1Synonyms = getAllSynonymsOfCompoundWords(s1, languageTag, conn1);
		final List<String> s2Synonyms = getAllSynonymsOfCompoundWords(s2, languageTag, conn2);
		if (s1Synonyms == null || s2Synonyms == null)
			return 0.0;
		for (final String x : s1Synonyms) {
			for (final String y : s2Synonyms) {
				if (x.equals(y))
					return 1.0;
			}
		}
		return 0.0;
	}

}
