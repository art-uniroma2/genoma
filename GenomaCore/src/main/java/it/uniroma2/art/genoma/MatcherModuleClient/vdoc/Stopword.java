/**
 * Copyright 2010 Websoft research group, Nanjing University, PR China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.uniroma2.art.genoma.MatcherModuleClient.vdoc;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

/**
 * @author Wei Hu
 * @see http://ws.nju.edu.cn
 */
public class Stopword {
	private ArrayList<String> stopwordlist = new ArrayList<String>();

	public Stopword() {
		try {
			final URL url = Stopword.class.getClassLoader().getResource("stopwords.txt");;
			final InputStreamReader fr = new InputStreamReader(url.openStream());
			final BufferedReader br = new BufferedReader(fr);
			String s = br.readLine();
			while (s != null) {
				stopwordlist.add(s.trim());
				s = br.readLine();
			}
			fr.close();
			br.close();
		} catch (final FileNotFoundException e) {
			e.printStackTrace();
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	public String[] removeStopword(final String src[]) {
		final int length = src.length;
		if (length == 1) {
			return src;
		} else {
			final ArrayList<String> temp = new ArrayList<String>();
			for (int i = 0; i < src.length; i++) {
				if (!stopwordlist.contains(src[i])) {
					temp.add(src[i]);
				}
			}
			final String des[] = new String[temp.size()];
			for (int i = 0; i < temp.size(); i++) {
				des[i] = temp.get(i);
			}
			return des;
		}
	}

	public ArrayList<String> getStopwordList() {
		final Stopword sw = new Stopword();
		return sw.stopwordlist;
	}
}
