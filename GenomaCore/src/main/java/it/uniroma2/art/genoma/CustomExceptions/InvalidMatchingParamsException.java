/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.CustomExceptions;

/**
 *
 * @author Roberto Enea
 */
public class InvalidMatchingParamsException extends Exception {
	// Parameterless Constructor
	public InvalidMatchingParamsException() {
	}

	// Constructor that accepts a message
	public InvalidMatchingParamsException(final String message) {
		super(message);
	}
}
