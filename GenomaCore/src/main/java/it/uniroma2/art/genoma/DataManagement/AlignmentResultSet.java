/*
 * To change this license header, choose License Headers in Project Properties.
 *9632 To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.DataManagement;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Set;

/**
 *
 * @author Roberto Enea
 */
public class AlignmentResultSet {
	private static String filePath;
	//private static OutputStream resultSet;
	private static LinkedHashMap<String, Result> resultRepository = new LinkedHashMap<>();

	/*
	public synchronized static void init(final OutputStream resSet) {
		resultSet = resSet;
	}
	 */

	public synchronized static void addResult(final String id, final Result res) throws IOException {
		final String key = res.getAlignmentId();
		if (AlignmentResultSet.resultRepository.containsKey(key))
			throw new IOException("Alignment already computed.");
		AlignmentResultSet.resultRepository.put(key, res);
	}

	public synchronized static Result getResult(final String id) {
		return AlignmentResultSet.resultRepository.get(id);
	}

	public synchronized static Set<String> getKeySet() {
		return AlignmentResultSet.resultRepository.keySet();
	}

	/*
	public synchronized static OutputStream getResultSetOutputStream() {
		return AlignmentResultSet.resultSet;
	}
	 */

	public synchronized static Result removeResult(final String id) {
		return AlignmentResultSet.resultRepository.remove(id);
	}

	@SuppressWarnings("unchecked")
	public static boolean loadRepositoryFromFile(final String filePath) {
		AlignmentResultSet.filePath = filePath;
		try {
			XStream xstream = new XStream();
			xstream.allowTypesByWildcard(new String[] { "it.uniroma2.art.genoma.**" });
			try (FileInputStream fis = new FileInputStream(filePath)) {
				AlignmentResultSet.resultRepository = (LinkedHashMap<String, Result>) xstream
						.fromXML(IOUtils.toString(fis));
			}
			return true;
		} catch (IOException | XStreamException e) {
			AlignmentResultSet.resultRepository = new LinkedHashMap<>();
			return false;
		}
	}

	/*
	public synchronized static void saveRepositoryToFile() throws IOException {
		DataManagementUtils.saveRepositoryToFile(resultRepository, filePath);
	}
	 */

	public synchronized static void saveRepositoryToFile(ByteArrayOutputStream resultSet) throws IOException {
		DataManagementUtils.saveRepositoryToFile(resultRepository, resultSet, filePath);
	}

	public static boolean isRepositoryNull() {
		return (resultRepository == null);
	}

}