/**
 * Copyright 2010 Websoft research group, Nanjing University, PR China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.uniroma2.art.genoma.AlignmentTools;

import it.uniroma2.art.genoma.ComposerAggregator.MatcherConfiguration;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Roberto Enea
 *
 */
public class AlignmentWriter {
	private Collection<EntityAlignment> alignment = null;
	//private OutputStream os = null;
	private ArrayList<String> writeList = null;

	public AlignmentWriter(final Collection<EntityAlignment> as) throws FileNotFoundException {
		alignment = as;
		writeList = new ArrayList<>();
	}

	public void write(MatcherConfiguration matcherConfiguration, MatcherConfiguration.FileType fileType,
					  final String onto1, final String onto2, final String uri1, final String uri2)
			throws IOException {
		writeNS();
		writeStart("yes", "0", "??", onto1, onto2, uri1, uri2);
        for (EntityAlignment align : this.alignment) {
            final String e1 = align.getEntity1().getURIString();
            final String e2 = align.getEntity2().getURIString();
            final String measure = Double.toString(align.getSimilarityValue());
            final String type = "" + align.getRelationType();
            writeElement(e1, e2, measure, type);
        }
		writeEnd();
		writeToFile(matcherConfiguration, fileType);
	}

	public void writeNS() {
		final String temp = "<?xml version='1.0' encoding='utf-8'?>\n"
				+ "<rdf:RDF xmlns='http://knowledgeweb.semanticweb.org/heterogeneity/alignment#' \n"
				+ "xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#' \n"
				+ "xmlns:xsd='http://www.w3.org/2001/XMLSchema#'>\n";
		writeList.add(temp);
	}

	public void writeStart(final String xml, final String level, final String type, final String onto1,
			final String onto2, final String uri1, final String uri2) {
		final String temp = "<Alignment>\n" + "  <xml>" + xml + "</xml>\n" + "  <level>" + level + "</level>\n"
				+ "  <type>" + type + "</type>\n" + "  <onto1>" + onto1 + "</onto1>\n" + "  <onto2>" + onto2
				+ "</onto2>\n" + "  <uri1>" + uri1 + "</uri1>\n" + "  <uri2>" + uri2 + "</uri2>";
		writeList.add(temp);
	}

	public void writeEnd() {
		final String temp = "  </Alignment>\n</rdf:RDF>";
		writeList.add(temp);
	}

	public void writeElement(final String res1, final String res2, final String measure) {
		final String temp = "    <map>\n" + "      <Cell>\n" + "        <entity1 rdf:resource=\"" + res1 + "\"/>\n"
				+ "        <entity2 rdf:resource=\"" + res2 + "\"/>\n"
				+ "        <measure rdf:datatype=\"http://www.w3.org/2001/XMLSchema#float\">" + measure + "</measure>\n"
				+ "        <relation>=</relation>\n" + "      </Cell>\n" + "    </map>";
		writeList.add(temp);
	}

	public void writeElement(final String res1, final String res2, final String measure, final String r) {
		final String temp = "    <map>\n" + "      <Cell>\n" + "        <entity1 rdf:resource=\"" + res1 + "\"/>\n"
				+ "        <entity2 rdf:resource=\"" + res2 + "\"/>\n"
				+ "        <measure rdf:datatype=\"http://www.w3.org/2001/XMLSchema#float\">" + measure + "</measure>\n"
				+ "        <relation>" + r + "</relation>\n" + "      </Cell>\n" + "    </map>";
		writeList.add(temp);
	}

	public void writeToFile(MatcherConfiguration matcherConfiguration, MatcherConfiguration.FileType fileType) throws IOException {
		try (OutputStream os = matcherConfiguration.getOutFile(fileType)) {
			//final StringBuffer s = new StringBuffer();
            for (String s : writeList) {
                final String t = s + "\r\n";
                //s.append(t);
                os.write(t.getBytes());
            }
		}
	}
}
