/**
 * Copyright 2010 Websoft research group, Nanjing University, PR China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.uniroma2.art.genoma.MatcherModuleClient.vdoc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.MatcherModuleClient.SPARQLQueryBuilder;
import it.uniroma2.art.genoma.ontolex.ONTOLEX;
import org.eclipse.rdf4j.model.BNode;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.vocabulary.*;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.AlignmentTools.OntoResource;

/**
 * @author Roberto Enea
 *
 */
public class VDocParser {
	private TokenNodeSet tokenNodes = new TokenNodeSet();
	private NeighborSet neighbors = new NeighborSet();
	private Tokenizer tokenizer = new Tokenizer();
	private Stopword stopword = new Stopword();
	private ArrayList<Collection<EntityAlignment>> InputMatrix;
	private int modelNumber;
	private MatchingResource onto;

	VDocParser(final ArrayList<Collection<EntityAlignment>> im, final int mn, final MatchingResource onto) {
		this.InputMatrix = im;
		this.modelNumber = mn;
		this.onto = onto;
	}

	public void run(final RepositoryConnection conn) throws Exception {
		if (this.InputMatrix.isEmpty()) {
			parseNamedClasses(conn);
			if(onto.getLexicalization().equals(MatchingResource.Lexicalization.RDFS)) {
				parseProperties(conn);
			}
			if (Parameters.inclInstMatch) {
				parseInstances(conn);
			}

			if (Parameters.inclBlankNode) {
				parseBlankNodes(conn);
			}
			if (Parameters.inclNeighbor) {
				parseNeighbors(conn);

			}
		} else {
			parseAllEntities();
		}
	}

	private TupleQueryResult getCategoryNodesFromOntology(final RepositoryConnection model, final String entityType,
														  final MatchingResource.Lexicalization lexicalization,
														  final String language) throws Exception {
		// @formatter:off
		/*
		final String queryString = "SELECT DISTINCT ?entity ?entityLabel ?entityComment " + "\nWHERE {" + "\n?entity <"
				+ RDF.TYPE.stringValue() + "> <" + entityType + ">. " + "\nOPTIONAL{ ?entity <" + RDFS.LABEL
				+ "> ?entityLabel. " + "\nFILTER(lang(?entityLabel) = \"en\")} ." + "\nOPTIONAL{?entity <"
				+ RDFS.COMMENT + "> ?entityComment." + "\nFILTER(lang(?entityComment) = \"en\") }" + "\n}";*/
		// @formatter:on
		final String queryString = SPARQLQueryBuilder.getQueryFromLexicalization(lexicalization, entityType, language);
		final TupleQuery query1 = model.prepareTupleQuery(queryString);
		query1.setIncludeInferred(false);
        return query1.evaluate(); // it should be closed by the calling method
	}

	private Iterator<EntityAlignment> getCategoryNodesFromInputMatrix() {
		return this.InputMatrix.iterator().next().iterator();
	}

	private TupleQueryResult getInstancesOfClass(final RepositoryConnection model, final String classIri) {
		// @formatter:off
		final String prefix = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ "PREFIX owl: <http://www.w3.org/2002/07/owl#>"
				+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n";
		final String queryString = prefix + "SELECT DISTINCT ?instance ?instanceLabel ?class " + "\nWHERE {"
				+ "\n?class rdfs:subClassOf* <" + classIri + "> . " + "\n?instance rdf:type ?class ." + "\n?instance <"
				+ RDFS.LABEL + "> ?instanceLabel" + "\n}";
		// @formatter:off
		final TupleQuery query1 = model.prepareTupleQuery(queryString);
		query1.setIncludeInferred(false);
		return query1.evaluate(); // it should be closed by the calling method
	}

	private TupleQueryResult getAllStatements(final RepositoryConnection model) {
		// @formatter:off
		final String queryString = "SELECT DISTINCT ?subject ?predicate ?object "
				+ "\nWHERE {?subject ?predicate ?object}";
		// @formatter:off
		final TupleQuery query1 = model.prepareTupleQuery(queryString);
		query1.setIncludeInferred(false);
		return query1.evaluate(); // it should be closed by the calling method
	}

	private void parseAllEntities() {
		final Iterator<EntityAlignment> it = getCategoryNodesFromInputMatrix();
		OntoResource entity;
		while (it.hasNext()) {
			if (modelNumber == 1) {
				entity = it.next().getEntity1();
			} else {
				entity = it.next().getEntity2();
			}
			final String n = entity.getLocalName();
			final HashMap<String, WordFrequency> localname = new HashMap<String, WordFrequency>();
			if (n != null) {
				setWordTimes(n, localname);
			}
			ArrayList<String> l = null;
			final HashMap<String, WordFrequency> label = new HashMap<String, WordFrequency>();
			if (entity.getLabel() != null) {
				l = new ArrayList<String>(Arrays.asList(entity.getLabel().split("\\s*,\\s*")));
			} else if(entity.getLabels() != null) {
				l = new ArrayList<>();
				for(String lab: entity.getLabels()) {
					l.addAll(Arrays.asList(lab.split("\\s*,\\s*")));
				}
			}
			if (l != null) {
				for (int i = 0; i < l.size(); i++) {
					setWordTimes(l.get(i), label);
				}
			}
			ArrayList<String> c = null;
			if (entity.getComment() != null) {
				c = new ArrayList<String>(Arrays.asList(entity.getComment().split("\\s*,\\s*")));
			}

			final HashMap<String, WordFrequency> comment = new HashMap<String, WordFrequency>();
			if (c != null) {
				for (int i = 0; i < c.size(); i++) {
					setWordTimes(c.get(i), comment);
				}
			}
			tokenNodes.add(new TokenNode(entity, localname, label, comment));
			neighbors.addNeighbor(new Neighbor(entity.getURIString()));
		}
	}

	private void parseEntity(final RepositoryConnection model, final String entityType, final int category) throws Exception {
		try (final TupleQueryResult it = getCategoryNodesFromOntology(model, entityType, onto.getLexicalization(), onto.getLanguage())) {
			while (it.hasNext()) {
				final BindingSet tuplebinding = it.next();
				final Value uri = tuplebinding.getBinding(SPARQLQueryBuilder.getBindingLabels(onto.getLexicalization()).get(0)).getValue();
				if (uri instanceof BNode)
					continue;
				// if(!IsUriInLocalNamespace(model, uri) && Parameters.inclExt == false)
				// continue;
				if (!IsUriInLocalNamespace(model, (IRI) uri))
					continue;
				Value lab = null;
				ArrayList<String> l = null;
				if (tuplebinding.hasBinding(SPARQLQueryBuilder.getBindingLabels(onto.getLexicalization()).get(1))) {
					lab = tuplebinding.getBinding(SPARQLQueryBuilder.getBindingLabels(onto.getLexicalization()).get(1)).getValue();
					l = new ArrayList<String>(Arrays.asList(lab.stringValue().split("\\s*,\\s*")));
				}
				Value com = null;
				ArrayList<String> c = null;
				if (tuplebinding.hasBinding("entityComment")) {
					com = tuplebinding.getBinding("entityComment").getValue();
					c = new ArrayList<String>(Arrays.asList(com.stringValue().split("\\s*,\\s*")));
				}
				final String n = ((IRI) uri).getLocalName();
				final HashMap<String, WordFrequency> localname = new HashMap<String, WordFrequency>();
				if (n != null) {
					setWordTimes(n, localname);
				}

				final HashMap<String, WordFrequency> label = new HashMap<String, WordFrequency>();
				if (l != null) {
					for (int i = 0; i < l.size(); i++) {
						setWordTimes(l.get(i), label);
					}
				}
				final HashMap<String, WordFrequency> comment = new HashMap<String, WordFrequency>();
				if (c != null) {
					for (int i = 0; i < c.size(); i++) {
						setWordTimes(c.get(i), comment);
					}
					tokenNodes.add(new TokenNode(new OntoResource(uri.stringValue(), ((IRI) uri).getLocalName(),
							lab.stringValue(), com.stringValue(), category), localname, label, comment));
				} else if (l != null) {
					tokenNodes.add(new TokenNode(new OntoResource(uri.stringValue(), ((IRI) uri).getLocalName(),
							lab.stringValue(), "", category), localname, label, comment));
				} else {
					tokenNodes.add(
							new TokenNode(new OntoResource(uri.stringValue(), ((IRI) uri).getLocalName(), "", "", category),
									localname, label, comment));
				}
				neighbors.addNeighbor(new Neighbor(uri.stringValue()));
			}
		}
	}

	private void parseNamedClasses(final RepositoryConnection conn) throws Exception {
		switch(onto.getLexicalization()) {
			case RDFS:
				parseEntity(conn, RDFS.CLASS.stringValue(), OntoResource.CLASS);
				parseEntity(conn, OWL.CLASS.stringValue(), OntoResource.CLASS);
				break;
			case SKOS:
			case SKOS_XL:
				parseEntity(conn, SKOS.CONCEPT.stringValue(), OntoResource.CLASS);
				break;
			case ONTOLEX:
				parseEntity(conn, ONTOLEX.CONCEPT.stringValue(), OntoResource.CLASS);
				break;
		}

	}

	private void parseProperties(final RepositoryConnection conn) throws Exception {
		parseEntity(conn, OWL.DATATYPEPROPERTY.stringValue(), OntoResource.DATATYPEPROPERTY);
		parseEntity(conn, OWL.OBJECTPROPERTY.stringValue(), OntoResource.OBJECTPROPERTY);
	}

	private void parseInstances(final RepositoryConnection conn) throws Exception {
		TupleQueryResult it1 = null;
		try {
			it1 = getCategoryNodesFromOntology(conn, RDFS.CLASS.stringValue(), onto.getLexicalization(), onto.getLanguage());
			if (!it1.hasNext()) {
				it1.close(); // it is closed since a new TupleQueryResult will be immediately assigned to it
				it1 = getCategoryNodesFromOntology(conn, OWL.CLASS.stringValue(), onto.getLexicalization(), onto.getLanguage());
			}
			while (it1.hasNext()) {
				final BindingSet tuplebinding1 = it1.next();
				final Value uri1 = tuplebinding1.getBinding("entity").getValue();
				if (uri1 instanceof BNode)
					continue;
				try (final TupleQueryResult it2 = getInstancesOfClass(conn, uri1.stringValue())) {
					while (it2.hasNext()) {
						final BindingSet tuplebinding = it2.next();
						final Value uri2 = tuplebinding.getBinding("instance").getValue();
						final Value lab = tuplebinding.getBinding("instanceLabel").getValue();
						final String n = ((IRI) uri2).getLocalName();
						final HashMap<String, WordFrequency> localname = new HashMap<String, WordFrequency>();
						if (n != null) {
							setWordTimes(n, localname);
						}
						final ArrayList<String> l = new ArrayList<String>(Arrays.asList(lab.stringValue().split("\\s*,\\s*")));
						final HashMap<String, WordFrequency> label = new HashMap<String, WordFrequency>();
						if (l != null) {
							for (int i = 0; i < l.size(); i++) {
								setWordTimes(l.get(i), label);
							}
						}

						// ArrayList<String> c = (ArrayList<String>)
						// Arrays.asList(com.stringValue().split("\\s*,\\s*"));
						final HashMap<String, WordFrequency> comment = new HashMap<String, WordFrequency>();
						/*
						 * if (c != null) { for (int i = 0; i < c.size(); i++) { setWordTimes(c.get(i),
						 * comment); } }
						 */
						tokenNodes.add(new TokenNode(new OntoResource(uri2.stringValue(), ((IRI) uri2).getLocalName(),
								lab.stringValue(), lab.stringValue(), OntoResource.INSTANCE), localname, label, comment));
						neighbors.addNeighbor(new Neighbor(uri2.stringValue()));
					}
				}
			}
		} finally {
			if (it1 != null) {
				// it is manaully closed, and not using a try-with-resources, since multiple TupleQueryResult could be
				// assigned to it (and all previous ones are manually closed as well)
				it1.close();
			}
		}

	}

	private void setWordTimes(final String s, final HashMap<String, WordFrequency> hm) {
		final String str[] = stopword.removeStopword(tokenizer.split(s, onto.getLanguage()));
		if (str != null && str.length != 0) {
			final int size = str.length;
			for (int i = 0; i < size; i++) {
				if (hm.containsKey(str[i])) {
					final WordFrequency wf = hm.get(str[i]);
					wf.setFrequency(wf.getFrequency() + 1);
				} else {
					hm.put(str[i], new WordFrequency(str[i], 1));
				}
			}
		}
	}

	private void parseBlankNodes(final RepositoryConnection conn) {
		final ArrayList<Value> bnode = new ArrayList<Value>();
		final ArrayList<BindingSet> relatedTriples = new ArrayList<BindingSet>();
		try (final TupleQueryResult it = getAllStatements(conn)) {
			while (it.hasNext()) {
				final BindingSet tuplebinding = it.next();
				final Value subject = tuplebinding.getBinding("subject").getValue();
				final Value predicate = tuplebinding.getBinding("predicate").getValue();
				final Value object = tuplebinding.getBinding("object").getValue();
				if (subject instanceof BNode) {
					if (!bnode.contains(subject)) {
						bnode.add(subject);
					}
					if (!relatedTriples.contains(tuplebinding)) {
						relatedTriples.add(tuplebinding);
					}
				}
			}
		}
		for (int i = 0, n = bnode.size(); i < n; i++) {
			final Value b = bnode.get(i);
			final HashMap<String, WordFrequency> hmn = new HashMap<String, WordFrequency>();
			final HashMap<String, WordFrequency> hml = new HashMap<String, WordFrequency>();
			final HashMap<String, WordFrequency> hmc = new HashMap<String, WordFrequency>();
			labelOfBlankNode(b, relatedTriples, hmn, hml, hmc, Parameters.initBlankNode);
			tokenNodes.add(new TokenNode(new OntoResource("", "", "", "", OntoResource.BLANK), hmn, hml, hmc));

		}
	}

	private void labelOfBlankNode(final Value b, final ArrayList<BindingSet> list,
			final HashMap<String, WordFrequency> hmn, final HashMap<String, WordFrequency> hml,
			final HashMap<String, WordFrequency> hmc, final double v) {
		for (int i = 0, n = list.size(); i < n; i++) {
			final BindingSet stmt = list.get(i);
			String t = null;
			if (stmt.getBinding("subject").getValue() instanceof BNode) {
				continue;
			} else {
				t = stmt.getBinding("subject").getValue().stringValue();
			}
			if (((BNode) b).getID().equals(t) == true) {
				final Value predicate = stmt.getBinding("predicate").getValue();
				if (predicate instanceof BNode) {
					labelOfBlankNode(predicate, list, hmn, hml, hmc, v / Parameters.iterValue);
				} else {
					final TokenNode node = tokenNodes.get(predicate.stringValue());
					if (node != null) {
						final HashMap<String, WordFrequency> localname = node.getLocalName();
						if (localname != null) {
							for (final WordFrequency wf : localname.values()) {
								if (hmn.containsKey(wf.getWord())) {
									final WordFrequency temp = hmn.get(wf.getWord());
									temp.setFrequency(temp.getFrequency() + v);
								} else {
									final WordFrequency temp = new WordFrequency(wf.getWord(), v);
									hmn.put(temp.getWord(), temp);
								}
							}
						}
						final HashMap<String, WordFrequency> label = node.getLabel();
						if (label != null) {
							for (final WordFrequency wf : label.values()) {
								if (hml.containsKey(wf.getWord())) {
									final WordFrequency temp = hml.get(wf.getWord());
									temp.setFrequency(temp.getFrequency() + v);
								} else {
									final WordFrequency temp = new WordFrequency(wf.getWord(), v);
									hml.put(temp.getWord(), temp);
								}
							}
						}
						final HashMap<String, WordFrequency> comment = node.getComment();
						if (comment != null) {
							for (final WordFrequency wordFrequency : comment.values()) {
								final WordFrequency wf = wordFrequency;
								if (hmc.containsKey(wf.getWord())) {
									final WordFrequency temp = hmc.get(wf.getWord());
									temp.setFrequency(temp.getFrequency() + v);
								} else {
									final WordFrequency temp = new WordFrequency(wf.getWord(), v);
									hmc.put(temp.getWord(), temp);
								}
							}
						}
					}
				}
				final Value object = stmt.getBinding("object").getValue();
				if (!(object instanceof Literal)) {
					if (object instanceof BNode) {
						labelOfBlankNode(object, list, hmn, hml, hmc, v / Parameters.iterValue);
					} else {
						final TokenNode node = tokenNodes.get(object.stringValue());
						if (node != null) {
							final HashMap<String, WordFrequency> localname = node.getLocalName();
							if (localname != null) {
								for (final WordFrequency wf : localname.values()) {
									if (hmn.containsKey(wf.getWord())) {
										final WordFrequency temp = hmn.get(wf.getWord());
										temp.setFrequency(temp.getFrequency() + v);
									} else {
										final WordFrequency temp = new WordFrequency(wf.getWord(), v);
										hmn.put(temp.getWord(), temp);
									}
								}
							}
							final HashMap<String, WordFrequency> label = node.getLabel();
							if (label != null) {
								for (final WordFrequency wf : label.values()) {
									if (hml.containsKey(wf.getWord())) {
										final WordFrequency temp = hml.get(wf.getWord());
										temp.setFrequency(temp.getFrequency() + v);
									} else {
										final WordFrequency temp = new WordFrequency(wf.getWord(), v);
										hml.put(temp.getWord(), temp);
									}
								}
							}
							final HashMap<String, WordFrequency> comment = node.getComment();
							if (comment != null) {
								for (final WordFrequency wf : comment.values()) {
									if (hmc.containsKey(wf.getWord())) {
										final WordFrequency temp = hmc.get(wf.getWord());
										temp.setFrequency(temp.getFrequency() + v);
									} else {
										final WordFrequency temp = new WordFrequency(wf.getWord(), v);
										hmc.put(temp.getWord(), temp);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private void parseNeighbors(final RepositoryConnection model) {
		try (final TupleQueryResult it = getAllStatements(model)) {
			while (it.hasNext()) {
				final BindingSet tuplebinding = it.next();
				final Value subject = tuplebinding.getBinding("subject").getValue();
				final Value predicate = tuplebinding.getBinding("predicate").getValue();
				final Value object = tuplebinding.getBinding("object").getValue();
				if (subject instanceof BNode || object instanceof BNode)
					continue;
				final TokenNode s = tokenNodes.get(subject.stringValue());
				final TokenNode p = tokenNodes.get(predicate.stringValue());
				TokenNode o = null;
				if (!(object instanceof Literal)) {
					o = tokenNodes.get(object.stringValue());
				}
				if (s != null) {
					final Neighbor neighbor = neighbors.getNeighbor(s.getURI());
					if (neighbor != null) {
						if (p != null) {
							neighbor.addPredicate(p);
						}
						if (o != null) {
							neighbor.addObject(o);
						}
					}
				}
				if (p != null) {
					final Neighbor neighbor = neighbors.getNeighbor(p.getURI());
					if (neighbor != null) {
						if (s != null) {
							neighbor.addSubject(s);
						}
						if (o != null) {
							neighbor.addObject(o);
						}
					}
				}
				if (o != null) {
					final Neighbor neighbor = neighbors.getNeighbor(o.getURI());
					if (neighbor != null) {
						if (s != null) {
							neighbor.addSubject(s);
						}
						if (p != null) {
							neighbor.addPredicate(p);
						}
					}
				}
			}
		}
	}

	public TokenNodeSet getTokenNodes() {
		return tokenNodes;
	}

	public NeighborSet getNeighbors() {
		return neighbors;
	}

	public void show() {
		tokenNodes.show();
		neighbors.show();
	}

	private boolean IsUriInLocalNamespace(final RepositoryConnection conn, final IRI uri) {
		if (conn.getNamespace("my") == null) {
			/*
			 * Map<String, String> map = model.getNamespacePrefixMapping();
			 * for(Map.Entry<String,String> entry : map.entrySet()) {
			 * if(uri.stringValue().contains(entry.getValue())) return false; }
			 */
			return true;
		} else {
			if (uri.stringValue().contains(conn.getNamespace("my")))
				return true;
			else
				return false;
		}
	}
}
