/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.MatcherModuleClient;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.codec.digest.DigestUtils;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.ComposerAggregator.TripleStore;
import it.uniroma2.art.genoma.CustomExceptions.InvalidMatchingParamsException;

import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Roberto Enea
 */
public class SimilarityMatrixExtractor extends Matcher {
	private String alignmentURL;

	public SimilarityMatrixExtractor(final MatchingResource ontology1, final MatchingResource ontology2,
			final TripleStore tripleStore, final ArrayList<Collection<EntityAlignment>> SimMatrix,
			final String options) {
		super(ontology1, ontology2, tripleStore, SimMatrix, options);
		matcherId = "Similarity Matrix Extractor";
		this.alignmentId = DigestUtils.shaHex(this.ontology1.getUri() + this.ontology2.getUri() + this.matcherId);
		this.alignmentURL = null;
	}

	@Override
	protected void matchingAlgorithm() {
		try {
			//System.out.println(matcherId);
			Logger.getLogger(SimilarityMatrixExtractor.class.getName()).log(Level.INFO, matcherId);
			result.addLog("*****************************************************************");
			result.addLog(matcherId);
			result.addLog("*****************************************************************");
			parseOptions();
			if (alignmentURL != null) {
				result.setSimMatrix(extractAlignment());
			} else {
				super.matchingAlgorithm();
			}
		} catch (final InvalidMatchingParamsException ex) {
			Logger.getLogger(SimilarityMatrixExtractor.class.getName()).log(Level.SEVERE, null, ex);
			result.setError();
			result.setErrorMessage(ex.getMessage());
			result.addLog(ex.getMessage());
			//System.out.println(e.getMessage());
			Logger.getLogger(SimilarityMatrixExtractor.class.getName()).log(Level.SEVERE, ex.getMessage());
		} catch (final MalformedURLException ex) {
			Logger.getLogger(SimilarityMatrixExtractor.class.getName()).log(Level.SEVERE, null, ex);
			result.setError();
			result.setErrorMessage(ex.getMessage());
			result.addLog(ex.getMessage());
			//System.out.println(ex.getMessage());
			Logger.getLogger(SimilarityMatrixExtractor.class.getName()).log(Level.SEVERE, ex.getMessage());
		} catch (final XStreamException ex) {
			Logger.getLogger(SimilarityMatrixExtractor.class.getName()).log(Level.SEVERE, null, ex);
			result.setError();
			result.setErrorMessage(ex.getMessage());
			result.addLog(ex.getMessage());
			//System.out.println(ex.getMessage());
			Logger.getLogger(SimilarityMatrixExtractor.class.getName()).log(Level.SEVERE, ex.getMessage());
		} catch (final Exception ex) {
			Logger.getLogger(SimilarityMatrixExtractor.class.getName()).log(Level.SEVERE, null, ex);
			result.setError();
			result.setErrorMessage(ex.getMessage());
			result.addLog(ex.getMessage());
			//System.out.println(ex.getMessage());
			Logger.getLogger(SimilarityMatrixExtractor.class.getName()).log(Level.SEVERE, ex.getMessage());
		}
	}

	@Override
	protected void parseOptions() throws InvalidMatchingParamsException {
		final String[] tmp = options.split("-");
		for (final String element : tmp) {
			if (element.contains("align=") && !element.contains("align=none")) {
				alignmentURL = element.substring(element.lastIndexOf("align=") + ("align=").length(), element.length());
			} else if (element.contains("align=none")) {
				alignmentURL = null;
			} else if (element.contains("cfsize")) {
				cFSize = Integer.parseInt(element
						.substring(element.lastIndexOf("cfsize=") + ("cfsize=").length()).trim());
			}
		}
	}

	@Override
	protected double similarityFunction(final String l1, final String l2, final String c1, final String c2,
										final String n1, final String n2) {
		return 0.0;
	}

	@Override
	protected double similarityFunction(final String l1, final String l2, final String n1, final String n2) {
		return 0.0;
	}

	@Override
	protected double similarityFunction(final String n1, final String n2) {
		return 0.0;
	}


	private ArrayList<EntityAlignment> extractAlignment() throws MalformedURLException, XStreamException {
		XStream xstream = new XStream();
		xstream.allowTypesByWildcard(new String[] { "it.uniroma2.art.genoma.**" });
		final ArrayList<EntityAlignment> res = (ArrayList<EntityAlignment>) xstream.fromXML(new URL(alignmentURL));
		return res;
	}


}
