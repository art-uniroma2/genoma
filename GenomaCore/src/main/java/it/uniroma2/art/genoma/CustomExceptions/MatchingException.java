/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.CustomExceptions;

/**
 *
 * @author Roberto Enea
 */
public class MatchingException extends Exception {
	// Parameterless Constructor
	public MatchingException() {
	}

	// Constructor that accepts a message
	public MatchingException(final String message) {
		super(message);
	}
}
