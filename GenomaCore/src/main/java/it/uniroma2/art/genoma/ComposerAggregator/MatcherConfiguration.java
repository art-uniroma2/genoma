/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.ComposerAggregator;

import com.thoughtworks.xstream.XStream;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

/**
 *
 * @author Roberto Enea
 */
public class MatcherConfiguration {
	private Graph matcherGraph;
	private final String alignmentId;
	private final MatchingResource ontology1;
	private final MatchingResource ontology2;
	private final String refAlignmentPath;
	private final TripleStore tripleStore;
	private final Map<FileType, OutputStream> outFiles; // either this or the fileTypeToFilaPathMap should be null
	private final Map<FileType, String> fileTypeToFilaPathMap; // either this or the outFiles should be null

	public static enum FileType {
		LOG_FILE, RES_FILE, GRAPH_FILE
	}

	public MatcherConfiguration(final String id, final MatchingResource o1, final MatchingResource o2,
			final TripleStore ts, final String refal, final Map<FileType, OutputStream> outFiles, final Map<FileType, String> fileTypeToFilaPathMap) {
		matcherGraph = new Graph();
		ontology1 = o1;
		ontology2 = o2;
		alignmentId = id;
		tripleStore = ts;
		refAlignmentPath = refal;
		this.outFiles = outFiles==null || outFiles.isEmpty() ? null : outFiles;
		this.fileTypeToFilaPathMap = fileTypeToFilaPathMap==null || fileTypeToFilaPathMap.isEmpty() ? null : fileTypeToFilaPathMap;
	}

	public void saveArchitecture(final String filename) throws IOException {
		try (final FileWriter out = new FileWriter(filename)) {
			XStream xstream = new XStream();

			xstream.allowTypesByWildcard(new String[] { "it.uniroma2.art.genoma.**" });
			final String xml = xstream.toXML(this.matcherGraph);
			out.write(xml);
		}
	}

	public void loadArchitecture(final String filename) throws IOException {
		final File xml = new File(filename);
		XStream xstream = new XStream();

		xstream.allowTypesByWildcard(new String[] { "it.uniroma2.art.genoma.**" });
		this.matcherGraph = (Graph) xstream.fromXML(xml);
	}

	public Graph getGraph() {
		return this.matcherGraph;
	}

	public void setGraph(Graph matcherGraph) {
		this.matcherGraph = matcherGraph;
	}

	public MatchingResource getOntology1() {
		return this.ontology1;
	}

	public MatchingResource getOntology2() {
		return this.ontology2;
	}

	public OutputStream getOutFile(final FileType fileType) throws FileNotFoundException {
		//depending on whether outFiles or fileTypeToFilaPathMap is null, behave differently
		if (fileTypeToFilaPathMap!=null) {
			return new FileOutputStream(fileTypeToFilaPathMap.get(fileType));
		} else {
			return this.outFiles.get(fileType);
		}
	}

	public String getAlignmentId() {
		return this.alignmentId;
	}

	public TripleStore getTripleStore() {
		return this.tripleStore;
	}

	public String getRefAlignmentPath() {
		return this.refAlignmentPath;
	}

}
