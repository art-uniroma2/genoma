/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.AlignmentTools;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.apache.commons.codec.digest.DigestUtils;

import com.thoughtworks.xstream.XStream;

/**
 *
 * @author Roberto Enea
 */
public class EntityAlignment implements Serializable {

	private String id;
	private OntoResource entity1;
	private OntoResource entity2;
	private char r;
	private double n;

	public EntityAlignment(final OntoResource e1, final OntoResource e2, final char r, final double n) {
		this.id = DigestUtils.shaHex(e1.getURIString() + e2.getURIString());
		this.entity1 = e1;
		this.entity2 = e2;
		this.r = r;
		this.n = n;
	}

	public String getAlignmentId() {
		return id;
	}

	public void setEntity1(final OntoResource e1) {
		this.entity1 = e1;
	}

	public void setEntity2(final OntoResource e2) {
		this.entity2 = e2;
	}

	public void setRelationType(final char r) {
		this.r = r;
	}

	public void setSimilarityValue(final double n) {
		this.n = n;
	}

	public OntoResource getEntity1() {
		return this.entity1;
	}

	public OntoResource getEntity2() {
		return this.entity2;
	}

	public char getRelationType() {
		return this.r;
	}

	public double getSimilarityValue() {
		return this.n;
	}

	private void writeObject(final ObjectOutputStream out) throws IOException {
		out.defaultWriteObject();
		XStream xstream = new XStream();

		xstream.allowTypesByWildcard(new String[] { "it.uniroma2.art.genoma.**" });
		out.writeObject(xstream.toXML(entity1));
		out.writeObject(xstream.toXML(entity2));
	}

	private void readObject(final ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		XStream xstream = new XStream();

		xstream.allowTypesByWildcard(new String[] { "it.uniroma2.art.genoma.**" });
		entity1 = (OntoResource) xstream.fromXML((String) in.readObject());
		entity2 = (OntoResource) xstream.fromXML((String) in.readObject());
	}
}
