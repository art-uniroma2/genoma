/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.MatcherModuleClient;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.codec.digest.DigestUtils;

import info.debatty.java.stringsimilarity.NGram;
import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.ComposerAggregator.TripleStore;
import it.uniroma2.art.genoma.CustomExceptions.InvalidMatchingParamsException;

/**
 *
 * @author Roberto Enea
 */

public class NgramCompareMatcher extends Matcher {
	private int n;

	public NgramCompareMatcher(final MatchingResource ontology1, final MatchingResource ontology2,
			final TripleStore tripleStore, final ArrayList<Collection<EntityAlignment>> SimMatrix,
			final String options) {
		super(ontology1, ontology2, tripleStore, SimMatrix, options);
		matcherId = "Ngram Compare Matcher";
		this.n = 0;
		this.alignmentId = DigestUtils.shaHex(this.ontology1.getUri() + this.ontology2.getUri() + this.matcherId);
	}

	@Override
	protected void parseOptions() throws InvalidMatchingParamsException {
		super.parseOptions();
		final String[] tmp = options.split("-");
		for (final String element : tmp) {
			if (element.contains("-ngram=")) {
				n = Integer.parseInt(element
						.substring(element.lastIndexOf("-ngram=") + ("-ngram=").length(), element.length()).trim());
			}

		}
		if (idWeight == -1)
			throw new InvalidMatchingParamsException("No id Weight Value");
		if (labelWeight == -1)
			throw new InvalidMatchingParamsException("No Label Weight Found");
		if (commentWeight == -1)
			throw new InvalidMatchingParamsException("No Comment Weight Found");
	}

	@Override
	protected double similarityFunction(final String l1, final String l2, final String c1, final String c2,
			final String n1, final String n2) {
		return Math.max(ngramCompare(l1, l2) * labelWeight,
				Math.max(ngramCompare(c1, c2) * commentWeight, ngramCompare(n1, n2) * idWeight));
	}

	@Override
	protected double similarityFunction(final String l1, final String l2, final String n1, final String n2) {
		return Math.max(ngramCompare(l1, l2) * labelWeight, ngramCompare(n1, n2) * idWeight);
	}

	@Override
	protected double similarityFunction(final String n1, final String n2) {
		return ngramCompare(n1, n2) * idWeight;
	}

	private double ngramCompare(String s1, String s2) {
		if (s1 == null || s2 == null)
			return 0.0;
		NGram ngram = new NGram(n);
	    return ngram.distance(s1, s2);
	}

}
