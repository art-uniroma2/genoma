/**
 * Copyright 2010 Websoft research group, Nanjing University, PR China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.uniroma2.art.genoma.MatcherModuleClient.gmo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
//import nju.websoft.falcon.model.Constants;
//import nju.websoft.falcon.model.Node;
//import nju.websoft.falcon.model.NodeCategory;
//import nju.websoft.falcon.model.RBGModel;
//import nju.websoft.falcon.output.Mapping;
//import nju.websoft.falcon.output.Alignment;

import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.AlignmentTools.OntoResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Wei Hu & Ningsheng Jian
 * @see http://ws.nju.edu.cn
 */
//this class has been modified by replacing the various prints of System.out or System.err with a Logger
public class ExternalMatch {
	private static final Logger log = LoggerFactory.getLogger(ExternalMatch.class);
	private RepositoryConnection connA = null;
	private RepositoryConnection connB = null;
	private Collection<EntityAlignment> ClassAlignments;
	private Collection<EntityAlignment> PropertyAlignments;
	private Collection<EntityAlignment> InstanceAlignments;

	public ExternalMatch(final ArrayList<Collection<EntityAlignment>> im) {
		ClassAlignments = new ArrayList<EntityAlignment>();
		PropertyAlignments = new ArrayList<EntityAlignment>();
		InstanceAlignments = new ArrayList<EntityAlignment>();
		initExternalMatch(im.get(0));
	}

	public ExternalMatch() {
		ClassAlignments = null;
		PropertyAlignments = null;
		InstanceAlignments = null;
	}

	public RepositoryConnection getRepConnA() {
		return connA;
	}

	public RepositoryConnection getRepConnB() {
		return connB;
	}

	public void initExternalMatch(final Collection<EntityAlignment> all) {
		final Iterator<EntityAlignment> it = all.iterator();
		while (it.hasNext()) {
			final EntityAlignment al = it.next();
			switch (al.getEntity1().getCategory()) {
			case OntoResource.CLASS:
				al.getEntity1().setLevel(OntoResource.EXTERNAL);
				al.getEntity2().setLevel(OntoResource.EXTERNAL);
				ClassAlignments.add(al);
				break;
			case OntoResource.DATATYPEPROPERTY:
				al.getEntity1().setLevel(OntoResource.EXTERNAL);
				al.getEntity2().setLevel(OntoResource.EXTERNAL);
				PropertyAlignments.add(al);
				break;
			case OntoResource.OBJECTPROPERTY:
				al.getEntity1().setLevel(OntoResource.EXTERNAL);
				al.getEntity2().setLevel(OntoResource.EXTERNAL);
				PropertyAlignments.add(al);
				break;
			case OntoResource.INSTANCE:
				InstanceAlignments.add(al);
				break;
			}
		}
	}

	public double getSimilarity(final OntoResource left, final OntoResource right) {
		double sim = 0;
		if (left.getCategory() == OntoResource.LITERAL && right.getCategory() == OntoResource.LITERAL) {
			sim = DataLiteralSim.getDataLiteralSim(left.getLabel(), right.getLabel());
		} else if (NodeCategory.getCategory(left) == Constants.ONTOLOGY_INSTANCE
				&& NodeCategory.getCategory(right) == Constants.ONTOLOGY_INSTANCE) {
			if (InstanceAlignments != null) {
				if (left.getCategory() == OntoResource.BLANK || right.getCategory() == OntoResource.BLANK) {
					sim = 0;
				} else {
					sim = getSimilarityFromMatrix(InstanceAlignments, left, right);
					if (sim <= 0) {
						sim = getSimilarityFromMatrix(InstanceAlignments, right, left);
					}
				}
			}
		} else if (left.getLevel() == OntoResource.LANGUAGE_LEVEL && right.getLevel() == OntoResource.LANGUAGE_LEVEL
				&& left.getCategory() == right.getCategory()) {
			if (left.getURIString().contains(Constants.XSD_NS) && right.getURIString().contains(Constants.XSD_NS)) {
				sim = DatatypeSim.getSimilarity(left.toString(), right.toString());
			} else if (left.getURIString().contains(Constants.XSD_NS)
					|| right.getURIString().contains(Constants.XSD_NS)) {
				sim = 0;
			} else {
				sim = BuiltInVocSim.getSimilarity(left.toString(), right.toString());
			}
		} else if (left.getLevel() == OntoResource.EXTERNAL && right.getLevel() == OntoResource.EXTERNAL) {
			if (left.getCategory() == OntoResource.CLASS && right.getCategory() == OntoResource.CLASS) {
				if (ClassAlignments != null) {
					if (left.getCategory() == OntoResource.BLANK || right.getCategory() == OntoResource.BLANK) {
						sim = 0;
					} else {
						sim = getSimilarityFromMatrix(ClassAlignments, left, right);
						log.info("sim:" + sim);
						if (sim <= 0) {
							sim = getSimilarityFromMatrix(ClassAlignments, right, left);
						}
					}
				}
			} else if ((left.getCategory() == OntoResource.OBJECTPROPERTY
					|| left.getCategory() == OntoResource.DATATYPEPROPERTY)
					&& (right.getCategory() == OntoResource.OBJECTPROPERTY
							|| right.getCategory() == OntoResource.DATATYPEPROPERTY)) {
				if (PropertyAlignments != null) {
					sim = getSimilarityFromMatrix(PropertyAlignments, left, right);
					log.info("sim:" + sim);
					if (sim <= 0) {
						sim = getSimilarityFromMatrix(PropertyAlignments, right, left);
					}
				}
			}
		} else if (left.toString().equals(right.toString())) {
			sim = 1;
		}
		if (sim < 0) {
			sim = 0;
		}
		return sim;
	}

	private double getSimilarityFromMatrix(final Collection<EntityAlignment> all, final OntoResource left,
			final OntoResource right) {
		final Iterator<EntityAlignment> it = all.iterator();
		while (it.hasNext()) {
			final EntityAlignment al = it.next();
			if (al.getEntity1().getURIString().equals(left.getURIString())
					&& al.getEntity2().getURIString().equals(right.getURIString())) {
				return al.getSimilarityValue();
			}
		}
		return -1;
	}
	/*
	 * public void setInstanceSim(Alignment as) { if (as == null || as.size() == 0)
	 * { return; } for (int i = 0, n = as.size(); i < n; i++) { Mapping map =
	 * as.getMapping(i); Node nodeA = null; Node nodeB = null; nodeA =
	 * rbgModelA.getNode(map.getEntity1().toString()); if (nodeA == null) { nodeA =
	 * rbgModelA.getNode(map.getEntity2().toString()); nodeB =
	 * rbgModelB.getNode(map.getEntity1().toString()); } else { nodeB =
	 * rbgModelB.getNode(map.getEntity2().toString()); } instanceSim.addMapping(new
	 * Mapping(nodeA, nodeB, map.getSimilarity())); } }
	 * 
	 * public Alignment getInstanceSim() { return instanceSim; }
	 * 
	 * public void setClassSim(Alignment as) { if (as == null || as.size() == 0) {
	 * return; } for (int i = 0, n = as.size(); i < n; i++) { Mapping map =
	 * as.getMapping(i); Node nodeA = null; Node nodeB = null; nodeA =
	 * rbgModelA.getNode(map.getEntity1().toString()); if (nodeA == null) { nodeA =
	 * rbgModelA.getNode(map.getEntity2().toString()); nodeB =
	 * rbgModelB.getNode(map.getEntity1().toString()); } else { nodeB =
	 * rbgModelB.getNode(map.getEntity2().toString()); }
	 * nodeA.setNodeLevel(Node.EXTERNAL); nodeB.setNodeLevel(Node.EXTERNAL);
	 * classSim.addMapping(new Mapping(nodeA, nodeB, map.getSimilarity())); } }
	 * 
	 * public Alignment getClassSim() { return classSim; }
	 * 
	 * public void setPropertySim(Alignment as) { if (as == null || as.size() == 0)
	 * { return; } for (int i = 0, n = as.size(); i < n; i++) { Mapping map =
	 * as.getMapping(i); Node nodeA = null; Node nodeB = null; nodeA =
	 * rbgModelA.getNode(map.getEntity1().toString()); if (nodeA == null) { nodeA =
	 * rbgModelA.getNode(map.getEntity2().toString()); nodeB =
	 * rbgModelB.getNode(map.getEntity1().toString()); } else { nodeB =
	 * rbgModelB.getNode(map.getEntity2().toString()); }
	 * nodeA.setNodeLevel(Node.EXTERNAL); nodeB.setNodeLevel(Node.EXTERNAL);
	 * propertySim.addMapping(new Mapping(nodeA, nodeB, map.getSimilarity())); } }
	 * 
	 * public Alignment getPropertySim() { return propertySim; }
	 */
}
