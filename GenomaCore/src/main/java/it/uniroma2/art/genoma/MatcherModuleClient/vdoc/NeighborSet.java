/**
 * Copyright 2010 Websoft research group, Nanjing University, PR China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.uniroma2.art.genoma.MatcherModuleClient.vdoc;

import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Wei Hu
 * @see http://ws.nju.edu.cn
 */
//this class has been modified by replacing the various prints of System.out or System.err with a Logger
public class NeighborSet {
	private HashMap<String, Neighbor> neighbors = new HashMap<String, Neighbor>();

	public void addNeighbor(final Neighbor neighbor) {
		neighbors.put(neighbor.getURI(), neighbor);
	}

	public Neighbor getNeighbor(final String uri) {
		return neighbors.get(uri);
	}

	public HashMap<String, Neighbor> getNeighbors() {
		return neighbors;
	}

	public int size() {
		return neighbors.size();
	}

	public void show() {
		final Iterator<Neighbor> iter = neighbors.values().iterator();
		while (iter.hasNext()) {
			final Neighbor neighbor = iter.next();
			//System.out.println(neighbor.getURI());
			Logger.getLogger(NeighborSet.class.getName()).log(Level.INFO, neighbor.getURI());
			final Iterator<TokenNode> i1 = neighbor.getSubjects().values().iterator();
			while (i1.hasNext()) {
				final TokenNode node = i1.next();
				//System.out.println("->[s]->" + node.getURI());
				Logger.getLogger(NeighborSet.class.getName()).log(Level.INFO, "->[s]->" + node.getURI());
			}
			final Iterator<TokenNode> i2 = neighbor.getPredicates().values().iterator();
			while (i2.hasNext()) {
				final TokenNode node = i2.next();
				//System.out.println("->[p]->" + node.getURI());
				Logger.getLogger(NeighborSet.class.getName()).log(Level.INFO, "->[p]->" + node.getURI());
			}
			final Iterator<TokenNode> i3 = neighbor.getObjects().values().iterator();
			while (i3.hasNext()) {
				final TokenNode node = i3.next();
				//System.out.println("->[o]->" + node.getURI());
				Logger.getLogger(NeighborSet.class.getName()).log(Level.INFO, "->[o]->" + node.getURI());
			}
			//System.out.println();
			Logger.getLogger(NeighborSet.class.getName()).log(Level.INFO, "");
		}
	}
}
