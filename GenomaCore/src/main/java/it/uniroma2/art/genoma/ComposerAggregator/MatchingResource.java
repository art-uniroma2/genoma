package it.uniroma2.art.genoma.ComposerAggregator;

public class MatchingResource {
	public static enum Lexicalization {
		SKOS("http://www.w3.org/2004/02/skos/core"), //
		SKOS_XL("http://www.w3.org/2008/05/skos-xl"), //
		RDFS("http://www.w3.org/2000/01/rdf-schema"), //
		ONTOLEX("http://www.w3.org/ns/lemon/ontolex");

		private String uri;

		Lexicalization(final String uri) {
			this.uri = uri;
		}

		public String getUri() {
			return uri;
		}

	}

	private String uri;
	private DataService sparqlEndpoint;
	private Lexicalization lexicalization;
	private String language;
	private DataService synonymizerSPARQLEndpoint;

	public MatchingResource(final String uri, final DataService sparqlEndpoint,
			final Lexicalization lexicalization, final String language, final DataService synonymizer) {
		this.uri = uri;
		this.sparqlEndpoint = sparqlEndpoint;
		this.lexicalization = lexicalization;
		this.language = language;
		this.synonymizerSPARQLEndpoint = synonymizer;
	}

	public String getUri() {
		return uri;
	}

	public DataService getSparqlEndpoint() {
		return sparqlEndpoint;
	}

	public Lexicalization getLexicalization() {
		return lexicalization;
	}

	public String getLanguage() {
		return language;
	}

	public DataService getSynonymizerSPARQLEndpoint() {
		return synonymizerSPARQLEndpoint;
	}

}
