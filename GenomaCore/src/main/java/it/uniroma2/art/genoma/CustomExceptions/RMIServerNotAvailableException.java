/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.CustomExceptions;

/**
 *
 * @author Roberto Enea
 */
public class RMIServerNotAvailableException extends Exception {
	// Parameterless Constructor
	public RMIServerNotAvailableException() {
	}

	// Constructor that accepts a message
	public RMIServerNotAvailableException(final String message) {
		super(message);
	}
}
