/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.MatcherModuleClient;

import it.uniroma2.art.genoma.ComposerAggregator.MatcherConfiguration;
import it.uniroma2.art.genoma.ComposerAggregator.MatcherNode;
import it.uniroma2.art.genoma.CustomExceptions.InvalidMatchingParamsException;

/**
 *
 * @author Roberto Enea
 */
public class MatcherDictionary {
	public static final String[] matcherList = { "SimilarityMatrixExtractor", "AlignmentExtractor", "JunctionPoint",
			"StringEquality", "StringHammingDistance", "SubStringCompare", "NgramCompare", "SynonymsMatcher",
			"CosynonymsMatcher", "EditDistanceMatcher", "ISubMatcher", "VirtualDocMatcher", "GraphMatcher",
			"GraphExtractor", "CustomMatcher" };

	public static Matcher getMatcher(final MatcherNode mn, final MatcherConfiguration mc)
			throws InvalidMatchingParamsException {
		if (mn.getMatcherType().equals(matcherList[0])) {
			return (new SimilarityMatrixExtractor(mc.getOntology1(), mc.getOntology2(), mc.getTripleStore(), null,
					mn.getOptions()));

		} else if (mn.getMatcherType().equals(matcherList[1])) {
			return (new AlignmentExtractor(mc.getOntology1(), mc.getOntology2(), mc.getTripleStore(),
					mn.getInputSimMatrix(), mn.getOptions(), mc.getRefAlignmentPath()));

		} else if (mn.getMatcherType().equals(matcherList[2])) {
			return (new JunctionPoint(mc.getOntology1(), mc.getOntology2(), mc.getTripleStore(), mn.getInputSimMatrix(),
					mn.getOptions()));

		} else if (mn.getMatcherType().equals(matcherList[3])) {
			return (new StringEqualityMatcher(mc.getOntology1(), mc.getOntology2(), mc.getTripleStore(),
					mn.getInputSimMatrix(), mn.getOptions()));

		} else if (mn.getMatcherType().equals(matcherList[4])) {
			return (new StringHammingDistanceMatcher(mc.getOntology1(), mc.getOntology2(), mc.getTripleStore(),
					mn.getInputSimMatrix(), mn.getOptions()));
		} else if (mn.getMatcherType().equals(matcherList[5])) {
			return (new SubStringCompareMatcher(mc.getOntology1(), mc.getOntology2(), mc.getTripleStore(),
					mn.getInputSimMatrix(), mn.getOptions()));
		} else if (mn.getMatcherType().equals(matcherList[6])) {
			return (new NgramCompareMatcher(mc.getOntology1(), mc.getOntology2(), mc.getTripleStore(),
					mn.getInputSimMatrix(), mn.getOptions()));
		} else if (mn.getMatcherType().equals(matcherList[7])) {
			return (new SynonymsMatcher(mc.getOntology1(), mc.getOntology2(), mc.getTripleStore(),
					mn.getInputSimMatrix(), mn.getOptions()));
		} else if (mn.getMatcherType().equals(matcherList[8])) {
			return (new CosynonymsMatcher(mc.getOntology1(), mc.getOntology2(), mc.getTripleStore(),
					mn.getInputSimMatrix(), mn.getOptions()));
		} else if (mn.getMatcherType().equals(matcherList[9])) {
			return (new EditDistanceMatcher(mc.getOntology1(), mc.getOntology2(), mc.getTripleStore(),
					mn.getInputSimMatrix(), mn.getOptions()));
		} else if (mn.getMatcherType().equals(matcherList[10])) {
			return (new ISubMatcher(mc.getOntology1(), mc.getOntology2(), mc.getTripleStore(), mn.getInputSimMatrix(),
					mn.getOptions()));
		} else if (mn.getMatcherType().equals(matcherList[11])) {
			return (new VirtualDocMatcher(mc.getOntology1(), mc.getOntology2(), mc.getTripleStore(),
					mn.getInputSimMatrix(), mn.getOptions()));
		} else if (mn.getMatcherType().equals(matcherList[12])) {
			return (new GraphMatcher(mc.getOntology1(), mc.getOntology2(), mc.getTripleStore(), mn.getInputSimMatrix(),
					mn.getOptions()));
		} else if (mn.getMatcherType().equals(matcherList[13])) {
			return (new GraphExtractor(mc.getOntology1(), mc.getOntology2(), mc.getTripleStore(),
					mn.getInputSimMatrix(), mn.getOptions()));
		} else if (mn.getMatcherType().equals(matcherList[14])) {
			return (new CustomMatcher(mc.getOntology1(), mc.getOntology2(), mc.getTripleStore(), mn.getInputSimMatrix(),
					mn.getOptions()));
		}
		throw new InvalidMatchingParamsException("Matcher choosen has not been found in Genoma's db");
	}

}
