/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.MatcherModuleClient;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import it.uniroma2.art.genoma.AlignmentTools.EntityAlignment;
import it.uniroma2.art.genoma.CustomExceptions.IllegalSimMatrixSizeException;
import it.uniroma2.art.genoma.CustomExceptions.InvalidMatchingParamsException;

/**
 *
 * @author Roberto Enea
 */
public class AggregationStrategyDictionary {
	public static final String[] matchingStrategyList = { "TriangularNormMax", "TriangularNormMin",
			"TriangularNormWeightedProducts" };

	/*
	 * Matching Strategy using Triangular Norm z = min(x,y)
	 * 
	 */
	public static Collection<EntityAlignment> applyMatchingStrategy(final ArrayList<Collection<EntityAlignment>> x,
			final String mt) throws IllegalSimMatrixSizeException, InvalidMatchingParamsException {
		if(x.size() == 1) {
			return x.get(0);
		} else if(x.size() == 0) {
			throw new  IllegalSimMatrixSizeException("Both matrices cannot be null");
		}
		if (mt.equals(matchingStrategyList[0])) {
			return triangularNormMax(x);
		} else if (mt.equals(matchingStrategyList[1])) {
			return triangularNormMin(x);
		} else if (mt.equals(matchingStrategyList[2])) {
			return triangularNormWeightedProduct(x);
		}
		throw new InvalidMatchingParamsException("The matching strategy selected is not present");
	}

	private static Collection<EntityAlignment> triangularNormMin(final ArrayList<Collection<EntityAlignment>> x)
			throws IllegalSimMatrixSizeException {
		/*
		 * try { if(x.get(0).size() != x.get(1).size()) { throw new
		 * IllegalSimMatrixSizeException("Sim Matrix Size not coherent"); } }
		 * catch(IllegalSimMatrixSizeException e){ e.printStackTrace(); return null; }
		 */
		final Collection<EntityAlignment> res = new ArrayList<EntityAlignment>();
		boolean found = false;
		while (x.get(0).iterator().hasNext()) {
			final EntityAlignment ea1 = x.get(0).iterator().next();
			final Iterator<EntityAlignment> it = x.get(1).iterator();
			while (it.hasNext()) {
				final EntityAlignment ea2 = it.next();
				if (ea1.getAlignmentId().equals(ea2.getAlignmentId())) {
					res.add(new EntityAlignment(ea1.getEntity1(), ea1.getEntity2(), ea1.getRelationType(),
							1.0 - Math.min(1.0 - ea1.getSimilarityValue(), 1.0 - ea2.getSimilarityValue())));
					x.get(0).remove(ea1);
					found = true;
					break;
				}
			}
			if (found) {
				x.get(0).remove(ea1);
				found = false;
			} else {
				res.add(ea1);
				x.get(0).remove(ea1);
			}
		}
		return getEntityAlignments(x, res);
	}

	private static Collection<EntityAlignment> triangularNormMax(final ArrayList<Collection<EntityAlignment>> x)
			throws IllegalSimMatrixSizeException {
		/*
		 * try { if(x.get(0).size() != x.get(1).size()) { throw new
		 * IllegalSimMatrixSizeException("Sim Matrix Size not coherent"); } }
		 * catch(IllegalSimMatrixSizeException e){ e.printStackTrace(); return null; }
		 */
		final Collection<EntityAlignment> res = new ArrayList<EntityAlignment>();
		boolean found = false;
		while (x.get(0).iterator().hasNext()) {
			final EntityAlignment ea1 = x.get(0).iterator().next();
			final Iterator<EntityAlignment> it = x.get(1).iterator();
			while (it.hasNext()) {
				final EntityAlignment ea2 = it.next();
				if (ea1.getAlignmentId().equals(ea2.getAlignmentId())) {
					res.add(new EntityAlignment(ea1.getEntity1(), ea1.getEntity2(), ea1.getRelationType(),
							1.0 - Math.max(1.0 - ea1.getSimilarityValue() + 1.0 - ea2.getSimilarityValue() - 1.0, 0)));
					x.get(0).remove(ea1);
					found = true;
					break;
				}
			}
			if (found) {
				x.get(0).remove(ea1);
				found = false;
			} else {
				res.add(ea1);
				x.get(0).remove(ea1);
			}
		}
		return getEntityAlignments(x, res);
	}

	private static Collection<EntityAlignment> getEntityAlignments(ArrayList<Collection<EntityAlignment>> x, Collection<EntityAlignment> res) {
		boolean found;
		final Iterator<EntityAlignment> it1 = x.get(1).iterator();
		while (it1.hasNext()) {
			found = false;
			final EntityAlignment ea = it1.next();
			final Iterator<EntityAlignment> it = res.iterator();
			while (it.hasNext()) {
				final EntityAlignment ea1 = it.next();
				if (ea.getAlignmentId().equals(ea1.getAlignmentId()))
					found = true;
			}
			if (!found)
				res.add(ea);
		}
		return res;
	}

	private static Collection<EntityAlignment> triangularNormWeightedProduct(
			final ArrayList<Collection<EntityAlignment>> x) throws IllegalSimMatrixSizeException {
		/*
		 * try { if(x.get(0).size() != x.get(1).size()) { throw new
		 * IllegalSimMatrixSizeException("Similarity Matrix Size not coherent"); } }
		 * catch(IllegalSimMatrixSizeException e){ e.printStackTrace(); return null; }
		 */
		final Collection<EntityAlignment> res = new ArrayList<EntityAlignment>();
		boolean found = false;
		while (x.get(0).iterator().hasNext()) {
			final EntityAlignment ea1 = x.get(0).iterator().next();
			final Iterator<EntityAlignment> it = x.get(1).iterator();
			while (it.hasNext()) {
				final EntityAlignment ea2 = it.next();
				if (ea1.getAlignmentId().equals(ea2.getAlignmentId())) {
					res.add(new EntityAlignment(ea1.getEntity1(), ea1.getEntity2(), ea1.getRelationType(),
							1.0 - ((1.0 - ea1.getSimilarityValue()) * (1.0 - ea2.getSimilarityValue()))));
					x.get(0).remove(ea1);
					found = true;
					break;
				}
			}
			if (found) {
				x.get(0).remove(ea1);
				found = false;
			} else {
				res.add(ea1);
				x.get(0).remove(ea1);
			}
		}
		return getEntityAlignments(x, res);

	}
}
