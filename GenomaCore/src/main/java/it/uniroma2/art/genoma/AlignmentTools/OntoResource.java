/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.genoma.AlignmentTools;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Roberto Enea
 */
public class OntoResource implements Serializable {

	private String ResourceURIString;
	private String LocalName;
	private String Label;
	private List<String> labels;
	private String Comment;
	private int Category;
	private int Level = 0;

	// Categories
	public final static int CLASS = 1;
	public final static int PROPERTY = 2;
	public final static int INSTANCE = 3;
	public final static int BLANK = 4;
	public static final int LIST = 5;
	public static final int OBJECTPROPERTY = 6;
	public static final int DATATYPEPROPERTY = 7;
	public static final int LITERAL = 8;
	public static final int STATEMENT = 9;

	// Levels
	public static final int LANGUAGE_LEVEL = 1;
	public static final int ONTOLOGY_LEVEL = 2;
	public static final int INSTANCE_LEVEL = 4;
	public static final int EXTERNAL = 8;

	public static final int UNDEFINED = 0;

	public OntoResource(final String URI, final String label) {
		this.ResourceURIString = URI;
		this.Label = label;
		this.Comment = null;
		this.LocalName = null;
	}

	public OntoResource(final String URI, final String localName, final List<String> labels, int category) {
		this.ResourceURIString = URI;
		this.Label = null;
		this.Comment = null;
		this.LocalName = localName;
		this.labels = labels;
		this.Category = category;
	}

	public OntoResource(final String URI, final String localName, final String label, final String comment) {
		this.ResourceURIString = URI;
		this.Label = label;
		this.Comment = comment;
		this.LocalName = localName;
	}

	public OntoResource(final String URI, final String localName, final String label, final String comment,
			final int category) {
		this.ResourceURIString = URI;
		this.Label = label;
		this.Comment = comment;
		this.LocalName = localName;
		this.Category = category;
	}

	public OntoResource(final String URI, final String localName, final String label, final String comment,
			final int category, final int level) {
		this.ResourceURIString = URI;
		this.Label = label;
		this.Comment = comment;
		this.LocalName = localName;
		this.Category = category;
		this.Level = level;
	}

	public String getURIString() {
		return this.ResourceURIString;
	}

	public String getLabel() {
		return this.Label;
	}

	public String getComment() {
		return this.Comment;
	}

	public String getLocalName() {
		return this.LocalName;
	}

	public int getCategory() {
		return this.Category;
	}

	public void setLevel(final int level) {
		this.Level = level;
	}

	public int getLevel() {
		return this.Level;
	}

	public List<String> getLabels() {
		return labels;
	}
}
