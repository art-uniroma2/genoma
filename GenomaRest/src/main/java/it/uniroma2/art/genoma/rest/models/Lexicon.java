package it.uniroma2.art.genoma.rest.models;

public class Lexicon {
	private String lexicon;
	private String conceptualizationSet;
	
	public String getLexicon() {
		return lexicon;
	}
	
	public String getConceptualizationSet() {
		return conceptualizationSet;
	}
}
