package it.uniroma2.art.genoma.rest.models;

public class UploadFileResponse {
	private String fileName;
	private String fileDownloadUri;
	private String fileType;
	private long size;

	public UploadFileResponse(final String fileName, final String fileDownloadUri, final String fileType,
			final long size) {
		this.fileName = fileName;
		this.fileDownloadUri = fileDownloadUri;
		this.fileType = fileType;
		this.size = size;
	}

	public String getFileName() {
		return this.fileName;
	}

	public String getFileDownloadUri() {
		return this.fileDownloadUri;
	}

	public String getFileType() {
		return this.fileType;
	}

	public long getSize() {
		return this.size;
	}
}
