package it.uniroma2.art.genoma.rest.controllers;

import it.uniroma2.art.genoma.ComposerAggregator.Composer;
import it.uniroma2.art.genoma.ComposerAggregator.DataService;
import it.uniroma2.art.genoma.ComposerAggregator.Graph;
import it.uniroma2.art.genoma.ComposerAggregator.GraphArch;
import it.uniroma2.art.genoma.ComposerAggregator.MatcherConfiguration;
import it.uniroma2.art.genoma.ComposerAggregator.MatcherConfiguration.FileType;
import it.uniroma2.art.genoma.ComposerAggregator.MatcherNode;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource;
import it.uniroma2.art.genoma.ComposerAggregator.MatchingResource.Lexicalization;
import it.uniroma2.art.genoma.ComposerAggregator.TripleStore;
import it.uniroma2.art.genoma.DataManagement.AlignmentResultSet;
import it.uniroma2.art.genoma.DataManagement.DataManagementUtils;
import it.uniroma2.art.genoma.DataManagement.Engine;
import it.uniroma2.art.genoma.DataManagement.EnginesSet;
import it.uniroma2.art.genoma.DataManagement.Result;
import it.uniroma2.art.genoma.rest.exceptions.BaseException;
import it.uniroma2.art.genoma.rest.models.Dataset;
import it.uniroma2.art.genoma.rest.models.EngineGraph;
import it.uniroma2.art.genoma.rest.models.GraphEdge;
import it.uniroma2.art.genoma.rest.models.GraphNode;
import it.uniroma2.art.genoma.rest.models.LexicalizationSet;
import it.uniroma2.art.genoma.rest.models.MapleMatchingSettings;
import it.uniroma2.art.genoma.rest.models.MatcherModel;
import it.uniroma2.art.genoma.rest.models.MatchingSettings;
import it.uniroma2.art.genoma.rest.models.MatchingStatus;
import it.uniroma2.art.genoma.rest.models.Pairing;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class GenomaRestController {

    private static final Logger logger = LoggerFactory.getLogger(GenomaRestController.class);

    @Autowired
    private Environment env;

    @Autowired
    private TaskExecutor taskExecutor;

    @GetMapping("/")
    String home() {
        return "{\"service\":\"Genoma REST API\"," +
                "\"version\":\"1.0\"," +
                "\"status\":\"active\"," +
                "\"documentation\":\"https://bitbucket.org/art-uniroma2/dome/wiki/Home\"" +
                "}";
    }

    @GetMapping("/getMatchingStatus")
    public MatchingStatus getMatchingStatus(@RequestParam(value = "id") final String id) {
        final Result result = AlignmentResultSet.getResult(id);
        return new MatchingStatus(result.getAlignmentId(), result.getOntology1().toString(),
                result.getOntology2().toString(), result.getEngine().toString(), result.getStatus(),
                result.getStartDate() != null ? result.getStartDate().toString() : null,
                result.getEndDate() != null ? result.getEndDate().toString() : null);
    }

    @GetMapping("/getMatchingList")
    public List<MatchingStatus> getMatchingList() {
        final Set<String> keySet = AlignmentResultSet.getKeySet();
        final List<MatchingStatus> results = new ArrayList<>();
        for (final String key : keySet) {
            final Result result = AlignmentResultSet.getResult(key);
            results.add(new MatchingStatus(result.getAlignmentId(), result.getOntology1().toString(),
                    result.getOntology2().toString(), result.getEngine().toString(), result.getStatus(),
                    result.getStartDate() != null ? result.getStartDate().toString() : null,
                    result.getEndDate() != null ? result.getEndDate().toString() : null));
        }
        return results;
    }

    @GetMapping("/getEnginesList")
    public List<Engine> getEnginesList() {
        return new ArrayList<>(EnginesSet.getEngines());
    }

    @GetMapping("getEngineGraph")
    public EngineGraph getEngineGraph(@RequestParam(value = "id") final String id) throws IOException {
            return getEngineGraphFromEngineId(id);
    }

    @GetMapping("/getMatchersList")
    public List<MatcherModel> getMatchersList() throws IOException, ParserConfigurationException, SAXException {
            List<MatcherModel> matchers = new ArrayList<>();
            final String basePath = Paths.get(Objects.requireNonNull(env.getProperty("repository.path"))).toAbsolutePath().normalize().toString();
            final String matchersDataPath = basePath + File.separator + "matcherdata" + File.separator;
            File folder = new File(matchersDataPath);
            for (final File file : Objects.requireNonNull(folder.listFiles((dir, name) -> name.toLowerCase(Locale.ROOT).endsWith(".xml")))) {
                matchers.add(getMatcherModelFromXml(file));
            }
            return matchers;

    }

    private MatcherModel getMatcherModelFromXml(File xml) throws IOException, SAXException, ParserConfigurationException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                .newInstance();
        documentBuilderFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
        documentBuilderFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(xml);
        String name = document.getElementsByTagName("Name").item(0).getTextContent();
        String type = document.getElementsByTagName("Type").item(0).getTextContent();
        String description = document.getElementsByTagName("Description").item(0).getTextContent();
        String inputParams = document.getElementsByTagName("InputParams").item(0).getTextContent();
        String defaultParams = document.getElementsByTagName("DefaultParams").item(0).getTextContent();
        Map<String, Object> params = getOptionsMapFromString(defaultParams);
        return new MatcherModel(xml.getName().split("\\.")[0],name, description, type, 1.0, inputParams, params );
    }

    private EngineGraph getEngineGraphFromEngineId(String engineId) throws IOException {
        final String basePath = Paths.get(Objects.requireNonNull(env.getProperty("repository.path"))).toAbsolutePath().normalize().toString();
        final String enginePath = basePath + File.separator + "engines" + File.separator + engineId;

        Graph graph = EnginesSet.getEngineGraph(enginePath);
        Engine engine = EnginesSet.getEngine(engineId);
        List<GraphNode> nodes = new ArrayList<>();
        for (MatcherNode node : graph.getNodes()) {
            GraphNode graphNode = new GraphNode(String.valueOf(node.getId()), node.getMatcherType(), node.getWeight(), getOptionsMapFromString(node.getOptions()));
            nodes.add(graphNode);
        }
        List<GraphEdge> edges = new ArrayList<>();
        for (GraphArch graphArch : graph.getArches()) {
            GraphEdge graphEdge = new GraphEdge("ED" + graphArch.getIdNodeFrom() + graphArch.getidNodeTo(),
                    String.valueOf(graphArch.getIdNodeFrom()),
                    String.valueOf(graphArch.getidNodeTo()), null, null);
            edges.add(graphEdge);
        }
        return new EngineGraph(engineId, engine.getName(), engine.getDescription(), nodes, edges);
    }

    private Map<String, Object> getOptionsMapFromString(String options) {
        Map<String, Object> opts = new HashMap<>();
        String[] optionsArray = options.split("-");
        for (String option : optionsArray) {
            if (option.contains("=")) {
                opts.put(option.split("=")[0], option.split("=")[1]);
            }
        }
        return opts;
    }

    @PostMapping("/runMatching")
    public String runMatching(@RequestBody final MatchingSettings matchingSettings) {
        try {
            final String basePath = Paths.get(env.getProperty("repository.path")).toAbsolutePath().normalize().toString();
            final String engine = basePath + File.separator + "engines" + File.separator + matchingSettings.getEngine();
            final TripleStore server = matchingSettings.getServer();

            final Result res = new Result(matchingSettings.getOntology1().getUri(),
                    matchingSettings.getOntology2().getUri(), "file:///" + engine, "file:///" + basePath);
            final String idalignmentjob = res.getAlignmentId();
            AlignmentResultSet.addResult(idalignmentjob, res);
            Map<FileType, String> outFilesMap = prepareOutputFiles(idalignmentjob, basePath);
            AlignmentResultSet.saveRepositoryToFile(null);

            final MatcherConfiguration mc = new MatcherConfiguration(idalignmentjob,
                    new MatchingResource(matchingSettings.getOntology1().getUri(),
                            new DataService(matchingSettings.getOntology1().getSparqlEndpoint()),
                            getLexicalizationFromString(matchingSettings.getOntology1().getLexicalization()),
                            matchingSettings.getOntology1().getLanguage(),
                            null),
                    new MatchingResource(matchingSettings.getOntology2().getUri(),
                            new DataService(matchingSettings.getOntology2().getSparqlEndpoint()),
                            getLexicalizationFromString(matchingSettings.getOntology2().getLexicalization()),
                            matchingSettings.getOntology2().getLanguage(), null),
                    server, matchingSettings.getRefAlignment(), null, outFilesMap);
            return startMatching(engine, idalignmentjob, mc);
        } catch (final IOException e) {
            return "{ \"error\":\"" + e.getMessage() + "\"}";
        }
    }


    @PostMapping("/runMatch")
    public String runMatch(@RequestBody final MapleMatchingSettings matchingSettings) {
        try {
            checkMatchingSettings(matchingSettings);
            final File resourcesDirectory = new File(env.getProperty("repository.path"));
            final String basePath = resourcesDirectory.getAbsolutePath();
            Map<String, Dataset> id2supportDataset = matchingSettings.getSupportDatasets().stream()
                    .collect(Collectors.toMap(Dataset::getId, Function.identity()));

            Pairing bestPairing = getBestPairing(matchingSettings.getPairings());
            LexicalizationSet sourceLexicalizationSet = (LexicalizationSet) id2supportDataset.get(bestPairing.getSource().getLexicalizationSet());
            LexicalizationSet targetLexicalizationSet = (LexicalizationSet) id2supportDataset.get(bestPairing.getTarget().getLexicalizationSet());

            String engine = basePath + File.separator + "engines" + File.separator + env.getProperty("maple.engine");
            Dataset sourceSynonymizer = null, targetSynonymizer = null;

            if (bestPairing.getSource().getSynonymizer() != null || bestPairing.getTarget().getSynonymizer() != null) {
                engine = basePath + File.separator + "engines" + File.separator + env.getProperty("maple.engine.withSynonymizer");
                sourceSynonymizer = bestPairing.getSource().getSynonymizer() != null ? id2supportDataset.get(bestPairing.getSource().getSynonymizer().getLexicon()) : null;
                targetSynonymizer = bestPairing.getTarget().getSynonymizer() != null ? id2supportDataset.get(bestPairing.getTarget().getSynonymizer().getLexicon()) : null;
            }

            final Result res = new Result(matchingSettings.getSourceDataset().getId(),
                    matchingSettings.getTargetDataset().getId(), "file:///" + engine, "file:///" + basePath);
            final String idalignmentjob = res.getAlignmentId();
            AlignmentResultSet.addResult(idalignmentjob, res);
            Map<FileType, String> outFilesMap = prepareOutputFiles(idalignmentjob, basePath);
            AlignmentResultSet.saveRepositoryToFile(null);

            final MatcherConfiguration mc = new MatcherConfiguration(idalignmentjob,
                    new MatchingResource(matchingSettings.getSourceDataset().getId(),
                            new DataService(matchingSettings.getSourceDataset().getSparqlEndpoint()),
                            getLexicalizationFromString(sourceLexicalizationSet.getLexicalizationModel()),
                            sourceLexicalizationSet.getLanguageTag(),
                            sourceSynonymizer != null ? new DataService(sourceSynonymizer.getSparqlEndpoint()) : null),
                    new MatchingResource(matchingSettings.getTargetDataset().getId(),
                            new DataService(matchingSettings.getTargetDataset().getSparqlEndpoint()),
                            getLexicalizationFromString(targetLexicalizationSet.getLexicalizationModel()),
                            targetLexicalizationSet.getLanguageTag(),
                            targetSynonymizer != null ? new DataService(targetSynonymizer.getSparqlEndpoint()) : null),
                    null, matchingSettings.getRefAlignment(), null, outFilesMap);
            return startMatching(engine, idalignmentjob, mc);
        } catch (final BaseException | IOException e) {
            return "{ \"error\":\"" + e.getMessage() + "\"}";
        }
    }

    @PostMapping("/saveEngine")
    public void saveEngine(@RequestBody EngineGraph engine) throws IOException {
        String engineId = engine.getId().endsWith(".xml")?engine.getId():engine.getId() + ".xml";
        Graph graph = new Graph();
        for(GraphNode graphNode: engine.getNodes()) {
            String options = getOptionsStringFromParams(graphNode.getParams());
            graph.addNode(new MatcherNode(Integer.parseInt(graphNode.getId()),graphNode.getLabel(),graphNode.getWeight(), options));
        }
        for(GraphEdge graphEdge: engine.getLinks()) {
            graph.addArch(new GraphArch(Integer.parseInt(graphEdge.getSource()), Integer.parseInt(graphEdge.getTarget())));
        }
        String filePath = env.getProperty("repository.path") + "/engines/" + engineId;

        /*
        try (FileOutputStream fos = new FileOutputStream(filePath)) {
            DataManagementUtils.saveRepositoryToFile(graph, fos,filePath);
            EnginesSet.addEngine(new Engine(engine.getName(), engine.getDescription(), engineId));
            EnginesSet.saveRepositoryToFile();
        }
        */
        DataManagementUtils.saveRepositoryToFile(graph, null, filePath);
        EnginesSet.addEngine(new Engine(engine.getName(), engine.getDescription(), engineId));
        EnginesSet.saveRepositoryToFile();
    }

    private String getOptionsStringFromParams(Map<String,Object> params) {
        StringBuilder options = new StringBuilder(" ");
        for(String key: params.keySet()) {
            options.append("-").append(key).append("=").append(params.get(key)).append(" ");
        }
        return options.toString();
    }


    @DeleteMapping("/delete")
    public MatchingStatus delete(@RequestParam(value = "id") final String id) throws IOException {
        final Result result = AlignmentResultSet.getResult(id);
        AlignmentResultSet.removeResult(id);
        removeFiles(id, env.getProperty("repository.path") + "/results/");
        removeFiles(id, env.getProperty("repository.path") + "/logs/");
        AlignmentResultSet.saveRepositoryToFile(null);
        return new MatchingStatus(result.getAlignmentId(), result.getOntology1().toString(),
                result.getOntology2().toString(), result.getEngine().toString(), result.getStatus(),
                result.getStartDate() != null ? result.getStartDate().toString() : null,
                result.getEndDate() != null ? result.getEndDate().toString() : null);
    }

    private String startMatching(String engine, String idalignmentjob, MatcherConfiguration mc) throws IOException {
        mc.loadArchitecture(engine);
        final Composer composer = new Composer(mc);
        taskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    composer.startMatching(null);
                } catch (final InterruptedException | IOException e) {
                    composer.addLog(e.getMessage());
                }
            }
        });

        return "{ \"id\":\"" + idalignmentjob + "\"}";
    }

    private Map<FileType, String> prepareOutputFiles(String idalignmentjob, String basePath) throws IOException {
        final Map<FileType, String> outFiles = new HashMap<>();
        String filePath = basePath + File.separator + "results" + File.separator + idalignmentjob + ".rdf";
        try (FileOutputStream fileOutputStream = new FileOutputStream(filePath)) {
            outFiles.put(MatcherConfiguration.FileType.RES_FILE, filePath);
        }
        filePath = basePath + File.separator + "logs" + File.separator + idalignmentjob + ".log";
        try (FileOutputStream fileOutputStream = new FileOutputStream(filePath)) {
            outFiles.put(MatcherConfiguration.FileType.LOG_FILE, filePath);
        }
        filePath = basePath + File.separator + "results" + File.separator+ idalignmentjob + ".xml";
        try (FileOutputStream fileOutputStream = new FileOutputStream(filePath)) {
            outFiles.put(MatcherConfiguration.FileType.GRAPH_FILE, filePath);
        }
        /*
        outFiles.put(MatcherConfiguration.FileType.RES_FILE,
                new FileOutputStream(basePath + File.separator +"results" + File.separator + idalignmentjob + ".rdf"));
        outFiles.put(MatcherConfiguration.FileType.LOG_FILE,
                new FileOutputStream(basePath + File.separator + "logs" + File.separator + idalignmentjob + ".log"));
        outFiles.put(MatcherConfiguration.FileType.GRAPH_FILE,
                new FileOutputStream(basePath + File.separator + "results" + File.separator + idalignmentjob + ".xml"));
         */
        return outFiles;
    }

    private Lexicalization getLexicalizationFromString(final String uri) {
        if (uri.equals(Lexicalization.ONTOLEX.getUri())) {
            return Lexicalization.ONTOLEX;
        } else if (uri.equals(Lexicalization.SKOS.getUri())) {
            return Lexicalization.SKOS;
        } else if (uri.equals(Lexicalization.SKOS_XL.getUri())) {
            return Lexicalization.SKOS_XL;
        } else {
            return Lexicalization.RDFS;
        }
    }

    private Pairing getBestPairing(List<Pairing> pairings) {
        Pairing bestPairing = new Pairing();
        for (Pairing pairing : pairings) {
            if (pairing.getScore() > bestPairing.getScore()) {
                bestPairing = pairing;
            }
        }
        return bestPairing;
    }

    private void removeFiles(String id, String folder) {
        final List<File> files = (List<File>) FileUtils.listFiles(new File(folder), new WildcardFileFilter(id + ".*"), null);
        for (final File file : files) {
            if (!file.delete()) {
                logger.error("Can't remove " + file.getAbsolutePath());
            }
        }
    }

    private void checkMatchingSettings(MapleMatchingSettings matchingSettings) throws BaseException {
        if (matchingSettings.getSourceDataset() == null || matchingSettings.getTargetDataset() == null)
            throw new BaseException("Both datasets have to be not null");

    }

}
