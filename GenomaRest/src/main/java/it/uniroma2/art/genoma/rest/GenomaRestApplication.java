package it.uniroma2.art.genoma.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import it.uniroma2.art.genoma.rest.config.FileStorageProperties;

@SpringBootApplication

@EnableConfigurationProperties({ FileStorageProperties.class })

public class GenomaRestApplication {

	public static void main(final String[] args) throws Exception {
		SpringApplication.run(GenomaRestApplication.class, args);
	}

}