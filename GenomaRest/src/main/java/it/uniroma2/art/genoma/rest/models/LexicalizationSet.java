package it.uniroma2.art.genoma.rest.models;

import java.util.List;

public class LexicalizationSet extends Dataset {
	 // Tag (in theory, conforming with BCP-47) that identifies the natural language of
    // this lexicalization set
    private String languageTag;
    // Model that was adopted for the representation of lexicalizations (i.e. binding
    // of semantic entities to lexical entries).
    // While the set of lexicalization models is open, common possibilies are listed below:
    // - http://www.w3.org/2000/01/rdf-schema : RDFS: rdfs:label
    // - http://www.w3.org/2004/02/skos/core  : SKOS: skos:{pref|alt|Hidden}Label
    // - http://www.w3.org/2008/05/skos-xl    : SKOS-XL: skosxl:{pref|alt|Hidden}Label
    // - http://www.w3.org/ns/lemon/ontolex   : OntoLex-Lemon: conceptually, ontolex:isDenotedBy;
    //                                          however, this case is trickier since the lexicalization
    //                                          can be represented in the opposite order (ontolex:denotes)
    //                                          or reified as a sense (which can be connected in both directions
    //                                          to the semantic entity and the lexical entry)
    private String lexicalizationModel;
    // When lexical entries are decribes as first-class citizens, this property
    // contains an object representing the lexicon. This object is shaped as the
    // values of the properties sourceDataset and targetDataset
    private Dataset lexiconDataset;
    // A series of statistics descring the lexicalization set (as defined by OntoLex-Lemon)
    private int lexicalizations;
    private int references;
    private List<String> lexicalEntries;
    private double avgNumOfLexicalizations;
    private double percentage;
    
    public String getLanguageTag() {
    	return languageTag;
    }
    
    public String getLexicalizationModel() {
    	return lexicalizationModel;
    }
    
    public Dataset getLexiconDataset() {
    	return lexiconDataset;
    }
    
    public int getLexicalizations() {
    	return lexicalizations;
    }
    
    public int getReferences() {
    	return references;
    }
    
    public List<String> getLexicalEntries() {
    	return lexicalEntries;
    }
    
    public double getAvgNumOfLexicalizations() {
    	return avgNumOfLexicalizations;
    }
    
    public double getPercentage() {
    	return percentage;
    }
}
