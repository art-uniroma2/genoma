package it.uniroma2.art.genoma.rest.models;

import it.uniroma2.art.genoma.ComposerAggregator.TripleStore;

public class MatchingSettings {
	private MatchingResourceSettings ontology1;
	private MatchingResourceSettings ontology2;
	private String engine;
	private String refAlignment;
	private TripleStore server;

	public MatchingResourceSettings getOntology1() {
		return this.ontology1;
	}

	public MatchingResourceSettings getOntology2() {
		return this.ontology2;
	}

	public String getEngine() {
		return this.engine;
	}

	public String getRefAlignment() {
		return this.refAlignment;
	}

	public TripleStore getServer() {
		return this.server;
	}
}
