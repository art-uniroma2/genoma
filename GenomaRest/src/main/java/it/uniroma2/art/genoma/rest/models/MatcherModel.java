package it.uniroma2.art.genoma.rest.models;

import java.util.Map;

public class MatcherModel {
    private String id;
    private String name;
    private String description;
    private String type;
    private String inputParams;
    private double weight;
    private Map<String, Object> defaultInputParams;

    public MatcherModel(String id, String name, String description, String type, double weight, String inputParams, Map<String,Object> defaultInputParams) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.type = type;
        this.weight = weight;
        this.inputParams = inputParams;
        this.defaultInputParams = defaultInputParams;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getType() {
        return type;
    }

    public String getInputParams() {
        return inputParams;
    }

    public Map<String, Object> getDefaultInputParams() {
        return defaultInputParams;
    }

    public double getWeight() {
        return weight;
    }
}
