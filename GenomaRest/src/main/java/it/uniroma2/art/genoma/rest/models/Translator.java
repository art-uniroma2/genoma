package it.uniroma2.art.genoma.rest.models;

import java.util.List;

public class Translator {
	private Lexicon source;
	private Lexicon target;
	private List<String> lexicalLinksets;
	
	public Lexicon getSource() {
		return source;
	}
	
	public Lexicon getTarget() {
		return target;
	}
	
	public List<String> getLexicalLinksets() {
		return lexicalLinksets;
	}
}
