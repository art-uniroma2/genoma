package it.uniroma2.art.genoma.rest.models;

public class DatasetInfo {
	String lexicalizationSet;
	Lexicon synonymizer;
	Translator translator;
	
	public String getLexicalizationSet() {
		return lexicalizationSet;
	}
	
	public Lexicon getSynonymizer() {
		return synonymizer;
	}
	
	public Translator getTranslator() {
		return translator;
	}
}
