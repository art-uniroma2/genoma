package it.uniroma2.art.genoma.rest.models;

import java.util.Map;

public class GraphNode {
    private String id;
    private String label;
    private double weight;
    private Map<String, Object> params;

    public GraphNode() {}

    public GraphNode(String id, String label, double weight, Map<String, Object> params) {
        this.id = id;
        this.label = label;
        this.weight = weight;
        this.params = params;
    }

    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public double getWeight() {
        return weight;
    }
}
