package it.uniroma2.art.genoma.rest.models;

public class MatchingResourceSettings {

	private String uri;
	private String sparqlEndpoint;
	private String lexicalization;
	private String language;

	public String getUri() {
		return uri;
	}

	public String getSparqlEndpoint() {
		return sparqlEndpoint;
	}

	public String getLexicalization() {
		return lexicalization;
	}

	public String getLanguage() {
		return language;
	}

}
