package it.uniroma2.art.genoma.rest.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "file")
public class FileStorageProperties {
	private String enginesDir;
	private String logsDir;
	private String resultsDir;

	public String getEnginesDir() {
		return enginesDir;
	}

	public void setEnginesDir(final String enginesDir) {
		this.enginesDir = enginesDir;
	}

	public String getLogsDir() {
		return logsDir;
	}

	public void setLogsDir(final String logsDir) {
		this.logsDir = logsDir;
	}

	public String getResultsDir() {
		return resultsDir;
	}

	public void setResultsDir(final String resultsDir) {
		this.resultsDir = resultsDir;
	}
}
