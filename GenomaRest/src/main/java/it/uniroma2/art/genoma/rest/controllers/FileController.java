package it.uniroma2.art.genoma.rest.controllers;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import it.uniroma2.art.genoma.DataManagement.Engine;
import it.uniroma2.art.genoma.DataManagement.EnginesSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import it.uniroma2.art.genoma.rest.models.UploadFileResponse;
import it.uniroma2.art.genoma.rest.service.FileStorageService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api")
public class FileController {

	private static final Logger logger = LoggerFactory.getLogger(FileController.class);

	@Autowired
	private FileStorageService fileStorageService;

	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping("/uploadFile")
	public UploadFileResponse uploadFile(@RequestParam("file") final MultipartFile file,
										 @RequestParam("id") final String id,
										 @RequestParam("name") final String name,
										 @RequestParam("description") final String description) {
		try {
			final String fileName = fileStorageService.storeFile(file);

			final String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/downloadFile/")
					.path(fileName).toUriString();
			EnginesSet.addEngine(new Engine(name, description, id));
			EnginesSet.saveRepositoryToFile();
			return new UploadFileResponse(fileName, fileDownloadUri, file.getContentType(), file.getSize());
		} catch(IOException e) {
			return new UploadFileResponse(e.getMessage(), null, null, 0);
		}
	}
/*
	@PostMapping("/uploadMultipleFiles")
	public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("files") final MultipartFile[] files) {
		return Arrays.asList(files).stream().map(file -> uploadFile(file)).collect(Collectors.toList());
	}
*/
	@GetMapping("/downloadFile/{id:.+}")
	public ResponseEntity<Resource> downloadFile(@PathVariable final String id, final HttpServletRequest request,
			@RequestParam(value = "fileType") final String fileType) {
		// Load file as Resource
		final Resource resource = fileStorageService.loadFileAsResource(id, fileType);

		// Try to determine file's content type
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (final IOException ex) {
			logger.info("Could not determine file type.");
		}

		// Fallback to the default content type if type could not be determined
		if (contentType == null) {
			contentType = "application/octet-stream";
		}

		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}
}