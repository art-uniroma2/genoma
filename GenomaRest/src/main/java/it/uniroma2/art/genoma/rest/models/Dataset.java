package it.uniroma2.art.genoma.rest.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type", defaultImpl = Dataset.class)
@JsonSubTypes({
		@JsonSubTypes.Type(name = "http://www.w3.org/ns/lemon/lime#LexicalizationSet", value = LexicalizationSet.class) })
public class Dataset {
	@JsonProperty("@id")
	private String id;

	@JsonProperty("@type")
	private String type;

	@JsonProperty("sparqlEndpoint")
	private String sparqlEndpoint;

	@JsonProperty("linkPredicate")
	private String linkPredicate;

	public String getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public String getSparqlEndpoint() {
		return sparqlEndpoint;
	}

	public String getLinkPredicate() {
		return linkPredicate;
	}
}
