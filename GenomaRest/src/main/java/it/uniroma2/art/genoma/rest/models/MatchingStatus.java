package it.uniroma2.art.genoma.rest.models;

public class MatchingStatus {
	private String id;
	private String ontology1;
	private String ontology2;
	private String engine;
	private String status;
	private String startTime;
	private String endTime;

	public MatchingStatus(final String alignmentId, final String ontology1, final String ontology2, final String engine,
			final String status, final String startTime, final String endTime) {
		this.id = alignmentId;
		this.ontology1 = ontology1;
		this.ontology2 = ontology2;
		this.engine = engine;
		this.status = status;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public String getId() {
		return this.id;
	}

	public String getOntology1() {
		return this.ontology1;
	}

	public String getOntology2() {
		return this.ontology2;
	}

	public String getEngine() {
		return this.engine;
	}

	public String getStatus() {
		return this.status;
	}

	public String getStartTime() {
		return this.startTime;
	}

	public String getEndTime() {
		return this.endTime;
	}

}
