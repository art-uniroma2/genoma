package it.uniroma2.art.genoma.rest.models;

import java.util.Map;

public class GraphEdge {
    private String id;
    private String source;
    private String target;
    private String label;
    private Map<String, Object> params;

    public GraphEdge() {}

    public GraphEdge(String id, String source, String target, String label, Map<String, Object> params) {
        this.id = id;
        this.source = source;
        this.target = target;
        this.label = label;
        this.params = params;
    }

    public String getId() {
        return id;
    }

    public String getSource() {
        return source;
    }

    public String getTarget() {
        return target;
    }

    public String getLabel() {
        return label;
    }

    public Map<String, Object> getParams() {
        return params;
    }
}
