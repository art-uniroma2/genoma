package it.uniroma2.art.genoma.rest.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import it.uniroma2.art.genoma.rest.config.FileStorageProperties;
import it.uniroma2.art.genoma.rest.exceptions.FileStorageException;
import it.uniroma2.art.genoma.rest.exceptions.GRFileNotFoundException;

@Service
public class FileStorageService {
	private static final String LOG_TYPE = "log";
	private static final String ALIGNMENT_TYPE = "alignment";
	private static final String ENGINE_TYPE = "engine";
	private static final String GRAPH_TYPE = "graph";

	private final Path fileStorageLocationEngines;
	private final Path fileStorageLocationLogs;
	private final Path fileStorageLocationResults;

	@Autowired
	public FileStorageService(final FileStorageProperties fileStorageProperties) {
		this.fileStorageLocationEngines = Paths.get(fileStorageProperties.getEnginesDir()).toAbsolutePath().normalize();
		this.fileStorageLocationLogs = Paths.get(fileStorageProperties.getLogsDir()).toAbsolutePath().normalize();
		this.fileStorageLocationResults = Paths.get(fileStorageProperties.getResultsDir()).toAbsolutePath().normalize();

		try {
			Files.createDirectories(this.fileStorageLocationEngines);
			Files.createDirectories(this.fileStorageLocationLogs);
			Files.createDirectories(this.fileStorageLocationResults);
		} catch (final Exception ex) {
			throw new FileStorageException("Could not create the directory where the uploaded files will be stored.",
					ex);
		}
	}

	public String storeFile(final MultipartFile file) {
		// Normalize file name
		final String fileName = StringUtils.cleanPath(file.getOriginalFilename());

		try {
			// Check if the file's name contains invalid characters
			if (fileName.contains("..")) {
				throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
			}

			// Copy file to the target location (Replacing existing file with the same name)
			final Path targetLocation = this.fileStorageLocationEngines.resolve(fileName);
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

			return fileName;
		} catch (final IOException ex) {
			throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
		}
	}

	public Resource loadFileAsResource(final String id, final String fileType) {
		try {
			Path filePath = null;
			switch (fileType) {
			case LOG_TYPE:
				filePath = this.fileStorageLocationLogs.resolve(id + ".log").normalize();
				break;
			case ALIGNMENT_TYPE:
				filePath = this.fileStorageLocationResults.resolve(id + ".rdf").normalize();
				break;
			case ENGINE_TYPE:
				filePath = this.fileStorageLocationEngines.resolve(id + ".xml").normalize();
				break;
			case GRAPH_TYPE:
				filePath = this.fileStorageLocationResults.resolve(id + ".xml").normalize();
				break;
			default:
				throw new GRFileNotFoundException("File type not existing" + fileType);
			}
			final Resource resource = new UrlResource(filePath.toUri());
			if (resource.exists()) {
				return resource;
			} else {
				throw new GRFileNotFoundException("File not found " + id);
			}
		} catch (final MalformedURLException ex) {
			throw new GRFileNotFoundException("File not found " + id, ex);
		}
	}
}
