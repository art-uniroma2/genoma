package it.uniroma2.art.genoma.rest.models;

public class Pairing {
	private double score = 0.0;
	private DatasetInfo source;
	private DatasetInfo target;
	
	public double getScore() {
		return score;
	}
	
	public DatasetInfo getSource() {
		return source;
	}
	
	public DatasetInfo getTarget() {
		return target;
	}
}
