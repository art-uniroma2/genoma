package it.uniroma2.art.genoma.rest.models;

import java.util.List;

public class EngineGraph {
    private String id;
    private String name;
    private String description;
    private List<GraphNode> nodes;
    private List<GraphEdge> links;

    public EngineGraph() {}

    public EngineGraph(String id, String name, String description, List<GraphNode> nodes, List<GraphEdge> links) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.nodes = nodes;
        this.links = links;
    }

    public List<GraphEdge> getLinks() {
        return links;
    }

    public List<GraphNode> getNodes() {
        return nodes;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }
}
