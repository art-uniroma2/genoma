package it.uniroma2.art.genoma.rest.models;

import java.util.List;

public class MapleMatchingSettings {
	private Dataset sourceDataset;
	private Dataset targetDataset;
	private String refAlignment;
	
	private List<Dataset> supportDatasets;
	private List<Pairing> pairings;
	
	public Dataset getSourceDataset() {
		return sourceDataset;
	}
	
	public Dataset getTargetDataset() {
		return targetDataset;
	}
	
	public List<Dataset> getSupportDatasets() {
		return supportDatasets;
	}
	
	public List<Pairing> getPairings() {
		return pairings;
	}
	
	public String getRefAlignment() {
		return refAlignment;
	}
}
