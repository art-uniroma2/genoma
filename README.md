# Genoma

Genoma is an Ontology Matching Environment that provides the user with a powerful tool to design and test ontology matching architectures.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

- JDK 8
- Maven 3.5.2 or later versions
- NodeJS 10.16.3 or later versions to build angular UI

### Build

To build the project just run 

```
mvn clean install
```

from the command prompt.


## Deployment

The build process automatically generates a zip file for deployment in GenomaREST/target folder.
To deploy it just unzip the content of the file and run the start.bat batch file.

## Version Notes

* 2.0.1 bug fix 
* 2.0.0 refactoring of rest service and implementation of Angular UI
* 1.0.0 first version


## Authors

* **Roberto Enea** - *Initial work and maintenance*
* **Andrea Turbati** - *RDF4J introduction*
* **Manuel Fiorelli** - *Genoma Alignment Service Module*



